function linkify(name) {
    return name.split(/[^A-Za-z\s]/).join('').split(/\s/).join('-').toLowerCase();
}

var links = null;
var options = {};

var comparer = function(x, y) {
    if (x[1] == y[1]) {
        var alphabetize = 0;
        if (x[0] > y[0]) {
            alphabetize = 1;
        } else if (x[0] < y[0]) {
            alphabetize = -1;
        }

        var a = x[0].indexOf('(');
        var b = y[0].indexOf('(');

        if (a == -1 && b == -1) {
            return alphabetize;
        } else if (a == -1) {
            return -1;
        } else if (b == -1) {
            return 1;
        } else {
            a = x[0].indexOf('[');
            b = y[0].indexOf('[');

            if (a == -1 && b == -1) {
                return alphabetize;
            } else if (a == -1) {
                return -1;
            } else if (b == -1) {
                return 1;
            } else {
                return alphabetize;
            }
        }
    }
    return x[1] - y[1];
};

function s(q, n) {
    if (links === null) loadLinks();
    q = q.toLowerCase();
    var c = 0, b = [], m = {}, r = [];
    
    search_match:
    if (q in options) {
        b.push([q, 0]);
        m[q] = null;
    }
    b.sort(comparer);
    for (var i in b) {
        if (c >= n) break;
        r.push(b[i]);
        c++;
    }
    if (c >= n) return r;
    b = [];
    
    search_exact:
    for (var t in options) {
        if (t in m) continue;
        var o = t.indexOf(q);
        if (o == -1) continue;
        b.push([t, o]);
        m[t] = null;
    }
    b.sort(comparer);
    for (var i in b) {
        if (c >= n) break;
        r.push(b[i]);
        c++;
    }
    if (c >= n) return r;
    b = [];
    
    search_contains:
    for (var t in options) {
        if (t in m) continue;
        var score = 0;
        for (var j = 0, k = 0, l = q.length; j < l; j++) {
            var o = t.indexOf(q[j], k);
            if (o == -1) continue search_contains;
            score += o == k ? -5 : Math.pow(o - k, 1.5);
            k = o + 1;
        }
        b.push([t, score]);
        m[t] = null;
    }
    /*
    b.sort(comparer);
    for (var i in b) {
        r.push(b[i][0]);
    }
    b = [];
    
    search_one_off:
    for (var t in options) {
        if (c >= n) break;
        if (t in m) continue;
        var j = 0, l = q.length, o = 0;
        for (; j < l; j++) {
            var k = q.substring(0, j) + q.substring(j + 1);
            o = t.indexOf(k);
            if (o != -1) break;
        }
        if (o == -1) continue;
        var score = j == 0 ? l / 2 : l - j;
        b.push([t, score]);
        m[t] = null;
        c++;
    }
    b.sort(comparer);
    for (var i in b) {
        r.push(b[i][0]);
    }
    b = [];
    */
    search_one_off_anywhere:
    for (var t in options) {
        if (t in m) continue;
        var e = 1, score = 0;
        for (var j = 0, k = 0, l = q.length; j < l; j++) {
            var o = t.indexOf(q[j], k);
            if (o == -1) {
                if (e > 0) e -= 1;
                else continue search_one_off_anywhere;
            } else {
                score += o == k ? -5 : Math.pow(o - k, 1.5);
                k = o + 1;
            }
        }
        if (j < l) continue;
        if (e == 0) {
            if (score > 0) score *= 2.5;
            else score /= 2.5;
        }
        b.push([t, score]);
        m[t] = null;
    }
    b.sort(comparer);
    for (var i in b) {
        if (c >= n) break;
        r.push(b[i]);
        c++;
    }
    return r;
}

translateSearch = function(t) {
    return options[t] || '';
}

linkSearch = function(t) {
    return links[options[t]] || '';
}
