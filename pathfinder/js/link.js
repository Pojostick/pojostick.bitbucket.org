var all = document.getElementsByTagName('a');

for (var i=0, max=all.length; i < max; i++) {
    var elem = all[i];
    var href = elem.getAttribute('href');
    if (href === null) {
        var url = elem.getAttribute('url');
        if (!url) url = '';
        var link = elem.innerHTML.split(/[^A-Za-z\s]/).join('').split(/\s/).join('-').toLowerCase();
        elem.setAttribute('href', url + '#' + link);
    }
}

all = document.getElementsByTagName('b');

for (var i=0, max=all.length; i < max; i++) {
    var elem = all[i];
    var a = elem.getAttribute('a');
    if (a !== null) {
        var link = elem.innerHTML.split(/[^A-Za-z\s]/).join('').split(/\s/).join('-').toLowerCase();
        elem.setAttribute('id', link);
    }
}

all = document.getElementsByTagName('t');

for (var i=0, max=all.length; i < max; i++) {
    var elem = all[i];
    var link = elem.innerHTML.split(/[^A-Za-z\s]/).join('').split(/\s/).join('-').toLowerCase();
    elem.setAttribute('id', link);
}

var html = '<style>\
@media only screen and (min-width: 800px) {\
div#search-anywhere { position: fixed; background-color: rgba(0,0,0,0.75); left: 0; right: 0; top: 0; bottom: 0; box-shadow: inset 0px 0px 25px rgba(0,0,0,0.25); text-align: center; display: none; opacity: 0; transition: opacity 0.1s; padding-top: 25px; }\
div#search-help { display: inline-block; font-family: Lato; font-size: 16px; color: #f8f6e7; width: 50%; box-sizing: border-box; text-align: center; }\
b.button { cursor: pointer; font-weight: normal; padding: 1px 6px 4px 6px; margin: 3px; line-height: 50px; background-color: rgba(204,204,204,0.5); border: 1px solid rgba(0,0,0,0.75); border-radius: 0.5px; box-shadow: 2px 2px 0.5px 2px rgba(0,0,0,0.75); -moz-user-select: none; -webkit-user-select: none; -ms-user-select: none; user-select: none; }\
input#search-box { background-color: #f8f6e7; box-shadow: 0px 0px 10px rgba(0,0,0,0.5); font-family: PT serif; font-size: 24px; margin: 0px 50px; padding: 25px; width: 50%; box-sizing: border-box; }\
div#search-suggestions { background-color: #f8f6e7; text-align: left; box-shadow: 0px 0px 10px rgba(0,0,0,0.5); display: none; z-index: 9999; max-height: 250px; overflow: hidden; overflow-y: auto; width: 50%; box-sizing: border-box; }\
div.search-suggestion { position: relative; padding: 0 0.75em; line-height: 25px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; font-size: 16px; color: #333; }\
a.search-link { text-decoration: none; }\
a.search-link:hover div, a.search-link.selected div { color: #111; background-color: #ccc; }\
div#search-timing { display: inline-block; font-family: Lato; font-size: 13px; color: #ccc; width: 50%; box-sizing: border-box; text-align: right; }\
div.search-suggestion span.search-suggestion-name { float: left; }\
div.search-suggestion span.search-suggestion-rating { color: #777; font-size: 13px; float: right; }\
b.button.clickable { border-radius: 1em; padding: 2px 9px 5px 9px; }\
b.button.clickable:hover { color: #f8f6e7; background-color: rgba(204,204,204,0.75); border: 1px solid black; box-shadow: 2px 2px 1.5px 1px rgba(0,0,0,0.75); }\
b#search-help-button-escape-mobile { display: none !important; }\
}\
@media only screen and (max-width: 800px) {\
div#search-anywhere { position: fixed; background-color: rgba(0,0,0,0.75); left: 0; right: 0; top: 0; bottom: 0; box-shadow: inset 0px 0px 25px rgba(0,0,0,0.25); text-align: center; display: none; opacity: 0; transition: opacity 0.1s; }\
div#search-help { display: none !important; }\
b.button { cursor: pointer; font-weight: normal; padding: 1px 6px 4px 6px; margin: 3px; line-height: 2.5em; background-color: rgba(204,204,204,0.5); border: 1px solid rgba(0,0,0,0.75); border-radius: 0.5px; box-shadow: 2px 2px 0.5px 2px rgba(0,0,0,0.75); -moz-user-select: none; -webkit-user-select: none; -ms-user-select: none; user-select: none; }\
input#search-box { background-color: #f8f6e7; box-shadow: 0px 0px 10px rgba(0,0,0,0.5); font-family: PT serif; font-size: 24px; margin: 0px; padding: 5px; width: calc(100% - 100px); box-sizing: border-box; }\
div#search-suggestions { background-color: #f8f6e7; text-align: left; box-shadow: 0px 0px 10px rgba(0,0,0,0.5); display: none; z-index: 9999; max-height: calc(100% - 100px); overflow: hidden; overflow-y: auto; width: calc(100% - 100px); box-sizing: border-box; }\
div.search-suggestion { position: relative; padding: 0 0.75em; line-height: 25px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; font-size: 16px; color: #333; }\
a.search-link { text-decoration: none; }\
a.search-link:hover div, a.search-link.selected div { color: #111; background-color: #ccc; }\
div#search-timing { display: inline-block; font-family: Lato; font-size: 13px; color: #ccc; width: calc(100% - 100px); box-sizing: border-box; text-align: right; }\
div.search-suggestion span.search-suggestion-name { float: left; }\
div.search-suggestion span.search-suggestion-rating { color: #777; font-size: 13px; float: right; }\
b.button.clickable { border-radius: 1em; padding: 2px 9px 5px 9px; }\
b.button.clickable:hover { color: #f8f6e7; background-color: rgba(204,204,204,0.75); border: 1px solid black; box-shadow: 2px 2px 1.5px 1px rgba(0,0,0,0.75); }\
b#search-help-button-escape-mobile { display: inline; float: left; margin: 3px 0px; padding: 0px calc(17% - 25px)}\
}\
</style>\
<div id="search-anywhere">\
<div id="search-help" class="no-mobile"><span style="float: left;"><b id="search-help-button-enter" class="button">enter</b> &nbsp; to select</span> <span><b id="search-help-button-up" class="button">&uarr;</b> <b id="search-help-button-down" class="button">&darr;</b> &nbsp; to navigate</span> <span style="float:right;"><b id="search-help-button-escape" class="button">esc</b> &nbsp; to dismiss</span></div>\
<input id="search-box" type="search" autocomplete="off" placeholder = "Search anything!"/>\
<div id="search-suggestions"></div><br>\
<div id="search-timing"><b id="search-help-button-escape-mobile" class="button">Cancel</b>Showing <span id="search-timing-number"></span><span id="search-timing-total"></span> results (<span id="search-timing-microseconds"></span> microseconds). &nbsp; <b id="search-show-more" class="button clickable">Show&nbsp;more</b></div>\
</div>\
<script type="text/javascript" src="/pathfinder/js/search.js"></script>';
document.write(html);

const INIT_SEARCH = 10;
const MAX_SEARCH = 100;

var searchAnywhere = document.getElementById('search-anywhere');
var searchBox = document.getElementById('search-box');
var searchSuggestions = document.getElementById('search-suggestions');
var searching = false;
var results = [];
var selected = 0;

var timingNumber = document.getElementById('search-timing-number');
var timingTotal = document.getElementById('search-timing-total');
var timingMicroseconds = document.getElementById('search-timing-microseconds');

var rendering = null;
var searchQueued = null;
var searchWrapup = null;
var displayCount = INIT_SEARCH;

var IS_TOUCH = ("ontouchstart" in window) || window.navigator.msMaxTouchPoints > 0;

function displaySearch() {
    if (!searching) {
        searching = true;

        searchAnywhere.style.display = 'block';
        searchSuggestions.style.display = 'none';
        setTimeout(function() { searchAnywhere.style.opacity = 1; }, 0);

        displayCount = INIT_SEARCH;
        timingNumber.innerHTML = '0';
        timingTotal.innerHTML = '';
        timingMicroseconds.innerHTML = '0';

        searchBox.focus();
    }
}

var handleInput = function(keyCode) {
    var l = Math.min(displayCount, results.length);
    switch(keyCode) {
    case 27:                                        // escape
        if (searchBox.value) {
            searchBox.value = '';
            searchBox.focus();
        } else {
            searching = false;
            searchAnywhere.style.opacity = 0;
            setTimeout(function() { searchAnywhere.style.display = 'none'; }, 100);
        }
        break;
    /*case 37:                                        // left
        displayCount -= 5;
        if (displayCount < 5) displayCount = 5;
        renderSuggestions(function() { selectAndFocus(selected); });
        break;*/
    case 38:                                        // up
        var elem = document.getElementById('search-option-' + selected);
        if (elem) elem.classList.remove('selected');
        
        selected--;
        if (selected < 0 || selected >= l) selected = l - 1;

        selectAndFocus(selected);
        break;
    /*case 39:                                        // right
        displayCount += 5;
        if (displayCount > MAX_SEARCH) displayCount = MAX_SEARCH;
        renderSuggestions(function() { selectAndFocus(selected); });
        break;*/
    case 9:                                         // tab
    case 40:                                        // down
        var elem = document.getElementById('search-option-' + selected);
        if (elem) elem.classList.remove('selected');
        
        selected++;
        if (selected < 0 || selected >= l) selected = 0;
        
        selectAndFocus(selected);

        break;
    case 13:                                        // enter
        if (IS_TOUCH) {
            hideKeyboard();
            break;
        }
        if (results[selected]) {
            window.location.href = linkSearch(results[selected][0]);
        }
        searchBox.value = '';
        searching = false;
        searchAnywhere.style.opacity = 0;
        setTimeout(function() { searchAnywhere.style.display = 'none'; }, 100);
        break;
    default:
        return false;
    }
    return true;
}

window.onkeydown = function(e) {
    if (e.keyCode === 32 && !(e.target instanceof HTMLInputElement || e.target instanceof HTMLTextAreaElement)) {   // space
        e.preventDefault();
        displaySearch();
    } else if (searching) {
        if (handleInput(e.keyCode)) e.preventDefault();
    }
};

function selectAndFocus(option) {
    var elem = document.getElementById('search-option-' + option);
    if (elem) {
        elem.classList.add('selected');
        elem.focus();
        searchBox.focus();
    }
}

function hideKeyboard() {
    searchBox.readOnly = true;
    searchBox.blur();
    setTimeout(function() { searchBox.readOnly = false; }, 100);
}

function renderSuggestions(callback) {
    if (rendering === null) rendering = setTimeout(renderElements, 50, callback);
}

function renderElements(callback) {
    try {
        var suggestions = '';
        var i = 0, l = results.length;
        for (; i < l; i++) {
            if (i >= displayCount) break;
            var t = results[i][0], r = results[i][1];
            suggestions += '<a id="search-option-' + i + '" href="' + linkSearch(t) + '" class="search-link"><div class="search-suggestion"><span class="search-suggestion-name">' + translateSearch(t) + '</span><span class="search-suggestion-rating">' + (100 - Math.round(r)) + '</span></div>';
        }
        searchSuggestions.innerHTML = suggestions;
        searchSuggestions.style.display = 'inline-block';
        timingNumber.innerHTML = i;
        if (i < l) {
            if (l == MAX_SEARCH) {
                timingTotal.innerHTML = ' / ' + l + '+';
            } else {
                timingTotal.innerHTML = ' / ' + l;
            }
        } else {
            timingTotal.innerHTML = '';
        }
        if (callback) callback();
    } finally {
        rendering = null;
    }
}

searchBox.oninput = function(e) {
    if (searchQueued !== null) clearTimeout(searchQueued);
    searchQueued = setTimeout(function() {
        var start = window.performance.now();
        var end = start;
        if (searchBox.value) {
            results = s(searchBox.value, MAX_SEARCH);
            end = window.performance.now();
                    
            var elem = document.getElementById('search-option-' + selected);
            if (elem) elem.classList.remove('selected');
            
            selected = 0;
            renderSuggestions(function() {
                elem = document.getElementById('search-option-' + selected);
                if (elem) elem.classList.add('selected');
            });
        } else {
            end = window.performance.now();
            searchSuggestions.style.display = 'none';
            timingNumber.innerHTML = '0';
            timingTotal.innerHTML = '';
        }
        timingMicroseconds.innerHTML = Math.round(1000 * (end - start)) / 1000;
        searchQueued = null;
    }, 50);
}

document.getElementById('search-help-button-enter').onclick = function() { handleInput(13); };
document.getElementById('search-help-button-up').onclick = function() { handleInput(38); };
document.getElementById('search-help-button-down').onclick = function() { handleInput(40); };
document.getElementById('search-help-button-escape').onclick = function() { searchBox.value = ''; handleInput(27); };
document.getElementById('search-help-button-escape-mobile').onclick = function() { searchBox.value = ''; handleInput(27); };
document.getElementById('search-show-more').onclick = function(e) {
    e.preventDefault();
    if (searchBox.value) {
        if (displayCount < results.length) displayCount += 10;
        if (displayCount > MAX_SEARCH) displayCount = MAX_SEARCH;
        renderSuggestions(function() { selectAndFocus(selected); });
    }
    return false;
}

// Linkify HASH component
var element = null;
function highlight() {
    if (element !== null) {
        var classes = element.className;
        element.className = classes.replace(' highlighted', '')
    }

    var hash = window.location.hash;
    if (hash !== null) {
        element = document.getElementById(hash.substring(1));
        console.log(element)
        if (element !== null) {
            var classes = element.className;
            if (classes.indexOf('highlighted') == -1) {
                element.className += ' highlighted'
            }
        }
    }
}
window.addEventListener('popstate', highlight);
highlight();

// See if there is a search term
var searcher = function() {
    if (typeof s === 'undefined') {
        autoSearcher();
    }

    var query = null;

    var items = location.search.substr(1).split('&');
    for (var index = 0; index < items.length; index++) {
        var tmp = items[index].split('=');
        if (tmp[0] === 's') {
            query = decodeURIComponent(tmp[1]);
            break;
        }
    }
    
    if (query !== null) {
        var search = s(query, 1);
        if (search && search[0]) {
            window.location.replace(linkSearch(search[0][0]));
        }
    }
}
var autoSearcher = function() {
    setTimeout(searcher, 100);
}
autoSearcher();
