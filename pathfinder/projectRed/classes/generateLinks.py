import re

links = {}
regex = re.compile('[^a-zA-Z]')

for c in ['barbarian', 'bard', 'brawler', 'cleric', 'monk', 'oracle', 'paladin', 'wizard']:
    with open(c + '/index.html', 'r') as f:
        for line in f:
            start = line.find('<b a>')
            if start > -1:
                end = line.find('</b>', start)
                try:
                    name = line[start+5:end]
                    if c in links:
                        links[c] |= {name}
                    else:
                        links[c] = {name}
                except:
                    print 'Bad line:', line

for k in sorted(links.keys()):
    print ''.join([repr(k), ": ", str(sorted(links[k])), ","])