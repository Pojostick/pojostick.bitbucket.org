var tsn = 'sheetName',
	tft = 'flavorText',
	tcn = 'characterName',
	tal = 'alignment',
	tsi = 'size',
	tra = 'race',
	tcl = 'classes';

var alv = 'level',
	ain = 'initiative',
	ado = 'dodge',
	ahp = 'hp',
	ahd = 'hitDice',
	ahc = 'hpConMod',
	afo = 'fort',
	awi = 'will',
	asp = 'speed';

var STR = 'strStat',
	DEX = 'dexStat',
	CON = 'conStat',
	INT = 'intStat',
	WIS = 'wisStat',
	CHA = 'chaStat';

var sae = 'athleticsEndurance',
	sap = 'athleticsPrecision',
	sbw = 'bows',
	sbx = 'boxing',
	scb = 'crossbows',
	sgr = 'grappling',
	s1b = 'oneHandedBalanced',
	s1h = 'oneHandedHefted',
	sth = 'throwing',
	s2b = 'twoHandedBalanced',
	s2h = 'twoHandedHefted',
	spo = 'polearms',
	skr = 'knowledgeArcana',
	skl = 'knowledgeAlchemy',
	ska = 'knowledgeArt',
	skc = 'knowledgeEconomics',
	ske = 'knowledgeEngineering',
	sks = 'knowledgeSocialStudies',
	skn = 'knowledgeNature',
	skm = 'knowledgeSmithing',
	spe = 'perception',
	sso = 'sociability',
	sul = 'unlisted';

var races = {
	'default': {
		'name': '',
		'size': '',
		'speed': 0
	},
	'dwarf': {
		'name': 'Dwarf',
		'size': 'Medium',
		'speed': 20
	},
	'elf': {
		'name': 'Elf',
		'size': 'Medium',
		'speed': 30
	},
	'gnome': {
		'name': 'Gnome',
		'size': 'Small',
		'speed': 20
	},
	'halfElf': {
		'name': 'Half-Elf',
		'size': 'Medium',
		'speed': 30
	},
	'halfOrc': {
		'name': 'Half-Orc',
		'size': 'Medium',
		'speed': 30
	},
	'halfling': {
		'name': 'Halfling',
		'size': 'Small',
		'speed': 20
	},
	'human': {
		'name': 'Human',
		'size': 'Medium',
		'speed': 30
	}
};

var classes = {
	'default': {
		'name': '',
		'hitDice': 0,
		'fort': [],
		'will': [],
		'skills': []
	},
	'barbarian': {
		'name': '',
		'hitDice': 0,
		'fort': [],
		'will': [],
		'skills': [sul]
	},
	'bard': {
		'name': 'Bard',
		'hitDice': 8,
		'fort': [0, 0, 0, 1, 1, 1, 2, 2, 2, 3],
		'will': [2, 2, 3, 3, 4, 4, 5, 5, 6, 6],
		'skills': [sap, sbx, scb, sgr, s1b, s1h, sth, skl, skr, ska, skc, ske, skn, skm, sks, sso, sul]
	},
	'brawler': {
		'name': 'Brawler',
		'hitDice': 10,
		'fort': [2, 2, 3, 3, 4, 4, 5, 5, 6, 6],
		'will': [0, 0, 0, 1, 1, 1, 2, 2, 2, 3],
		'skills': [sae, sap, sbx, sgr, s1b, s1h, sth, sks, spe, sso, sul]
	},
	'cleric': {
		'name': '',
		'hitDice': 0,
		'fort': [],
		'will': [],
		'skills': [sul]
	},
	'monk': {
		'name': 'Monk',
		'hitDice': 8,
		'fort': [2, 2, 3, 3, 4, 4, 5, 5, 6, 6],
		'will': [2, 2, 3, 3, 4, 4, 5, 5, 6, 6],
		'skills': [sae, sap, sbw, sbx, sgr, s1b, spo, sth, sul]
	},
	'oracle': {
		'name': 'Oracle',
		'hitDice': 8,
		'fort': [0, 0, 0, 1, 1, 1, 2, 2, 2, 3],
		'will': [2, 2, 3, 3, 4, 4, 5, 5, 6, 6],
		'skills': [sbw, scb, s1b, s1h, spo, sth, skr, skn, sks, spe, sso, sul]
	},
	'paladin': {
		'name': 'Paladin',
		'hitDice': 10,
		'fort': [2, 2, 3, 3, 4, 4, 5, 5, 6, 6],
		'will': [2, 2, 3, 3, 4, 4, 5, 5, 6, 6],
		'skills': [sae, sbw, sbx, scb, sgr, s1b, s1h, spo, sth, s2b, s2h, ske, skn, skm, sks, spe, sso, sul]
	},
	'wizard': {
		'name': 'Wizard',
		'hitDice': 6,
		'fort': [0, 0, 0, 1, 1, 1, 2, 2, 2, 3],
		'will': [2, 2, 3, 3, 4, 4, 5, 5, 6, 6],
		'skills': [sbx, scb, s1b, spo, sth, skl, skr, skn, sks, spe, sul]
	}
};

var skills = {};
skills['sae'] = [STR, CON];
skills['sap'] = [DEX];
skills['sbw'] = [DEX];
skills['sbx'] = [STR, DEX];
skills['scb'] = [DEX];
skills['sgr'] = [STR, DEX];
skills['s1b'] = [STR, DEX];
skills['s1h'] = [STR];
skills['sth'] = [DEX];
skills['s2b'] = [STR, DEX];
skills['s2h'] = [STR];
skills['spo'] = [STR, DEX];
skills['skr'] = [INT, WIS];
skills['skl'] = [INT, WIS];
skills['ska'] = [INT, WIS];
skills['skc'] = [INT, WIS];
skills['ske'] = [INT, WIS];
skills['sks'] = [INT, WIS];
skills['skn'] = [INT, WIS];
skills['skm'] = [INT, WIS];
skills['spe'] = [INT, WIS];
skills['sso'] = [CHA];
skills['sul'] = [STR, DEX];

var translation = {};
translation[tsn] = '';
translation[tft] = '';
translation[tcn] = '';
translation[tal] = '';
translation[tsi] = '';
translation[tra] = '';
translation[tcl] = '';

translation[alv] = 0;
translation[ain] = 0;
translation[ado] = 0;
translation[ahp] = 0;
translation[ahd] = 0;
translation[ahc] = 0;
translation[afo] = 0;
translation[awi] = 0;
translation[asp] = 0;

translation[STR] = 10;
translation[DEX] = 10;
translation[CON] = 10;
translation[INT] = 10;
translation[WIS] = 10;
translation[CHA] = 10;

translation[sae] = 0;
translation[sap] = 0;
translation[sbw] = 0;
translation[sbx] = 0;
translation[scb] = 0;
translation[sgr] = 0;
translation[s1b] = 0;
translation[s1h] = 0;
translation[sth] = 0;
translation[s2b] = 0;
translation[s2h] = 0;
translation[spo] = 0;
translation[skr] = 0;
translation[skl] = 0;
translation[ska] = 0;
translation[skc] = 0;
translation[ske] = 0;
translation[sks] = 0;
translation[skn] = 0;
translation[skm] = 0;
translation[spe] = 0;
translation[sso] = 0;
translation[sul] = 0;

translation['extra'] = '';

var template = {
	'tsn': 'Untitled',
	'tcn': 'Unnamed',
	'tal': 'N',
	'tra': 'default',
	'tcl': 'default',
	'STR': 10,
	'DEX': 10,
	'CON': 10,
	'INT': 10,
	'WIS': 10,
	'CHA': 10,
	'tft': '',
	'jlv': 0,
	'jhp': 0,
	'sae': 0,
	'sap': 0,
	'sbw': 0,
	'sbx': 0,
	'scb': 0,
	'sgr': 0,
	's1b': 0,
	's1h': 0,
	'sth': 0,
	's2b': 0,
	's2h': 0,
	'spo': 0,
	'skr': 0,
	'skl': 0,
	'ska': 0,
	'skc': 0,
	'ske': 0,
	'sks': 0,
	'skn': 0,
	'skm': 0,
	'spe': 0,
	'sso': 0,
	'sul': 0,
	'jul': ''
}

function statmod(n) {
	return Math.floor((n - 10) / 2);
}

translate = function(json) {
	var input = JSON.parse(json);
	for (var attrname in template) {
		input[attrname] = input[attrname] || template[attrname];
	}
	
	var inRace = races[input['tra']] || races['default'];
	var inClass = classes[input['tcl']] || classes['default'];
	var output = {};
	
	output[tsn] = input['tsn'] || translation[tsn];
	output[tft] = input['tft'] || translation[tft];
	output[tcn] = input['tcn'] || translation[tcn];
	output[tal] = input['tal'] || translation[tal];
	output[tsi] = inRace.size || translation[tsi];
	output[tra] = inRace.name || translation[tra];
	output[tcl] = inClass.name || translation[tcl];

	output[STR] = input['STR'] || translation[STR];
	output[DEX] = input['DEX'] || translation[DEX];
	output[CON] = input['CON'] || translation[CON];
	output[INT] = input['INT'] || translation[INT];
	output[WIS] = input['WIS'] || translation[WIS];
	output[CHA] = input['CHA'] || translation[CHA];

	output[alv] = input['jlv'] || translation[alv];
	output[ain] = output[INT] > output[WIS] ? statmod(output[INT]) : statmod(output[WIS]);
	output[ado] = output[DEX] > output[WIS] ? statmod(output[DEX]) : statmod(output[WIS]);
	output[ahp] = input['jhp'] || translation[ahp];
	output[ahd] = inClass.hitDice;
	output[ahc] = output[alv] * (statmod(output[CON]) + 1);
	output[afo] = (inClass.fort[output[alv] - 1] || 0) + statmod(output[CON]);
	output[awi] = (inClass.will[output[alv] - 1] || 0) + statmod(output[CHA]);
	output[asp] = inRace.speed;

	for (var attrname in skills) {
		var skillname = eval(attrname);
		var skillstats = skills[attrname];
				
		var statscore = 0;
        if (skillname.indexOf('knowledge') == 0 && (input[attrname] || translation[skillname]) == 0) {
            output[skillname] = NaN;
        } else {
            for (var i in skillstats) {
                var statname = skillstats[i];
                if (output[statname] > statscore) {
                    statscore = output[statname];
                }
            }
            
            output[skillname] = (input[attrname] || translation[skillname]) * (inClass.skills.includes(skillname) ? 2 : 1) + statmod(statscore);
        }
	}
	
	output['extra'] = input['jul'] || translation['extra'];
		
	return output;
}

// https://tc39.github.io/ecma262/#sec-array.prototype.includes
if (!Array.prototype.includes) {
  Object.defineProperty(Array.prototype, 'includes', {
    value: function(searchElement, fromIndex) {

      // 1. Let O be ? ToObject(this value).
      if (this == null) {
        throw new TypeError('"this" is null or not defined');
      }

      var o = Object(this);

      // 2. Let len be ? ToLength(? Get(O, "length")).
      var len = o.length >>> 0;

      // 3. If len is 0, return false.
      if (len === 0) {
        return false;
      }

      // 4. Let n be ? ToInteger(fromIndex).
      //    (If fromIndex is undefined, this step produces the value 0.)
      var n = fromIndex | 0;

      // 5. If n ≥ 0, then
      //  a. Let k be n.
      // 6. Else n < 0,
      //  a. Let k be len + n.
      //  b. If k < 0, let k be 0.
      var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

      function sameValueZero(x, y) {
        return x === y || (typeof x === 'number' && typeof y === 'number' && isNaN(x) && isNaN(y));
      }

      // 7. Repeat, while k < len
      while (k < len) {
        // a. Let elementK be the result of ? Get(O, ! ToString(k)).
        // b. If SameValueZero(searchElement, elementK) is true, return true.
        // c. Increase k by 1. 
        if (sameValueZero(o[k], searchElement)) {
          return true;
        }
        k++;
      }

      // 8. Return false
      return false;
    }
  });
}
