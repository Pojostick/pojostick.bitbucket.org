function loadLinks() {
    links = {
        'Project Red' : '/pathfinder/projectRed/index.html',
        'Character Sheets': '/pathfinder/projectRed/sheets/index.html',
        'Magic Index': '/pathfinder/projectRed/magic/magicIndex/index.html',
        'General Changes': '/pathfinder/projectRed/general.html'
    };

    // general projectRed pages
    ['Races', 'Skills', 'Morality', 'Combat', 'Currency'].forEach(function(e) {
        links[e + ' (rules)'] = '/pathfinder/projectRed/' + e.toLowerCase() + '.html';
    });

    // classes
    ['Barbarian', 'Bard', 'Brawler', 'Cleric', 'Monk', 'Oracle', 'Paladin', 'Wizard'].forEach(function(e) {
        links[e + ' (class)'] = '/pathfinder/projectRed/classes/' + e.toLowerCase() + '/index.html';
    });

    // magic list
    ['Arcane Phrases', 'Divine Magics', 'Supernatural Abilities'].forEach(function(e) {
        links[e] = '/pathfinder/projectRed/magic/' + e.split(' ')[0].toLowerCase() + '/index.html';
    });

    // divine magic list
    ['Bargains', "Compassion's Touches", 'Predictions', 'Sigils'].forEach(function(e) {
        links[e + ' (divine magics)'] = '/pathfinder/projectRed/magic/divine/index.html#' + linkify(e);
    });

    // supernatural ability list
    ['Martial Techniques', 'Perception Shifts', 'Rage Powers'].forEach(function(e) {
        links[e + ' (supernatural abilities)'] = '/pathfinder/projectRed/magic/supernatural/index.html#' + linkify(e);
    });
    
    // skills
    skills = ['Athletics (Endurance)', 'Athletics (Precision)', 'Bows', 'Boxing', 'Crossbows', 'Grappling', 'Knowledge (alchemy)', 'Knowledge (arcana)', 'Knowledge (art)', 'Knowledge (economics)', 'Knowledge (engineering)', 'Knowledge (nature)', 'Knowledge (smithing)', 'Knowledge (social studies)', 'One-handed balanced', 'One-handed hefted', 'Perception', 'Polearms', 'Sociability', 'Throwing', 'Two-handed balanced', 'Two-handed hefted', '[Unlisted]'];
    for (var i in skills) {
        var k = skills[i];
        links[k + ' (skill)'] = '/pathfinder/projectRed/skills.html#' + linkify(k);;
    }

    // class abilities
    var abilities = {
    'barbarian': ['Animal Fury', 'Damage Reduction', 'Fast Movement', 'Festering Rage', 'Fight the Power', 'Hardening', 'Rage', 'Rage Power', 'Raging Athlete'],
    'bard': ['Accelerando', 'Bardic Scholar', 'Jack-Of-All-Trades', 'Magnum opus', 'Perception Shifts', 'Song Critic', 'Song Movement', 'Soundwave'],
    'brawler': ['Body of Retribution', "Brawler's Maneuver", 'Calculating Feint', 'Chronic Stress', 'Defiance', 'Enforcer Reputation', 'Knockout', 'Mind of Retribution', 'Punch Out'],
    'monk': ['Battle Aura', 'Discipline', 'Ki', 'Light', 'Martial Art', 'Martial Instinct', 'Purity', 'Residual Meditation', 'Technique', 'Travel Instinct'],
    'oracle': ['Bargain', 'Detection', 'Drugged Prophecy', 'Fate', "Fate's Protection", 'Foresight', 'Hindsight', 'Obsessive', 'Prediction', 'Understanding', 'Warp Fate'],
    'paladin': ['Ascension', 'Blessings of Compassion', 'Bond of Light', 'Brand of the Order', 'Burns of the Void', 'Combat Style', 'Compassion', "Compassion's Touch", 'Gift of the Void', 'Sigil', 'Soul of Virtue'],
    'wizard': ['Arcane Conduit', 'Arcane Identification', 'Arcane Pattern', 'Arcane Pool', 'Counter Pattern', 'Experimental Pattern', 'Field of Study', 'Meta Phrase', 'Pattern Matching Mastery']
    };
    for (var k in abilities) {
        for (var i in abilities[k]) {
            var name = abilities[k][i];
            links[name + ' (' + k + ' class ability)'] = '/pathfinder/projectRed/classes/' + k + '/index.html#' + linkify(name);
        }
    }

    // arcane phrases
    var arcane = ['Acid [bivariate]', 'Acid [matter]', 'Acid [nullify]', 'Acid [pure energy]', 'Acid [reverse]', 'Adhere [matter]', 'Adhere [reverse]', 'Amplifier', 'Arcane [bivariate]', 'Arcane [matter]', 'Arcane [nullify]', 'Arcane [pure energy]', 'Arcane [reverse]', 'Armor [diminish]', 'Armor [matter]', 'Bivariate Energy', 'Bolster [property]', 'Bolt [energy]', 'Bouncing [meta]', 'Burst [targeting]', 'Charged [energy]', 'Close Creature', 'Close Object', 'Close Tile', 'Cloud [energy]', 'Cold [bivariate]', 'Cold [matter]', 'Cold [nullify]', 'Cold [pure energy]', 'Cold [reverse]', 'Collapse [matter]', 'Collapse [reverse]', 'Cone [targeting]', 'Construct [matter]', 'Construct [reverse]', 'Criticality [matter]', 'Criticality [reverse]', 'Damage [diminish]', 'Damage [matter]', 'Devastation [matter]', 'Devastation [reverse]', 'Difficult [meta]', 'Diminish', 'Echoing [meta]', 'Ectoplasmic [meta]', 'Energy', 'Energy Amplifier', 'Energy Form', 'Enhancer', 'Equilibrium', 'Equilibrium - Diminish Property', 'Equilibrium - Nullify Energy', 'Equilibrium - Reverse Energy', 'Equilibrium - Reverse Enhancer', 'Equilibrium - Reverse Form', 'Fields of Study', 'Fire [bivariate]', 'Fire [matter]', 'Fire [nullify]', 'Fire [pure energy]', 'Fire [reverse]', 'Force [bivariate]', 'Force [matter]', 'Force [nullify]', 'Force [pure energy]', 'Force [reverse]', 'Forceful [matter]', 'Forceful [reverse]', 'Form', 'Form Amplifier', 'Grabbing [matter]', 'Grabbing [reverse]', 'Health [diminish]', 'Health [matter]', 'Intensified [energy]', 'Invisible [matter]', 'Invisible [reverse]', 'Light [diminish]', 'Light [matter]', 'Lightning [bivariate]', 'Lightning [matter]', 'Lightning [nullify]', 'Lightning [pure energy]', 'Lightning [reverse]', 'Line [targeting]', 'Line-of-sight Creature', 'Line-of-sight Object', 'Line-of-sight Tile', 'Lingering [property]', 'Liquify [matter]', 'Liquify [reverse]', 'Matter Energy', 'Matter Enhancer', 'Matter Form', 'Matter Property', 'Mental Abilities [diminish]', 'Mental Abilities [matter]', 'Mental Ability [diminish]', 'Mental Ability [matter]', 'Mental Skill [diminish]', 'Mental Skill [matter]', 'Mental Skills [diminish]', 'Mental Skills [matter]', 'Meta Phrase', 'Neutral [meta]', 'Nullify', 'Orb [energy]', 'Padding [diminish]', 'Padding [matter]', 'Permanent [form]', 'Physical Abilities [diminish]', 'Physical Abilities [matter]', 'Physical Ability [diminish]', 'Physical Ability [matter]', 'Physical Skill [diminish]', 'Physical Skill [matter]', 'Physical Skills [diminish]', 'Physical Skills [matter]', 'Plaguing [meta]', 'Property', 'Property Amplifier', 'Proximity [targeting]', 'Pure Energy', 'Rapid [matter]', 'Rapid [reverse]', 'Ray [energy]', 'Refine [form]', 'Replenishing [meta]', 'Repulse [matter]', 'Repulse [reverse]', 'Reverse', 'Save [diminish]', 'Save [matter]', 'Saves [diminish]', 'Saves [matter]', 'Seeking [meta]', 'Selective [targeting]', 'Sharpen [matter]', 'Sharpen [reverse]', 'Shrapnel [energy]', 'Silent [meta]', 'Smoothen [matter]', 'Smoothen [reverse]', 'Solidify [matter]', 'Solidify [reverse]', 'Sonic [bivariate]', 'Sonic [matter]', 'Sonic [nullify]', 'Sonic [pure energy]', 'Sonic [reverse]', 'Target', 'Targeting Amplifier', 'Time [targeting]', 'Touched Creature', 'Touched Object', 'Vaporize [matter]', 'Vaporize [reverse]', 'Voluminous [meta]', 'Well [energy]'];
    for (var i in arcane) {
        var k = arcane[i];
        links[k + ' (arcane phrase)'] = '/pathfinder/projectRed/magic/arcane/index.html#' + linkify(k);;
    }

    // divine magics
    var divine = {
        'Bargain Accuracy': 'fateOfAccuracy',
        'Bargain Action': 'fateOfAction',
        'Bargain Athletics': 'fateOfAthletics',
        'Bargain Bolster': 'fateOfBolster',
        'Bargain Communication': 'fateOfCommunication',
        'Bargain Criticality': 'fateOfCriticality',
        'Bargain Defense': 'fateOfDefense',
        'Bargain Dodge': 'fateOfDodge',
        'Bargain Knowledge': 'fateOfKnowledge',
        'Bargain Luck': 'fateOfLuck',
        'Bargain Penetration': 'fateOfPenetration',
        'Bargain Power': 'fateOfPower',
        'Bargain Recovery': 'fateOfRecovery',
        'Bargain Resistance': 'fateOfResistance',
        'Bargain Severity': 'fateOfSeverity',
        'Bargain Talent': 'fateOfTalent',
        'Predict Accuracy': 'fateOfAccuracy',
        'Predict Action': 'fateOfAction',
        'Predict Athletics': 'fateOfAthletics',
        'Predict Bolster': 'fateOfBolster',
        'Predict Communication': 'fateOfCommunication',
        'Predict Criticality': 'fateOfCriticality',
        'Predict Defense': 'fateOfDefense',
        'Predict Dodge': 'fateOfDodge',
        'Predict Knowledge': 'fateOfKnowledge',
        'Predict Luck': 'fateOfLuck',
        'Predict Penetration': 'fateOfPenetration',
        'Predict Power': 'fateOfPower',
        'Predict Recovery': 'fateOfRecovery',
        'Predict Resistance': 'fateOfResistance',
        'Predict Severity': 'fateOfSeverity',
        'Predict Talent': 'fateOfTalent',
        'Sigil of Armor': 'sigilOfArmor',
        'Sigil of Binding': 'sigilOfBinding',
        'Sigil of Deterrence': 'sigilOfDeterrence',
        'Sigil of Endurance': 'sigilOfEndurance',
        'Sigil of Immunity': 'sigilOfImmunity',
        'Sigil of Resistance': 'sigilOfResistance',
        'Sigil of Revelation': 'sigilOfRevelation',
        'Sigil of Sacrifice': 'sigilOfSacrifice',
        'Sigil of Sealing': 'sigilOfSealing',
        'Sigil of Smiting': 'sigilOfSmiting',
        'Sigil of Truth': 'sigilOfTruth',
        'Sigil of Weaponry': 'sigilOfWeaponry',
        'Touch of Clarity': 'touchOfVision',
        'Touch of Coherence': 'touchOfVision',
        'Touch of Courage': 'touchOfSpine',
        'Touch of Empathy': 'touchOfMind',
        'Touch of Exhilaration': 'touchOfEnergy',
        'Touch of Fitness': 'touchOfBody',
        'Touch of Grit': 'touchOfSpine',
        'Touch of Health': 'touchOfBody',
        'Touch of Invigoration': 'touchOfEnergy',
        'Touch of Rousing': 'touchOfEnergy',
        'Touch of Sympathy': 'touchOfMind',
        'Touch of Wellness': 'touchOfBody'
    }
    for (var k in divine) {
        links[k + ' (divine magic)'] = '/pathfinder/projectRed/magic/magicIndex/' + divine[k] + '.html#' + linkify(k);;
    }

    // supernatural abilities
    var supernatural = {
        'Add/Remove Memory': 'addRemoveMemory',
        'Ageusia': 'ageusia',
        'Anosmia': 'anosmia',
        'Blind Rage': 'blindRage',
        'Blindness': 'blindness',
        'Cause Affliction': 'causeAffliction',
        'Cause Virtue': 'causeVirtue',
        'Challenging Howl': 'challengingHowl',
        'Clear Mind': 'clearMind',
        'Controlled Rage': 'controlledRage',
        'Deafness': 'deafness',
        'Disarming Encounter': 'disarmingEncounter',
        'Encourage/Discourage Anger': 'encourageDiscourageAnger',
        'Encourage/Discourage Concentration': 'encourageDiscourageConcentration',
        'Encourage/Discourage Disgust': 'encourageDiscourageDisgust',
        'Encourage/Discourage Fear': 'encourageDiscourageFear',
        'Encourage/Discourage Happiness': 'encourageDiscourageHappiness',
        'Encourage/Discourage Interest': 'encourageDiscourageInterest',
        'Encourage/Discourage Sadness': 'encourageDiscourageSadness',
        'Encourage/Discourage Surprise': 'encourageDiscourageSurprise',
        'Fei Koppo': 'feiKoppo',
        'Ghost Euphoria': 'ghostEuphoria',
        'Ghost Illness': 'ghostIllness',
        'Ghost Pain': 'ghostPain',
        'Ghost Sight': 'ghostSight',
        'Ghost Smell': 'ghostSmell',
        'Ghost Sound': 'ghostSound',
        'Ghost Taste': 'ghostTaste',
        'Ghost Touch': 'ghostTouch',
        'Hypoesthesia': 'hypoesthesia',
        'Increased Damage Resistance': 'increasedDamageResistance',
        'Internal Fortitude': 'internalFortitude',
        'Intimidating Glare': 'intimidatingGlare',
        'Ip Sappo': 'ipSappo',
        'Iten-ohm Byat': 'itenOhmByat',
        "K'nell Fei": 'kNellFei',
        'Kah Des': 'kahDes',
        'Kit Nu': 'kitNu',
        'Kliearto': 'kliearto',
        'Kliekorpo': 'kliekorpo',
        'Knockback': 'knockback',
        'Koppo Unsig': 'koppoUnsig',
        'Luoko Wu': 'luokoWu',
        'Moment of Euphoria': 'momentOfEuphoria',
        'No Escape': 'noEscape',
        'Puppeteer': 'puppeteer',
        'Raging Bravado': 'ragingBravado',
        'Renewed Vigor': 'renewedVigor',
        'Retribution': 'retribution',
        'Sappo Kukinem': 'sappoKukinem',
        'Shut Down': 'shutDown',
        'Surprise Attack': 'surpriseAttack',
        'Tripping Sweep': 'trippingSweep',
        'Vitor Sech': 'vitorSech',
        'Volitto': 'volitto',
        'Wrathful Weapon': 'wrathfulWeapon',
        'Yatag Soo': 'yatagSoo',
        'Zen Kin': 'zenKin'
    }
    for (var k in supernatural) {
        links[k + ' (supernatural ability)'] = '/pathfinder/projectRed/magic/magicIndex/' + supernatural[k] + '.html#' + linkify(k);;
    }

    // sheets
    var characters = {
        'John': 'johns-character',
        'Tordah Turner': 'johns-character',
        'Michael': 'monkey-business',
        'Keive Cufong': 'monkey-business',
        'Roandask': 'roandasks-character',
        'Lucan Hawklight': 'roandasks-character',
        'Seajade': 'seajades-character',
        'Hurdy-Gurdy': 'seajades-character',
        'Steve': 'steves-character',
        'John Bone': 'steves-character',
        'Pojostick': 'the-silver-oracle',
        'Silver': 'the-silver-oracle'
    }
    for (var key in characters) {
        links[key + ' (character sheet)'] = '/pathfinder/projectRed/sheets/character.html?character=' + characters[key];
    }

    for (var t in links) {
        options[t.toLowerCase()] = t;
    }
}
