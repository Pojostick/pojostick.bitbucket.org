var html = '<div class="header"> <style>a{cursor:pointer}</style> <div class="header-content"> <div id="header-full"> <a href="/pathfinder/projectRed/?activeMenu=menu-rules"><img src="/pathfinder/images/ProjectRed.png" alt="Project Red" style="margin:15px 0 0 0" width=250></a> </div> <div id="header-mobile"> <a href="/pathfinder/projectRed/?activeMenu=menu-rules"><img src="/pathfinder/images/ProjectRed.png" alt="Project Red" style="margin:5px 0 0 10px" height=35></a> </div> </div> <div id="nav-menu" class="nav-menu" style="pointer-events:none"> <a showmenu class="menu-link has-subnav" style="pointer-events:auto"><img src="/pathfinder/images/menu.png"></a> <div id="menu" class="menu" style="pointer-events:auto"> <ul class="active"> <li class="search"> <input id="searchBox" maxlength="256" type="text" name="q" placeholder="Search (press space)" /> </li> <li class="has-subnav"> <a id="menu-rules" submenu>Project Red Rules</a> <ul class="level-2"> <li><a href="/pathfinder/projectRed/general.html?activeMenu=menu-rules">General Changes</a></li> <li><a href="/pathfinder/projectRed/races.html?activeMenu=menu-rules">Races</a></li> <li><a href="/pathfinder/projectRed/skills.html?activeMenu=menu-rules">Skills</a></li> <li><a href="/pathfinder/projectRed/morality.html?activeMenu=menu-rules">Morality</a></li> <li><a href="/pathfinder/projectRed/combat.html?activeMenu=menu-rules">Combat</a></li> <li><a href="/pathfinder/projectRed/currency.html?activeMenu=menu-rules">Currency</a></li> </ul> </li> <li class="has-subnav"> <a id="menu-classes" submenu>Classes</a> <ul class="level-2"> <li><a href="/pathfinder/projectRed/classes/barbarian/?activeMenu=menu-classes">Barbarian</a></li> <li><a href="/pathfinder/projectRed/classes/bard/?activeMenu=menu-classes">Bard</a></li> <li><a href="/pathfinder/projectRed/classes/brawler/?activeMenu=menu-classes">Brawler</a></li> <li><a href="/pathfinder/projectRed/classes/cleric/?activeMenu=menu-classes">Cleric~</a></li> <li><a href="/pathfinder/projectRed/classes/monk/?activeMenu=menu-classes">Monk</a></li> <li><a href="/pathfinder/projectRed/classes/oracle/?activeMenu=menu-classes">Oracle</a></li> <li><a href="/pathfinder/projectRed/classes/paladin/?activeMenu=menu-classes">Paladin</a></li> <li><a href="/pathfinder/projectRed/classes/wizard/?activeMenu=menu-classes">Wizard</a></li> </ul> </li> <li class="has-subnav"> <a id="menu-magic" submenu>Magic</a> <ul class="level-2"> <li><a href="/pathfinder/projectRed/magic/arcane/?activeMenu=menu-magic">Arcane Magic</a></li> <li><a href="/pathfinder/projectRed/magic/divine/?activeMenu=menu-magic">Divine Magic</a></li> <li><a href="/pathfinder/projectRed/magic/supernatural/?activeMenu=menu-magic">Supernatural Abilities</a></li> <li><a href="/pathfinder/projectRed/magic/magicIndex/?activeMenu=menu-magic">Magic Index</a></li> </ul> </li> <li> <a href="/pathfinder/projectRed/sheets">Character Sheets</a> </li> <li> <a href="http://paizo.com/pathfinderRPG/prd/openGameLicense.html">Open Game License</a> </li> </ul> </div> </div> </div> <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha256-k2WSCIexGzOj3Euiig+TlR8gA0EmPjuc79OEeY5L45g=" crossorigin="anonymous" async></script>';
document.write(html);

function showmenu() {
    toggle(document.getElementById('menu'));
}
function submenu(root) {
    toggle(root);
    toggle(root.nextElementSibling);
    return root;
}
function toggle(root) {
    root.className = root.className.replace('active-level', 'placeholderclass')
    if (root.className.indexOf('active') !== -1) {
        root.className = root.className.replace(' active', '');
        root.className = root.className.replace('active', '');
    } else {
        root.className += ' active';
    }
    root.className = root.className.replace('placeholderclass', 'active-level')
}
function findGetParameter(parameterName) {
    var result = null,
    tmp = [];
    var items = location.search.substr(1).split('&');
    for (var index = 0; index < items.length; index++) {
        tmp = items[index].split('=');
        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
    }
    return result;
}
var activeMenu = findGetParameter('activeMenu');
if (activeMenu)
    submenu(document.getElementById(activeMenu)).className += ' active-level';

var searchBox = document.getElementById('searchBox');
if (searchBox) {
    searchBox.onfocus = function(e) {
        if (displaySearch !== undefined) displaySearch();
    };
}

// Added JQuery, so only code below will use it

var jqueryTries = 0;
tryJquery = function() {
    if (typeof $ !== 'undefined') {
        $('a[showmenu]').click(function (e) { e.preventDefault(); showmenu(); });
        $('a[submenu]').click(function (e) { e.preventDefault(); submenu(this); });
    } else {
		if (++jqueryTries > 100) return;
        window.setTimeout(tryJquery, 50);
    }
}
tryJquery();

// Icons
// https://stackoverflow.com/questions/5132488/how-to-insert-script-into-html-head-dynamically-using-javascript
// http://realfavicongenerator.net
