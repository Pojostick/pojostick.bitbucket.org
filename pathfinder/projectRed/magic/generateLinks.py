import re

blacklist = {'Arcane Magic', 'Arcane Phrases'}
links = []
regex = re.compile('[^a-zA-Z]')

with open('arcane/index.html', 'r') as f:
    print 'arcane'
    for line in f:
        start = line.find('<t>')
        if start > -1:
            end = line.find('</t>', start)
            try:
                name = line[start+3:end]
                if name in blacklist: continue
                links += [name]
            except:
                print 'Bad line:', line
    print sorted(links)


prefix = '<p><b><a url="/pathfinder/projectRed/magic/magicIndex/'
for c in ['divine', 'supernatural']:
    print '\n' + c
    links = {}
    with open(c + '/index.html', 'r') as f:
        for line in f:
            start = line.find(prefix)
            if start > -1:
                end = line.find('</a></b>', start)
                try:
                    url, name = line[start+len(prefix):end].split('.html">')
                    links[name] = url
                except:
                    print 'Bad line:', line
    for k in sorted(links.keys()):
        print ''.join([repr(k),": ", repr(links[k]), ","])    
