aBlock = '<a>{a}</a>'

block = '''<p class="stat-block-title"><t>{name}</t></p>
<p class="stat-block-1">{actionicon}<b>Actions</b> {actions}; <b>Traits</b> {traits}</p>
<p class="stat-block-1"><b>Pages</b> {p}</p>
<p class="stat-block-1"><b>Requirements</b> {requirements}</p>
<p class="stat-block-1"><b>Trigger</b> {trigger}</p>
<p>{desc}</p>
<p class="stat-block-1" style="margin-left: 2em;"><b>Success</b> {success}</p>
<p class="stat-block-1" style="margin-left: 2em;"><b>Critical Success</b> {critsuccess}</p>
<p class="stat-block-1" style="margin-left: 2em;"><b>Failure</b> {failure}</p>
<p class="stat-block-1" style="margin-left: 2em;"><b>Critical Failure</b> {critfailure}</p>'''

html = '''<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <title>{uName}</title>
        <link rel="stylesheet" type="text/css" href="/pathfinder/css/style.css">
</head>

<body class="js">
    
    <script type="text/javascript" src="/pathfinder/pf2e/js/menu.js"></script>
    
    <div class="body-content">
        <div id="nav-path">
            <a href="/">Pojostick</a> / <a href="/pathfinder/">Pathfinder</a> / <a href="/pathfinder/pf2e/">Pathfinder Playtest</a> / <a href="/pathfinder/pf2e/skills/{lName}.html?activeMenu=menu-skills">{uName}</a>
        </div>
        
        <div class="body">
            
            <h1><t>{uName}</t> ({ability})</h1>

            <p>{description}</p>
            
            <h2>Uses</h2>
            <p>{abilities}</p>
            
            <h2><t>Untrained Uses</t></h2>
            
            {untrained}

            <h2><t>Trained Uses</t></h2>

            {trained}

        </div>
    </div>
    
    <script type="text/javascript" src="/pathfinder/pf2e/js/links.js"></script>
<script type="text/javascript" src="/pathfinder/js/link.js"></script>
    
</body></html>
'''

skills = {}
with open('skills.json', 'r') as f:
    skills = eval(f.read())

def process(d):
    if d['traits'] != 'none':
        ts = d['traits'].replace(', ', ',').split(',')
        d['traits'] = '<a url="/pathfinder/pf2e/traits.html">' + '</a>, <a url="/pathfinder/pf2e/traits.html">'.join(ts) + '</a>'
    return d

for skill, vals in skills.items():
    lName = skill.capitalize().replace(' ', '')
    lName = lName[0].lower() + lName[1:]
    uName = skill
    with open(lName + '.html', 'w') as f:
        aBlocks = ''
        aList = [aBlock.format(a=ability['name']) for ability in sorted(vals["untrained"] + vals["trained"], key=lambda v: v['name'])]
        if aList:
            aBlocks = '<br>'.join(aList)

        # allow optional fields
        empty = {'actionicon': '', 'requirements': '', 'trigger': '', 'success': '', 'critsuccess': '', 'failure': '', 'critfailure': ''}

        uContent = '<p>None</p>'
        if vals["untrained"]:
            uContent = ''.join(block.format(**{**empty, **process(ability)}) for ability in sorted(vals["untrained"], key=lambda v: v['name']))

        tContent = '<p>None</p>'
        if vals["trained"]:
            tContent = ''.join(block.format(**{**empty, **process(ability)}) for ability in sorted(vals["trained"], key=lambda v: v['name']))

        # get rid of empty fields
        replace = ['<p class="stat-block-1"><b>Requirements</b> </p>', '<p class="stat-block-1"><b>Trigger</b> </p>', '<p class="stat-block-1" style="margin-left: 2em;"><b>Success</b> </p>', '<p class="stat-block-1" style="margin-left: 2em;"><b>Critical Success</b> </p>', '<p class="stat-block-1" style="margin-left: 2em;"><b>Failure</b> </p>', '<p class="stat-block-1" style="margin-left: 2em;"><b>Critical Failure</b> </p>']
        for s in replace:
            uContent = uContent.replace(s, '')
            tContent = tContent.replace(s, '')

        content = html.format(
            lName=lName,
            uName=uName,
            ability=vals["ability"],
            description=vals["description"],
            abilities=aBlocks,
            untrained=uContent,
            trained=tContent
        )

        f.write(content)