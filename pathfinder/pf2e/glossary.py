def process(t):
    line = iter(t.split('\n'))
    done = False
    terms = []
    with open('glossary.generated.html', 'w') as f:
        while not done:
            try:
                term = next(line)
                if ' (' in term:
                    parts = term.split(' (')
                    terms.append(parts[0])
                    parts[0] += '</t></b>'
                    term = ' ('.join(parts)
                else:
                    terms.append(term)
                    term += '</t></b>'
                term = '<p><b><t>' + term
                
                content = []
                while True:
                    try:
                        more = next(line)
                    except StopIteration:
                        done = True
                        break
                    if not more:
                        break
                    content.append(more)
                
                f.write(term)
                f.write(': ')
                f.write(' '.join(content))
                f.write('</p>\n')
            except StopIteration:
                break
        # f.write(str(terms))



glossary = '''Ability
An ability is anything you can do that provides an exception
to the basic rules of play. Ability is most often used as a general
term to refer to rules that could come from a number of sources
without restricting them to a particular source, so "an ability
that gives you a bonus to damage rolls" could be a feat, a spell,
a class feature, and so on.

Ability Boost
An ability boost allows you to increase one of your
ability scores. When you gain an ability boost, you increase
one of your ability scores by 2, or by 1 if the ability score was
already 18 or higher. You gain several ability boosts during
character creation and more at levels 5, 10, 15, and 20. Some
ability boosts must go to a particular score, and others can be
spent as you choose. See page 18 for more.

Ability Flaw
An ability flaw decreases one of your ability scores
by 2 and usually comes from your ancestry (see page 18).

Ability Modifier
An ability modifier is the value added to or
subtracted from your roll based on your ability score.

Ability Score
Your character has six ability scores: Strength,
Dexterity, Constitution, Intelligence, Wisdom, and Charisma.
These scores represent your raw potential and basic attributes.
The higher the score, the greater your potential in tasks related
to that ability score. Because ability scores are used only to
track your growth and calculate your ability modifiers, most
adversaries omit ability scores and instead list only the derived
ability modifiers. Learn more on page 18.

Action (<img src="/pathfinder/images/action-one.svg" style="height: 1.5em; vertical-align: middle;">)
An action is a discrete task performed during your
turn that provides a discrete effect, possibly requiring a check
on your part to determine the result. Actions can be used to
accomplish a variety of things, such as moving, attacking,
casting some part of a spell, or interacting with an item or
object. Most creatures can take up to 3 actions during their
turn. Learn more on page 296.

Activity
An activity is an ability that uses 1 or more of a creature's
actions to perform a special effect for the activity as a whole,
rather than an effect for each action used. Activities that use
2 actions start with this symbol: <img src="/pathfinder/images/action-two.svg" style="height: 1.5em; vertical-align: middle;">. Activities that use 3
actions start with this symbol: <img src="/pathfinder/images/action-three.svg" style="height: 1.5em; vertical-align: middle;">. See page 296.

Affliction
An affliction can affect a creature for a long time,
progressing through different and often increasingly
debilitating stages. The most common kinds of afflictions are
curses, diseases, and poisons. See pages 324&ndash;325.

Alignment
Alignment represents a creature's basic moral and
ethical attitude. Alignment has two axes: the first axis describes
whether a creature is lawful, chaotic, or neutral on that axis,
and the second describes whether the creature is good, evil, or
neutral on that axis. A creature's full alignment is a combination
of their alignment on each axis in order, so a creature lawful
on the first axis and good on the second axis is lawful good
(abbreviated LG). A character neutral along both axes is called
true neutral (abbreviated as just N, rather than NN). See page
16 for more.

Ancestry
An ancestry is the broad family of people to which a
character or other creature belongs. An ancestry determines a
character's ancestry Hit Points, starting languages, Speed, and
senses. An ancestry also gives access to a set of ancestry feats.
Ancestries and their rules appear in Chapter 2.

Arcane
Arcane magic is the tradition that blends material and
mental essences, understanding the magic of the universe
based on experimentation and measurable effects.

Archetype
An archetype is an optional group of special feats that
you can take in place of your regular class feats to give your
character a different theme or suite of abilities. Learn more on
page 279.

Armor Class (AC)
All creatures have an Armor Class. This score
represents how hard it is to hit and damage a creature. It typically
serves as the Difficulty Class for hitting a creature with an attack.</p><p>Your Armor Class is equal to 10 plus your Dexterity modifier (up
to your armor's Dexterity modifier cap), your proficiency modifier
with your armor, and your armor's item bonus to AC, in addition to
any other bonuses and penalties (see page 176).

Attack
When a creature attacks another, it makes an attack roll
against the target's Armor Class. Most attack rolls are made
using the Strike action, but the attack trait sometimes appears
on spells or other abilities (such as Shove). Your attack modifier
is equal to your proficiency modifier with your weapon plus
your Strength modifier for a melee weapon (unless the
weapon's entry says otherwise) or Dexterity modifier for a
ranged weapon, plus any item bonus from the weapon and any
other bonuses and penalties.

Aura
An aura automatically affects creatures or objects within
a certain radius of the source creature without that creature
needing to spend an action. Its effects are applied at a certain
time, such as at the end of each creature's turn.

Background
A background represents a profession or other
significant aspect of your life before becoming an adventurer.
Backgrounds give you ability boosts, signature skills, and feats.
Learn more on page 38.

Bonus
Bonuses are positive numbers that are added to a score
or a roll. They come in three types: item, conditional, and
circumstance. If you gain multiple bonuses of a given type,
you apply only the highest bonus, and ignore the others. See
page 291 for more. See "modifier" and "penalty" for the other
numbers that affect your rolls.

Bulk
Bulk is a measure of how much you are carrying. If you are
carrying total Bulk equal to or more than 5 plus your Strength
modifier, you are encumbered. You can't carry Bulk that exceeds
10 plus your Strength modifier. Learn more on pages 174&ndash;175.

Cantrip
Cantrips are simple spells that a spellcaster can cast as
many times as she likes, and they are always heightened to the
maximum spell level she can cast in that class (or half her level
rounded up, if the cantrip is not tied to a spellcasting class,
such as an innate spell). See page 193 for more information.

Character
The term character is synonymous with creature (see
page 421) but is more often used to refer to player characters
and nonplayer characters than monsters.

Charisma (Cha)
This mental ability score measures your charm
and force of personality.

Check
A check is a type of roll that involves rolling a 1d20, adding
your modifier, and comparing the result to a DC. Attack rolls,
saving throws, Perception checks, and skill checks are the most
common types of checks. Learn more on page 290.

Class
Classes represent the main adventuring profession chosen
by a character. A class determines a character's starting
proficiencies in weapons, armor, spells (if any), and Perception,
the character's signature skills, and the Hit Points a character
gains when gaining a new level, and it gives access to a set of
class features and feats. Classes appear in Chapter 3.

Class DC
Your class DC = 10 + your level + your key ability modifier.
Nonspellcasters use their class DC as the DC for many of their
class features.

Class Feature
Any ability granted by a class is a class feature. These
mainly consist of feats and other abilities specific to the class.

Common
The common rarity indicates that an ability, item, or
spell is available to all players who meet the prerequisites for
it. If something is common, its level is indicated in black (the
darkest shading of the rarities).

Condition
An ongoing effect that changes how you can act or that
alters some of your statistics is called a condition. These often
come from spells. Some frequently occurring conditions are
called basic conditions, which appear on page 320.

Constitution (Con)
This physical ability score measures your
toughness and durability.

Creature
A creature is an active participant in the story or
world. This includes player characters, nonplayer characters,
and monsters.

Critical Failure
A critical failure is a degree of success that results
from a check result that is 10 or more lower than the Difficulty
Class, or is a result of a natural 1 (as long as that natural 1 does
not result in a roll that is higher than the Difficulty Class). A
critical failure is also a failure, though a critical failure entry
supersedes the failure entry if present.

Critical Success
A critical success is a degree of success that
results from a check result that is 10 or more higher than
the Difficulty Class, or a result of natural 20 (as long as that
natural 20 does not result in a roll that is lower than the
Difficulty Class). A critical success is also a success, though a
critical success entry supersedes the success entry if present.
Degree of Success The four degrees of success are critical success,
success, failure, and critical failure. Checks produce one of
these four outcomes depending on the check result compared
to the Difficulty Class (DC). Learn more on page 292.

Deity
Deities are powerful entities that live beyond the world, who
grant power in the form of spells to their truly devoted believers.
Clerics must select a deity at 1st level, but most characters pick
a deity to venerate in the course of their adventuring career. You
can find descriptions of the deities on page 288.

Dexterity (Dexterity)
This physical ability score measures your
agility and adroitness.

Difficulty Class (DC)
The number that a check result must meet
or exceed to determine if a check is successful is called the
Difficulty Class. Some DCs also go by other names, like
Armor Class. When you attempt a check against another
creature, you must compare your result to a DC of 10 plus
its relevant check modifier. For example, if you are sneaking
past a guard, you would roll your Stealth check and compare
the result to the guard's Perception DC (see page 291).

Divine
Divine magic is the tradition that blends spiritual and vital
essences, steeped in faith, the unseen, and belief in power from
beyond the Material Plane.

Downtime
Downtime is a mode of play that covers long spans
of time quickly, breaking down activities day by day or in
even longer spans. When not adventuring, characters are in
downtime. Learn more on page 318.

Effect
An effect is the result of an ability, though an ability's effect
is sometimes contingent on the result of a check or other roll.

Encounter
Within encounter mode, play is broken down into
6-second rounds with extremely precise rules for who acts
when and to what extent. Encounter mode is used for combat,
intense debates, and similar conflicts where time is of the
essence. Learn more on page 304.

Enhancement
Enhancements are extra effects added to the
normal success and critical success effects of a Strike as long
as the Strike deals damage.

Experience Points (XP)
As a player character overcomes
challenges, defeats monsters, and completes quests, she gains
Experience Points. Once she reaches 1,000 XP, she gains a
level, subtracts 1,000 from her XP, and continues accumulating
XP. Learn more on page 278.

Exploration
Within exploration mode, play is more free-form,
using abstracted tactics rather than precise action-by-action
accounting. Exploration mode is used for travel and exploration,
and where in-world time is more fluid and passes more quickly.
Learn more on page 316.

Failure
A failure is a degree of success that results from a check
result lower than the Difficulty Class or a result of natural 1 that
still meets or exceeds the Difficulty Class. If a check has no
failure entry, that means there is no effect on a failure.

Feat
A feat is an ability that a character selects based on his
ancestry, background, class, general training, or skill training.

Flat Check
A flat check is a d20 roll that measures pure chance. A
flat check can't have any modifiers applied to it (see page 292).

Free Action (<img src="/pathfinder/images/free.svg" style="height: 1.5em; vertical-align: middle;">)
Free actions are triggered abilities that you can
use any time the trigger occurs. There's no limit to the number
of free actions you can take per round, but you can't use more
than one on the same trigger. Learn more on page 297.

Free Boost
You can increase an ability score of your choice with
this type of ability boost.

Game Master (GM)
The Game Master is the player who controls
all of the elements of the story and adjudicates the rules, while
cooperating with the other players to craft a fun game.

Hazard
Hazards are non-creature dangers that adventurers
encounter during their journeys, including environmental
hazards, haunts, and traps. Simple hazards have a onetime
effect, but negotiating a complex hazard takes place in
encounter mode, wherein the hazard has a specific routine.

Hit Points (HP)
Hit Points represent the amount of punishment a
creature can take before it dies or begins dying. Damage decreases
Hit Points on a 1-to-1 basis, while healing restores Hit Points at the
same rate. Learn more on page 294.

Initiative
At the start of an encounter, all creatures involved roll
initiative to determine the order in which participants will act
during combat. The higher the result of the initiative roll, the
earlier a creature gets to act. Usually you'll roll using your
Perception skill for initiative, but you might roll a different skill
check instead, or even another kind of check. Learn more on
page 304.

Innate Spell
An innate spell is one granted not by your class, but
by your ancestry or a magic item. Learn more on pages 192&ndash;193.

Intelligence (Int)
This mental ability score measures your reason
and intellect.

Item
An item is an object that you carry, hold, or use. Armor,
weapons, gear, and magic items are all items. Items
sometimes grant an item bonus to a certain type of check
associated with the item. Standard nonmagical items are in
the Equipment chapter (page 174), and magic items are in
Chapter 11.

Key Ability
Your key ability is the ability score you use to
determine your class DC as well as your spell roll modifier
(and thus spell DC) if you are a spellcaster.

Level
Level is a number that measures something's overall power.
A character has a level (sometimes called a character level when
necessary for clarity), ranging from 1 to 20, representing her
overall level of experience. Monsters, hazards, and afflictions also
have levels from 1 to 20 that measure their power and danger. A
magic item's level (also from 1 to 20) indicates its power and its
suitability as treasure for a group or character. Spells have levels
ranging from 1 to 10 that measure their power.

Modifier
Modifiers can be either negative or positive and adjust a
roll. Most d20 rolls add an ability modifier based on an ability
score and a proficiency modifier based on your level of training.
See page 290 for more, and see "bonus" and "penalty" for the
other numbers that affect your rolls.

Monster
A monster is a creature that is typically inhuman and
serves to thwart the PCs in some ways. While some monsters
can become beneficial, or even friendly, those types of monsters
are exceptions in most games.

Multiple Attack Penalty
When you make more than one attack
action in a round, you take a penalty on attacks after the first.
Your multiple attack penalty is &ndash;5 on your second attack and
&ndash;10 on any subsequent attacks. Learn more on page 178.

Nonplayer Character (NPC)
Nonplayer characters are a type
of creature. They can represent a friendly, beneficial, or
challenging person in the game world, in addition to an
adversary the player characters fight. Their mechanics can be
built like that of a monster or a player character, depending on
the GM's preference for the particular NPC.

Occult
Occult magic is the tradition that blends spiritual and
mental essences, understanding the unexplainable, categorizing
the bizarre, and otherwise accounting the ephemeral in a
systematic way.

Penalty
Penalties are negative values that reduce a roll or a
score. They come in three types: conditional, circumstance,
and (rarely) item. If you have multiple penalties of a given
type, you apply only the worst penalty and ignore the others.
Some penalties that you gain due to inherent drawbacks
in your choices, such as the multiple attack penalty, are
untyped, in which case they are cumulative and all apply. See
page 291 for more, and see "modifier" and "bonus" for the
other numbers that affect your rolls.

Perception
Perception measures your character's ability to notice
hidden objects and items. It also determines how quickly you
notice danger and act when a battle's engaged. Your Perception
modifier is equal to your Perception proficiency modifier plus
your Wisdom modifier, as well as any other bonuses and
penalties that apply. Learn more on page 301.

Physical Damage
Bludgeoning, piercing, and slashing damage
fall under the umbrella term physical damage. An effect that
resists or triggers from physical damage (such as resistance 10
to physical) works against any and all of these damage types.

Player
All the actual people at the gaming table or playing
remotely are the players in a Pathfinder game. Most control
player characters (PCs), though the GM also controls monsters,
the environment, and nonplayer characters (NPCs).

Player Character (character or PC)
A character controlled by a
player, a player character goes on adventures, meets other
characters, fights, grows, and pursues all sorts of other goals
that the player imagines. In most games, each player except the
GM will have one player character.

Power
A power is a type of spell cast using Spell Points. Powers
are always heightened to the maximum spell level a character
can cast in that class (or half her level rounded up, if she isn't a
spellcaster). Powers always have a descriptive term describing
the type of power, such as "domain power" or "ki power." See
page 193.

Prerequisite
Many feats and other abilities can be taken only
if you meet their prerequisites. Prerequisites are often other
feats, class talents, or proficiency levels.

Primal
Primal magic is the tradition that blends material and vital
essences, rooted in an instinctual connection to and faith in the
natural world.

Proficiency
Your proficiency measures your training in the use of
a weapon, armor, skill, saving throw, or some other check or
score. There are five ranks of proficiency: untrained, trained,
expert, master, and legendary. Your proficiency modifier is
equal to your level plus a value depending on your proficiency
rank (&ndash;2 for untrained, 0 for trained, 1 for expert, 2 for master,
or 3 for legendary). You're considered untrained unless an
ability gives you better proficiency. Learn more on page 290.

Rare
This rarity indicates that an ability, item, or spell is available
to players only if the GM decides to include it in the game,
typically through discovery during an adventure. If something
is rare, its level is indicated in orange (the third-darkest shading
of the rarities when viewed in black and white).

Reaction (<img src="/pathfinder/images/reaction.svg" style="height: 1.5em; vertical-align: middle;">)
A reaction is something you can do even when it
is not your turn, as long as the reaction's trigger occurs. You
typically can use only 1 reaction per turn, and regardless you
can use only 1 on any given trigger. Learn more on page 297.

Requirement
You must satisfy the requirements of an action, feat,
spell, item, or other ability in order to use it.

Resonance Points (RP)
Every character has a pool of Resonance
Points. Magic items require you to spend Resonance Points to
activate them or wear them. Your RP are equal to your level
plus your Charisma modifier. Learn more on page 376.

Retrain
If you're unhappy with one of your character choices, you
can spend time during downtime to change it. Learn more on
page 318.

Roll
Any time you roll dice, you're making a roll. This could be a
roll using a d20 (often called a check), a damage roll using dice
other than d20s, or any other die roll.

Round
Time during encounter mode is measured in rounds
of roughly 6 seconds of time in the game world. During the
course of a round, all creatures have a chance to act, in an order
determined by initiative. Learn more on page 304.

Saving Throw (Save)
When you are subjected to a dangerous
effect, you can often attempt a saving throw (sometimes
called a "save") to mitigate the effect. You roll a saving throw
automatically&mdash;you don't have to spend an action or a reaction.
Unlike most checks, the person who isn't acting rolls the d20
for a saving throw, and the person who is acting generates
the DC.</p><p>There are three types of saving throws: Fortitude (based
on Constitution and used to resist poisons, diseases, forced
movement, and other physical effects), Reflex (based on
Dexterity and used to mitigate effects that you could quickly
dodge), and Will (based on Wisdom and used to resist effects
that target your mind or spirit). Your saving throw modifier
is equal to your proficiency modifier plus the ability modifier
listed above, along with any other bonuses or penalties. Learn
more on page 17.

Secret Check
A secret check is a check the GM rolls in secret,
typically because seeing the result of the die would bias the
player's response to the effect. See page 293 for more.

Signature Skill
You can gain master and legendary proficiency in
your signature skills, which are usually granted via your class.

Skill
A skill represents a creature's ability to perform certain tasks
that require experience or training. Skills typically have a list
of uses you can perform even if you're untrained with the skill,
followed by ones that require you to be trained with the skill.
Your skill bonus is equal to your proficiency modifier plus the
skill's associated ability modifier, along with any other bonuses
and penalties. Learn more on page 142.

Skill Feat
Skill feats are a type of general feat that relate
specifically to a skill. You can select a skill feat only if you have
the prerequisite proficiency rank in the associated skill.

Speed
Speed is the number of feet that a character can move using
the Stride action. You might have other Speeds that determine
how far you can move with other actions. See page 310.

Spell
Spells are magical effects generated by the Cast a Spell
activity. Spells specify what they target, their effects upon
successful casting, the actions needed to cast them, and how
they can be resisted or negated. Spells and their rules appear
in Chapter 7 (page 192).

Spell DC
See Spell Roll.

Spell Level
A spell has a level ranging from 1 to 10. As a character
gets more powerful, he can cast spells of a higher level.

Spell Roll
You make a spell roll when you're testing the power of
your magic against a particular target. Your spell roll modifier
is equal to your proficiency modifier plus your key ability
modifier, as well as any other bonuses and penalties (though
these are quite rare). Many of your spells will call for saving
throws against your spell DC (10 plus your spell roll modifier).

Spellcaster
A spellcaster is a character whose class or archetype
grants them the spellcasting class feature. NPCs and monsters
might also be considered spellcasters if they primarily rely on
spells to be effective. The ability to cast powers or innate spells
does not by itself make a character a spellcaster.

Strength (Str)
This physical ability score measures your brawn.

Stride
You can move up to your Speed with the Stride action,
which appears on page 308.

Strike
The Strike action lets you make an attack. You can find it
on page 308.

Success
A success is a degree of success that results from a check
result equal to or greater than the Difficulty Class or a result of
natural 20 that is lower than the Difficulty Class. If a check has
no success entry, that means there is no effect on a success.

Touch Armor Class (TAC)
All creatures in the game have a Touch
Armor Class. This score represents how hard it is to hit and
damage the creature with an attack that can bypass some of
the protection granted by armor. Your TAC is equal to 10 plus
your Dexterity modifier (up to your armor's Dexterity modifier
cap), plus your proficiency modifier with your armor, plus
your armor's item bonus to TAC and any other bonuses and
penalties. Learn more on page 176.

Trait
A trait is an indicator that a feat, action, item, spell, monster,
or some other rules item obeys special rules defined by the trait
or interacts with other rules in a special way. For instance, a fire
spell or a monster made of fire has the fire trait. You'll find a
glossary of traits on page 414.

Trap
Traps are the most common type of hazard, constructed
intentionally to harm or obstruct trespassers.

Trigger
A trigger is a discrete event or action that must occur in
order for you to use a reaction or a free action. Sometimes spell
effects, monsters, traps, or other effects in the game respond
to triggers. Learn more on page 297.

Turn
Over the course of a round, each creature receives a single turn;
it can typically take up to 3 actions on its turn (see page 304).

Uncommon
Something of uncommon rarity isn't available to
everyone. It requires special training or comes from a particular
part of the world. Some character choices can automatically
give a character access to uncommon options, and the GM
can allow access for anyone she chooses. If something is
uncommon, its level is indicated in red (the second-darkest
shading of the rarities when viewed in black and white).

Unique
The unique rarity applies to anything that's one of a
kind. Since this book doesn't include artifacts, there aren't any
entries that are unique, though some of the playtest monsters
are unique. If something is unique, its level is indicated in light
blue (the lightest shading of the rarities when viewed in black
and white).

Wisdom (Wis)
This mental ability score measures your awareness
and intuition.'''

process(glossary)