var html = '<div class="header"> <style>a{cursor:pointer}</style> <div class="header-content"> <div id="header-full"> <a href="/pathfinder/pf2e/"><img src="http://cdn.paizo.com/image/content/PathfinderRPG/BetaLogo.png" alt="Pathfinder Playtest" style="margin:15px 0 0 0" width=250></a> </div> <div id="header-mobile"> <a href="/pathfinder/pf2e/"><img src="http://cdn.paizo.com/image/content/PathfinderRPG/BetaLogo.png" alt="Pathfinder Playtest" style="margin:5px 0 0 10px" height=35></a> </div> </div> <div id="nav-menu" class="nav-menu" style="pointer-events:none"> <a showmenu class="menu-link has-subnav" style="pointer-events:auto"><img src="/pathfinder/images/menu.png"></a> <div id="menu" class="menu" style="pointer-events:auto"> <ul class="active"> <li class="search"> <input id="searchBox" maxlength="256" type="text" name="q" placeholder="Search (press space)" /> </li> <li class="has-subnav"> <a id="menu-skills" submenu>Skills</a> <ul class="level-2"> <li><a href="/pathfinder/pf2e/skills/acrobatics.html?activeMenu=menu-skills">Acrobatics</a></li> <li><a href="/pathfinder/pf2e/skills/arcana.html?activeMenu=menu-skills">Arcana</a></li> <li><a href="/pathfinder/pf2e/skills/athletics.html?activeMenu=menu-skills">Athletics</a></li> <li><a href="/pathfinder/pf2e/skills/crafting.html?activeMenu=menu-skills">Crafting</a></li> <li><a href="/pathfinder/pf2e/skills/deception.html?activeMenu=menu-skills">Deception</a></li> <li><a href="/pathfinder/pf2e/skills/diplomacy.html?activeMenu=menu-skills">Diplomacy</a></li> <li><a href="/pathfinder/pf2e/skills/intimidation.html?activeMenu=menu-skills">Intimidation</a></li> <li><a href="/pathfinder/pf2e/skills/lore.html?activeMenu=menu-skills">Lore</a></li> <li><a href="/pathfinder/pf2e/skills/medicine.html?activeMenu=menu-skills">Medicine</a></li> <li><a href="/pathfinder/pf2e/skills/nature.html?activeMenu=menu-skills">Nature</a></li> <li><a href="/pathfinder/pf2e/skills/occultism.html?activeMenu=menu-skills">Occultism</a></li> <li><a href="/pathfinder/pf2e/skills/performance.html?activeMenu=menu-skills">Performance</a></li> <li><a href="/pathfinder/pf2e/skills/religion.html?activeMenu=menu-skills">Religion</a></li> <li><a href="/pathfinder/pf2e/skills/society.html?activeMenu=menu-skills">Society</a></li> <li><a href="/pathfinder/pf2e/skills/stealth.html?activeMenu=menu-skills">Stealth</a></li> <li><a href="/pathfinder/pf2e/skills/survival.html?activeMenu=menu-skills">Survival</a></li> <li><a href="/pathfinder/pf2e/skills/thievery.html?activeMenu=menu-skills">Thievery</a></li> </ul> </li> <li> <a href="/pathfinder/pf2e/actions.html">Actions</a> </li> <li> <a href="/pathfinder/pf2e/conditions.html">Conditions</a> </li> <li> <a href="/pathfinder/pf2e/tactics.html">Tactics</a> </li> <li> <a href="/pathfinder/pf2e/traits.html">Traits</a> </li> <li> <a href="/pathfinder/pf2e/glossary.html">Glossary</a> </li> <li> <a href="http://paizo.com/pathfinderRPG/prd/openGameLicense.html">Open Game License</a> </li> </ul> </div> </div> </div> <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha256-k2WSCIexGzOj3Euiig+TlR8gA0EmPjuc79OEeY5L45g=" crossorigin="anonymous" async></script>';
document.write(html);

function showmenu() {
    toggle(document.getElementById('menu'));
}
function submenu(root) {
    toggle(root);
    toggle(root.nextElementSibling);
    return root;
}
function toggle(root) {
    root.className = root.className.replace('active-level', 'placeholderclass')
    if (root.className.indexOf('active') !== -1) {
        root.className = root.className.replace(' active', '');
        root.className = root.className.replace('active', '');
    } else {
        root.className += ' active';
    }
    root.className = root.className.replace('placeholderclass', 'active-level')
}
function findGetParameter(parameterName) {
    var result = null,
    tmp = [];
    var items = location.search.substr(1).split('&');
    for (var index = 0; index < items.length; index++) {
        tmp = items[index].split('=');
        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
    }
    return result;
}
var activeMenu = findGetParameter('activeMenu');
if (activeMenu)
    submenu(document.getElementById(activeMenu)).className += ' active-level';

var searchBox = document.getElementById('searchBox');
if (searchBox) {
    searchBox.onfocus = function(e) {
        if (displaySearch !== undefined) displaySearch();
    };
}

// Added JQuery, so only code below will use it

var jqueryTries = 0;
tryJquery = function() {
    if (typeof $ !== 'undefined') {
        $('a[showmenu]').click(function (e) { e.preventDefault(); showmenu(); });
        $('a[submenu]').click(function (e) { e.preventDefault(); submenu(this); });
    } else {
		if (++jqueryTries > 100) return;
        window.setTimeout(tryJquery, 50);
    }
}
tryJquery();

// Icons
// https://stackoverflow.com/questions/5132488/how-to-insert-script-into-html-head-dynamically-using-javascript
// http://realfavicongenerator.net
