function loadLinks() {
    links = {
        'Pathfinder Playtest' : '/pathfinder/pf2e/index.html'
    };

    // general
    var general = ['Actions', 'Conditions', 'Traits', 'Tactics', 'Glossary'];
    for (var i in general) {
        var k = general[i];
        links[k] = '/pathfinder/pf2e/' + k.toLowerCase() + '.html';
    }

    // skills
    var skills = ['Acrobatics', 'Arcana', 'Athletics', 'Crafting', 'Deception', 'Diplomacy', 'Intimidation', 'Lore', 'Medicine', 'Nature', 'Occultism', 'Performance', 'Religion', 'Society', 'Stealth', 'Survival', 'Thievery'];
    for (var i in skills) {
        var k = skills[i];
        links[k + ' (skill)'] = '/pathfinder/pf2e/skills/' + k.toLowerCase() + '.html#' + linkify(k);
    }

    // skill abilities
    var sAbilities = {'Acrobatics': ['Balance', 'Escape', 'Grab Edge', 'Maintain Balance', 'Squeeze', 'Tumble Through', 'Maneuver in Flight'], 'Arcana': ['Recall Knowledge', 'Borrow an Arcane Spell', 'Identify Magic', 'Learn an Arcane Spell', 'Read Magic'], 'Athletics': ['Break Grapple', 'Break Open', 'Climb', 'Grapple', 'High Jump', 'Long Jump', 'Shove', 'Swim', 'Trip', 'Disarm'], 'Crafting': ['Repair', 'Craft', 'Identify Alchemy'], 'Deception': ['Create a Diversion', 'Impersonate', 'Lie', 'Feint'], 'Diplomacy': ['Gather Information', 'Make an Impression', 'Request'], 'Intimidation': ['Demoralize', 'Coerce'], 'Lore': ['Recall Knowledge', 'Practice a Trade'], 'Medicine': ['Administer First Aid', 'Treat Disease', 'Treat Poison'], 'Nature': ['Command an Animal', 'Handle an Animal', 'Recall Knowledge', 'Identify Magic', 'Learn a Primal Spell'], 'Occultism': ['Recall Knowledge', 'Identify Magic', 'Learn an Occult Spell', 'Read Esoterica'], 'Performance': ['Perform', 'Stage a Performance'], 'Religion': ['Recall Knowledge', 'Identify Magic', 'Learn a Divine Spell', 'Read Scripture'], 'Society': ['Recall Knowledge', 'Subsist on the Streets', 'Create Forgery', 'Decipher Writing'], 'Stealth': ['Conceal an Object', 'Hide', 'Sneak'], 'Survival': ['Sense Direction', 'Survive in the Wild', 'Cover Tracks', 'Track'], 'Thievery': ['Palm an Object', 'Steal an Object', 'Disable a Device', 'Pick a Lock']};
    for (var k in sAbilities) {
        for (var i in sAbilities[k]) {
            var v = sAbilities[k][i];
            links[v + ' (' + k + ')'] = '/pathfinder/pf2e/skills/' + k.toLowerCase() + '.html#' + linkify(v);
        }
    }

    // actions
    var actions = ['Activate an Item', 'Aid', 'Arrest a Fall', 'Assist', 'Attack of Opportunity', 'Breathe Deep', 'Burrow', 'Command Activation', 'Concentrate on a Spell', 'Crawl', 'Delay', 'Drop', 'Drop Prone', 'Fly', 'Focus Activation', 'Interact', 'Invest an Item', 'Leap', 'Material Casting', 'Mount', 'Operate Activation', 'Point Out', 'Raise a Shield', 'Ready', 'Seek', 'Shield Block', 'Somatic Casting', 'Stand', 'Step', 'Stride', 'Strike', 'Take Cover', 'Verbal Casting'];
    for (var i in actions) {
        var k = actions[i];
        links[k + ' (action)'] = '/pathfinder/pf2e/actions.html#' + linkify(k);
    }

    // conditions
    var conditions = ['Accelerated', 'Asleep', 'Blinded', 'Broken', 'Concealed', 'Confused', 'Dazzled', 'Dead', 'Deafened', 'Drained', 'Dying', 'Encumbered', 'Enervated', 'Enfeebled', 'Entangled', 'Fascinated', 'Fatigued', 'Flat-Footed', 'Fleeing', 'Friendly', 'Frightened', 'Grabbed', 'Hampered', 'Helpful', 'Hostile', 'Immobile', 'Indifferent', 'Paralyzed', 'Persistent Damage', 'Petrified', 'Prone', 'Quick', 'Restrained', 'Sensed', 'Sick', 'Slowed', 'Sluggish', 'Stunned', 'Stupefied', 'Unconscious', 'Unfriendly', 'Unseen'];
    for (var i in conditions) {
        var k = conditions[i];
        links[k + ' (condition)'] = '/pathfinder/pf2e/conditions.html#' + linkify(k);
    }

    // tactics
    var tactics = ['Carousing', 'Casting a Spell', 'Concentrating on a Spell', 'Conversing', 'Covering Tracks', 'Defending', 'Detecting Magic', 'Exploration Tactics', 'Following Tracks', 'Hustling', 'Investigating', 'Looking Out', 'Searching', 'Shopping', 'Sneaking', 'Social Tactics', 'Stealing', 'Wandering'];
    for (var i in tactics) {
        var k = tactics[i];
        links[k + ' (tactic)'] = '/pathfinder/pf2e/tactics.html#' + linkify(k);
    }

    // traits
    var traits = ['Abjuration', 'Acid', 'Activate', 'Additive', 'Agile', 'Air', 'Alchemical', 'Alchemist', 'Animal', 'Animal Order', 'Arcane', 'Attached', 'Attack', 'Auditory', 'Backstabber', 'Backswing', 'Barbarian', 'Bard', 'Chaotic', 'Charge', 'Cleric', 'Clumsy', 'Cold', 'Composition', 'Concentrate', 'Conjuration', 'Consecration', 'Consumable', 'Contact', 'Curse', 'Darkness', 'Deadly', 'Death', 'Dedication', 'Detection', 'Dinosaur', 'Disarm', 'Disease', 'Divination', 'Divine', 'Downtime', 'Druid', 'Dwarf', 'Earth', 'Electricity', 'Elf', 'Elixir', 'Emotion', 'Enchantment', 'Evil', 'Evocation', 'Extradimensional', 'Fatal', 'Fear', 'Fighter', 'Finesse', 'Fire', 'Force', 'Forceful', 'Fortune', 'Fragile', 'General', 'Gnome', 'Goblin', 'Good', 'Half-Elf', 'Half-Orc', 'Halfling', 'Healing', 'Heritage', 'Human', 'Humanoid', 'Illusion', 'Infused', 'Ingested', 'Inhaled', 'Injury', 'Invested', 'Lawful', 'Leaf Order', 'Light', 'Lingual', 'Litany', 'Magical', 'Manipulate', 'Mechanical', 'Mental', 'Metamagic', 'Mindless', 'Minion', 'Misfortune', 'Monk', 'Morph', 'Move', 'Multiclass', 'Mutagen', 'Necromancy', 'Negative', 'Noisy', 'Nonlethal', 'Oath', 'Occult', 'Oil', 'Open', 'Orc', 'Paladin', 'Parry', 'Plant', 'Poison', 'Polymorph', 'Positive', 'Possession', 'Potent', 'Potion', 'Prediction', 'Press', 'Prestige', 'Primal', 'Propulsive', 'Rage', 'Ranger', 'Reach', 'Revelation', 'Rogue', 'Scroll', 'Scrying', 'Secret', 'Shadow', 'Shove', 'Skill', 'Snare', 'Sonic', 'Sorcerer', 'Special', 'Spellcasting', 'Splash', 'Staff', 'Stance', 'Storm Order', 'Summoned', 'Sweep', 'Teleportation', 'Thrown', 'Totem', 'Transmutation', 'Trap', 'Trinket', 'Trip', 'Twin', 'Two-Hand', 'Unarmed', 'Undead', 'Versatile', 'Virulent', 'Visual', 'Volley', 'Wand', 'Water', 'Wild Order', 'Wizard'];
    for (var i in traits) {
        var k = traits[i];
        links[k + ' (trait)'] = '/pathfinder/pf2e/traits.html#' + linkify(k);
    }

    // glossary
    var glossary = ['Ability', 'Ability Boost', 'Ability Flaw', 'Ability Modifier', 'Ability Score', 'Action', 'Activity', 'Affliction', 'Alignment', 'Ancestry', 'Arcane', 'Archetype', 'Armor Class', 'Attack', 'Aura', 'Background', 'Bonus', 'Bulk', 'Cantrip', 'Character', 'Charisma', 'Check', 'Class', 'Class DC', 'Class Feature', 'Common', 'Condition', 'Constitution', 'Creature', 'Critical Failure', 'Critical Success', 'Deity', 'Dexterity', 'Difficulty Class', 'Divine', 'Downtime', 'Effect', 'Encounter', 'Enhancement', 'Experience Points', 'Exploration', 'Failure', 'Feat', 'Flat Check', 'Free Action', 'Free Boost', 'Game Master', 'Hazard', 'Hit Points', 'Initiative', 'Innate Spell', 'Intelligence', 'Item', 'Key Ability', 'Level', 'Modifier', 'Monster', 'Multiple Attack Penalty', 'Nonplayer Character', 'Occult', 'Penalty', 'Perception', 'Physical Damage', 'Player', 'Player Character', 'Power', 'Prerequisite', 'Primal', 'Proficiency', 'Rare', 'Reaction', 'Requirement', 'Resonance Points', 'Retrain', 'Roll', 'Round', 'Saving Throw', 'Secret Check', 'Signature Skill', 'Skill', 'Skill Feat', 'Speed', 'Spell', 'Spell DC', 'Spell Level', 'Spell Roll', 'Spellcaster', 'Strength', 'Stride', 'Strike', 'Success', 'Touch Armor Class', 'Trait', 'Trap', 'Trigger', 'Turn', 'Uncommon', 'Unique', 'Wisdom'];
    for (var i in glossary) {
        var k = glossary[i];
        links[k] = '/pathfinder/pf2e/glossary.html#' + linkify(k);
    }

    for (var t in links) {
        options[t.toLowerCase()] = t;
    }
}
