def process(t):
    line = iter(t.split('\n'))
    done = False
    traits = []
    with open('traits.generated.html', 'w') as f:
        while not done:
            try:
                trait = next(line)
                if ' (' in trait:
                    parts = trait.split(' (')
                    traits.append(parts[0])
                    parts[0] += '</t></b>'
                    trait = ' ('.join(parts)
                else:
                    traits.append(trait)
                    trait += '</t></b>'
                trait = '<p><b><t>' + trait
                
                content = []
                while True:
                    try:
                        more = next(line)
                    except StopIteration:
                        done = True
                        break
                    if not more:
                        break
                    content.append(more)
                
                f.write(trait)
                f.write(': ')
                f.write(' '.join(content))
                f.write('</p>\n')
            except StopIteration:
                break
        # f.write(str(traits))



traits = '''Abjuration
Effects and magic items with this trait are associated
with the abjuration school of magic, typically involving
protections, wards, or countermagic.

Acid
Effects with this trait deal acid damage. An item with
Hardness 5 or lower takes 1 extra Dent from acid damage.
Creatures with this trait have a magical connection to acid.

Activate (page 376)
An action with this trait is used as part of the
Activate an Item activity.

Additive
Alchemists use additives. Feats with this trait allow you
to spend actions to add special substances to bombs or elixirs.
You can add only one additive to a single alchemical item, and
attempting to add another spoils the item. You can typically
take actions with the additive trait only when you're creating
an infused alchemical item, and some can be used only with the
Quick Alchemy action.

Agile
The multiple attack penalty you take on the second attack
each turn with this weapon is &ndash;4 instead of &ndash;5, and &ndash;8 instead
of &ndash;10 on the third and subsequent attacks in the turn.

Air
Effects with the air trait either manipulate or conjure air.
Those that manipulate air have no effect in a vacuum or an area
without air. Creatures with this trait consist primarily of air or
have a magical connection to the element.

Alchemical
Alchemical items are powered by reactions of
alchemical reagents, sometimes sparked by the user's Resonance
Points. Alchemical items aren't magical, and they don't radiate a
magical aura. Characters can Craft these items only if they have
the Alchemical Crafting feat (see pages 162-163).

Alchemist
This indicates abilities from the alchemist class.

Animal
An animal is a creature with a relatively low Intelligence.
They typically do not have an Intelligence ability score over 3,
can't speak languages, and can't be trained in Intelligencebased
skills.

Animal Order
This gives druids of the animal order an extra benefit.

Arcane
This spell or magic item comes from an arcane spellcaster
or from the arcane magical tradition.
Archetype (page 279) This feat belongs to an archetype.

Attached
An attached weapon must be combined with another
piece of gear in order to be used. The trait lists what type of
item the weapon must be attached to. You must be wielding or
wearing the item the weapon is attached to in order to attack
with it. For example, shield spikes are attached to a shield,
allowing you to attack with the spikes instead of a shield bash,
but only if you're wielding the shield. An attached weapon is
usually bolted onto or built into the item it's attached to, and
typically an item can have only one weapon attached to it. An
attached weapon can be removed from one item and attached
to another with successful use of the Crafting skill. If an item is
destroyed, its attached weapon can usually be salvaged.

Attack
This ability grants an attack. For each attack you make
beyond the first on your turn, you take a multiple attack
penalty (see page 305).

Auditory
Auditory actions and effects rely on sound. An action
with the auditory trait can be successfully performed only if
the creature using the action can speak or otherwise produce
the required sounds. A spell or effect with the auditory trait has
its effect only if the target can hear it. This is different from a
sonic effect, which still affects targets who can't hear it (such
as deaf targets) as long as the effect makes sound.

Backstabber
When you hit a flat-footed creature, this weapon
deals 1 precision damage. The damage increases to 2 if the
weapon is master quality or to 3 if it's legendary.

Backswing
You can use this weapon's momentum from a miss
to lead into your next attack. After missing with this weapon
on your turn, you gain a +1 circumstance bonus to your next
attack with this weapon before the end of your turn.

Barbarian
This indicates abilities from the barbarian class.

Bard
This indicates abilities from the bard class.
Bomb (page 359) Alchemical bombs are a type of alchemical
item that must be thrown instead of activated. Most of them
explode, dealing damage to their target and splash damage to
other nearby creatures.
Cantrip (page 193) A spell with this trait can be cast at will and
automatically heightens to the highest level spell you can cast.

Chaotic
Chaotic effects often manipulate energy from chaos-aligned
Outer Planes and are anathema to lawful divine
servants or divine servants of lawful deities. A creature with
this trait is chaotic in alignment. An ability with this trait can
be selected or used by chaotic creatures only.

Charge
If you moved at least 10 feet on the action before your
attack, add a circumstance bonus to damage for that attack
equal to the number of damage dice for the weapon.

Cleric
This indicates abilities from the cleric class.

Clumsy
This armor's Dexterity modifier cap also applies to Reflex
saves and on all Dexterity-based skill and ability checks that
don't have the attack trait.

Cold
Effects with this trait deal cold damage. Creatures with this
trait have a connection to magical cold.

Composition (page 64)
Compositions are special spells available
to bards.

Concentrate
It takes a degree of mental concentration and
discipline to use this type of action.

Conjuration
Effects and magic items with this trait are associated
with the conjuration school of magic, typically involving
summoning, creation, teleportation, or moving things from
place to place.

Consecration
A consecration spell enhances an area for an
extended period of time. A given area can have only a single
consecration effect at a time. The new consecration effect
attempts to dispel the old one (see page 197).

Consumable
An item with this trait can be used only once. Unless
stated otherwise, the item is destroyed after activation, though
in some cases, part of it might be recoverable for other purposes.
For instance, while a potion is a consumable item, the vial the
potion comes in is not destroyed after the potion is activated.
When a character creates consumable items, she can make
them in batches of four, as described in the Craft activity (see
page 148).

Contact (page 360)
This poison activates on touch.

Curse
A curse is an effect that places some long-term affliction on
a creature. Curses are always magic and are typically the result
of a spell or trap.

Darkness (page 197)
Darkness effects extinguish light in the
area, including less powerful magical light. Darkness and light
effects can negate one another.

Deadly
On a critical hit, a weapon with this trait adds a weapon
damage die of the listed size. This damage increases to two dice
if the weapon is master quality and three dice if the weapon
is legendary. For instance, a master-quality rapier deals 2d6
additional piercing damage on a critical hit.

Death (page 296)
A death effect kills living creatures more easily.

Dedication (page 279)
You must select a feat with this trait to
start following an archetype.

Detection
Effects with this trait attempt to determine the
presence or location of a person, thing, or aura. Most effects
with the detection trait also have the divination trait.

Dinosaur
These reptiles have survived from prehistoric times.

Disarm
You can use this weapon to Disarm with the Athletics skill
even if you don't have a free hand. This uses the weapon's reach
(if different from your own) and adds the weapon's item bonus
to attack rolls (if any) as a bonus to the Athletics check. If you
critically fail a check to Disarm using the weapon, you can drop
the weapon to treat it as a normal failure. On a critical success,
you still need a free hand if you want to take the item.

Disease
Effects with this trait apply one or more diseases.
Diseases are typically afflictions (see pages 324&ndash;325).

Divination
Effects and magic items with this trait are associated
with the divination school of magic, typically involving
obtaining or transferring information.

Divine
This spell or magic item comes from deific power, from a
divine spellcaster, or from the divine magical tradition.

Downtime
This ability can be used only during the downtime
between adventures.

Druid
This indicates abilities from the druid class.

Dwarf
A creature with this trait is one of the dwarves, stout folk
who often live underground and typically have darkvision. An
ability with this trait can be used or chosen only by dwarves. A
weapon with this trait is created and used by dwarves.

Earth
Effects with the earth trait either manipulate or conjure
earth. Those that manipulate earth have no effect in an area
without earth. Creatures with this trait consist primarily of
earth or have a magical connection to the element.

Electricity
Effects with this trait deal electricity damage. A
creature with this trait has a magical connection to electricity.

Elf
A creature with this trait is one of the elves, mysterious people
with rich traditions of magic and scholarship who typically
have low-light vision. An ability with this trait can be used or
chosen only by elves. A weapon with this trait is created and
used by elves.

Elixir (page 359)
The alchemical items called elixirs must be
drunk to be activated.

Emotion
These effects alter a creature's emotions. Effects with
this trait always have the mental trait as well. Creatures with
special training or that have mechanical or artificial intelligence
are immune to emotion effects.

Enchantment
Effects and magic items with this trait are associated
with the enchantment school of magic, typically involving mind
control, emotional alteration, and other mental effects.

Evil
Evil effects often manipulate energy from evil-aligned Outer
Planes and are anathema to good divine servants or divine
servants of good deities. A creature with this trait is evil in
alignment. An ability with this trait can be selected or used by
evil creatures only.

Evocation
Effects and magic items with this trait are associated
with the evocation school of magic, typically involving energy
and other elemental forces.

Extradimensional
This effect or item creates an extradimensional
space. An extradimensional effect placed inside another
extradimensional space ceases to function until it is removed.

Fatal
The fatal trait includes a die size. On a critical hit, all the
weapon's damage dice increase to that die size instead of the
normal size, and another weapon damage die of the listed size
is added to the damage roll.

Fear
Fear effects evoke the emotion of fear. Effects with this trait
always have the mental and emotion traits as well.

Fighter
This indicates abilities from the fighter class.

Finesse
You can use your Dexterity modifier instead of your
Strength modifier when making attack rolls with this melee
weapon. You still use Strength when calculating damage.

Fire
Effects with the fire trait deal fire damage or either conjure or
manipulate fire. Those that manipulate fire have no effect in an
area without fire. Creatures with this trait consist primarily of
fire or have a magical connection to the element.

Force
Effects with this trait deal force damage or create objects
made of pure magical force.

Forceful
This weapon becomes more dangerous when you build
up momentum. When you attack with the weapon more than
once on your turn, the second attack adds a circumstance
bonus to damage equal to the number of weapon damage dice,
and each subsequent attack adds a circumstance bonus to
damage equal to double the number of weapon damage dice.

Fortune (page 293)
Effects that improve your destiny or luck have
the fortune trait. They usually change the results of your dice
rolls. Fortune and misfortune cancel each other out.

Fragile
If fragile armor takes a Dent, it's automatically broken.
Free-Hand This weapon doesn't take up your hand, usually because
it is built into your armor. A free-hand weapon can't be Disarmed.
You can use the hand covered by your free-hand weapon to wield
other items, perform manipulate actions, and so forth. You can't
attack with a free-hand weapon if you're wielding anything in
that hand or using the hand for something else. When you're not
wielding anything and not using the hand, you can use abilities
that require you to have a hand free as well as those that require
you to be wielding a weapon in that hand. Each of your hands
can have only one free-hand weapon in it.

General
Any character, regardless of ancestry and class, can take
a general feat as long as they meet the prerequisites.

Gnome
A creature with this trait is one of the gnomes, small
people skilled at magic who seek out new experiences and
usually have low-light vision. An ability with this trait can be
used or chosen only by gnomes. A weapon with this trait is
created and used by gnomes.

Goblin
A creature with this trait can come from multiple tribes
of creatures, including small and canny goblins, militaristic
hobgoblins, and hulking, savage bugbears. Goblins tend to
have darkvision. An ability with this trait can be used or
chosen only by goblins. A weapon with this trait is created
and used by goblins.

Good
Good effects often manipulate energy from good-aligned
Outer Planes and are anathema to evil divine servants or divine
servants of evil deities. A creature with this trait is good in
alignment. An ability with this trait can be selected or used
only by good creatures.

Half-Elf
A creature with this trait is part human and part elf. An
ability with this trait can be used or chosen only by half-elves.

Halfling
A creature with this trait is one of the halflings, small
people considered to be lucky and friendly wanderers. An
ability with this trait can be used or chosen only by halflings. A
weapon with this trait is created and used by halflings.

Half-Orc
A creature with this trait is part human and part orc. An
ability with this trait can be used or chosen only by half-orcs.

Healing
A healing effect restores a creature's body in some way,
typically by restoring Hit Points, but sometimes by removing
other debilitating effects.

Heritage
Ancestry feats that have the heritage trait are feats that
your character can select only at 1st level. Unlike with other
feats, you cannot retrain your character to learn a heritage
feat or to change her existing heritage feat at any point. Your
character can never have more than one heritage feat.

Human
A creature with this trait is one of the humans, a diverse
array of people known for their adaptability. An ability with
this trait can be used or chosen only by humans.

Humanoid
Humanoid creatures reason and act much like humans.
They typically stand upright and have two arms and two legs.

Illusion
Effects and magic items with this trait are associated
with the illusion school of magic, typically involving false or
magically altered sensory stimuli.

Infused
An alchemical item with the infused trait doesn't cost
its crafter any Resonance Points to activate (whether using
Operate Activation or any other relevant action), though
anyone else must spend Resonance Points to activate it
normally. An infused item is potent for only 24 hours, after
which it becomes inert. Alchemists create infused items.

Ingested (page 360)
This poison activates when consumed.

Inhaled (page 360)
This poison activates when inhaled.

Injury (page 360)
This poison activates on a damaging Strike.

Invested
An item that a character needs to prepare by investing
it with a Resonance Point (see page 377) has the invested trait.
Most invested items are worn, like armor, clothing, or jewelry.
None of the magical effects of the item apply if the character
hasn't invested it, though the character still gains any normal
benefits from the physical item (like a hat keeping rain off a
character's head).

Lawful
Lawful effects often manipulate energy from law-aligned
Outer Planes and are anathema to chaotic divine servants or
divine servants of chaotic deities. A creature with this trait is
lawful in alignment. An ability with this trait can be selected or
used by lawful creatures only.

Leaf Order
This gives druids of the leaf order an extra benefit.

Light (page 197)
Light effects generate light. Darkness and light
effects can negate one another.

Lingual
A lingual effect depends on language comprehension. A
lingual effect that targets a creature works only if the target
understands the language you are using. Effects with the
lingual trait are also likely to have the auditory trait.

Litany
Litanies are special champion powers, typically used by
paladins and requiring a single action. Usually a creature you
target with a litany becomes bolstered against all of your litanies.

Magical
Magical items are imbued with magical energies. Each
one radiates a magic aura infused with its dominant school
of magic (abjuration, conjuration, divination, enchantment,
evocation, illusion, necromancy, or transmutation). A character
can craft these items only if she has the Magical Crafting feat
(see page 168).</p><p>Some items are closely tied to a particular tradition of magic.
In these cases, the item has the arcane, divine, occult, or primal
trait instead of the magical trait. Any of these traits indicates
that the item is magical.

Manipulate
You must physically manipulate an item or make
gestures to use this type of action. Creatures without a suitable
appendage cannot perform actions with this trait. Manipulate
actions often trigger reactions.

Mechanical
A hazard with this trait&mdash;typically a trap&mdash;is a
constructed physical object.

Mental
Mental spells affect the target's mind. They have no effect
on mindless creatures or objects. Mental can also appear as a
damage type for spells that deal damage through psychic trauma.

Metamagic
Metamagic feats trigger when you begin casting a spell
and typically require you to spend an additional spellcasting
action to alter the spell's effects. You can't use metamagic feats
if adding an action to the spell would make the spell require
more actions than you have remaining on your turn. Because
most metamagic feats have the same trigger, you usually can't
use more than one metamagic feat per spell.

Mindless
A mindless creature has either programmed or
rudimentary mental attributes. Most, if not all, of their mental
ability scores are 1. They are immune to all mental effects.

Minion
A creature with this trait can use only 2 actions per turn
and can't use reactions. A minion acts on your turn in combat
when you spend an action to issue it verbal commands (this
action has the concentrate trait). If given no commands, minions
use no actions except to defend themselves or to escape obvious
harm. If left unattended for at least 1 minute, mindless minions
don't act, whereas intelligent ones act as they please.

Misfortune (page 293)
The misfortune trait appears on spells that
cause you ill fortune. They usually change the results of your
dice rolls. Fortune and misfortune can cancel each other out.

Monk
This indicates abilities from the monk class. If this trait
appears on a weapon, monks can use that weapon with their
abilities that normally require unarmed attacks.

Morph
Spells that slightly alter a creature's form have the morph
trait. You can be affected by multiple morph spells at once,
but if you morph the same body part more than once, the first
morph effect is immediately dismissed.</p><p>Your morph effects can also be dismissed if you are
polymorphed and the polymorph effect invalidates or overrides
your morph effect. For instance, a morph that sharpened your
fingers into claws would be dismissed if you polymorphed into a
form that lacked hands, and a morph that gave you wings would
be dismissed if you polymorphed into a form that had wings of
its own (however, if your new form lacked wings, you'd keep the
wings from your morph). The GM determines which morph and
polymorph effects can be used together and which can't.

Move
This action involves moving from one space to another
in some way. Attacks of Opportunity and other reactions are
triggered when a creature within a certain distance of the
reactor moves from one space to another (see page 311).

Multiclass (page 279)
Archetypes with the multiclass trait
represent diversifying your training into another class's
specialties. You can't select a multiclass archetype's dedication
feat if you are a member of the class of the same name.

Mutagen (pages 359&ndash;360)
The alchemical elixirs called mutagens
change the imbiber's mental and physical characteristics.

Necromancy
These effects and magic items are associated with the
necromancy school of magic, typically involving life and death.

Negative
Effects with this trait heal undead creatures with
negative energy, deal negative damage to living creatures, or
manipulate negative energy.

Noisy
This armor's check penalty to Stealth checks increases by 1.

Nonlethal
All attacks made with this weapon are nonlethal (see
page 294) and are used to knock creatures unconscious instead
of killing them.

Oath
Feats with the oath trait always add an additional tenet to the
paladin's code, and a paladin can usually have only one such feat.

Occult
This spell or magic item comes from an occult spellcaster
or from the occult magical tradition.

Oil (page 378)
An oil is a liquid magic item applied to the surface
of an item or creature.

Open
You can use an action with the open trait only if you haven't
used an action with the attack or open trait yet this turn.

Orc
A creature with this trait is one of the orcs, green-skinned
people with a reputation as warmongers. Orcs tend to have
darkvision. An ability with this trait can be used or chosen only
by orcs (including half-orcs). A weapon with this trait is created
and used by orcs and half-orcs.

Paladin
This indicates abilities from the paladin class.

Parry
This weapon can be used defensively to block attacks.
While wielding this weapon, you can spend an action to
position your weapon defensively, gaining a +1 circumstance
bonus to AC until the start of your next turn.

Plant
Vegetable creatures have the plant trait. They are
distinct from normal plants. Magical effects with this trait
manipulate or conjure plants or plant matter in some way.
Those that manipulate plants have no effect in a dead area
with no plants.

Poison
These effects deliver a poison or deal poison damage. An
item with the poison trait is poisonous and might cause an
affliction (see pages 324&ndash;325).

Polymorph
These effects transform the target into a new form.
Neither a creature nor an object can be under the effect of more
than one polymorph effect at a time. If a creature or object
comes under the effect of a second polymorph effect, the
second polymorph effect dispels the first one. Unless otherwise
stated, spells with the polymorph trait don't allow the target to
take on the appearance of a specific creature, but rather just a
generic creature of a general type or ancestry.

Positive
Effects with this trait heal living creatures with positive
energy, deal positive energy damage to undead, or manipulate
positive energy.

Possession
Effects with this trait allow a creature to project its
mind and spirit into a target. A creature immune to mental
effects can't use a possession effect. While possessing a target,
a possessor's true body is unconscious (and can't wake up
normally), unless the possession effect allows the creature to
physically enter the target. Whenever the target takes damage,
the possessor takes half that much damage as mental damage.</p><p>A possessor loses the benefits of any of its active spells
or abilities, though it gains the benefits of the target's active
spells and abilities. A possessor can use any of the target's
abilities that are purely physical, and it can't use any of its
own abilities except spells and purely mental abilities. The GM
decides whether an ability is purely physical or purely mental.
A possessor uses the target's attack modifier, AC, Fortitude
save, Reflex save, Perception, and physical skills, and its own
Will save, mental skills, and spell DC; this includes any invested
items where applicable (the possessor's invested items apply
when using its own values, and the target's invested items
apply when using the target's values). A possessor gains no
benefit from casting spells that normally affect only the caster
since it isn't in its own body.</p><p>The possessor must use its own actions and reactions to
make the possessed creature act.</p><p>If a possessor reaches 0 Hit Points through any
combination of damage to its true body and mental
damage from the possession, it becomes dying as normal
and the possession ends immediately. If the target reaches
0 Hit Points first, the possessor can choose to either go
unconscious with the body and continue the possession
or end the effect as a free action and return to its body. If
the target dies, the possession ends immediately and the
possessor is stunned for 1 minute.

Potent (page 345)
When a character Invests an Item with this
trait, the item improves one of that character's ability scores,
either increasing it to 18 or increasing it by 2, whichever grants
the higher ability score. A potent item grants this benefit
only the first time it's invested within a 24-hour period, and a
character can benefit from only one potent item at a time.

Potion (page 378)
A creature activates and consumes these
magical liquids by drinking them as an Operate Activation.

Prediction
Effects with this trait determine what is likely to
happen in the near future. Most predictions are divinations.

Press
An action with the press trait can be used only if you are
currently affected by a multiple attack penalty. Some actions
with the press trait also grant an effect on a failure. Effects
on a failure can be gained only if the action took a &ndash;4 multiple
attack penalty or worse. The effects that are added on a failure
don't apply on a critical failure. If your press action succeeds or
critically succeeds but deals no damage and causes no other
effects (typically due to resistance), you can apply the failure
effect instead.

Prestige (page 279)
Archetypes with the prestige trait represent the
pinnacle of power and ability in a narrow field of expertise. You
can never have more than one prestige archetype dedication feat.

Primal
This spell or magic item comes from the untamed forces
of nature, from a primal spellcaster, or from the primal
magical tradition.

Propulsive
You add half your Strength modifier to damage rolls
with a propulsive ranged weapon so long as your Strength
modifier is not negative. If you have a negative Strength
modifier, you add your full Strength modifier instead.

Rage
You must be raging to use abilities with the rage trait, and they
end automatically when you stop raging. Barbarians can Rage.

Ranger
This indicates abilities from the ranger class.

Reach
This weapon is long and can be used to attack creatures
up to 10 feet away instead of only adjacent creatures. For
creatures that already have reach with the limb or limbs that
wield the weapon, the weapon increases their reach by 5 feet.

Revelation
Effects with this trait see things as they truly are.
Most revelations are divinations.

Rogue
This indicates abilities from the rogue class.

Scroll (page 378)
A spell can be written on a piece of parchment
called a scroll to be cast later.

Scrying
A scrying effect lets you see, hear, or otherwise get
sensory information from a distance from a sensor or apparatus,
rather than your own eyes and ears.

Secret (page 293)
This trait indicates that the GM rolls the check
for this ability in secret.

Shadow
Magic with this trait manipulates or depends on shadows
or the energy of the Shadow Plane.

Shove
You can use this weapon to Shove with the Athletics skill
even if you don't have a free hand. This uses the weapon's reach
(if different from your own) and adds the weapon's item bonus
to attack rolls (if any) as a bonus to the Athletics check. If you
critically fail a check to Shove using the weapon, you can drop
the weapon to treat the outcome as a normal failure.

Skill
A feat with the skill trait improves your skills and their uses
or gives you new uses for a skill.

Snare (page 357)
Traps typically made by rangers, snares follow
special rules that allow them to be constructed quickly and
used on the battlefield.

Sonic
An effect with the sonic trait functions only if it makes sound,
meaning it has no effect in an area of silence or in a vacuum. This
is different than an auditory spell, which is effective only if the
target can hear it. A sonic effect might deal sonic damage.

Sorcerer
This indicates abilities from the sorcerer class.

Special (page 354)
A material with the special trait is harder to
find or work than normal. They can be substituted for base
materials when you are constructing items.

Spellcasting
This action is used as part of the Cast a Spell activity.

Splash (page 359)
When a character uses a thrown weapon with
the splash trait (such as an alchemical bomb or holy water),
she doesn't add her Strength modifier to the damage roll. A
splash weapon can deal splash damage in addition to its normal
damage. If an attack with a splash weapon fails, succeeds, or
critically succeeds, all creatures within 5 feet of the target take
the indicated amount of splash damage. On a failure (but not a
critical failure), the target of the attack also takes the splash
damage. Splash damage is not multiplied on a critical hit.

Staff (page 379)
This magic item holds spells of a particular
theme. A spellcaster who has invested a staff can expend their
own spells or charges held in the staff to cast the spells.

Stance
A stance is a general combat strategy that you enter by
using an action with the stance trait. You can enter a stance
only in encounter mode. A stance lasts until you get knocked
out, until its requirements (if any) are violated, or until you
enter a new stance, whichever comes first.

Storm Order
This gives druids of the storm order an extra benefit.

Summoned (page 195)
A creature called by way of a conjuration
spell or effect gains the summoned trait. A summoned creature
can't summon other creatures, create things of value, or cast
spells that require an expensive material component or special
focus. It can use only 2 actions on its turn, and it can't use
reactions. It takes its actions when you finish Casting the Spell
and when you spend an action to Concentrate on a Spell.
Summoned creatures can be banished by various spells
and effects and are automatically banished if reduced to 0 Hit
Points or if the spell that calls them is dismissed.

Sweep
This weapon makes wide-sweeping or spinning attacks,
making it easier to attack multiple enemies. When you attack
with this weapon, you gain a +1 circumstance bonus to your
attack roll if you already attempted an attack this turn against
a creature other than the target of this attack.

Teleportation
Teleportation effects allow you to instantaneously
move from one point in space to another. Teleportation does not
usually trigger reactions based on movement.

Thrown
You can throw this weapon as a ranged attack. A thrown
weapon adds your Strength modifier to damage just like a melee
weapon does. When this trait appears on a melee weapon, it
also includes the range in feet. Ranged weapons with this trait
can be thrown at a range equal to the weapon's specified range.

Totem
Abilities with the totem trait require a specific totem, and you
lose access to them temporarily when you perform actions that
are anathema to your totem. Barbarian characters choose totems.

Transmutation
Effects and magic items with this trait are associated
with the transmutation school of magic, typically involving
changing the qualities or form of one thing into another.

Trap
A hazard or item with this trait is purposefully placed or
constructed to hinder interlopers.

Trinket (pages 379)
The consumable magic items called trinkets
must be affixed to items before they can be activated. Most
require the user to have a special feat or skill to Activate them.

Trip
You can use this weapon to Trip with the Athletics skill even
if you don't have a free hand. This uses the weapon's reach (if
different from your own) and adds the weapon's item bonus
to attack rolls (if any) as a bonus to the Athletics check. If you
critically fail a check to Trip using the weapon, you can drop
the weapon to treat the outcome as a normal failure.

Twin
These weapons are used as a pair, each complementing
the other. When you are wielding two of the same weapon
with this trait and attack with one of them, if you already
attacked with the other weapon this turn, you add a
circumstance bonus to the damage roll equal to the weapon's
number of damage dice. For instance, if you attacked with a
twin weapon in your right hand with your first action, you
would gain this bonus to damage when you attack with a
matching twin weapon in your left hand for your second
action. On your third action, an attack with either weapon
would receive this bonus, since you'd already attacked with
both. The weapons must be of the same type to benefit from
this trait, but they don't need to be the same quality or have
the same runes.

Two-Hand
This weapon can be wielded with two hands. This
increases its weapon damage die to the indicated value. If this
trait applies to a magic weapon, the extra weapon damage
dice from it being a magic weapon also increases to the
indicated damage die while it's being wielded with two hands.

Unarmed
An unarmed attack is made using your body rather than
a manufactured weapon. An unarmed attack isn't a weapon,
though it's categorized with weapons for weapon tables and
weapon groups, and it might have weapon traits. Because it's
a part of your body, an unarmed attack can't be Disarmed. It
also doesn't take up a hand, though a fist or other grasping
appendage follows the same rules as a free-hand weapon.

Undead
Once living, these creatures were infused after death
with negative energy and soul-corrupting evil magic. When
reduced to 0 Hit Points, an undead creature is destroyed.
Undead creatures are damaged by positive energy, are healed
by negative energy, and don't benefit from healing effects.

Versatile
A versatile weapon can be used to deal a different type
of damage than the type listed in the damage entry. This trait
indicates the alternate damage type. For instance, a piercing
weapon that has versatile S can be used to deal piercing or
slashing damage. You choose the damage type each time you
make an attack.

Virulent (page 325)
This affliction is harder to remove.

Visual
A visual spell can affect only creatures that can see it.

Volley
This ranged weapon is less effective at close distances.
Your attacks take a &ndash;2 penalty against targets that are at a
distance within the number of feet listed.

Wand (page 380)
A wand is a stick or similar object containing a
spell that can be cast a certain number of times.

Water
Effects with the water trait either manipulate or conjure
water. Those that manipulate water have no effect in an area
without water. Creatures with this trait consist primarily of
water or have a magical connection to the element.

Wild Order
This gives druids of the wild order an extra benefit.

Wizard
This indicates abilities from the wizard class.'''

process(traits)