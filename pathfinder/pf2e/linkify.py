import re

links = {
    # 'Pathfinder Playtest' : '/pathfinder/pf2e/index.html'
}

duplicates = set()

def link(k, v, ignore=False):
    if k in duplicates: return
    if k in links:
        if not ignore:
            duplicates.add(k)
            del links[k]
    else:
        links[k] = v

# general
'''  # don't linkify non-direct links (no valid HASH value)
general = ['Actions', 'Conditions', 'Traits', 'Tactics', 'Glossary']
for k in general:
    website = '/pathfinder/pf2e/' + k.lower() + '.html'
    link(k, website)
'''

# skills
skills = ['Acrobatics', 'Arcana', 'Athletics', 'Crafting', 'Deception', 'Diplomacy', 'Intimidation', 'Lore', 'Medicine', 'Nature', 'Occultism', 'Performance', 'Religion', 'Society', 'Stealth', 'Survival', 'Thievery']
for k in skills:
    website = '/pathfinder/pf2e/skills/' + k.lower() + '.html'
    link(k, website)

# skill abilities
sAbilities = {'Acrobatics': ['Balance', 'Escape', 'Grab Edge', 'Maintain Balance', 'Squeeze', 'Tumble Through', 'Maneuver in Flight'], 'Arcana': ['Recall Knowledge', 'Borrow an Arcane Spell', 'Identify Magic', 'Learn an Arcane Spell', 'Read Magic'], 'Athletics': ['Break Grapple', 'Break Open', 'Climb', 'Grapple', 'High Jump', 'Long Jump', 'Shove', 'Swim', 'Trip', 'Disarm'], 'Crafting': ['Repair', 'Craft', 'Identify Alchemy'], 'Deception': ['Create a Diversion', 'Impersonate', 'Lie', 'Feint'], 'Diplomacy': ['Gather Information', 'Make an Impression', 'Request'], 'Intimidation': ['Demoralize', 'Coerce'], 'Lore': ['Recall Knowledge', 'Practice a Trade'], 'Medicine': ['Administer First Aid', 'Treat Disease', 'Treat Poison'], 'Nature': ['Command an Animal', 'Handle an Animal', 'Recall Knowledge', 'Identify Magic', 'Learn a Primal Spell'], 'Occultism': ['Recall Knowledge', 'Identify Magic', 'Learn an Occult Spell', 'Read Esoterica'], 'Performance': ['Perform', 'Stage a Performance'], 'Religion': ['Recall Knowledge', 'Identify Magic', 'Learn a Divine Spell', 'Read Scripture'], 'Society': ['Recall Knowledge', 'Subsist on the Streets', 'Create Forgery', 'Decipher Writing'], 'Stealth': ['Conceal an Object', 'Hide', 'Sneak'], 'Survival': ['Sense Direction', 'Survive in the Wild', 'Cover Tracks', 'Track'], 'Thievery': ['Palm an Object', 'Steal an Object', 'Disable a Device', 'Pick a Lock']}
for k, l in sAbilities.items():
    for v in l:
        website = '/pathfinder/pf2e/skills/' + k.lower() + '.html'
        link(v, website)

# actions
actions = ['Activate an Item', 'Aid', 'Arrest a Fall', 'Assist', 'Attack of Opportunity', 'Breathe Deep', 'Burrow', 'Command Activation', 'Concentrate on a Spell', 'Crawl', 'Delay', 'Drop', 'Drop Prone', 'Fly', 'Focus Activation', 'Interact', 'Invest an Item', 'Leap', 'Material Casting', 'Mount', 'Operate Activation', 'Point Out', 'Raise a Shield', 'Ready', 'Seek', 'Shield Block', 'Somatic Casting', 'Stand', 'Step', 'Stride', 'Strike', 'Take Cover', 'Verbal Casting']
for k in actions:
    website = '/pathfinder/pf2e/actions.html'
    link(k, website)

# conditions
conditions = ['Accelerated', 'Asleep', 'Blinded', 'Broken', 'Concealed', 'Confused', 'Dazzled', 'Dead', 'Deafened', 'Drained', 'Dying', 'Encumbered', 'Enervated', 'Enfeebled', 'Entangled', 'Fascinated', 'Fatigued', 'Flat-Footed', 'Fleeing', 'Friendly', 'Frightened', 'Grabbed', 'Hampered', 'Helpful', 'Hostile', 'Immobile', 'Indifferent', 'Paralyzed', 'Persistent Damage', 'Petrified', 'Prone', 'Quick', 'Restrained', 'Sensed', 'Sick', 'Slowed', 'Sluggish', 'Stunned', 'Stupefied', 'Unconscious', 'Unfriendly', 'Unseen']
for k in conditions:
    website = '/pathfinder/pf2e/conditions.html'
    link(k, website)

# tactics
tactics = ['Carousing', 'Casting a Spell', 'Concentrating on a Spell', 'Conversing', 'Covering Tracks', 'Defending', 'Detecting Magic', 'Exploration Tactics', 'Following Tracks', 'Hustling', 'Investigating', 'Looking Out', 'Searching', 'Shopping', 'Sneaking', 'Social Tactics', 'Stealing', 'Wandering']
for k in tactics:
    website = '/pathfinder/pf2e/tactics.html'
    link(k, website)

# traits
traits = ['Abjuration', 'Acid', 'Activate', 'Additive', 'Agile', 'Air', 'Alchemical', 'Alchemist', 'Animal', 'Animal Order', 'Arcane', 'Attached', 'Attack', 'Auditory', 'Backstabber', 'Backswing', 'Barbarian', 'Bard', 'Chaotic', 'Charge', 'Cleric', 'Clumsy', 'Cold', 'Composition', 'Concentrate', 'Conjuration', 'Consecration', 'Consumable', 'Contact', 'Curse', 'Darkness', 'Deadly', 'Death', 'Dedication', 'Detection', 'Dinosaur', 'Disarm', 'Disease', 'Divination', 'Divine', 'Downtime', 'Druid', 'Dwarf', 'Earth', 'Electricity', 'Elf', 'Elixir', 'Emotion', 'Enchantment', 'Evil', 'Evocation', 'Extradimensional', 'Fatal', 'Fear', 'Fighter', 'Finesse', 'Fire', 'Force', 'Forceful', 'Fortune', 'Fragile', 'General', 'Gnome', 'Goblin', 'Good', 'Half-Elf', 'Half-Orc', 'Halfling', 'Healing', 'Heritage', 'Human', 'Humanoid', 'Illusion', 'Infused', 'Ingested', 'Inhaled', 'Injury', 'Invested', 'Lawful', 'Leaf Order', 'Light', 'Lingual', 'Litany', 'Magical', 'Manipulate', 'Mechanical', 'Mental', 'Metamagic', 'Mindless', 'Minion', 'Misfortune', 'Monk', 'Morph', 'Move', 'Multiclass', 'Mutagen', 'Necromancy', 'Negative', 'Noisy', 'Nonlethal', 'Oath', 'Occult', 'Oil', 'Open', 'Orc', 'Paladin', 'Parry', 'Plant', 'Poison', 'Polymorph', 'Positive', 'Possession', 'Potent', 'Potion', 'Prediction', 'Press', 'Prestige', 'Primal', 'Propulsive', 'Rage', 'Ranger', 'Reach', 'Revelation', 'Rogue', 'Scroll', 'Scrying', 'Secret', 'Shadow', 'Shove', 'Skill', 'Snare', 'Sonic', 'Sorcerer', 'Special', 'Spellcasting', 'Splash', 'Staff', 'Stance', 'Storm Order', 'Summoned', 'Sweep', 'Teleportation', 'Thrown', 'Totem', 'Transmutation', 'Trap', 'Trinket', 'Trip', 'Twin', 'Two-Hand', 'Unarmed', 'Undead', 'Versatile', 'Virulent', 'Visual', 'Volley', 'Wand', 'Water', 'Wild Order', 'Wizard']
for k in traits:
    website = '/pathfinder/pf2e/traits.html'
    link(k, website)

# glossary
glossary = ['Ability', 'Ability Boost', 'Ability Flaw', 'Ability Modifier', 'Ability Score', 'Action', 'Activity', 'Affliction', 'Alignment', 'Ancestry', 'Arcane', 'Archetype', 'Armor Class', 'Attack', 'Aura', 'Background', 'Bonus', 'Bulk', 'Cantrip', 'Character', 'Charisma', 'Check', 'Class', 'Class DC', 'Class Feature', 'Common', 'Condition', 'Constitution', 'Creature', 'Critical Failure', 'Critical Success', 'Deity', 'Dexterity', 'Difficulty Class', 'Divine', 'Downtime', 'Effect', 'Encounter', 'Enhancement', 'Experience Points', 'Exploration', 'Failure', 'Feat', 'Flat Check', 'Free Action', 'Free Boost', 'Game Master', 'Hazard', 'Hit Points', 'Initiative', 'Innate Spell', 'Intelligence', 'Item', 'Key Ability', 'Level', 'Modifier', 'Monster', 'Multiple Attack Penalty', 'Nonplayer Character', 'Occult', 'Penalty', 'Perception', 'Physical Damage', 'Player', 'Player Character', 'Power', 'Prerequisite', 'Primal', 'Proficiency', 'Rare', 'Reaction', 'Requirement', 'Resonance Points', 'Retrain', 'Roll', 'Round', 'Saving Throw', 'Secret Check', 'Signature Skill', 'Skill', 'Skill Feat', 'Speed', 'Spell', 'Spell DC', 'Spell Level', 'Spell Roll', 'Spellcaster', 'Strength', 'Stride', 'Strike', 'Success', 'Touch Armor Class', 'Trait', 'Trap', 'Trigger', 'Turn', 'Uncommon', 'Unique', 'Wisdom']
for k in glossary:
    website = '/pathfinder/pf2e/glossary.html'
    link(k, website, ignore=True)

webpages = ['traits.html', 'tactics.html', 'glossary.html', 'conditions.html', 'actions.html']
for k in skills:
    webpages.append('skills/' + k.lower() + '.html')
blacklist = {
    '^<a[\s>]': '</a>',
    '^<b[\s>]': '</b>',
    '^<b\s[^>]*a[\s>]': '</b>',
    '^<h1>': '</h1>',
    '^<h2>': '</h2>',
    '^<t[\s>]': '</t>',
    '^<head>': '</head>',
    '^<script\s': '</script>'
}
suffixes = ['[^A-Za-z]', 's', 'es']

def startswith(c):
    def check(w):
        return w and w[0] == c
    return check

def pluralized(suffix):
    for term in suffixes:
        if re.match(term, suffix):
            return True
    return False

def crosslink(page, override=False, verbose=False):
    count = 0
    construct = ''
    with open(page, 'r') as infile:
        content = infile.read()
        scanner = enumerate(content)
        for i, c in scanner:
            if c == '<':
                tag = c
                while c != '>':
                    _, c = next(scanner)
                    tag += c
                for entity, ending in blacklist.items():
                    if re.match(entity, tag):
                        l = len(ending)
                        while True:
                            _, e = next(scanner)
                            tag += e
                            if e == '>' and tag[-l:] == ending:
                                break
                construct += tag
                continue
            match = ''
            for k in filter(startswith(c), links):
                l = len(k)
                if content[i:i+l] == k and pluralized(content[i+l:]) and l > len(match):
                    match = k
            if match:
                tag = '<a>'
                if page not in links[match]:
                    tag = '<a url="' + links[match] + '">'
                construct += tag + match + '</a>'
                for _ in range(len(match) - 1):
                    next(scanner)
                count += 1
                if verbose:
                    print(match, '\n\t=>', tag + match + '</a>')
            else:
                construct += c

    output = page[:-5] + '.processed.html'
    if override:
        output = page
    with open(output, 'w') as outfile:
        outfile.write(construct)
    print('Made', count, 'replacements to', page, 'and wrote output to', output)

for webpage in webpages:
    crosslink(webpage, override=True)
