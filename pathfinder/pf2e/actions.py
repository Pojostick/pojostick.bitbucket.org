aBlock = '<a>{a}</a>'

block = '''<p class="stat-block-title"><t>{name}</t></p>
<p class="stat-block-1"><b>Actions</b> {actions}; <b>Traits</b> {traits}</p>
<p class="stat-block-1"><b>Pages</b> {p}</p>
<p class="stat-block-1"><b>Requirements</b> {requirements}</p>
<p class="stat-block-1"><b>Trigger</b> {trigger}</p>
<p>{desc}</p>
<p class="stat-block-1" style="margin-left: 2em;"><b>Success</b> {success}</p>
<p class="stat-block-1" style="margin-left: 2em;"><b>Critical Success</b> {critsuccess}</p>
<p class="stat-block-1" style="margin-left: 2em;"><b>Failure</b> {failure}</p>
<p class="stat-block-1" style="margin-left: 2em;"><b>Critical Failure</b> {critfailure}</p>'''

html = '''<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <title>Actions</title>
        <link rel="stylesheet" type="text/css" href="/pathfinder/css/style.css">
</head>

<body class="js">
    
    <script type="text/javascript" src="/pathfinder/pf2e/js/menu.js"></script>
    
    <div class="body-content">
        <div id="nav-path">
            <a href="/">Pojostick</a> / <a href="/pathfinder/">Pathfinder</a> / <a href="/pathfinder/pf2e/">Pathfinder Playtest</a> / <a href="/pathfinder/pf2e/actions.html">Actions</a>
        </div>
        
        <div class="body">
            
            <h1><t>Actions</t></h1>

            <p>These basic functions of the game are available to all creatures, and are how the game represents common tasks like moving around, attacking, and helping others. Every creature can use basic actions, activities, free actions, and reactions except in some extreme circumstances, and many are used with great frequency. Most notably, you'll use Interact, Step, Stride, and Strike a great deal. Many feats and other actions call upon you to use one of these basic actions or modify them to produce different effects. For example, a more complex action might let you Stride up to double your Speed instead of just up to your Speed, and a large number of activities include a Strike.</p>
            
            <p>Actions that are used less frequently but are still open to most creatures are presented in Specialty Basic Actions and Reactions on page 309. These typically have requirements that characters are less likely to meet, such as wielding a shield or having a burrow Speed.</p>
            
            <p>If you can cast spells, the activities and actions for spellcasting can be found on pages 195-196, and if you have any magic items, the activities and actions for using those appear on page 376.</p>
            
            <h2>Actions</h2>
            <p>{abilities}</p>
            
            {content}

        </div>
    </div>
    
    <script type="text/javascript" src="/pathfinder/pf2e/js/links.js"></script>
<script type="text/javascript" src="/pathfinder/js/link.js"></script>
    
</body></html>
'''

actions = {}
with open('actions.json', 'r') as f:
    actions = eval(f.read())

def process(d):
    if d['traits'] != 'none':
        ts = d['traits'].replace(', ', ',').split(',')
        d['traits'] = '<a url="/pathfinder/pf2e/traits.html">' + '</a>, <a url="/pathfinder/pf2e/traits.html">'.join(ts) + '</a>'
    return d

with open('actions.html', 'w') as f:
    aList = []
    cList = []
    for action in sorted(actions['actions'], key=lambda d: d['name']):        
        aList.append(aBlock.format(a=action['name']))

        # allow optional fields
        empty = {'requirements': '', 'trigger': '', 'success': '', 'critsuccess': '', 'failure': '', 'critfailure': ''}

        cList.append(block.format(**{**empty, **process(action)}))
    
    aBlocks = ''
    if aList:
        aBlocks = '<br>'.join(aList)

    content = ''
    if cList:
        content = ''.join(cList)

    # get rid of empty fields
    replace = ['<p class="stat-block-1"><b>Requirements</b> </p>', '<p class="stat-block-1"><b>Trigger</b> </p>', '<p class="stat-block-1" style="margin-left: 2em;"><b>Success</b> </p>', '<p class="stat-block-1" style="margin-left: 2em;"><b>Critical Success</b> </p>', '<p class="stat-block-1" style="margin-left: 2em;"><b>Failure</b> </p>', '<p class="stat-block-1" style="margin-left: 2em;"><b>Critical Failure</b> </p>']
    for s in replace:
        content = content.replace(s, '')

    output = html.format(
        abilities=aBlocks,
        content=content
    )

    f.write(output)