import re

links = {}
regex = re.compile('[^a-zA-Z]')

for c in ['conditions', 'tactics', 'traits', 'glossary']:
    with open(c + '.html', 'r') as f:
        for line in f:
            start = line.find('<t>')
            if start > -1:
                end = line.find('</t>', start)
                try:
                    name = line[start+3:end]
                    if c in links:
                        links[c] |= {name}
                    else:
                        links[c] = {name}
                except:
                    print('Bad line:', line)

for k in sorted(links.keys()):
    print(''.join([repr(k), ": ", str(sorted(links[k])), ","]))