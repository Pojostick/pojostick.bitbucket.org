<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Joseph Chen  |  CS 184</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/style.css" media="screen" />
</head>
<body id="zoomwrapper">
<br />
<h1 align="middle">Assignment 2: GeoMenagerie</h1>
    <h2 align="middle">Joseph Chen</h2>
        <p>In this project, we add several features to a mesh editing and shading program. Like in many 3D modelling programs, being able to easily alter the mesh is important, and in this project we provide several features for adjusting edges and subdividing. Afterwards, we continue onto building shaders to render our new mesh.</p>
        
        <p><i>Note: Hover over any image for a zoomed in version. Up-scaling is done using nearest-pixel to preserve pixel definition. This can be used to see very small details like edge lines.</i></p>

    <h2 align="middle">Part 1: Fun with Bezier Patches</h2>
        <p>We start by first adding support to Bezier patches. These are essentially 4 cubic Bezier curves which span a 3D surface. So how do we interpolate points on the patch? Well the basic idea is to do this:</p>
        
        <ul>
        <li>Interpolate across the 4 individual cubic Bezier curves.</li>
        <li>Now that we have four points, use these points as the control points of another cubic Bezier curve.</li>
        <li>Finally, interpolate the 3D point using this new curve.</li>
        </ul>
        
        <p>So how do we interpolate a Bezier curve? Well we use De Casteljau's algorithm to recursively solve the Bezier. The general idea works like this: We want to find the point that is 40% along the curve. That is, if we take the length of the curve, this point lies at <tt>u=0.4</tt> on a normalized parametric. It turns out that the way Bezier curves are defined, this is rather easy.</p>
        
        <p>We start with 4 control points on a cubic Bezier. You can imagine these control points as "rough estimate" of what the curve looks like. 4 control points means there are 3 line segments. For each of these, we simply take the <tt>u=0.4</tt> point. This results in 3 output points. If we continue this process recursively, we'll see that those 3 output points are considered a "closer estimate" of the bezier curve, and we can keep dividing the segments until finally we get exactly one point. This one point <i>by definition</i> lies on the Bezier curve. Thus we have interpolated the <tt>u=0.4</tt> point recursively.</p>
        
        <p>This is the result when we interpolate across a 8x8 square, creating 8x8x2 triangles:</p>
        
        <img class="zoomable" src="images/1.1.png"/>
        <figcaption align="middle">Fig. 1.1: Interpolating Bezier curves.</figcaption>
        
        <p>8x8 looks slightly chunky, so what if we increased depth to 32x32? We end up with this result:</p>
        
        <img class="zoomable" src="images/1.2.png"/>
        <figcaption align="middle">Fig. 1.2: Interpolating Bezier curves on denser grid.</figcaption>
        
        <p>However, if we do so this way, we end up with 32x32x2 triangles per patch. This increases on the order of O(n^2)! This is very bad. The main issue being that not all curves need that many polygons. Take the tiny handle on the teapot. We don't really need <b>2048</b> triangles for such a small surface. To fix this, we use adaptive tessellation.</p>
        
        <p>In this scheme, we basically measure the approximate "real world" length of the Bezier curve. This length will allow us to determine (roughly) the size of the curve, and thus how many polygons are needed to completely cover it in high-enough resolution. The issue? Well to find the length, we need to interpolate the entire curve! And that defeats the purpose of this time saving process.</p>
        
        <p>The solution? Remember that the control points act as a rough "estimate" of the curve. We can use a rough square-root curve that cuts through the middle of the four control points to determine a rough estimate of size. Another good method is to interpolate only the <tt>u=0.5</tt> point as that lies in the exact center, and can occasionally be used to guess the length of the curve. Once we have this length, we simply tessellate only as many triangles as needed to cover the patch with high enough resolution. We'll leave this as an exercise for the reader.</p>
        
    <h2 align="middle">Part 2: Average normals for half-edge meshes</h2>
        <p>We can render geometry. Yay! But how do we smooth it out to get rid of those flat faces. Well let's think about how we render. Each face surface has a normal vector which is perpendicular to the surface. This normal vector determines the direction light bounces off the object.</p>
        
        <p>To render smoothly, we simply have to render enough faces so that each face is smaller than a screen pixel! Just kidding. We would never want that. The amount of polygons would be enormous. Rather, we can "fake" this super-sampling by simply interpolating the normal vector. How? Well we can consider that at any point on a face, the normal of a "perfectly smooth" surface would be affected only by the three vertices that surround it. Specifically, the normals of those vertices. We use barycentric interpolation!</p>
        
        <p>So all we need to do is determine how to calculate the normal of a vertex. What does that even mean? Well the normal of a vertex is determined by the <i>area-weighted averages of the normals of its incident faces</i>. In other words, we take the normal of each face that touches the vertex, and we weight that face relative to the area of the face. It turns out nicely that such an area-weighted normal vector can be calculated by taking the cross product of the two rays from the vertex that span the face. Even though this cross product will be twice the normal, it doesn't matter since we will normalize the output vector anyways.</p>
        
        <p>Here is using "flat" shading:</p>
        
        <img class="zoomable" src="images/2.1.png"/>
        <figcaption align="middle">Fig. 2.1: Flat shading of teapot.</figcaption>
        
        <p>And now we apply our calculated vertex normals:</p>
        
        <img class="zoomable" src="images/2.2.png"/>
        <figcaption align="middle">Fig. 2.2: Smooth shading of teapot.</figcaption>

        <p>As you can see, the results are pretty obvious.</p>
        
    <h2 align="middle">Part 3: Edge Flip</h2>
        <p>Now we move onto mesh editing. The first edit we introduce is an edge flip. This will convert two triangles ABC and CBD incident on edge BC into two triangles ABD and ADC incident on edge AD. The following image from UCB cs184 illustrates the action:</p>
        
        <img class="zoomable" src="images/6_1.jpg"/>
        <figcaption align="middle">Fig. 3.1: Illustration of edge flip. <i>(Ren Ng), editted</i></figcaption>

        <p>Here we have a cube. We will demonstrate the edge flip:</p>
        
        <img class="zoomable" src="images/3.1.png"/>
        <figcaption align="middle">Fig. 3.2: The selected edge to flip.</figcaption>

        <img class="zoomable" src="images/3.2.png"/>
        <figcaption align="middle">Fig. 3.3: After flipping the first edge. The selected edge was the flipped edge.</figcaption>
        
        <p>And now we flip another edge.</p>
        
        <img class="zoomable" src="images/3.3.png"/>
        <figcaption align="middle">Fig. 3.4: Another edge to flip.</figcaption>
        
        <img class="zoomable" src="images/3.4.png"/>
        <figcaption align="middle">Fig. 3.5: Flipping the selected edge, with the edge highlighted.</figcaption>
        
        <p>The best way to approach this section while introducing the minimum number of bugs is to write out (somewhat verbosely) all of the components of the triangles as listed in Fig. 3.1. The code we wrote ended up looking somewhat like this:</p>
        
        <pre><code>
        Vertex vA, vB, vC, vD = ...
        Edge eAB, eBD, eDC, eCA, eAD = ...
        Face fABC, fCBD, fABD, fADC = ...
        Halfedge AB, BA, BD, DB, DC, CD, CA, AC, BC, CB, AD, DA = ...
        </pre></code>
        
        <p>Though verbose, this ensures that every possible case is covered, and thus bugs will only be introduced in typos. As a side note, the compiler will also optimize all unused variables away, and simplify pointer reassignment so even such verbose code will run just as fast as compact code, while being much more legible and less error-prone.</p>
        
        <p>The edge flip works for all combinations and ordering of flips (that we've tested). We'll experiment more with it in the following two sections to show that it does in fact work as expected.</p>
        
    <h2 align="middle">Part 4: Edge Split</h2>
        <p>Next, we introduce the edge split. This subdivides a edge using the midpoint formula, and inserts the required edges and faces. We approach the problem in the same way we did edge flips. Here is a good illustration of the process, which is a bit more complicated than edge flips:</p>
        
        <img class="zoomable" src="images/6_2.jpg"/>
        <figcaption align="middle">Fig. 4.1: Illustration of edge split. <i>(Ren Ng), editted</i></figcaption>

        <p>Here we have a cube. We will demonstrate the edge split:</p>
        
        <img class="zoomable" src="images/4.1.png"/>
        <figcaption align="middle">Fig. 4.2: The selected edge to split. This is the input of the function.</figcaption>

        <img class="zoomable" src="images/4.2.png"/>
        <figcaption align="middle">Fig. 4.3: After splitting the first edge. The selected vertex is the newly created point, and is also the output of the function.</figcaption>
        
        <p>And now we will for fun recursively split this corner to create this interesting pattern:</p>
        
        <img class="zoomable" src="images/4.3.png"/>
        <figcaption align="middle">Fig. 4.4: Recursive edge split. There's also three edge flips. See if you can spot them!</figcaption>

    <h2 align="middle">Part 5: Upsampling via Loop Subdivision</h2>
        <p>Loop subdivision combines the previous two parts to smooth out a mesh. It is a form of surface subdivision that follows the following scheme:</p>
        
        <img class="zoomable" src="images/6_3.jpg"/>
        <figcaption align="middle">Fig. 5.1: Illustration of loop subdivision. <i>(Ren Ng), editted</i></figcaption>
        
        <ul>
        <li>Split every edge in the original mesh. The new mesh now has a vertex at the midpoint of every edge.</li>
        <li>Calculate the new positions of each vertex using this formula:</li>
        </ul>
        
        <img class="zoomable" src="images/6_9.jpg"/>
        <figcaption align="middle">Fig. 5.2: The formula for vertex positions. <i>(Ren Ng), editted</i></figcaption>

        <ul>
        <li>Flip the edges that cut through each triangle such that they form a loop like in Fig. 5.1</li>
        </ul>
        
        <p>Let's take a look at the first step. If we split every edge in the original mesh at the midpoint and move the midpoint node to its relative position, we get the following output:</p>
        
        <img class="zoomable" src="images/5.1.png"/>
        <figcaption align="middle">Fig. 5.3: It's almost like wrapping paper!</figcaption>
        
        <p>Obviously this was a bug (haha), but after finding the cause, we decided to include it, because, as soon as we fixed this bug, we hit another one. So in the following image, we take the previous image, and in addition also interpolate (correctly) the vertex positions of the original nodes.</p>
        
        <img class="zoomable" src="images/5.2.png"/>
        <figcaption align="middle">Fig. 5.4: Or maybe a wad of cotton!</figcaption>

        <p>The final fix that solved all issues was the final step, which is flipping all the edges such that they form loops. In the following 6 images, we will repeatedly apply loop subdivision to show you the effects it has on a mesh. Note the incongruencies that come up!</p>
        
        <img class="zoomable" src="images/5.3.png"/>
        <figcaption align="middle">Fig. 5.5: The cube</figcaption>

        <img class="zoomable" src="images/5.4.png"/>
        <figcaption align="middle">Fig. 5.6: First loop subdivision</figcaption>
        
        <img class="zoomable" src="images/5.5.png"/>
        <figcaption align="middle">Fig. 5.7: Second loop subdivision</figcaption>
        
        <img class="zoomable" src="images/5.6.png"/>
        <figcaption align="middle">Fig. 5.8: Third loop subdivision (notice that there are still odd corners)</figcaption>
        
        <img class="zoomable" src="images/5.7.png"/>
        <figcaption align="middle">Fig. 5.9: Fourth loop subdivision</figcaption>
        
        <img class="zoomable" src="images/5.8.png"/>
        <figcaption align="middle">Fig. 5.10:Fifth loop subdivision</figcaption>
        
        <p>The reason that there are odd pointed areas of the cube is because at those points, the incident edges are not symmetric with other corners with less or more incident edges. To fix this, we can simply split the edges beforehand such that every corner is symmetric.</p>
        
        <img class="zoomable" src="images/5.9.png"/>
        <figcaption align="middle">Fig. 5.11: The pre-processed cube (use zoom to inspect symmetric edges)</figcaption>
        
        <img class="zoomable" src="images/5.a.png"/>
        <figcaption align="middle">Fig. 5.12: Five loop subdivisions, mesh view</figcaption>

        <img class="zoomable" src="images/5.b.png"/>
        <figcaption align="middle">Fig. 5.13: Five loop subdivisions, shaded (for comparison with above)</figcaption>
        
    <h2 align="middle">Part 6: Fun with Shaders</h2>
        <p>Time to mess with fragment shaders. These shaders are capable of running super fast because of several reasons.</p>
        <p>First, whenever we render a image, we guarantee that each pixel is independent of other pixels. In other words, no pixel can affect the color of another pixel</p>
        <p>What's the significance of this? Well this allows modern hardware (especially GPUs) to parallelize the every aspect it possibly can. Rather then rendering each pixel individually, we instead render all of them in parallel because none of them depend on each other.</p>
        <p>This brings us to the second point: since the first point is true, it must also be true that all data passed to a fragment shader remain consistent. This just means that while rendering an image, the data that we pass is "uniform" and does not change. If we pass a texture in for rendering, the shader should not change it.</p>
        <p>Finally, these two points imply that the shader is entirely deterministic, depending only on various variables like the vertex to render, its normal direction, the position of the lamp, the position of the camera, etc.</p>
        
        <p>Here, we will start by adding support for Phong shading. Here is a good summary of how it works:</p>
        
        <img class="zoomable" src="http://cs184.eecs.berkeley.edu/cs184_sp16_content/lectures/05_pipeline/images/slide_030.jpg"/>
        <figcaption align="middle">Fig. 6.1: Blinn-Phong Reflection Model. <i>(Ren Ng)</i></figcaption>
        
        <p>Applying the formula in our frag shader, we get the following result for the teapot:</p>
        
        <img class="zoomable" src="images/6.1.png"/>
        <figcaption align="middle">Fig. 6.2: Phong lighting of teapot.</figcaption>
        
        <p>In particular, notice the specular reflections on the teapot handle. Also note that the color of the light is warm, while the color of ambient light is cool.</p>
        
        <p>Here, we continue onto adding a reflection shader to render a world-mapped reflection shader. In this one, all we do is calculate the direction that light bounces off of the surface of the teapot, and convert it to polar coordinates so we can map it to a spherically mapped reflection texture.</p>
        
        <img class="zoomable" src="images/6.2.png"/>
        <figcaption align="middle">Fig. 6.3: Reflection shading of teapot.</figcaption>

		<p>In part 7, we'll walk through the process of designing a custom shader!</p>

    <h2 align="middle">Part 7: Design your own mesh</h2>
		<p>We want to create a shader for Earth. We start with the <i>albedo</i> which just means the raw color:</p>

        <img class="zoomable" src="images/7.1.png"/>
        <figcaption align="middle">Fig. 7.1: Albedo of Earth.</figcaption>

		<p>We can add a light source in. Using the dot product between the normal and the light source vector, we can determine the intensity of each location, thus lighting up our basic Earth.</p>
		<p>Simulataneously, we add a bump map layer to generate bumps. To do this, we simply take the vector from the bump map, dot it with the light vector again, and multiply with the raw color. Finally, we throw in some clouds on top of our Earth.</p>

        <img class="zoomable" src="images/7.2.png"/>
        <figcaption align="middle">Fig. 7.2: Raw Earth with shading.</figcaption>

		<p>We also need to add specular properties since the Earth should reflect sunlight from its surface.</p>

        <img class="zoomable" src="images/7.3.png"/>
        <figcaption align="middle">Fig. 7.3: Earth with specular layer.</figcaption>

		<p>However, note that the reflections also reflect off of the land. We can fix this by using a <b>mask</b> layer to tell our shader where we want reflections to occur. The end result allows only reflections from the ocean, and not the ground.</p>

        <img class="zoomable" src="images/7.4.png"/>
        <figcaption align="middle">Fig. 7.4: Earth with improved specular layer.</figcaption>

		<p>At this point, the background is kinda boring. Let's add in a universe background.</p>

        <img class="zoomable" src="images/7.5.png"/>
        <figcaption align="middle">Fig. 7.5: Earth with better background.</figcaption>

		<p>We introduce another new texture: a city-night light texture. This is a mask! We simply apply the mask only on areas that the light does not hit, and give it a orange-yellow tinge of incadescent lights.</p>
		<p>Furthermore, we know that the earth has an atmosphere. To create this volumetric lighting effect, we can take the camera vector and bounce it off of the surface of our Earth. We then dot the reflection vector with our camera vector. Knowing the IOR of various materials, we can determine what angle of refraction best suits the atmosphere. Then, we just shade based on how much the vector is incident.</p>

        <img class="zoomable" src="images/7.6.png"/>
        <figcaption align="middle">Fig. 7.6: Now with more pollution!</figcaption>

		<p>Without smooth shading, we'll see that we are still in low-poly state. We should upsample with our fancy loop subdivision scheme.</p>

        <img class="zoomable" src="images/7.7.png"/>
        <figcaption align="middle">Fig. 7.7: Default Earth.</figcaption>

        <img class="zoomable" src="images/7.8.png"/>
        <figcaption align="middle">Fig. 7.8: Upsampled Earth.</figcaption>

		<p>Some may keep the Earth in this artistic representation, however we want to reenable smooth shading for better effect.</p>

        <img class="zoomable" src="images/7.9.png"/>
        <figcaption align="middle">Fig. 7.9: Smooth Earth in Space.</figcaption>

		<p>After a bit more tweaking, and placing the camera at a better angle, we have our final shot:</p>

        <img class="zoomable" src="images/7.a.png"/>
        <figcaption align="middle">Fig. 7.10: Final shot of Earth.</figcaption>

		<p>Now of course, our shader is only capable of so much. For example, our program currently isn't capable of translating objects. This causes all of the objects to clump up in the center. Furthermore, we also cannot move the camera very much.</p>
		<p>With a more-developed 3D modelling program, we can mess around with much more. Here, we go into Blender, a free 3D modelling software, and continue editting our very basic Earth. Here is the process we go through:</p>
		
		<ul>
		<li>First we want to reposition the camera to a much nicer angle.</li>
		<li>We add an additional shadow layer. The mask is the cloud layer from previous, and the surface to project onto is Earth.</li>
		<li>Now we can go onto adding a nice volumetric shader to extend the atmosphere beyond the horizon.</li>
		<li>A real sun, and a bit of sci-fi glow effects later, we can render a better image.</li>
		</ul>

        <img class="zoomable" src="http://i.imgur.com/FBE2xHB.png"/>
        <figcaption align="middle">Fig. 7.11: Sci-fi Earth rendered in Blender.</figcaption>

		<p>Of course, rendering spheres might seem too simple. We can also use many other shapes. Here, we create a magic temple inspired by Minecraft. The shattered crystal is prime example of a generated shape, created by applying a noise texture to generate randomized cuts throughout a larger polyhedra.</p>

        <img class="zoomable" src="images/7.b.jpg"/>
        <figcaption align="middle">Fig. 7.12: A Minecraft Temple. <i>(textures courtesy SphaxBDCraft)</i></figcaption>

		<p>There's much more we can mess around with to enhance both models in shaders. For models, getting computers to generate geometric shapes can result in impressive results. For shaders, another impressive method of rendering involves subsurface scattering, where instead of using the perfect normal vector, we take a jittered one so that we can follow multiple light bounces (ray tracing).</p>

		<p>Thanks for reading! I hope you enjoyed all of the images.</p>

<div id="zoomed" class="zoomed"></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="js/index.js"></script>

</body>
</html>




