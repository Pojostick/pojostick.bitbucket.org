var offsetX = 0;
var offsetY = 0;
var scaleX = 1;
var scaleY = 1;
var imgSrc = "";

document.getElementById("zoomwrapper").onmousemove = function(event){
    var element;
    $(this).children(".zoomable").each(function() {
        var pos = $(this).offset();
        if(event.pageX < pos.left + this.width && event.pageY < pos.top + this.height && event.pageX > pos.left && event.pageY > pos.top)
        {
            element = this;
        }
    })
    if(element) {
        $(".zoomed").fadeIn(200);
        if (imgSrc != element.getAttribute("src")) {
            imgSrc = element.getAttribute("src");
            var img = new Image();
            img.src = imgSrc;
            scaleX = img.width / element.width;
            scaleY = img.height / element.height;
            offsetX = document.getElementById("zoomed").offsetWidth / 2;
            offsetY = document.getElementById("zoomed").offsetHeight / 2;
        }
        document.getElementById("zoomed").style.backgroundImage = "url('" + element.getAttribute("src") + "')";
        var mouseX = event.pageX - $(element).offset().left;
        var mouseY = event.pageY - $(element).offset().top;
        var pos = (-scaleX * mouseX + offsetX) + "px " + (-scaleY * mouseY + offsetY) + "px";
        document.getElementById("zoomed").style.backgroundPosition = pos;
    } else {
        $(".zoomed").fadeOut(200);
    }
}