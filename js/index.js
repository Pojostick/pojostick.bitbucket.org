var csx = 0, cex = 0, cy = 0; // cross start, end
var lastx = 0, lasty = 0; // last mouse
var state = -1; // state -1 = off, >=0 = mousedown, 6 = success

document.onmousedown = function(event) {
    state = 0;
    csx = 0;
    cex = 0;
    cy = 0;
    lastx = event.clientX;
    lasty = event.clientY;
}

document.onmouseup = function(event) {
    if (state == 6 && lasty > cy) {
        var e = document.getElementById("fontCrafter");
        e.href = "fontCrafter/index.html";
        e.style.backgroundColor = "#FFFFFF";
        e.style.color = "#00FFFF";
        e.style.transition = "all 5s";
    }
    state = -1;
}

document.onmousemove = function(event) {
    var x = event.clientX;
    var y = event.clientY;
    var dx = x - lastx;
    var dy = y - lasty;
    
    switch (state) {
        case 0:
            state = dx < 0 ? 1 : 0;
            break;
        case 1:
            state = dy > 0 ? 2 : 1;
            break;
        case 2:
            state = dx > 0 ? 3 : 2;
            csx = x;
            cy = Math.max(cy, y);
            break;
        case 3:
            state = dy < 0 ? 4 : 3;
            cy = Math.max(cy, y);
            break;
        case 4:
            state = dx < 0 ? 5 : 4;
            cex = x;
            break;
        case 5:
            state = dy > 0 ? 6 : 5;
            break;
        case 6:
            state = dy > 0 ? 6 : -1;
    }
    if (x < csx) {
        state = -1;
    }
    lastx = x;
    lasty = y;
}