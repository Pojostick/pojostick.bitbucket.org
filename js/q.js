var links = {
    'pf2e': '/pathfinder/pf2e',
    'red' : '/pathfinder/projectRed',
    'characters': '/pathfinder/projectRed/sheets',
    'sheets': '/pathfinder/projectRed/sheets',
    'magic': '/pathfinder/projectRed/magicIndex',
    'index': '/pathfinder/projectRed/magicIndex',
    'minecraft': '/minecraft',
};

// general projectRed pages
['general', 'races', 'skills', 'morality', 'combat', 'currency'].forEach(function(e) {
    links[e] = '/pathfinder/projectRed/' + e + '.html';
});

// classes
['barbarian', 'bard', 'brawler', 'cleric', 'monk', 'oracle', 'paladin', 'wizard'].forEach(function(e) {
    links[e] = '/pathfinder/projectRed/classes/' + e;
});

// magics
['arcane', 'divine', 'supernatural'].forEach(function(e) {
    links[e] = '/pathfinder/projectRed/magic/' + e;
});

// magic spells
var magics = {
    'addmemory': 'addRemoveMemory',
    'ageusia': 'ageusia',
    'anosmia': 'anosmia',
    'bargainaccuracy': 'fateOfAccuracy',
    'bargainaction': 'fateOfAction',
    'bargainathletics': 'fateOfAthletics',
    'bargainbolster': 'fateOfBolster',
    'bargaincommunication': 'fateOfCommunication',
    'bargaincriticality': 'fateOfCriticality',
    'bargaindefense': 'fateOfDefense',
    'bargaindodge': 'fateOfDodge',
    'bargainknowledge': 'fateOfKnowledge',
    'bargainluck': 'fateOfLuck',
    'bargainpenetration': 'fateOfPenetration',
    'bargainpower': 'fateOfPower',
    'bargainrecovery': 'fateOfRecovery',
    'bargainresistance': 'fateOfResistance',
    'bargainseverity': 'fateOfSeverity',
    'bargaintalent': 'fateOfTalent',
    'blindness': 'blindness',
    'blindrage': 'blindRage',
    'causeaffliction': 'causeAffliction',
    'causevirtue': 'causeVirtue',
    'challenginghowl': 'challengingHowl',
    'clearmind': 'clearMind',
    'controlledrage': 'controlledRage',
    'deafness': 'deafness',
    'disarmingencounter': 'disarmingEncounter',
    'discourageanger': 'encourageDiscourageAnger',
    'discourageconcentration': 'encourageDiscourageConcentration',
    'discouragedisgust': 'encourageDiscourageDisgust',
    'discouragefear': 'encourageDiscourageFear',
    'discouragehappiness': 'encourageDiscourageHappiness',
    'discourageinterest': 'encourageDiscourageInterest',
    'discouragesadness': 'encourageDiscourageSadness',
    'discouragesurprise': 'encourageDiscourageSurprise',
    'encourageanger': 'encourageDiscourageAnger',
    'encourageconcentration': 'encourageDiscourageConcentration',
    'encouragedisgust': 'encourageDiscourageDisgust',
    'encouragefear': 'encourageDiscourageFear',
    'encouragehappiness': 'encourageDiscourageHappiness',
    'encourageinterest': 'encourageDiscourageInterest',
    'encouragesadness': 'encourageDiscourageSadness',
    'encouragesurprise': 'encourageDiscourageSurprise',
    'feikoppo': 'feiKoppo',
    'ghosteuphoria': 'ghostEuphoria',
    'ghostillness': 'ghostIllness',
    'ghostpain': 'ghostPain',
    'ghostsight': 'ghostSight',
    'ghostsmell': 'ghostSmell',
    'ghostsound': 'ghostSound',
    'ghosttaste': 'ghostTaste',
    'ghosttouch': 'ghostTouch',
    'hypoesthesia': 'hypoesthesia',
    'increaseddamageresistance': 'increasedDamageResistance',
    'internalfortitud': 'internalFortitude',
    'intimidatingglare': 'intimidatingGlare',
    'ipsappo': 'ipSappo',
    'itenohmbyat': 'itenOhmByat',
    'kahdes': 'kahDes',
    'kitnu': 'kitNu',
    'kliearto': 'kliearto',
    'kliekorpo': 'kliekorpo',
    'knellfei': 'kNellFei',
    'knockback': 'knockback',
    'koppounsig': 'koppoUnsig',
    'luokowu': 'luokoWu',
    'momentofeuphoria': 'momentOfEuphoria',
    'noescape': 'noEscape',
    'predictaccuracy': 'fateOfAccuracy',
    'predictaction': 'fateOfAction',
    'predictathletics': 'fateOfAthletics',
    'predictbolster': 'fateOfBolster',
    'predictcommunication': 'fateOfCommunication',
    'predictcriticality': 'fateOfCriticality',
    'predictdefense': 'fateOfDefense',
    'predictdodge': 'fateOfDodge',
    'predictknowledge': 'fateOfKnowledge',
    'predictluck': 'fateOfLuck',
    'predictpenetration': 'fateOfPenetration',
    'predictpower': 'fateOfPower',
    'predictrecovery': 'fateOfRecovery',
    'predictresistance': 'fateOfResistance',
    'predictseverity': 'fateOfSeverity',
    'predicttalent': 'fateOfTalent',
    'puppeteer': 'puppeteer',
    'ragingbravado': 'ragingBravado',
    'removememory': 'addRemoveMemory',
    'renewedvigor': 'renewedVigor',
    'retribution': 'retribution',
    'sappokukinem': 'sappoKukinem',
    'shutdown': 'shutDown',
    'sigilofarmor': 'sigilOfArmor',
    'sigilofbinding': 'sigilOfBinding',
    'sigilofdeterrence': 'sigilOfDeterrence',
    'sigilofendurance': 'sigilOfEndurance',
    'sigilofimmunity': 'sigilOfImmunity',
    'sigilofresistance': 'sigilOfResistance',
    'sigilofrevelation': 'sigilOfRevelation',
    'sigilofsacrifice': 'sigilOfSacrifice',
    'sigilofsealing': 'sigilOfSealing',
    'sigilofsmiting': 'sigilOfSmiting',
    'sigiloftruth': 'sigilOfTruth',
    'sigilofweaponry': 'sigilOfWeaponry',
    'surpriseattack': 'surpriseAttack',
    'touchofclarity': 'touchOfVision',
    'touchofcoherence': 'touchOfVision',
    'touchofcourage': 'touchOfSpine',
    'touchofempathy': 'touchOfMind',
    'touchofexhilaration': 'touchOfEnergy',
    'touchoffitness': 'touchOfBody',
    'touchofgrit': 'touchOfSpine',
    'touchofhealth': 'touchOfBody',
    'touchofinvigoration': 'touchOfEnergy',
    'touchofrousing': 'touchOfEnergy',
    'touchofsympathy': 'touchOfMind',
    'touchofwellness': 'touchOfBody',
    'trippingsweep': 'trippingSweep',
    'vitorsech': 'vitorSech',
    'volitto': 'volitto',
    'wrathfulweapon': 'wrathfulWeapon',
    'yatagsoo': 'yatagSoo',
    'zenkin': 'zenKin'
};
for (var k in magics) {
    links[k] = '/pathfinder/projectRed/magic/magicIndex/' + magics[k] + '.html';
}

// class abilities
var abilities = {
'barbarian': ['Animal Fury', 'Damage Reduction', 'Fast Movement', 'Festering Rage', 'Fight the Power', 'Hardening', 'Rage', 'Rage Power', 'Raging Athlete'],
'bard': ['Accelerando', 'Bardic Scholar', 'Jack-Of-All-Trades', 'Magnum opus', 'Perception Shifts', 'Song Critic', 'Song Movement', 'Soundwave'],
'brawler': ['Body of Retribution', "Brawler's Maneuver", 'Calculating Feint', 'Chronic Stress', 'Defiance', 'Enforcer Reputation', 'Knockout', 'Mind of Retribution', 'Punch Out'],
'monk': ['Battle Aura', 'Discipline', 'Ki', 'Light', 'Martial Art', 'Martial Instinct', 'Purity', 'Residual Meditation', 'Technique', 'Travel Instinct'],
'oracle': ['Bargain', 'Detection', 'Drugged Prophecy', 'Fate', "Fate's Protection", 'Foresight', 'Hindsight', 'Obsessive', 'Prediction', 'Understanding', 'Warp Fate'],
'paladin': ['Ascension', 'Blessings of Compassion', 'Bond of Light', 'Brand of the Order', 'Burns of the Void', 'Combat Style', 'Compassion', "Compassion's Touch", 'Gift of the Void', 'Sigil', 'Soul of Virtue'],
'wizard': ['Arcane Conduit', 'Arcane Identification', 'Arcane Pattern', 'Arcane Pool', 'Counter Pattern', 'Experimental Pattern', 'Field of Study', 'Meta Phrase', 'Pattern Matching Mastery']
};
for (var k in abilities) {
    for (var i in abilities[k]) {
        var name = abilities[k][i];
        var link = name.split(/[^A-Za-z\s]/).join('').split(/\s/).join('-').toLowerCase();
        name = name.split(/[^a-zA-Z]/).join('').toLowerCase();
        links[name] = '/pathfinder/projectRed/classes/' + k + '#' + link;
    }
}

// sheets
var characters = {
    'john': 'johns-character',
    'tordah': 'johns-character',
    'michael': 'monkey-business',
    'keive': 'monkey-business',
    'roandask': 'roandasks-character',
    'lucan': 'roandasks-character',
    'seajade': 'seajades-character',
    'hurdy': 'seajades-character',
    'gurdy': 'seajades-character',
    'steve': 'steves-character',
    // 'john': 'steves-character',
    'silver': 'the-silver-oracle'
}
for (var key in characters) {
    links[key] = '/pathfinder/projectRed/sheets/character.html?character=' + characters[key];
}

var query = null, tail = '?', tmp = [];

var items = location.search.substr(1).split('&');
for (var index = 0; index < items.length; index++) {
    tmp = items[index].split('=');
    if (tmp[0] === 'q')
        query = decodeURIComponent(tmp[1]);
    else
        tail += items[index] + '&';
}

var hash = window.location.hash || '';

if (query === null) {
    window.location.replace('/projects.html' + tail + hash);
} else if (query in links || (query = query.toLowerCase()) in links) {
    var linkhash = links[query].indexOf('#');
    if (linkhash !== -1) {
        hash = links[query].substring(linkhash);
        links[query] = links[query].substring(0, linkhash);
    }
    if (links[query].indexOf('?') !== -1) {
        window.location.replace(links[query] + '&' + tail.substring(1) + hash);
    } else {
        window.location.replace(links[query] + tail + hash);
    }
} else {
    window.location.replace('/error.html');
}
