<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Joseph Chen  |  CS 184</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/style.css" media="screen" />
</head>
<body id="zoomwrapper">
<br />
<h1 align="middle">Assignment 1: Rasterizester</h1>
    <h2 align="middle">Joseph Chen</h2>
        <p>In this assignment, we build a program which can read in SVG files, a type of vector graphics, and render them on screen. Vector graphics are unique in that the image is defined by geometric components which can be defined by functions. As a result, it is possible to transform shapes within an SVG without distorting or even reducing quality of the image. Here is the step-by-step process of how this was done.</p>
        
        <p><i>Note: Hover over any image to display a zoomed-in version. Zooming is done using nearest-pixel sampling, and will preserve pixels when up-scaling.</i></p>

    <h2 align="middle">Part 1: Rasterizing Lines</h2>
        <p>The first rasterizing algorithm we used was an incremental one. All lines were rendered left-to-right, incrementally traversing the entire line stepwise. This is the end result:</p>
        
        <img class="zoomable" src="images/1.1.png"/>
        <figcaption align="middle">Fig. 1.1: First render of rasterizing lines.</figcaption>
        
        <p>If you look closely, you will notice there are a few flaws in this method. First of all, the right endpoints of all of the lines seem to be slightly off. This is most apparent when analyzing the lines on the left half of the screen, as they approach a small circle. And secondly, not all of the lines are bounded correctly.</p>
        <p>Now the reason for this is quite simple, yet we won't see the solution until Part 3 (anti-aliasing). Notice that coordinates are passed into the rasterize_line function in the following method:</p>
        
        <p align="middle"><pre align="middle">void DrawRend::rasterize_line( float x0, float y0, float x1, float y1, Color color) {...}</pre></p>
        
        <p>Floats! Although floats are capable of storing a much wider range of number, it is by no means more accurate, as it has the same precision as any other same-bit-width number. When we incrementally traverse the line, adding small values like 0.1 over the 100 or so pixels, we don't truly end up with adding a total of 10, but rather only a very close estimate of 10. This effect is magnified the larger numbers are, as the resolution of numbers decreases.</p>
        <p>The solution? Use integers!</p>
        <p>We were having similar problems when dealing with triangles, specifically the Point-in-Triangle test would not remain consistent. Because integers are capable of pixel-perfect math operations like addition and multiplication, we can use integers to instead represent various points on lines. Rearranging the code so we incrementally render with integers, we get the following rendering:</p>
        
        <img class="zoomable" src="images/1.2.png"/>
        <figcaption align="middle">Fig. 1.2: Rasterizing lines using integer calculations.</figcaption>
        
        <p>As you can see, the circles are more consistent, and the lines are bounded correctly.</p>

    <h2 align="middle">Part 2: Rasterizing single-color triangles</h2>
        <p>The approach we take for rasterizing triangles involves calculating a bounding box, then evaluating the Point-in-Triangle test at each point. What we get is a very choppy rendering of triangles:</p>
        
        <img class="zoomable" src="images/2.1.png"/>
        <figcaption align="middle">Fig. 2.1: Triangle rasterization.</figcaption>
        
        <p>So what's the issue here? Well the first issue (as stated in part 1) is that we should use integers instead of floats. We can create <tt>int64_t</tt> objects, which have 4 times as many bits as floats, yet are still capable of fast-math characteristic of ints.</p>
        
        <p>After fixing this, let's take a closer look at how the Point-in-Triangle calculations are being done. We first evaluate a bounding box representative of the triangle. Then, we scan through the box, evaluating the three half-triangle formulas. Here is the rendering process:</p>
        
        <img class="zoomable" src="images/2.2.png"/>
        <figcaption align="middle">Fig 2.2: Evaluating half-triangles in bounding boxes.</figcaption>

        <p>The colors are representative of which side of the half-triangle a point is in. Only if a point is contained completely within all 3 half-triangle tests may we proceed to render it.</p>
        <p>This method works quite well and is very easy to implement, but we have several problems. Firstly, this process is slow. Notice he very large skinny triangle that cuts through the center of the dragon. This triangle has a very large bounding box, yet much of the bounding box is empty. The second issue is more apparent in the following image:</p>
        
        <img class="zoomable" src="images/2.3.png"/>
        <figcaption align="middle">Fig 2.3: The dragon.</figcaption>
        
        <p>What's the issue? Well if we zoom in on the border of the outline against triangles, the problem becomes very apparent: we are missing portions of triangles (especially edge tests). To deal with this, we need to first learn what antialiasing does to a triangle (part 3).</p>
        
        <p>But let's first get back to the initial problem. The code is slow. So slow in fact it takes nearly 10 seconds just to render Fig 2.2. How do we fix this?</p>
        <p>Well there's quite a nice pattern we can see when evaluating the point-in-triangle formula:</p>

        <p align="middle"><pre align="middle">-(x - Xi)dYi + (y - Yi)dXi</pre></p>
        
        <p>We notice that we can factor out many terms of this test into the form:</p>
        <p align="middle"><pre align="middle">dYi*x + dXi*y + C</pre></p>
        
        <p>Notice that <tt>C</tt> can be precalculated for each triangle, saving many calculations. With something like that central triangle in the body of the dragon taking up 600x400 ~ 240000 calculations, we gain a large speed-up already. It no longer takes nearly as long to render Fig 2.2. But can we do better? Well we notice that our current Point-in-Triangle test requires 3 tests, each using 2 multiplications and 3 additions for a total of 6 multiplications and 9 additions. Multiplication is much slower than addition so we should try to optimize this.</p>
        <p>What if we told you multiplication is simply just addition. It works out quite nicely that everytime we loop, we increment <tt>x</tt> and <tt>y</tt> by only 1 to cover every pixel. This is just screaming to be optimized. Instead, we can rewrite our code so that we remove all multiplications in the loop. How?</p>
        <p>Note that every loop we simply increment <tt>x</tt> or <tt>y</tt>. Instead of doing a multiplication, we can instead just add the constant pre-calculated factor <tt>dXi, dYi</tt> to <tt>y, x</tt> respectively. This cuts down the math per loop to only 3 additions, speeding up rendering times greatly!</p>
        <p>There are a few more optimizations we can implement. We should evaluate block-by-block. Using this method, we can throw out entire blocks of points not in the triangle, and vice versa for those within. This speeds up the large triangle so much that it cuts it down from 240000 evaluations to less than 10000! Finally, the Point-in-Triangle setup we have is completely linear and deterministic. This means that it is entirely possible to SIMD-ize our rasterization algorithm by computing four rows of blocks in parallel!</p>
        <p>But enough of optimization, let's move onto how we can get rid of those jaggies.</p>

    <h2 align="middle">Part 3: Antialiasing triangles</h2>
        <p>The method of antialiasing we used was supersampling. Supersampling involves rendering the image first in a much larger environment, and then scaling downwards to the correct size. This allows multiple samples of sub-pixel resolution within a single pixel. To do this, we needed to add a new structure to the program: a scaled buffer.</p>
        <p>The changes made were quite simple. We switched the triangle rasterization functions to draw on the supersampled scaled buffer. Then, we insert a new function right before drawing pixels to the screen: a downscale function. This function uses the scaled buffer and averages all samples within each screen-sized pixel and then copies them over to the frame buffer, which in turn is rendered onto the screen.</p>
        <p>To fix the triangle rasterizing function, we simply scale the coordinates upwards before doing any calculations, and then leave the rest alone. After many, many points in debugging various calculations, the result of adding antialiasing is stunning:</p>
        
        <img class="zoomable" src="images/2.1.png"/>
        <figcaption align="middle">Fig. 3.1: 1x1 super-sampling.</figcaption>

        <img class="zoomable" src="images/3.2.png"/>
        <figcaption align="middle">Fig. 3.2: 2x2 super-sampled.</figcaption>

        <img class="zoomable" src="images/3.3.png"/>
        <figcaption align="middle">Fig. 3.3: 3x3 super-sampled.</figcaption>

        <img class="zoomable" src="images/3.4.png"/>
        <figcaption align="middle">Fig. 3.4: 4x4 super-sampled.</figcaption>

        <p>The effect of antialiasing is quite obvious. In the 1x1 image, we see sharp edges on all sides of the triangles. Moving on to 2x2, it almost seems like we have double the amount of sharp edges, and yet half-as-sharp edges, making the image seemed almost like its been blurred. This is not the case. Because we render at 2x2 resolution, the 2x2 image truly still remains as sharp-edged as the 1x1 image. When we downsample, we mix end up blurring the image so that it appears to have twice the number of sharp edges, thus making it look much smoother.</p>
        <p>The effect only becomes more apparent when comparing the 3x3 image to the 1x1. Now we downsample a total of 9 pixels into 1 pixel, and sharp edges start to look smooth.</p>
        <p>Finally, the 4x4 image appears much nicer and more appealing then the others due to a total of 16 buffer samples per screen pixel.</p>
        
        <p>We stated in part 2 that we can use this to solve some of the issues we had previously. Particularly, let's look at the dragon again:</p>
        
        <img class="zoomable" src="images/2.3.png"/>
        <figcaption align="middle">Fig 2.3: The dragon.</figcaption>

        <p>The first issue we should deal with is edge cases. Here's the issue: what happens if we have two triangles exactly meeting on an edge? Who should render that edge? If the triangles are transparent, how do we conserve transparency (as double-rendering causes multiplicative increase not linear averaging). Let's consult the <a href="https://msdn.microsoft.com/en-us/library/windows/desktop/cc627092%28v=vs.85%29.aspx">Official Guidelines</a> on these things.</p>
        
        <p>According to them, we should render horizontal top edges, and left edges. This illustration puts it nicely:</p>
        
        <img class="zoomable" src="https://i-msdn.sec.s-msft.com/dynimg/IC520311.png"/>
        <figcaption align="middle">Fig 3.5: MSDN Triangle Rasterization Rules</figcaption>
        
        <p>This is quite a simple change to make. In face, since we precomputed the Point-in-Triangle tests, then we already know which edges need to be rendered. So, we simply disable all edge rendering for triangles. Then, we test the three cases and render the edges separately if they qualify under the rules.</p>
        <p>Since we are antialiasing, we need to implement our antialiasing algorithm for both lines and points. This turns out to be more than easy as all we do is scale points up so they fit on our scaled buffer</p>
        <p>Finally, we get this wonderful dragon at 4x4 antialiased and edge-cased triangles (rendered in less than a second I might add):</p>
        
        <img class="zoomable" src="images/3.5.png"/>
        <figcaption align="middle">Fig 3.6: The new and improved dragon</figcaption>

        <p>Quickly going back to part 1, we can test our antialiased version and see that it looks much nicer and smoother:</p>
        
        <img class="zoomable" src="images/3.6.png"/>
        <figcaption align="middle">Fig 3.7: Antialiased lines</figcaption>

        <p>Similarly, for points, antialiasing (although causing a bit of blur) makes the image look smoother</p>
        
        <img class="zoomable" src="images/3.7.png"/>
        <figcaption align="middle">Fig 3.8: Antialiased points</figcaption>

        <p>Now that we have our most basic version running, we can go ahead and fix the gui so we can move around our canvas and zoom in.</p>

    <h2 align="middle">Part 4: Transforms</h2>
        <p>We add transform matrices so we can move around and transform groups of geometric primitives at once. Here's a basic example of the rotation matrix:</p>
        
        <img class="zoomable" src="images/4.1.png"/>
        <figcaption align="middle">Fig 4.1: 5 - star</figcaption>
        
        <p>This was created by initializing 5 polygon groups (the red one), and then using the rotation transformation:</p>
        
        <p align="middle"><pre align="middle">rotate(72i 50% 50%) for i in [0:5)</pre></p>

        <p>This causes all 5 shapes to be evenly distributed about the center.</p>
        
        <p>The next transformation we apply will shift all shapes radially outwards. See if you can guess which type of transformation we added:</p>
        
        <img class="zoomable" src="images/4.2.png"/>
        <figcaption align="middle">Fig 4.2: Distributed 5 - star</figcaption>

        <p>Translation! Of course now the transformation stack is of utmost importance. If we translated after rotating, we would end up with our entire image shifted in some direction. That's not what we want. Instead, we apply the translation before rotation, moving all shapes further from the center. Then, when we rotate, the center is preserved, while all shapes are further apart.</p>
        
        <p>We can do even more. What if we rotate <i>before</i> translating. What would that look like?</p>
        <p>Like the previous example, we get all of the shapes rotated <i>with respect to its own centroid</i>. This is because we rotate before executing the rotation from Fig 4.1.</p>
        
        <img class="zoomable" src="images/4.3.png"/>
        <figcaption align="middle">Fig 4.3: Rotated distributed 5 - star</figcaption>

        <p>Ok we haven't done anything to those small triangles. Let's see what happens if we also rotate them!</p>
        
        <img class="zoomable" src="images/4.4.png"/>
        <figcaption align="middle">Fig 4.4: Rotated distributed opened 5 - star</figcaption>

        <p>And finally, let's try a transformation we haven't done yet. Scaling! Scaling is more difficult, however, due to the fact that it scales from the center, and can cause issues. So we need to translate beforehand to prevent the scaling transformation from moving the shape. And then we translate back to the original position.</p>
        
        <img class="zoomable" src="images/4.5.png"/>
        <figcaption align="middle">Fig 4.5: Rotated distributed opened scaled 5 - star</figcaption>
        
    <h2 align="middle">Part 5: Barycentric coordinates</h2>
        <p>Barycentric coordinates are a major part of texturing as we will see later. Let's first get a visual idea of what barycentric coordinates look like:</p>
        
        <img class="zoomable" src="http://cs184.eecs.berkeley.edu/cs184_sp16_content/lectures/04_texture/images/slide_027.jpg"/>
        <figcaption align="middle">Fig 5.1: Barycentric interpolation (I hope this works), <i>(Ren Ng)</i></figcaption>

        <p>Barycentric coordinates are bound in the range [0, 1]. We can use them to determine where in a triangle we are. Specifically, we can use them to linearly interpolate a blend of the three points on a triangle, and how much each effects a certain point inside that triangle.</p>
        
        <p>Firstly, we can point out that from part 2, our implementation <i>already calculates the Barycentric Coordinates</i>. Well close to them. We just need to divide by the area of the triangle. Plugging this in, we should get the same image as above:</p>
        
        <img class="zoomable" src="images/5.1.png"/>
        <figcaption align="middle">Fig 5.2: Barycentric linear interpolation gone wrong?</figcaption>
        
        <p>So what happened. Turns out we have several issues. First, in our triangle rasterization, we automatically orient the triangles such that the points are counter-clockwise. This saves much computation and because all triangles are now the "same", we don't need a large switch block per inside/outside half-triangle. However, this messes with the vertex values (which we did not rotate).</p>
        
        <p>Second, since we do calculations with integers, our numbers were scaled up in odd ways. Noting that color values are in the domain [0, 255], we simply needed to realign the fixed-point decimals on our integers, convert them to floats, and finally scale by 255. After all of this, we get our wonderful color wheel!</p>
        
        <img class="zoomable" src="images/5.2.png"/>
        <figcaption align="middle">Fig 5.3: Barycentric linear interpolation color wheel</figcaption>

    <h2 align="middle">Part 6: Pixel sampling for texture mapping</h2>
        <p>Like part 5, what if instead of interpolating triangles from simple colors, we took those colors from a larger data structure ... perhaps an image? A texture image. Yes. This one.</p>
        
        <img class="zoomable" src="images/map.jpg"/>
        <figcaption align="middle">Fig 6.1: A map of the world</figcaption>

        <p>Sampling. Pixel sampling is a good strategy for rendering textures onto triangles. In part 5, we provided colors for the three vertices on the triangle. Now, we evolve that so the triangles contain a different kind of information - data that describes a location on a texture. These are called uv-coordinates. We can use our barycentric coordinates to interpolate across coordinates on a UV map.</p>
        
        <p>Here, we use a method called nearest sampling. (It's what we use for that little zoom box in the top left). Nearest sampling ensures that whatever point we choose, it must be exactly a pixel on the texture. What's the issue with this? Well we'll see very shortly. First, the image:</p>
        
        <img class="zoomable" src="images/6.1.png"/>
        <figcaption align="middle">Fig 6.2: Nearest pixel sampling, 1x1 supersampled</figcaption>
        
        <p>Not very impressive, seeing as many of the lines of longitude and latitude look quite odd.</p>
        <p>But that's where we use other sampling strategies to help us out! Introduce bilinear pixel sampling. In this strategy, if we hit a pixel that isn't exactly on our texture map, we instead try to interpolate its color from the surrounding four pixels. This results in blending of colors, while also subtly blurring the image. (It's basically a convolution against a 2x2 pixel blur dependent on the pixel coordinates).</p>
        
        <img class="zoomable" src="images/6.2.png"/>
        <figcaption align="middle">Fig 6.3: Bilinear interpolated pixel sampling, 1x1 supersampled</figcaption>

        <p>Much better. So what made nearest pixel sampling so horrible (especially considering the zoom window on the top left seems perfectly fine). The issue comes down to when the triangle maps in non-axis directions. The more skew a triangle, the larger our issue because it becomes more and more difficult to "hit" the right pixel. In other words, if we have a part of the texture map that is very zoomed in, we start getting issues using nearest pixel, whereas interpolating the color can help create a smoother transition between points. The only reason the zoom window we use doesn't have this issue is because all pixels are uniformly scaled up. There is no stretching in the image, and thus no distortion.</p>
        
        <p>Well from the previous parts, we've seen that supersampling makes everything look better. Let's see what happens:</p>
        
        <img class="zoomable" src="images/6.3.png"/>
        <figcaption align="middle">Fig 6.4: Nearest pixel sampling, 16x16 supersampled</figcaption>
        
        <img class="zoomable" src="images/6.4.png"/>
        <figcaption align="middle">Fig 6.5: Bilinear interpolated pixel sampling, 16x16 supersampled</figcaption>
        
        <p>Now the bilinear one looks odd. It's been overblurred. This is because (like stated earlier), the bilinear interpolation is at its most basic level a 2x2 weighted convolution. Antialiasing also "blurs" the image. Effectively, we've overblurred, and now end up with the nearest-pixel looking better. Note: this only holds if the texture is large enough and contains enough pixel data.</p>

    <h2 align="middle">Part 7: Level sampling with mipmaps for texture mapping</h2>
        <p>Level sampling helps texture the image even better by preblending colors beforehand. Here, we can see the effects of selecting mipmap levels compounded with pixel sampling of this image from <a href="http://www.softicons.com/game-icons/minecraft-icons-by-paul-schulerr/minecraft-icon">here</a>:</p>

        <img class="zoomable" src="images/minecraft.jpg"/>
        <figcaption align="middle">Fig 7.1: A Minecraft logo texture</figcaption>

        <img class="zoomable" src="images/7.1.png"/>
        <figcaption align="middle">Fig 7.2: Minecraft warped, nearest level, nearest pixel</figcaption>

        <img class="zoomable" src="images/7.2.png"/>
        <figcaption align="middle">Fig 7.3: Minecraft warped, interpolated level, nearest pixel</figcaption>

        <img class="zoomable" src="images/7.3.png"/>
        <figcaption align="middle">Fig 7.4: Minecraft warped, nearest level, interpolated pixel</figcaption>

        <img class="zoomable" src="images/7.4.png"/>
        <figcaption align="middle">Fig 7.5: Minecraft warped, interpolated level, interpolated pixel</figcaption>

	<p>The differences are subtle, but they are still there. While going from nearest level to interpolated level, we can see that we've filtered out some of the jaggedness of the image in areas where the image is enlarged or shrunk a lot. This is particularly apparent in the zoomed-in portion where the pixels are originally very sharp, to the second one (which if if overlayed) we can see the pixel colors change to blend more. The effects of interpolating pixels still blur the image.</p>

    <h2 align="middle">Part 8: My drawing</h2>
        <p>In this part, we create a rendering of a paper crane. There are many images online and various sources to do this, however the output svg of many programs also include shapes like bezier paths, which our program is not able to interpolate (yet). As a result, we created a short program (using the 3D.js lib) that will convert the path shape into a polygon by evaluating the curve along various points. In total, the program took a few hours to write, and nearly 5 minutes to convert the paths into 2982 polygons, so there is still much improvement in that aspect. The end result is this crane:</p>

        <img class="zoomable" src="images/8.1.png"/>
        <figcaption align="middle">Fig 8.1: A computer generated crane.</figcaption>
</div>

	<p>Of course, we can also download a library that is capable of evaluating SVG paths. When we do this, the result is much more stunning (now with more boat!):</p>

        <img class="zoomable" src="images/origami.png"/>
        <figcaption align="middle">Fig 8.2: Crane rendered with paths enabled.</figcaption>
</div>

	<p>In any case, this project was overall very fun and enjoyable, though it did take quite a while fixing out various bugs. (Also that zoom dialog was very fun to create :D) Hope you enjoyed looking through pictures!</p>
<div id="zoomed" class="zoomed"></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="js/index.js"></script>
</body>
</html>




