// STRUCTURE

class Item {
    constructor(id, category, texture) {
        this.id = id;
        this.uid = Item.uid++;
        this.category = category;
        this.texture = texture;
        this.isBook = false;
    }

    enchant() {
        return new Enchantable(this);
    }

    setBook(isBook) {
        this.isBook = isBook;
        return this;
    }
}
Item.uid = 0;

class Enchantable {
    constructor(item) {
        this.item = item;
        this.name = null;
        this.priorWork = 0;
        this.order = [];
        this.enchantments = {};
        this.hash = null;
        this.enchantmentHash = null;
        this.enchantmentValue = null;
    }

    setName(name) {
        this.name = name;
        this.hash = null;
        return this;
    }

    setPriorWork(priorWork) {
        this.priorWork = priorWork;
        this.hash = null;
        return this;
    }

    // Assumes compatible enchant
    // Mutates and returns this
    addEnchantment(newEnchantment, level) {
        if (level === undefined) {
            level = 1;
        }
        if (level == 0) {
            return removeEnchantment(newEnchantment);
        }
        if (newEnchantment.id in this.enchantments) {
            let oldLevel = this.enchantments[newEnchantment.id].level;
            this.enchantments[newEnchantment.id].level = newEnchantment.combineLevels(oldLevel, level);
        } else {
            this.order.push(newEnchantment.id);
            this.enchantments[newEnchantment.id] = {enchantment: newEnchantment, level: newEnchantment.combineLevels(0, level)};
        }
        this.enchantmentHash = null;
        this.enchantmentValue = null;
        return this;
    }

    canCombine(newItem) {
        return newItem.item.isBook || this.item.id == newItem.item.id;
    }

    clone() {
        var obj = this.item.enchant();
        obj.name = this.name;
        obj.priorWork = this.priorWork;
        obj.order = this.order.slice();
        for (let id in this.enchantments) {
            obj.enchantments[id] = Object.assign({}, this.enchantments[id]);
        }
        return obj;
    }

    cached(newItem) {
        let key = this.hashcode() + ";" + newItem.hashcode();
        if (!(key in Enchantable.cache)) {
            let item = this.clone();
            item.priorWork = Math.max(item.priorWork, newItem.priorWork) + 1;
            for (let id in newItem.enchantments) {
                let {enchantment, level} = newItem.enchantments[id];
                if (item.isCompatibleEnchantment(enchantment)) {
                    item.addEnchantment(enchantment, level);
                }
            }
            let cost = RepairCost(this.priorWork) + RepairCost(newItem.priorWork);
            cost += this.computeEnchantmentCost(newItem.enchantments, newItem.item.isBook);
            Enchantable.cache[key] = {item, cost};
        }
        return Enchantable.cache[key];
    }

    // Assumes can combine
    // Returns different item
    combineItem(newItem) {
        return this.cached(newItem).item;
    }

    // Assumes can combine
    computeCombineCost(newItem, renaming, repairing) {
        let cost = this.cached(newItem).cost;
        if (renaming) {
            cost += 1;
        }
        if (repairing) {
            cost += 2;
        }
        return cost;
    }

    computeEnchantmentCost(enchantments, fromBook) {
        let cost = 0;
        for (let id in enchantments) {
            let {enchantment, level} = enchantments[id];
            if (!enchantment.canApplyTo(this.item)) {
                continue;
            }
            if (!this.isCompatibleEnchantment(enchantment)) {
                cost += 1;
                continue;
            }
            let newLevel = 0;
            if (id in this.enchantments) {
                newLevel = enchantment.combineLevels(this.enchantments[id].level, level);
            } else {
                newLevel = level;
            }
            cost += enchantment.computeCost(newLevel, fromBook);
        }
        return cost;
    }

    computeValue() {
        if (this.enchantmentValue != null) {
            return this.enchantmentValue;
        }
        let cost = 0;
        for (let id in this.enchantments) {
            let {enchantment, level} = this.enchantments[id];
            cost += enchantment.computeCost(level, this.item.isBook);
        }
        if (this.name != null) {
            cost += 0.5;
        }
        this.enchantmentValue = cost;
        return this.enchantmentValue;
    }

    enchantmentsEqual(other) {
        if (this.item.id != other.item.id) {
            return false;
        }
        return this.enchantmentHashcode() == other.enchantmentHashcode();
    }

    enchantmentHashcode() {
        if (this.enchantmentHash) {
            return this.enchantmentHash;
        }
        let hash = [];
        for (let id of Object.keys(this.enchantments).sort()) {
            hash.push(`${this.enchantments[id].enchantment.uid},${this.enchantments[id].level}`);
        }
        this.enchantmentHash = hash.join("|");
        return this.enchantmentHash;
    }

    hashcode() {
        if (this.enchantmentHash && this.hash) {
            return this.hash;
        }
        let hash = [this.name || "", this.item.uid, this.priorWork];
        this.hash = hash.concat(this.enchantmentHashcode()).join("|");
        return this.hash;
    }

    isCompatibleEnchantment(newEnchantment) {
        if (!newEnchantment.canApplyTo(this.item)) {
            return false;
        }
        for (let id in this.enchantments) {
            if (this.enchantments[id].enchantment.isIncompatible(newEnchantment)) {
                return false;
            }
        }
        return true;
    }

    removeEnchantment(oldEnchantment) {
        if (oldEnchantment === undefined) {
            this.order = [];
            this.enchantments = {};
        } else {
            this.order = this.order.filter(id => id != oldEnchantment.id);
            delete this.enchantments[oldEnchantment.id];
        }
        this.enchantmentHash = null;
        this.enchantmentValue = null;
        return this;
    }

    render() {
        let color = this.item.isBook ? "fgE" : "fgB",
            content = [],
            name = this.name || this.item.id;
        if (this.name != null) {
            color += " fgO";
        }
        for (let id of this.order) {
            if (!(id in this.enchantments)) {
                content.push("enchantment.unknown");
            }
            let {enchantment, level} = this.enchantments[id],
                text = `${enchantment.id}${ToRomanNumeral(level)}`;
            if (enchantment.isCurse) {
                content.push(`<span class="fgC shadow">${text}</span>`);
            } else {
                content.push(text);
            }
        }
        if (this.priorWork > 0) {
            content.push(`<span class="fg8 shadow">RepairCost=${RepairCost(this.priorWork)}</span>`)
        }
        return `<div class="${color} shadow tooltip-title">${name}</div><div class="fg7 shadow">${content.join("<br>")}</div>`;
    }
}
Enchantable.cache = {};

class Enchantment {
    constructor(id, maxLevel, multiplier) {
        this.id = id;
        this.uid = Enchantment.uid++;
        this.maxLevel = maxLevel;
        this.multiplier = multiplier;
        this.bookMultiplier = Math.ceil(multiplier / 2);
        this.applicable = {};
        this.isCurse = false;
        this.incompatible = {};
    }

    addIncompatible(...ids) {
        for (let id of ids) {
            this.incompatible[id] = true;
        }
        return this;
    }

    appliesTo(...ids) {
        for (let id of ids) {
            this.applicable[id] = true;
        }
        return this;
    }

    canApplyTo(item) {
        return item.isBook || item.category in this.applicable;
    }

    combineLevels(first, second) {
        return Math.min(CombineLevels(first, second), this.maxLevel);
    }

    computeCost(level, fromBook) {
        if (fromBook) {
            return level * this.bookMultiplier;
        } else {
            return level * this.multiplier;
        }
    }

    isIncompatible(enchantment) {
        return enchantment.id in this.incompatible;
    }

    setCurse(isCurse) {
        this.isCurse = isCurse;
        return this;
    }
}
Enchantment.uid = 0;

// ALGORITHM

class Optimizer {
    constructor(items, target) {
        this.items = items || [];
        this.target = target || null;
        this.iterations = 1;
    }

    addItem(item) {
        this.items.push(item);
    }

    addItems(items) {
        this.items = this.items.concat(items);
    }

    setTarget(target) {
        this.target = target;
    }

    clone() {
        var obj = new Optimizer([], this.target);
        obj.addItems(items);
        return obj;
    }

    // Assumes can combine
    combineAndClone(firstIndex, secondIndex) {
        var obj = new Optimizer([], this.target);
        let first = null,
            second = null;
        for (let index in this.items) {
            if (index == firstIndex) {
                first = this.items[index];
            } else if (index == secondIndex) {
                second = this.items[index];
            } else {
                obj.addItem(this.items[index]);
            }
        }
        obj.addItem(first.combineItem(second));
        return obj;
    }

    removeAndClone(firstIndex) {
        var obj = new Optimizer([], this.target);
        for (let index in this.items) {
            if (index != firstIndex) {
                obj.addItem(this.items[index]);
            }
        }
        return obj;
    }

    hashcode() {
        return this.items.map(i => i.hashcode()).sort().join(";");
    }

    optimize() {
        return this.searchExhaustive();  // Use exhaustive search algorithm
    }

    searchExhaustive() {
        if (this.items.length > 9) {
            throw "Current algorithm does not support more than 9 items.";
        }

        let hash = this.hashcode();
        if (hash in Optimizer.cache) {
            return Optimizer.cache[hash];
        }

        let bestValue = 0,
            bestCost = Infinity,
            bestStep = null,
            bestPath = null;

        if (this.items.length < 1) {
            return {path: [], cost: 0};
        }
        if (this.items.length < 2) {
            return {path: [{
                cost: 0,
                item: this.items[0],
            }], cost: 0};
        }

        for (let firstIndex = 0; firstIndex < this.items.length; firstIndex++) {
            let skipOptimizer = this.removeAndClone(firstIndex);  // Check value if skipping this item
            let skipOptimization = skipOptimizer.searchExhaustive();
            if (skipOptimization.path && skipOptimization.path.length > 0) {
                let value = skipOptimization.path[skipOptimization.path.length - 1].item.computeValue();
                if (value < bestValue) {
                    continue;  // Skip optimization of lesser value
                }
                if (value > bestValue || skipOptimization.cost < bestCost) {
                    bestValue = value;
                    bestCost = skipOptimization.cost;
                    bestStep = null;
                    bestPath = skipOptimization.path;
                }
            }
            if (this.items[firstIndex].order.length == 0) {
                continue;  // Skip if not enchantable
            }
            for (let secondIndex = firstIndex + 1; secondIndex < this.items.length; secondIndex++) {
                if (this.iterations > Optimizer.MaxIterations) {
                    throw "Max iterations exceeded!  Click 'Optimize Enchants' to try again with more compute.";  // Too many iterations kill-switch
                }
                if (this.items[secondIndex].order.length == 0) {
                    continue;  // Skip if not enchantable
                }
                let combineCost = Infinity,
                    otherCombineCost = Infinity;
                if (this.items[firstIndex].canCombine(this.items[secondIndex])) {
                    combineCost = this.items[firstIndex].computeCombineCost(this.items[secondIndex]);
                }
                if (this.items[secondIndex].canCombine(this.items[firstIndex])) {
                    otherCombineCost = this.items[secondIndex].computeCombineCost(this.items[firstIndex]);
                }
                if (combineCost >= 40 && otherCombineCost >= 40) {
                    continue;  // Skip if can't combine
                }

                let leftIndex = firstIndex,
                    rightIndex = secondIndex;
                if (otherCombineCost < combineCost) {
                    [combineCost, leftIndex, rightIndex] = [otherCombineCost, rightIndex, leftIndex];
                }

                let optimizer = this.combineAndClone(leftIndex, rightIndex);
                let optimization = optimizer.searchExhaustive();
                let value = optimization.path[optimization.path.length - 1].item.computeValue();
                if (value < bestValue) {
                    continue;  // Skip optimization of lesser value
                }
                if (value > bestValue || combineCost + optimization.cost < bestCost) {
                    bestValue = value;
                    bestCost = combineCost + optimization.cost;
                    bestStep = {
                        cost: combineCost,
                        item: optimizer.items[optimizer.items.length - 1],
                        left: this.items[leftIndex],
                        right: this.items[rightIndex],
                    };
                    bestPath = optimization.path;
                }

                this.iterations += optimizer.iterations;  // Count total iterations
            }
        }

        if (bestStep != null) {
            bestPath = [bestStep].concat(bestPath);
        }
        Optimizer.cache[hash] = {path: bestPath, cost: bestCost};
        return Optimizer.cache[hash];
    }
}
Optimizer.cache = {};
Optimizer.MaxIterations = 1000000;

// UTILITY

(function() {
    let RomanNumerals = ["0", "", " II", " III", " IV", " V"];

    ToRomanNumeral = function(number) {
        console.assert(Number.isInteger(number), "Expected number but got", number);
        if (number < 0 || number > 5) {
            return `.${number}`;
        }
        return RomanNumerals[number];
    }

    CombineLevels = function(first, second) {
        if (first == second) {
            return first + 1;
        }
        return Math.max(first, second);
    }

    RepairCost = function(priorWork) {
        console.assert(Number.isInteger(priorWork), "Expected number but got", priorWork);
        if (priorWork < 0 || priorWork > 5) {
            return Infinity;
        }
        return Math.pow(2, priorWork) - 1;
    }
})();

// CONTENT

var Items = {};
var Enchantments = {};

(function() {
    // ARMORS

    let ARMORS = ["Leather", "Chainmail", "Iron", "Golden", "Diamond", "Netherite"],
        HELMET = "Helmet",
        CHESTPLATE = "Chestplate",
        LEGGINGS = "Leggings",
        BOOTS = "Boots";

    (function() {
        for (let armor of [HELMET, CHESTPLATE, LEGGINGS, BOOTS]) {
            for (let variant of ARMORS) {
                let id = `${variant} ${armor}`;
                Items[id] = new Item(id, armor, null);
            }
        }
    })();

    // TOOLS

    let TOOLS = ["Wooden", "Stone", "Iron", "Golden", "Diamond", "Netherite"],
        PICKAXE = "Pickaxe",
        SHOVEL = "Shovel",
        AXE = "Axe",
        SWORD = "Sword",
        HOE = "Hoe";

    (function() {
        for (let tool of [PICKAXE, SHOVEL, AXE, SWORD, HOE]) {
            for (let variant of TOOLS) {
                let id = `${variant} ${tool}`;
                Items[id] = new Item(id, tool, null);
            }
        }
    })();

    // ===== Specific

    let FISHING_ROD = "Fishing Rod",
        BOW = "Bow",
        SHEARS = "Shears",
        FLINT_AND_STEEL = "Flint and Steel",
        CARROT_ON_A_STICK = "Carrot on a Stick",
        WARPED_FUNGUS_ON_A_STICK = "Warped Fungus on a Stick",
        SHIELD = "Shield",
        ELYTRA = "Elytra",
        PUMPKIN = "Carved Pumpkin",
        HEAD = "Head",
        TURTLE_HELMET = "Turtle Shell",
        TRIDENT = "Trident",
        CROSSBOW = "Crossbow",
        BOOK = "Enchanted Book";

    (function() {
        let items = [
            [FISHING_ROD, new Item(FISHING_ROD, FISHING_ROD, null)],
            [BOW, new Item(BOW, BOW, null)],
            [SHEARS, new Item(SHEARS, SHEARS, null)],
            [FLINT_AND_STEEL, new Item(FLINT_AND_STEEL, FLINT_AND_STEEL, null)],
            [CARROT_ON_A_STICK, new Item(CARROT_ON_A_STICK, CARROT_ON_A_STICK, null)],
            [WARPED_FUNGUS_ON_A_STICK, new Item(WARPED_FUNGUS_ON_A_STICK, WARPED_FUNGUS_ON_A_STICK, null)],
            [SHIELD, new Item(SHIELD, SHIELD, null)],
            [ELYTRA, new Item(ELYTRA, ELYTRA, null)],
            [PUMPKIN, new Item(PUMPKIN, PUMPKIN, null)],
            [HEAD, new Item(HEAD, HEAD, null)],
            [TURTLE_HELMET, new Item(TURTLE_HELMET, TURTLE_HELMET, null)],
            [TRIDENT, new Item(TRIDENT, TRIDENT, null)],
            [CROSSBOW, new Item(CROSSBOW, CROSSBOW, null)],
            [BOOK, new Item(BOOK, BOOK, null).setBook(true)],
        ];
        for (let item of items) {
            Items[item[0]] = item[1];
        }
    })();
        
    // ENCHANTMENTS

    let PROTECTION = "Protection",
        FIRE_PROTECTION = "Fire Protection",
        FEATHER_FALLING = "Feather Falling",
        BLAST_PROTECTION = "Blast Protection",
        PROJECTILE_PROTECTION = "Projectile Protection",
        THORNS = "Thorns",
        RESPIRATION = "Respiration",
        DEPTH_STRIDER = "Depth Strider",
        AQUA_AFFINITY = "Aqua Affinity",
        SHARPNESS = "Sharpness",
        SMITE = "Smite",
        BANE_OF_ARTHROPODS = "Bane of Arthropods",
        KNOCKBACK = "Knockback",
        FIRE_ASPECT = "Fire Aspect",
        LOOTING = "Looting",
        EFFICIENCY = "Efficiency",
        SILK_TOUCH = "Silk Touch",
        UNBREAKING = "Unbreaking",
        FORTUNE = "Fortune",
        POWER = "Power",
        PUNCH = "Punch",
        FLAME = "Flame",
        INFINITY = "Infinity",
        LUCK_OF_THE_SEA = "Luck of the Sea",
        LURE = "Lure",
        FROST_WALKER = "Frost Walker",
        MENDING = "Mending",
        CURSE_OF_BINDING = "Curse of Binding",
        CURSE_OF_VANISHING = "Curse of Vanishing",
        IMPALING = "Impaling",
        RIPTIDE = "Riptide",
        LOYALTY = "Loyalty",
        CHANNELING = "Channeling",
        MULTISHOT = "Multishot",
        PIERCING = "Piercing",
        QUICK_CHARGE = "Quick Charge",
        SOUL_SPEED = "Soul Speed",
        SWEEPING_EDGE = "Sweeping Edge";

    (function() {
        let enchantments = [
            [PROTECTION, new Enchantment(PROTECTION, 4, 1).appliesTo(HELMET, CHESTPLATE, LEGGINGS, BOOTS, TURTLE_HELMET).addIncompatible(FIRE_PROTECTION, BLAST_PROTECTION, PROJECTILE_PROTECTION)],
            [FIRE_PROTECTION, new Enchantment(FIRE_PROTECTION, 4, 2).appliesTo(HELMET, CHESTPLATE, LEGGINGS, BOOTS, TURTLE_HELMET).addIncompatible(PROTECTION, BLAST_PROTECTION, PROJECTILE_PROTECTION)],
            [FEATHER_FALLING, new Enchantment(FEATHER_FALLING, 4, 2).appliesTo(BOOTS)],
            [BLAST_PROTECTION, new Enchantment(BLAST_PROTECTION, 4, 4).appliesTo(HELMET, CHESTPLATE, LEGGINGS, BOOTS, TURTLE_HELMET).addIncompatible(PROTECTION, FIRE_PROTECTION, PROJECTILE_PROTECTION)],
            [PROJECTILE_PROTECTION, new Enchantment(PROJECTILE_PROTECTION, 4, 2).appliesTo(HELMET, CHESTPLATE, LEGGINGS, BOOTS, TURTLE_HELMET).addIncompatible(PROTECTION, FIRE_PROTECTION, BLAST_PROTECTION)],
            [THORNS, new Enchantment(THORNS, 3, 8).appliesTo(HELMET, CHESTPLATE, LEGGINGS, BOOTS, TURTLE_HELMET)],
            [RESPIRATION, new Enchantment(RESPIRATION, 3, 4).appliesTo(HELMET, TURTLE_HELMET)],
            [DEPTH_STRIDER, new Enchantment(DEPTH_STRIDER, 3, 4).appliesTo(BOOTS).addIncompatible(FROST_WALKER)],
            [AQUA_AFFINITY, new Enchantment(AQUA_AFFINITY, 1, 4).appliesTo(HELMET, TURTLE_HELMET)],
            [SHARPNESS, new Enchantment(SHARPNESS, 5, 1).appliesTo(SWORD, AXE).addIncompatible(SMITE, BANE_OF_ARTHROPODS)],
            [SMITE, new Enchantment(SMITE, 5, 2).appliesTo(SWORD, AXE).addIncompatible(SHARPNESS, BANE_OF_ARTHROPODS)],
            [BANE_OF_ARTHROPODS, new Enchantment(BANE_OF_ARTHROPODS, 5, 2).appliesTo(SWORD, AXE).addIncompatible(SHARPNESS, SMITE)],
            [KNOCKBACK, new Enchantment(KNOCKBACK, 2, 2).appliesTo(SWORD)],
            [FIRE_ASPECT, new Enchantment(FIRE_ASPECT, 2, 4).appliesTo(SWORD)],
            [LOOTING, new Enchantment(LOOTING, 3, 4).appliesTo(SWORD)],
            [EFFICIENCY, new Enchantment(EFFICIENCY, 5, 1).appliesTo(PICKAXE, SHOVEL, AXE, SHEARS)],
            [SILK_TOUCH, new Enchantment(SILK_TOUCH, 1, 8).appliesTo(PICKAXE, SHOVEL, AXE).addIncompatible(FORTUNE)],
            [UNBREAKING, new Enchantment(UNBREAKING, 3, 2).appliesTo(PICKAXE, SHOVEL, AXE, FISHING_ROD, HELMET, CHESTPLATE, LEGGINGS, BOOTS, SWORD, BOW, HOE, SHEARS, FLINT_AND_STEEL, CARROT_ON_A_STICK, WARPED_FUNGUS_ON_A_STICK, SHIELD, ELYTRA, TURTLE_HELMET, TRIDENT, CROSSBOW)],
            [FORTUNE, new Enchantment(FORTUNE, 3, 4).appliesTo(PICKAXE, SHOVEL, AXE).addIncompatible(SILK_TOUCH)],
            [POWER, new Enchantment(POWER, 5, 1).appliesTo(BOW)],
            [PUNCH, new Enchantment(PUNCH, 2, 4).appliesTo(BOW)],
            [FLAME, new Enchantment(FLAME, 1, 4).appliesTo(BOW)],
            [INFINITY, new Enchantment(INFINITY, 1, 8).appliesTo(BOW).addIncompatible(MENDING)],
            [LUCK_OF_THE_SEA, new Enchantment(LUCK_OF_THE_SEA, 3, 4).appliesTo(FISHING_ROD)],
            [LURE, new Enchantment(LURE, 3, 4).appliesTo(FISHING_ROD)],
            [FROST_WALKER, new Enchantment(FROST_WALKER, 2, 4).appliesTo(BOOTS).addIncompatible(DEPTH_STRIDER)],
            [MENDING, new Enchantment(MENDING, 1, 4).appliesTo(PICKAXE, SHOVEL, AXE, FISHING_ROD, HELMET, CHESTPLATE, LEGGINGS, BOOTS, SWORD, BOW, HOE, SHEARS, FLINT_AND_STEEL, CARROT_ON_A_STICK, WARPED_FUNGUS_ON_A_STICK, SHIELD, ELYTRA, TURTLE_HELMET, TRIDENT, CROSSBOW).addIncompatible(INFINITY)],
            [CURSE_OF_BINDING, new Enchantment(CURSE_OF_BINDING, 1, 8).appliesTo(HELMET, CHESTPLATE, LEGGINGS, BOOTS, ELYTRA, PUMPKIN, HEAD, TURTLE_HELMET).setCurse(true)],
            [CURSE_OF_VANISHING, new Enchantment(CURSE_OF_VANISHING, 1, 8).appliesTo(PICKAXE, SHOVEL, AXE, FISHING_ROD, HELMET, CHESTPLATE, LEGGINGS, BOOTS, SWORD, BOW, HOE, SHEARS, FLINT_AND_STEEL, CARROT_ON_A_STICK, WARPED_FUNGUS_ON_A_STICK, SHIELD, ELYTRA, PUMPKIN, HEAD, TURTLE_HELMET, TRIDENT, CROSSBOW).setCurse(true)],
            [IMPALING, new Enchantment(IMPALING, 5, 4).appliesTo(TRIDENT)],
            [RIPTIDE, new Enchantment(RIPTIDE, 3, 4).appliesTo(TRIDENT).addIncompatible(LOYALTY, CHANNELING)],
            [LOYALTY, new Enchantment(LOYALTY, 3, 1).appliesTo(TRIDENT).addIncompatible(RIPTIDE)],
            [CHANNELING, new Enchantment(CHANNELING, 1, 8).appliesTo(TRIDENT).addIncompatible(RIPTIDE)],
            [MULTISHOT, new Enchantment(MULTISHOT, 1, 4).appliesTo(CROSSBOW).addIncompatible(PIERCING)],
            [PIERCING, new Enchantment(PIERCING, 4, 1).appliesTo(CROSSBOW).addIncompatible(MULTISHOT)],
            [QUICK_CHARGE, new Enchantment(QUICK_CHARGE, 3, 2).appliesTo(CROSSBOW)],
            [SOUL_SPEED, new Enchantment(SOUL_SPEED, 3, 8).appliesTo(BOOTS)],
            [SWEEPING_EDGE, new Enchantment(SWEEPING_EDGE, 3, 4).appliesTo(SWORD)],
        ];
        for (let enchantment of enchantments) {
            Enchantments[enchantment[0]] = enchantment[1];
        }
    })();
})();

// ===== USER INTERFACE =====

$(function() {
    var AnvilCount = 1;
    var CurrentItem = null;
    var CurrentSlot = null;
    var EditorActive = false;
    var Inventory = Array(45).fill(null);
    var SCALE = 2;

    var Anvils = $("#anvils");
    var AnvilTitle = $("#anvil-title");
    var Editor = $("#json");
    var Slots = $(".gui.item.stored");
    var HeldItem = $("#current-item");

    function translateString(s) {
        if (s.charAt(0) != s.charAt(0).toLowerCase()) {
            return s;
        }
        return s.replace(/_/g, " ").replace(/\w\S*/g, function(txt){
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    }

    // Add row to anvil
    function addAnvil() {
        let html = 
`<div id="anvil-${AnvilCount}" class="gui anvil middle">
    <div id="target-${AnvilCount}" class="gui item stored anvil-target"></div>
    <div id="sacrifice-${AnvilCount}" class="gui item stored anvil-sacrifice"></div>
    <div id="output-${AnvilCount}" class="gui item stored anvil-output"></div>    
    <div id="cost-${AnvilCount}" class="anvil-cost fgEnchantment shadow"></div>    
</div>`;
        Anvils.append(html);
        $("#target-"+AnvilCount).contextmenu(() => false);
        $("#sacrifice-"+AnvilCount).contextmenu(() => false);
        $("#output-"+AnvilCount).contextmenu(() => false);
        AnvilCount++;
    }

    // Displays one anvil row
    function displayAnvil(row, target, sacrifice, output, cost) {
        if (row < 0 || row > 99) {
            return false;
        }
        while (AnvilCount <= row) {
            addAnvil();
        }
        let items = [target, sacrifice, output];
        let nodes = [$("#target-" + row), $("#sacrifice-" + row), $("#output-" + row)];
        for (let index in items) {
            let node = nodes[index];
            if (!node) {
                return false;
            }
            if (items[index]) {
                node.addClass("hoverable");
                node.css("background-position-y", `${-16 * SCALE * items[index].item.uid}px`);
                node.data("tooltip", items[index].render());
            } else {
                node.removeClass("hoverable");
                node.css("background-position-y", (16 * SCALE) + "px");
                node.data("tooltip", "");
            }
        }
        let node = $("#cost-" + row);
        node.text(cost || "");
        if (cost) {
            node.show();
        } else {
            node.hide();
        }
        return true;
    }

    // Displays a slot in inventory.
    function displaySlot(index, currentItem) {
        if (currentItem) {
            if (CurrentItem != null) {
                HeldItem.addClass("hoverable");
                HeldItem.css("background-position-y", `${-16 * SCALE * CurrentItem.item.uid}px`);
                HeldItem.data("tooltip", CurrentItem.render());            
            } else {
                HeldItem.removeClass("hoverable");
                HeldItem.css("background-position-y", (16 * SCALE) + "px");
                HeldItem.data("tooltip", "");
            }    
            return true;
        }
        let id = "";
        if (index < 9) {
            id = "#hotbar-" + index;
        } else if (index < 36) {
            id = "#player-" + (index - 9);
        } else {
            id = "#chest-" + (index - 36);
        }
        let target = $(id);
        if (!target) {
            return false;
        }
        if (Inventory[index]) {
            target.addClass("hoverable");
            target.css("background-position-y", `${-16 * SCALE * Inventory[index].item.uid}px`);
            target.data("tooltip", Inventory[index].render());            
        } else {
            target.removeClass("hoverable");
            target.css("background-position-y", (16 * SCALE) + "px");
            target.data("tooltip", "");
        }
        return true;
    }

    // Displays whatever is in Inventory
    function displayInventory() {
        for (let index in Inventory) {
            if (!displaySlot(index)) {
                return false;
            }
        }
        return true;
    }

    // Dumps Inventory
    function dumpInventory() {
        let obj = {};
        for (let index in Inventory) {
            let enchantable = Inventory[index],
                item = {};
            if (enchantable === null) {
                continue;
            }
            item.id = enchantable.item.id;
            if (enchantable.name) {
                item.Name = enchantable.name;
            }
            if (enchantable.priorWork) {
                item.PriorWork = enchantable.priorWork;
            }
            let Enchantments = [];
            if (enchantable.order.length > 0) {
                for (let id of enchantable.order) {
                    let lvl = enchantable.enchantments[id].level;
                    Enchantments.push({id, lvl});
                }
                item.Enchantments = Enchantments;
            }
            obj[index] = item;
        }
        return obj;
    }

    // Assumes well-formed items.
    function modifyInventory(items) {
        Inventory = Array(45).fill(null);
        for (let item of items) {
            Inventory[item.slot] = item.enchantable;
        }
        return true;
    }

    // Pick up or put down the current item
    function grabMoveItem(event, canGrab) {
        if (event.shiftKey) {
            return shiftItem(event);
        }
        let slot = $(event.target).data("slot");
        if (slot === undefined || slot < 0 || slot > 45) {
            return;
        }
        if (canGrab && Inventory[slot] != null) {
            CurrentSlot = slot;
            swapSlots(slot);
            moveItem(event);
            HeldItem.show();
            event.preventDefault();
        } else if (slot == CurrentSlot) {
            CurrentSlot = null;
        } else if (Inventory[slot] == null) {
            swapSlots(slot);
            HeldItem.hide();
            event.preventDefault();
        }
    }

    // Shift Click item to move between chest and hotbar (overflow to player)
    function shiftItem(event) {
        let slot = $(event.target).data("slot");
        if (slot === undefined || slot < 0 || slot > 45 || Inventory[slot] == null) {
            return;
        }
        let hotbarSlot = null,
            playerSlot = null,
            chestSlot = null;
        for (let index in Inventory) {
            if (index < 8) {
                if (hotbarSlot !== null) {
                    continue;
                }
                if (Inventory[index] === null) {
                    hotbarSlot = index;
                }
            } else if (index < 36) {
                if (playerSlot !== null) {
                    continue;
                }
                if (Inventory[index] === null) {
                    playerSlot = index;
                }
            } else {
                if (chestSlot !== null) {
                    break;
                }
                if (Inventory[index] === null) {
                    chestSlot = index;
                }
            }
        }
        if (hotbarSlot === null && playerSlot === null && chestSlot === null) {
            event.preventDefault();
            document.getSelection().removeAllRanges();
            return;
        }
        if (slot < 8) {
            if (chestSlot !== null) {
                swapSlots(slot, chestSlot);
            } else if (playerSlot !== null) {
                swapSlots(slot, playerSlot);
            }
        } else if (slot < 36) {
            if (chestSlot !== null) {
                swapSlots(slot, chestSlot);
            } else if (hotbarSlot !== null) {
                swapSlots(slot, hotbarSlot);
            }
        } else {
            if (hotbarSlot !== null) {
                swapSlots(slot, hotbarSlot);
            } else if (playerSlot !== null) {
                swapSlots(slot, playerSlot);
            }
        }
        event.preventDefault();
        document.getSelection().removeAllRanges();
    }

    function moveItem(event) {
        if (CurrentItem != null) {
            HeldItem.css({left: event.pageX - 8 * SCALE, top: event.pageY - 8 * SCALE});
        }
    }

    // Return an optimizer for the chest items
    function getOptimizer() {
        let chestItems = [];
        for (let index = 36; index <= 45; index++) {
            if (Inventory[index] != null) {
                chestItems.push(Inventory[index]);
            }
        }
        return new Optimizer(chestItems);
    }

    // Optimize all items in chest
    function optimize() {
        let optimizer = getOptimizer();
        try {
            let {path, cost} = optimizer.optimize();
            let name = null,
                count = 0;
            if (path.length > 0) {
                for (let index in path) {
                    displayAnvil(index, path[index].left, path[index].right, path[index].item, path[index].cost || cost);
                    name = path[index].item.name || path[index].item.item.id;
                    count++;
                    $("#anvil-" + index).show();
                }
            } else {
                displayAnvil(0);
                name = "";
                count = 1;
            }
            for (let i = count; i < AnvilCount; i++) {
                $("#anvil-" + i).hide();
            }
            AnvilTitle.text(name);
        } catch (error) {
            console.error(error);
            window.alert(`Error: ${error}`);
        }
    }

    /*
    Format
    items = {
        [0-45]: StoredItem,  // [0-8] (hotbar), [9-35] (player), [36-45] (chest)
    }
    StoredItem = {
        id: "Item Name",
        Name: "Custom Name",
        Enchantments: [Enchantment...],
        PriorWork: [0-5]
    }
    Enchantment = {
        id: "Enchantment Name",
        lvl: [0-5]
    }
    */
    function parseInventory(items) {
        let parsedItems = [];
        for (let index in items) {
            let slot = parseInt(index);
            if (isNaN(slot)) {
                throw `Invalid slot ${index}.`;
            }
            if (slot < 0 || slot > 45) {
                throw `Slot ${slot} out of bounds [0-45].`;
            }
            let obj = items[index];
            if (typeof obj !== 'object') {
                throw `Expected an object {id, Enchantments, PriorWork} but got ${obj}.`;
            }
            if (obj.id === undefined) {
                throw "Missing item ID.";
            }
            let id = translateString(obj.id);
            if (!(id in Items)) {
                throw `Unknown ID ${id}.`;
            }
            let enchantable = Items[id].enchant();
            if (obj.Name) {
                enchantable.setName(obj.Name);
            }
            if (obj.Enchantments && Array.isArray(obj.Enchantments)) {
                for (let enchantment of obj.Enchantments) {
                    if (enchantment.id === undefined) {
                        throw "Missing Enchantment ID.";
                    }
                    let eid = translateString(enchantment.id);
                    if (!(eid in Enchantments)) {
                        throw `Unknown Enchantment ID ${eid}.`;
                    }
                    let level = enchantment.lvl || 1;
                    enchantable.addEnchantment(Enchantments[eid], level);
                }
            }
            if (obj.PriorWork) {
                if (obj.PriorWork < 0 || obj.PriorWork > 5) {
                    throw `Prior Work ${obj.PriorWork} out of bounds [0-5].`;
                }
                enchantable.setPriorWork(obj.PriorWork);
            }
            parsedItems.push({slot, enchantable});
        }
        return modifyInventory(parsedItems);
    }
    
    function parseJson(event) {
        let success = false;
        try {
            json = JSON.parse(event.target.value.replace(/(?<!["A-Za-z_])([A-Za-z_]+)(?!["A-Za-z_])/g, "\"$1\"").replace(/(?<!["0-9])([0-9]+):/g, "\"$1\":"));
            success = parseInventory(json);
            if (success) {
                window.localStorage.setItem("json", event.target.value);
                displayInventory();
                showTarget();
                updateURL();
            }
        } catch (error) {
            if (!(error instanceof SyntaxError)) {
                console.log(`Error: ${error}`);
            }
            success = false;
        }
        if (success) {
            Editor.removeClass("fg4");
            Editor.addClass("fg2");
        } else {
            Editor.removeClass("fg2");
            Editor.addClass("fg4");
        }
        EditorActive = false;
    }

    function prettifyJson() {
        let json = JSON.stringify(dumpInventory(), null, 1);
        Editor.val(json);
        window.localStorage.setItem("json", json);
    }

    var showTarget = _.throttle(optimize, 500);

    function swapSlots(first, second) {
        if (second == null) {
            [Inventory[first], CurrentItem] = [CurrentItem, Inventory[first]];
            displaySlot(null, true);
        } else {
            [Inventory[first], Inventory[second]] = [Inventory[second], Inventory[first]];
            displaySlot(second);
        }
        displaySlot(first);
        HideTooltip();
        prettifyJson();
        showTarget();
        updateURL();
    }

    function urlDecode() {
        let params = new URLSearchParams(document.location.search.substring(1));
        if (params.has("inv")) {
            return LZString.decompressFromEncodedURIComponent(params.get("inv"));
        }
        return null;      
    }

    function urlEncode() {
        let url = new URL(document.location.href);
        let compressed = LZString.compressToEncodedURIComponent(JSON.stringify(dumpInventory()));
        if (compressed) {
            if (compressed.length > 2000) {
                throw "Inventory is too large to store in url.";
            }
            url.searchParams.set("inv", compressed);
            history.replaceState("", "", url.toString());
        }
    }

    var updateURL = _.debounce(urlEncode, 500);
        
    Editor.on("input", function() {
        if (EditorActive) {
            return;
        }
        Editor.removeClass("fg2 fg4");
        EditorActive = true;
    });
    Editor.on("input", _.debounce(parseJson, 500));

    Slots.bind("mousedown touchend", event => grabMoveItem(event, true));
    Slots.bind("mouseup", event => grabMoveItem(event, false));
    Slots.contextmenu(() => false);
    $("body").mousemove(moveItem);

    // Session start
    Editor.val(urlDecode() || window.localStorage.getItem("json") || "{}");
    Editor.trigger("input");
});
