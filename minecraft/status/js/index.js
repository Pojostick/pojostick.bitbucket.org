function query() {
    let disabled = $("#query-button").hasClass("disabled");
    if (disabled) return;
    let address = $("#address").val();
    $.ajax({
        url: `https://api.mcsrvstat.us/2/${address}`,
        success: function(data) {
            if (data.online) {
                $("#hostname-success").text(data.hostname);
                $("#players-online").text(data.players.online);
                $("#players-max").text(data.players.max);
                $("#motd").html(data.motd.html.join("<br>"));
                $("#ip").text(data.ip);
                $("#port").text(data.port);
                $("#software").text(data.software);
                $("#version").text(data.version);
                heads(data.players.list);

                $("#query-success").show();
                $("#query-fail").hide();
            } else {
                $("#hostname-fail").text(data.hostname); 
                heads([]);

                $("#query-success").hide();
                $("#query-fail").show();
            }
        },
        error: function() {
            $("#hostname-fail").text(address);
            heads([]);

            $("#query-success").hide();
            $("#query-fail").show();
        },
    });
}

function heads(players) {
    let html = "";

    $.each(players, function(index, item) {
        if (index >= 5) return false;
        html += `<img id="head-${item}" class="head hoverable" data-tooltip="${item}" src="https://minotar.net/helm/${item}/64.png" alt="${item}" width="64" height="64"></img>`
    });

    $("#heads").html(html);
}

var tooltipAactive = false;

$(function() {
    $("#address").on('input', function(event) {
        if (event.target.value.length > 0) {
            if ($("#query-button").hasClass("disabled")) {
                $("#query-button").removeClass("disabled");
            }
        } else {
            if (!$("#query-button").hasClass("disabled")) {
                $("#query-button").addClass("disabled");
            }
        }
    });
    $("#address").keyup(function(event) {
        if (event.which === 13) {
            event.preventDefault();
            query();
        }
    });

    $("#query-button").click(query);
});
  