$(function() {
    let html = 
`<div id="tooltip" class="tooltip border fgF shadow" style="display: none;">
    <div id="tooltip-content" class="tooltip content">
    </div>
</div>`;
    $("body").append(html);

    let lastTarget = null;
    let tooltip = $("#tooltip");
    let tooltipContent = $("#tooltip-content");
    function moveTooltip(event) {
        if (!event.target.id) {
            if (lastTarget != null) {
                lastTarget = null;
                if (tooltip.is(":visible")) {
                    tooltip.hide();
                }    
            }
        }
        if (lastTarget != null) {
            tooltip.css({left: event.pageX + 16, top: Math.max(0, event.pageY - 24)});
        }
        if (event.target.id == lastTarget) {
            return;
        }
        let target = $(event.target);
        if (target.hasClass("hoverable")) {
            lastTarget = event.target.id;
            tooltipContent.html(target.data("tooltip"));
            tooltip.css({left: event.pageX + 16, top: Math.max(0, event.pageY - 24)});
            if (tooltip.is(":hidden")) {
                tooltip.show();
            }
        } else {
            HideTooltip();
        }
    };
    HideTooltip = function() {
        lastTarget = null;
        if (tooltip.is(":visible")) {
            tooltip.hide();
        }
    };

    $("body").mousemove(moveTooltip);
    $("body").click(moveTooltip);
});