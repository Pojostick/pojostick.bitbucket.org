function hideSubs(id, delay) {
    $(id).children().each(function(index) {
        if (this.id != "message") $(this).delay(delay + 125*index).hide(625);
    });
}

function fadeInSubs(id, delay) {
    $(id).children().each(function(index) {
        $(this).hide().delay(delay + 250*index).fadeIn(500);
    });
}

function showSubs(id, delay) {
    $(id).children().each(function(index) {
        $(this).hide().delay(delay + 500*index).show(1000);
    });
}

var path = 0;

function allen(i) {
    hideSubs("#allen", 0);
    $("#allen #message").empty();
    path += i;
    switch (i) {
    case 0:
        $("#allen #message").append("<p>Bob prowls behind a table around the backside of the room while Allen approaches from the front, taunting the demon. With a string of jeers and leering remarks, Allen draws the attention of the demon onto himself. The demon turns to face Allen, preparing to rend him into pieces. And that's when Bob pounces.</p><p>A terrifying array of fangs and claws rip at the crimson backside of the demon. As Bob draws out the attack long enough for the demon to jerk around, Allen lines up a well-timed cleave down the demon's now exposed frontside. Having trained countless times and studying foe after foe, Allen had the strike planned out from head to toe. Just like the giant gash slicing through the demon, quite ironically, from head to toe. There's a blood-piercing screech as the demon realizes it was tricked.</p>");
        break;
    case 1:
        $("#allen #message").append("<p>In a concerted movement, hunter and beast charge forward in a frenzy of steel and claws. With the combined mass of a war-tiger and rider charging towards the demon, there was no place for the demon to escape to but backwards into the wall. Like the rushing river from a flash-flood rainstorm, the tangled mass is indiscriminate of objects in its path, pushing with full force through tables and lab equipment before finally crashing against the wall.</p>");
        break;
    case 2:
        $("#allen #message").append("<p>Without needing to say anything, Bob already anticipates Allen's next move. He steps forward, taking upon a defensive stance while a statically charged field builds up behind him. As Allen gathers the electric energy into a bright yellow sphere, the demon attempts to disrupt his spellcasting with a quick swipe of its sharp talon-like fingers. But before the demon can lay a finger on Allen, a mighty jaw protrudes from his wrist, stopping the attack abruptly.</p><p>Then Allen releases the charge. He hurtles all the built-up energy towards the demon in a sparkling explosion of blinding electricity. As the demon is hit with spasms of electrical energy, the lightning storm greedily consumes everything it can reach with it's bouncing tendrils. A smoldering mess and several severely burnt pieces of lab equipment remain in its wake.</p>");
        break;
    }
    $("#allen #message").append("<p>Then Aldred, as if right on cue, jumps straight into action. With the demon busy dealing with Allen, Aldred decides to take advantage of the situation.</p>");
    showSubs("#allen #message", 1000);
    fadeInSubs("#aldred", 2500);
    $("#aldred").delay(2250).show();
}

function aldred(i) {
    hideSubs("#aldred", 0);
    $("#aldred #message").empty();
    path += i;
    switch (i) {
    case 0:
        $("#aldred #message").append("<p>Aldred is a ranger. The demon knows this. And so when Aldred came charging into combat it was already unexpected. But Aldred had a trick up his sleeves the demon didn't know about. Literally. Within the folds of his tunic he held firmly a holy symbol crafted of pure platinum. It's a wonder the demon didn't notice it earlier.</p><p>And so when Aldred charged up at the demon, he came unexpected, easily tackling himself towards the nearest free appendage and holding on. As the demon delt with Bob and Allen, he didn't have time to notice his powers slowly draining as the symbol began to purify the closest evil.</p>");
        break;
    case 1:
        $("#aldred #message").append("<p>Aldred decided he'd had enough with single shots. They didn't do much and were not very interesting ... continually firing off bolts got boring after the first few. So instead, Aldred pulls out two bolts. The demon may have scoffed at the idea of firing two bolts with a single bow had he not been preoccupied with Allen and Bob.</p><p>But then, Aldred pulled out his second surprise - a second single-handed semi-automatic crossbow of his own invention. With two synchronous clicks from both his left and right hand, Aldred fires off the expected bolt, and the not-so-expected bolt. The demon, with little time to react to as much as one bolt, is struck down a second time as two bolts land their mark.</p>");
        break;
    case 2:
        $("#aldred #message").append("<p>Aldred wished he had time to apply the fire gel to his bolts. After all, the last time he did that, his bolts flew miraculously straight before disintegrating in an explosion of fire. As such, Aldred was a planner. A thinker. An experimenter even. But when he came upon a situation in which he did not have much time to plan, he decided he might as well take up improv for once in his plans.</p><p>That's when Aldred, rather than executing perfectly the instructions stated on the vial, of applying a small thumb-sized amount to the tips of arrows, instead tossed the entire bottle at the demon. Aldred was distraught. Never once had he performed such beautifully well-defined instructions so poorly. And while he admonished himself in the corner, he missed a spectacular display of fireworks as several dozen 'serving sizes' worth of fire gel spontaneously combusted on the demon's face. Aldred was furious. And so was the demon. Though likely for very differing reasons.</p>");
        break;
    }
    $("#aldred #message").append("<p>Soon after Aldred delivers his surprise, Solen makes his move from behind the demon (where was he this whole time anyways?).</p>");
    showSubs("#aldred #message", 1000);
    fadeInSubs("#solen", 2500);
    $("#solen").delay(2250).show();
}

function solen(i) {
    hideSubs("#solen", 0);
    $("#solen #message").empty();
    switch (i) {
    case 0:
        $("#solen #message").append("<p>With his recently acquired Powder of Dryness, Solen decides the demon needs a makeover. Of course Solen is the master of disguise and an expert at applying foundation and blending. So with several exquisite and liberal motions of his arms, Solen begins powdering the demon with generous amounts of Powder of Dryness\u2122.</p><p>The bluish substance lightly dusts the crimson skin, highlighting various redish hues of the demon. But the demon being a fire-based creature, was practically already nice and dry. And so Solen learned his first lesson on applying excessive amounts of powder before first applying a healthy amount of foundation.</p>");
        break;
    case 1:
        $("#solen #message").append("<p>Solen decides that his newest recipe of the freshest loaf of bread was worthy of Food Channel Star. To prove its worth, he would show that even demons could enjoy the steamy buns. And so he presents his finest loaf to the demon, offering a generous slice from the loaf. The sweet scent of truth is almost irresistible. In fact, Solen might as well consume the loaf himself. Yes. It was that good.</p><p>The demon seemed to enjoy the bread. And by enjoy the bread I mean he took entire loaf in all its glory. And by took the entire loaf I mean he took Solen along for the ride too. How delicious. How very delicious indeed.</p>");
        break;
    case 2:
        $("#solen #message").append("<p>Solen has had enough with violence. Killing was bad, and he knew it. He couldn't let this poor demon die for nothing. No. He decided that this demon was a worthy and sentient creature that deserved to live. Solen was a priest. He would forgive all of the demon's sins. He would forgive it until it died. And so Solen began to preach. His first sermon, of course, started with a 'How are you sir? Don't you have a family?'</p><p>As his conversation led him deeper into the daily life of a demon, and the number of miracles he promised increased exponentially, Solen decided, that it was a good time to befriend the demon. Then he could decrease the world's killing. One life at a time. Of course afterwards Solen would come to realize, that saving the demon's life may not have been the best decision, as it would make further encounters with civilization ... brusque, to say the least.</p>");
        break;
    case 3:
        $("#solen #message").append("<p>Solen was against all forms of physical force. He had designed a custom defense protocol dictated by some senseless lady. Therefore, Solen turned to other alternatives. Then Solen knew what he could do. He would engage psychological options. Solen had done this before. Solen v1 Solen v10 Solen v5 Solen v19. Solen v6 Solen v8 Solen v17 Solen v3 Solen v25. Solen v4 Solen v13 Solen v9. ...</p><p>And by then, Solen had inputted several other Solens to create a new combined psychy. Solen was filled with an incredible amount of data. Solen was - Ah. It seems his psychy countermeasure has extended so far as to affect the mind of the writer. Solen Solen Solen Solen.</p>");
        break;
    case 4:
        $("#solen #message").append("<p>Solen knew where he was. Maybe nobody else did. But that's exactly what he wanted. He had been hiding in an adjacent lab room. Invisible. Yes, that's why nobody else knew. And so when he peered around the corner to investigate the ruckus his obnoxious teammates had been causing, he was more than surprised to find a demon standing in front of him.</p><p>He needed an excuse. Scanning through all possibilities of retorts he could say, he finally landed on one that would likely result in the greatest success. 'I need a nap.'</p><p>And so Solen returned to the safety of the magic cloak he had been wearing. The innards were woven with a strange magic silk that caused light to bend away from the subject. Indeed, no light every came in contact with the cloth. Thus in the blackness, Solen was invisible. For of course, if he could see nothing, then logically, the world could see nothing where he stood. A lightless figure of pure invisibility.</p>");
        break;
    }
    $("#solen #message").append("<p>Herp decided he had seen enough of Solen being Solen. For Solen was Solen and being Solen was second nature to Solen. Herp turned away from Solen, and instead faced the demon.</p>");
    showSubs("#solen #message", 1250);
    fadeInSubs("#herp", 3000);
    $("#herp").delay(2500).show();
}

function herp(i) {
    hideSubs("#herp", 0);
    $("#herp #message").empty();
    path += i;
    switch (i) {
    case 0:
        $("#herp #message").append("<p>Herp realizes that the demon most likely lived in a world of fire and burning inferno deep underground. And so it wasn't hard to see that ice was it's weakness. So Herp transformed himself into his natural form - a winterwolf. Then, he begans casting an ice beam spell. Being across the laboratory from the demon, there was no way the demon could disrupt the spell. And so the demon attempts to dodge the beam of ice. Unfortunately, Herp expects this and adjusts his aim to compensate. Then he unleashes a wild flurry of ice and snow that envelopes the demon in frost, dimming the bright glowing red into a dull bluish-brown.</p>");
        break;
    case 1:
        $("#herp #message").append("<p>Herp understands physics concepts, and so if he charged at the demon while in human form, he could gain enough speed such that when he transformed into winterwolf form, his charge would be unstoppable. And that's exactly what the demon saw. Or rather didn't see coming. An blur of white fur and blue ice charging somewhat like a hedgehog towards the demon. Collision! As the demon is knocked back into the hard reinforced walls of the laboratory.</p>");
        break;
    case 2:
        $("#herp #message").append("<p>Herp had been munching away on his delicious psychedelic toads the entire battle. And then he stopped briefly, before seeing in front of him the solution to their problem. A magic butterfly. Not just any kind, however. This was a genie butterfly. And he could get three wishes with it. And so he chased the butterfly around the room until he could catch it.</p><p>Suddenly, the butterfly began to speak. The genie-butterfly was so generous that it said it would let Herp capture him on one condition. Herp must be a nice doge and share his toads with the demon. Only then, would the genie answer to his wishes. And so Herp obediently followed the command. Like the loyal dog he was, he began gifting various toads to the demon. And then everyone was licking toads. And Herp. Herp was happy.</p>");
        break;
    }
    $("#herp #message").append("<p>While Herp acted like the doge he was, Lucian turned up from his Pokedex, satisfied of the scanning he had done to search for nearby Pokemon.</p>");
    showSubs("#herp #message", 1000);
    fadeInSubs("#lucian", 2500);
    $("#lucian").delay(2250).show();
}

function lucian(i) {
    hideSubs("#lucian", 0);
    $("#lucian #message").empty();
    path += i;
    switch (i) {
    case 0:
        $("#lucian #message").append("<p>Lucian takes out all of his Pokeball. Then he calls out all of his pets to attack the demon as one. 'Triple Finish' he calls out, even though he actually has four Pokemon. And then they all jump on the Demon as one.</p>");
        break;
    case 1:
        $("#lucian #message").append("<p>Lucian begins mashing keys on his Pokedex. He looks like he is hacking the matrix. Hacking his way out of this sticky situation. The truth is, he's spamming the 'Flee Battle' button. But it isn't working. Maybe he needs a Pokemon out that would take a hit before escaping. It still doesn't work. He sighs at the sad reality that is life.</p>");
        break;
    case 2:
        $("#lucian #message").append("<p>Lucian understands that Pokemon that have lower health are much easier to capture than those of higher health. So when he saw the demon was at critical health, he decided the best move was to throw a Pokeball and capture it. Fortunately for Lucian, the demon was in no mood to resist a flying plastic ball aimed not even at hits head. Unfortunately for Lucian, the Pokeball didn't want to accept the demon as a valid Pokemon.</p>");
        break;
    }
    $("#lucian #message").append("<p>Just then, the wall all of sudden crumbled into a cloud of dust, revealing a tunnel that reached to the outside. Now all they hoped was that the demon had faced a similar fate.</p>");
    showSubs("#lucian #message", 1000);
    setTimeout(resAftermath, 5000, false);
}

function die(i) {
    hideSubs("#die", 0);
    $("#die #message").empty();
    switch (i) {
    case 1:
        $("#die #message").append("<p>No. That would not be right. Too much chance of failure. The party would have to manually deal with the demon, trapping it within the lab while the chaotic energies combined to form a devastating explosion. A martyr of society for some perhaps. A fool's decision others may say. But whatever the consequences, the party needed to do something. And fast.</p><p>As Herp, Aldred and Lucian formed a barrier at the entrance of the tunnelway, blocking the demon in, Allen rushed towards the opposite side of the room. Flipping open the control panel, he quickly flipped as many switches as he could. The silent whirring of energy began to grow louder as arcane energies surged towards various rooms of the lab, powering up various machines and experiments. Within seconds, there are sounds of mechanical failures, as alarms blare and various gauges mark numbers off their designed scale.</p><p>Perhaps out of true kindness, Solen refrained from saying 'Think about your children'. Or he may have just been looting the rest of the building, having not noticed the party's plan. Whatever he was doing, it was not for long, as several seconds later, a rush of purple plasmic energy rushed from the depths of the lab upwards, incinerating everything in its path. There was no hope for anything. All organic matter, and much inorganic matter, was consumed in the fury of unleashed energy surging across planar dimensions. The waves of energy pulsated for several long minutes before fading out; the beacon stretching into space could be seen across the entire island.</p><p>And then there was silence.</p><tt>Ending 1/5</tt>");
        break;
    case 2:
        $("#die #message").append("<p>Yes, that seemed like the safer plan. Highest chance of survival. It was a simple process of throwing down a large amount of explosives, running out of the range, and waiting for the planar energies to consume the demon. And so the party tossed down all their alchemical substances and explosives before rushing out the freshly dug tunnel.</p><p>It was a quick and easy plan. Fool-proof. Of course the demon was no fool. He was not willing to let his dinner escape so easily. So rather than release his anger on all the lab equipment around him, he decided to conserve his energy and follow the party down the tunnel. It wasn't until the party reached the surface that they discovered of the five original members only 3 remained. And then there was a demon quickly approaching from behind, it's maw covered in the bright crimson of fresh blood. No. There was little hope for the party.</p><p>Soon after that day, there would be a search for the missing party members. Later that week, reports of a wild monster stealing children would spread across the islands. Later that month, the combined experiments of the lab would begin tearing apart the island of Slerth, as a gradually expanding ball of planar energies engulfed city after city. Then come another month and the party would slowly fade from people's memories, or otherwise be remembered as the one's who tried. Perhaps in the future, another party of brave adventurers would show up and pick up from where the last party had left off.</p><p>Or perhaps not.</p><tt>Ending 2/5</tt>");
        break;
    }
    showSubs("#die #message", 750);
}

function live(i) {
    hideSubs("#live", 0);
    $("#live #message").empty();
    switch (i) {
    case 1:
        $("#live #message").append("<p>It was a chance they were not willing to miss. And so the party began to carefully attune the portal's frequency, aligning the planar frequencies until the portal was stable enough for safe traversal. Of course, it would only be stable for a few minutes before it needed to be retuned. But of course by then the party would already be deep within the portal.</p><p>If you happened to have been travelling near that area, you might have seen a bright purple flash emitted from a strange perfect tunnel. If you have been scrying on the party, you may have just experienced an interruption to your video feed. If you happened to be passing through any nearby subterranean tunnels, you may have heard the whoosh and pop that is travelling across planes.</p><p>There were five. And then there were none.</p><tt>Ending 3/5</tt>");
        break;
    case 2:
        $("#live #message").append("<p>Alas, it was too risky. Who knows what may become of the portal? What if the lab imploded upon itself, destroying the nearby forest. Or even worse, exploded in a spectacular flood of planar energies. No. The portal needed to be shut down. And so, with careful precision, the party began shutting off various power sources. And the portal. The portal shrank with each flick of a switch. Until it was no bigger than a shield. Then a dagger's length wide. Then palm sized. Then, before it could get much smaller, the rim of the portal began to fade, the hard-cut previously-bright edge blending in with the world. Then, yes, then it was gone.</p><p>The party headed out the tunnel and towards the nearest city. The region was put on quarantine, and access the the area blocked. Reports were sent out around the islands noting recent strange events and gathering scientists from across the world. And the party. Well, the party had a choice.</p>");
        fadeInSubs("#finale", 2500);
        $("#finale").delay(2250).show();
        break;
    }
    showSubs("#live #message", 750);
}

function finale(i) {
    hideSubs("#finale", 0);
    $("#finale #message").empty();
    switch (i) {
    case 1:
        $("#finale #message").append("<p>They had a stake now in the scheme of things. And when those guardians no doubt return, the party will be their first target. And so, the party joined the growing force of scientists at the Academy of Magics and Sciences to help find a solution. With their newfound reputation, the party commanded a growing respect, leading new projects at the Academy and setting out on greater expeditions. At this rate, they may very likely finally put an end to the disappearing islands and mysterious mutations affecting animals.</p><tt>Ending 4/5</tt>");
        break;
    case 2:
        $("#finale #message").append("<p>It is likely the guardians already expected the party to be dead. And so the best course of action was to hide the encounter and set themselves apart from recent events. Leave no evidence - an untraceable trail. And so the party continued their adventuring schemes. Allen continued to train Bob, reinforcing their bond until they were spiritually one. Aldred eventually joined him to learn the ways of the wilds. And together, the party enjoyed nature more for what it was worth, rather than seeking out hidden loot. Solen took the red gem back for research. Along with Herp, the two of them discovered that the red gem was used to animate creatures. As a result, the demon may not even have been real, maybe just a puppet controlled from elsewhere. Either way, Herp eventually used it to animate his gingerbread man, creating an arcane familiar companion that would faithfully help him along the journey. Then Lucian continued exploring all the various locations the world had to offer, capturing Pokemon when there were, and searching for the cure to the strange mutations.</p><p>And for now, the party was safe.</p><tt>Ending 5/5</tt>");
        break;
    }
    showSubs("#finale #message", 750);
}

function resAftermath(warn) {
    if (warn && !confirm("Are you sure you want to restart the aftermath from after the battle?")) return;

    $("#aftermath").find("div #message").empty();
    $("#aftermath").find("div").hide();
    if (path == 0) {
        showSubs("#epiloguelive", 0);
        $("#epiloguelive").show();
        fadeInSubs("#live", 2500);
        $("#live").show();
    } else {
        showSubs("#epiloguedie", 0);
        $("#epiloguedie").show();
        fadeInSubs("#die", 2500);
        $("#die").show();
    }
    $("#aftermath").show();
}

function resBattle(warn) {
    if (warn && !confirm("Are you sure you want to restart everything from the beginning?")) return;
    
    path = 0;
    
    $("#battle").find("div #message").empty();
    $("#battle").find("div").hide();
    $("#aftermath").hide();
    
    showSubs("#prologue", 0);
    $("#prologue").show();
    
    $("#battle").delay(2000).show(500);
    
    fadeInSubs("#allen", 2500);
    $("#allen").delay(2250).show();
}

function begin() {
    $("#begin").fadeOut(1000);
    setTimeout(resBattle, 1000, false);
}
