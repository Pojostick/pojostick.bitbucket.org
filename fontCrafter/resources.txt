Resizable panels:
http://jsfiddle.net/QkZL8/15/
http://jsfiddle.net/1v3obzjx/

Opentype.js Creating Font example
https://github.com/nodebox/opentype.js/blob/master/examples/creating-fonts.html
http://nodebox.github.io/opentype.js/examples/font-editor.html

Seed random
http://davidbau.com/encode/seedrandom.js

JSON Editor
https://github.com/josdejong/jsoneditor

EXAMPLES:
Decimate:
glyph #8 percent
Set percent (56-16)/56*100
Removes 16 points of least relevance = the control points