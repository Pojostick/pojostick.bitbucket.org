const BG = "#252728";
const FG = "#525758";
const BEIGE = "#ffeedd";
const SELECT = "#ff8500";
const INVERT = "#007aff";

const MODE_VIEW = "view";
const MODE_EDIT = "edit";

const MODIFIER_SUBDIV = "subdiv";
const MODIFIER_DECIMATE = "decimate";
const MODIFIER_DISPLACE = "displace";
const MODIFIER_ALIGN = "align";

const SUBDIV_EXACT = "Exact";
const SUBDIV_SMOOTH = "Smooth";

const DISPLACE_RANDOM_X = "Random X-axis";
const DISPLACE_RANDOM_Y = "Random Y-axis";
const DISPLACE_RANDOM_XY = "Random XY-plane";
const DISPLACE_RANDOM_N = "Random Normal";

var DELTA = 5;

const ISECT_SQUARE = "square";
const ISECT_CIRCLE = "circle";

var BVHSEGMENTS = 8;

const SELECT_NONE = "none";
const SELECT_READY = "boxready"
const SELECT_BOX = "box";
const SELECT_CIRCLE = "brush";

const SQRT2 = 1.41422;
const SQRT3 = 1.73206;

const EPS_D = 0.00001;
const MAX_FONT_SIZE = 100000;

// Position
var lastX = 0, lastY = 0, posX = 0, posY = 0;
var lastMouseX = 0, lastMouseY = 0, mouseX = 0, mouseY = 0, mouseButton = 0;
var beginMouseX = 0, beginMouseY = 0, dMouseX = 0, dMouseY = 0;
var clickElement;
var ctx, canvas, editor, overlayCtx, overlayCanvas;
var viewCtx, viewCanvas, viewControl, viewLabel, viewer;
var selectCanvas, selectControl;
var change = true;
var guiChanged = true;
var viewChanged = true;
var recalculate = true;
var reset = true;
var mode = MODE_VIEW;

var untitledNum = 1;
var fontsList = {}; // info is dict with font, savefile, change flag, intersection
var fontSelected;

var dialog = false;

var modifiersList = {};
makeModifier = function(modifier) {
    var id = new Date().getTime();
    var html = ""
    if (modifier == MODIFIER_SUBDIV) {
        html = "<li id=\"modifier-" + id + "\"><a href='javascript:void(0)' onclick='confirmDeleteModifier(\"" + id + "\")'>Subdivide</a> <a href='javascript:void(0)' id='subdiv' onclick='setModifier(\"" + id + "\", \"subdiv\")' style='padding-left: 16px;'>Exact interpolation</a> <a href='javascript:void(0)' id='recurse' onclick='setModifier(\"" + id + "\", \"recurse\")' style='padding-left: 16px;'>Recurse x1</a><br></li>";
    } else if (modifier == MODIFIER_DECIMATE) {
        html = "<li id=\"modifier-" + id + "\"><a href='javascript:void(0)' onclick='confirmDeleteModifier(\"" + id + "\")'>Decimate</a> <a href='javascript:void(0)' id='percent' onclick='setModifier(\"" + id + "\", \"percent\")' style='padding-left: 16px;'>100.000%</a><br></li>";
    } else if (modifier == MODIFIER_DISPLACE) {
        html = "<li id=\"modifier-" + id + "\"><a href='javascript:void(0)' onclick='confirmDeleteModifier(\"" + id + "\")'>Displace</a> <a href='javascript:void(0)' id='texture' onclick='setModifier(\"" + id + "\", \"texture\")' style='padding-left: 16px;'>Random X-axis texture</a> <a href='javascript:void(0)' id='seed' onclick='setModifier(\"" + id + "\", \"seed\")' style='padding-left: 16px;'>Seed: 0</a> <a href='javascript:void(0)' id='midpoint' onclick='setModifier(\"" + id + "\", \"midpoint\")' style='padding-left: 16px;'>Midpoint: 50.000%</a> <a href='javascript:void(0)' id='strength' onclick='setModifier(\"" + id + "\", \"strength\")' style='padding-left: 16px;'>Strength: 1.000</a><br></li>";
    } else if (modifier == MODIFIER_ALIGN) {
        html = "<li id=\"modifier-" + id + "\"><a href='javascript:void(0)' onclick='confirmDeleteModifier(\"" + id + "\")'>Align to Grid</a> <a href='javascript:void(0)' id='scale' onclick='setModifier(\"" + id + "\", \"scale\")' style='padding-left: 16px;'>Grid scale: 1.000</a> <a href='javascript:void(0)' id='align' onclick='setModifier(\"" + id + "\", \"align\")' style='padding-left: 16px;'>Strength: 0.500</a><br></li>";
    }
    return {id: id,
            data: {type: modifier,
                   recurse: 1,
                   subdiv: SUBDIV_EXACT,
                   percent: 100,
                   texture: DISPLACE_RANDOM_X,
                   seed: 0,
                   strength: 1,
                   midpoint: 50,
                   scale: 1,
                   align: 0.5},
            html: html};
};

var currentPangram = "", previousPangram = "";

var glyphSelection = {};

var vertSelection = [];
var selectionOrigin = { x : 0 , y : 0 };

var colorFill = "#808080";
var colorStroke = "#000000";
var widthStroke = 1;

var originX = 0, originY = 0, viewOriginX = 0, viewOriginY = 0;
var fontSize = 256, viewFontSize = 72, fontScale = 1;

var cellWidth = 48, cellHeight = 32, cellMargin = 1;

var saving = false;

var shift = false, ctrl = false, alt = false;

var vertCount, lineCount, curveCount, bezierCount;
var verts, vertsTotal, linesTotal, curvesTotal, beziersTotal;

var bvh = {};

var debug = false;

var progressbar, progressLabel, glyphCount = 0, glyphId = 0;

var selectMode = SELECT_NONE;

var frames = [], frameIndex = 0, fps = 60;
for (var i = 0; i < 60; i++) {
    frames[i] = 0;
}

var pangrams = ["Waltz, bad nymph, for quick jigs vex.",
                "Fox nymphs grab quick-jived waltz.",
                "Quick zephyrs blow, vexing daft Jim.",
                "Sphinx of black quartz, judge my vow.",
                "Fickle bog dwarves jinx empathy quiz.",
                "Five hexing wizard bots jump quickly.",
                "Five quacking zephyrs jolt my wax bed.",
                "The five boxing wizards jump quickly.",
                "Jackdaws love my big sphinx of quartz.",
                "Pack my box with five dozen liquor jugs.",
                "The jay, pig, fox, zebra and my wolves quack?",
                "Fix problem quickly with galvanized jets.",
                "The quick brown fox jumps over the lazy dog.",
                "Woven silk pyjamas exchanged for blue quartz.",
                "The quick onyx goblin jumps over the lazy dwarf.",
                "Amazingly few discotheques provide jukeboxes.",
                "Cozy lummox gives smart squid who asks for job pen.",
                "Have a pick: twenty-six letters - no forcing a jumbled quiz.",
                "Few black taxis drive up major roads on quiet hazy nights.",
                "Grumpy wizards make toxic brew for the evil queen and jack.",
                "A quick movement of the enemy will jeopardize six gunboats.",
                "All questions asked by five watched experts amaze the judge.",
                "The wizard quickly jinxed the gnomes before they vaporized.",
                "Zelda might fix the job growth plans very quickly on Monday."];

const TOOL_NONE = 0, TOOL_EDIT = 1, TOOL_DONE = 2;
const MASS_NONE = 0, MASS_LINEAR = 1, MASS_QUAD = 2, MASS_SMOOTH = 3, MASS_SHARP = 4;

var gState = TOOL_NONE, rState = TOOL_NONE, sState = TOOL_NONE;
var hState = false;
var mState = MASS_NONE;
var selectRadius = 10 * DELTA;

var recalculating = false;

const CONSTRAIN_NONE = 0, CONSTRAIN_X = 1, CONSTRAIN_Y = 2, CONSTRAIN_BOTH = 3;

var constrain = CONSTRAIN_NONE;

var angle = 0, distance = 1;

var number = null, decimal = false, negative = false;

var confirmModal = false;

var beginBoxX = 0, beginBoxY = 0;

var flipped = false;

var handles = {};

var modifiedGlyphs = {};
var modified = false;

// Setup requestAnimationFrame
(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
                                   || window[vendors[x]+'CancelRequestAnimationFrame'];
    }
 
    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
 
    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());

// Escape HTML
function escapeHtml(unsafe) {
    return unsafe
         .replace(/&/g, '&amp;')
         .replace(/</g, '&lt;')
         .replace(/>/g, '&gt;')
         .replace(/\u0022/g, '&quot;')
         .replace(/\u0027/g, '&#039;');
}

// Run all loading scripts after document is ready
$(document).ready(function() {
    // Begin applet loading dialog timer and prevent other loading dialogs from showing
    showAppletLoading();
    
    // Setup utility functions for transforms
    setupTransforms();
    
    // Setup utility functions for mesh editing
    setupMeshEdits();
    
    // Setup selection functions
    setupSelects();
    
    // Setup Window Listeners
    addWindowListeners();
    
    // Setup Dialogs
    addDialogHooks();
    
    // Resizeable
    setupResizeables();

    // Menu
    setupMenu();
    
    // Position
    addPositionEventHandlers();
    
    // Tools
    setupButtons();
    
    // Editor
    setupEditor();
    
    // Keybindings
    setupKeybindings(editor);
    
    // Viewer
    setupViewer();
    
    // Fonts list
    setupFontsList();
    
    // Modifiers dropdown
    setupModifiers();
    
    // JSON editor
    setupPropertyEditor();
    
    // Begin draw sequence
    redraw()
    
    // Load default font
    makeDefaultFont("Untitled 1", true);
    untitledNum++;
});

showAppletLoading = function() {
    var splash = -1;
    var lastSplash = -1;
    
    var splashText = "";
    var splashIndex = 0;
    
    var appletLoad = $(".applet-percent");
    var appletText = $(".applet-text");
    var loadValue = 0;
    
    var iteration = 0;
    var finished = false;
    
    var timedOut = setTimeout( function() {loadValue = 100;}, 25000 );
    var typing;
    
    newSplash = function() {
        lastSplash = splash;
        for (var i = 0; i < 10 && lastSplash == splash; i++) {
            splash = Math.floor(Math.random() * pangrams.length);
        }
        splashText = pangrams[splash];
    }
    
    newSplash();
    
    dialog = true;
    $( "#dialog-load-applet" ).dialog({
        modal: true,
        resizable: true,
        closeOnEscape: true,
        width: "90%",
        buttons: {
            "Loading...": {
                text: "Loading...",
                id: "load-applet-button",
                click: function() {
                    $(".ui-icon-closethick").trigger("click.close");
                }
            }
        },
        open: function() {
            $(this).parent().find(".ui-button").addClass("dialog-button-bg");
            $(".ui-widget-overlay").css({
                "opacity": "1",
                "filter": "Alpha(Opacity=100)",
            });
            $('.ui-widget-overlay', this).hide().fadeIn();
            $(".ui-widget-overlay").bind("click", function(event){
                $(".ui-icon-closethick").trigger("click.close");
                if (shift && ctrl && alt && event.which === 2) loadValue = 100;
            });
            $('.ui-icon-closethick').bind('click.close', function () {
                if (finished) {
                    $('.ui-widget-overlay').fadeOut(function () {
                        $('.ui-icon-closethick').unbind('click.close');
                        $('.ui-icon-closethick').trigger('click');
                        $("#cover").css({opacity: "0"});
                        setTimeout(function() {$("#cover").css({display: "none"});}, 1000);
                        clearTimeout(typing);
                    });
                }
                return false;
            });
        },
        beforeClose: function() {
            dialog = !finished;
            return finished;
        },
        show: "scale",
        hide: "scale"
    });
    $( "#applet-bar" ).progressbar({
        value: false
    });
    $(".ui-progressbar-value").css({width: "100%"});

    typing = setTimeout(function progress() {
        $( "#applet-bar" ).progressbar("value", loadValue);
        
        if (loadValue >= 100 && !finished) {
            clearTimeout(timedOut);
            $( "#dialog-load-applet" ).dialog("option", "title", "Finished loading Font Crafter! Click anywhere to start.");
            $("#load-applet-button > .ui-button-text").text("Let's get crafting!");
            finished = true;
        }
        var random = Math.random();
        var ms = 20;
        var message;
        if (finished) {
            message = "(Complete) ";
        } else {
            if (iteration % 90 < 15) message = "(:...) ";
            else if (iteration % 90 < 30) message = "(.:..) ";
            else if (iteration % 90 < 45) message = "(..:.) ";
            else if (iteration % 90 < 60) message = "(...:) ";
            else if (iteration % 90 < 75) message = "(..:.) ";
            else message = "(.:..) ";
        }

        if (loadValue < 100) {
            if (random < 0.5) {
                if (++splashIndex > splashText.length + 50) {
                    splashIndex = 0;
                    newSplash();
                }
                if (splashIndex < splashText.length) ms = 50;
            }
        } else {
            if (++splashIndex > splashText.length + 50) {
                splashIndex = 0;
                newSplash();
            }
            if (splashIndex < splashText.length) ms = 50;
        }
        if (splashIndex > splashText.length + 30) {
            message += splashText.substring(0, (50 - splashIndex + splashText.length) / 20 * splashText.length);
        } else if (splashIndex >= splashText.length) {
            message += splashText;
        } else {
            message += splashText.substring(0, splashIndex);
        }
        loadValue += random > 0.8 ? 2 * Math.random() : 0.5 * random * Math.random();
        loadValue = Math.min(loadValue, 100);
        appletLoad.text(loadValue.toFixed(1) + "%");
        appletText.text(message);
        iteration += ms / 10;
        typing = setTimeout(progress, ms);
    }, 1000);
}

setupTransforms = function() {
    rotate = function(a) {
        var cos = Math.cos(a);
        var sin = Math.sin(a);
        return function(pos) {
            return {x: cos * pos.x - sin * pos.y,
                    y: sin * pos.x + cos * pos.y};
        };
    }
    scale = function(dx, dy) {
        return function(pos) {
            return {x: dx * pos.x,
                    y: dy * pos.y};
        };
    }
    normalize = function(vector) {
        var vecLen = Math.sqrt(vector.x * vector.x + vector.y * vector.y);
        if (vecLen > 0) {
            return scale(1 / vecLen, 1 / vecLen)(vector);
        }
        return vector;
    }
    move = function(cons, prevX, prevY, nextX, nextY, prevA, nextA) {
        if (gState === TOOL_EDIT) {
            var dx = cons === CONSTRAIN_Y ? 0 : (nextX - prevX) * fontScale;
            var dy = cons === CONSTRAIN_X ? 0 : (prevY - nextY) * fontScale
            for (var key in vertSelection) {
                var pos = getPos(vertSelection[key]);
                pos.x += dx;
                pos.y += dy;
                setPos(vertSelection[key], pos.x, pos.y);
            }
            selectionOrigin.x += dx;
            selectionOrigin.y += dy;
            posX = selectionOrigin.x;
            posY = selectionOrigin.y;
            updatePosition();
            change = true;
        } else if (rState === TOOL_EDIT) {
            var x = originX + posX / fontScale;
            var y = originY - posY / fontScale;
            if (prevA === undefined) {
                prevA = Math.atan2(y - prevY, prevX - x);
                nextA = Math.atan2(y - nextY, nextX - x);
            }
            var a = nextA - prevA;
            var transform = rotate(a);
            for (var key in vertSelection) {
                var pos = getPos(vertSelection[key]);
                pos.x -= posX;
                pos.y -= posY;
                pos = transform(pos);
                setPos(vertSelection[key], pos.x + posX, pos.y + posY);
            }
            change = true;
        } else if (sState === TOOL_EDIT) {
            var x = originX + posX / fontScale;
            var y = originY - posY / fontScale;
            var d = Math.sqrt((x - nextX) * (x - nextX) + (y - nextY) * (y - nextY)) / Math.sqrt((x - prevX) * (x - prevX) + (y - prevY) * (y - prevY));;
            var dx = cons === CONSTRAIN_Y ? 1 : d;
            var dy = cons === CONSTRAIN_X ? 1 : d;
            var transform = scale(dx, dy);
            for (var key in vertSelection) {
                var pos = getPos(vertSelection[key]);
                pos.x -= posX;
                pos.y -= posY;
                pos = transform(pos);
                setPos(vertSelection[key], pos.x + posX, pos.y + posY);
            }
            change = true;
        }
    }

    undo = function(cons, amount) {
        var dx, dy, a, d;
        var x = originX + posX / fontScale;
        var y = originY - posY / fontScale;
        if (amount === undefined) {
            dx = cons === CONSTRAIN_X ? 0 : dMouseX * fontScale;
            dy = cons === CONSTRAIN_Y ? 0 : -dMouseY * fontScale;
            a = Math.atan2(y - mouseY, mouseX - x) - angle;
            d = distance / Math.sqrt( (x -= mouseX) * x + (y -= mouseY) * y );
            if (negative) d *= -1;
        } else {
            dx = cons === CONSTRAIN_X ? 0 : amount;
            dy = cons === CONSTRAIN_Y ? 0 : -amount;
            a = amount * Math.PI / 180;
            d = amount;
        }
        if (gState === TOOL_EDIT) {
            for (var key in vertSelection) {
                var pos = getPos(vertSelection[key]);
                pos.y -= dy;
                pos.x -= dx;
                setPos(vertSelection[key], pos.x, pos.y);
            }
            selectionOrigin.x -= dx;
            selectionOrigin.y -= dy;
            posX = selectionOrigin.x;
            posY = selectionOrigin.y;
            updatePosition();
            change = true;
        } else if (rState === TOOL_EDIT) {
            var transform = rotate(-a);
            for (var key in vertSelection) {
                var pos = getPos(vertSelection[key]);
                pos.x -= posX;
                pos.y -= posY;
                pos = transform(pos);
                setPos(vertSelection[key], pos.x + posX, pos.y + posY);
            }
            change = true;
        } else if (sState === TOOL_EDIT) {
            if (amount == 0) return;
            var sx = cons === CONSTRAIN_X ? 1 : d;
            var sy = cons === CONSTRAIN_Y ? 1 : d;
            var transform = scale(sx, sy);
            for (var key in vertSelection) {
                var pos = getPos(vertSelection[key]);
                pos.x -= posX;
                pos.y -= posY;
                pos = transform(pos);
                setPos(vertSelection[key], pos.x + posX, pos.y + posY);
            }
            change = true;
        }
    }

    redo = function(cons, amount) {
        var x = originX + posX / fontScale;
        var y = originY - posY / fontScale;
        if (amount === undefined) {
            dx = cons === CONSTRAIN_X ? 0 : dMouseX * fontScale;
            dy = cons === CONSTRAIN_Y ? 0 : -dMouseY * fontScale;
            a = Math.atan2(y - mouseY, mouseX - x) - angle;
            d = Math.sqrt( (x -= mouseX) * x + (y -= mouseY) * y ) / distance;
            if (negative) d *= -1;
        } else {
            dx = cons === CONSTRAIN_X ? 0 : amount;
            dy = cons === CONSTRAIN_Y ? 0 : -amount;
            a = amount * Math.PI / 180;
            d = 1 / amount;
        }
        if (gState === TOOL_EDIT) {
            for (var key in vertSelection) {
                var pos = getPos(vertSelection[key]);
                pos.y += dy;
                pos.x += dx;
                setPos(vertSelection[key], pos.x, pos.y);
            }
            selectionOrigin.x += dx;
            selectionOrigin.y += dy;
            posX = selectionOrigin.x;
            posY = selectionOrigin.y;
            updatePosition();
            change = true;
        } else if (rState === TOOL_EDIT) {
            var transform = rotate(a);
            for (var key in vertSelection) {
                var pos = getPos(vertSelection[key]);
                pos.x -= posX;
                pos.y -= posY;
                pos = transform(pos);
                setPos(vertSelection[key], pos.x + posX, pos.y + posY);
            }
            change = true;
        } else if (sState === TOOL_EDIT) {
            if (amount == 0) return;
            var sx = cons === CONSTRAIN_X ? 1 : d;
            var sy = cons === CONSTRAIN_Y ? 1 : d;
            var transform = scale(sx, sy);
            for (var key in vertSelection) {
                var pos = getPos(vertSelection[key]);
                pos.x -= posX;
                pos.y -= posY;
                pos = transform(pos);
                setPos(vertSelection[key], pos.x + posX, pos.y + posY);
            }
            change = true;
        }
    }
}

setupMeshEdits = function() {
    subdivide = function(fonts, selection) {
        var flipOffset = flipped ? 1 : 0;
        var newSelection = [], fromVerts = {}, toVerts = {}, edited = {};
        for (var key in selection) {
            var vert = selection[key];
            if (vert.command - flipOffset < 0 || +vert.command + 1 - flipOffset >= fonts[vert.font].font.glyphs.glyphs[vert.id].path.commands.length) continue;
            var command = fonts[vert.font].font.glyphs.glyphs[vert.id].path.commands[+vert.command + 1 - flipOffset];
            if (vert.index != "") continue;
            if (!(vert.font in edited)) {
                edited[vert.font] = {};
                fromVerts[vert.font] = {};
                toVerts[vert.font] = {};
            }
            if (!(vert.id in fromVerts[vert.font])) {
                fromVerts[vert.font][vert.id] = {};
                toVerts[vert.font][vert.id] = {};
            }
            edited[vert.font][vert.id] = true;
            fromVerts[vert.font][vert.id][vert.command - flipOffset] = vert;
            if (command.type == "Z") {
                var index = +vert.command + 1 - flipOffset;
                while (index > 0) {
                    index--;
                    if (fonts[vert.font].font.glyphs.glyphs[vert.id].path.commands[index].type == "M") {
                        break;
                    }
                }
                toVerts[vert.font][vert.id][vert.command - flipOffset] = makePos(vert.font, vert.id, index, "");
            } else {
                toVerts[vert.font][vert.id][vert.command - flipOffset] = makePos(vert.font, vert.id, +vert.command + 1 - flipOffset, "");
            }
        }
        for (var fontId in fromVerts) {
            for (var glyph in fromVerts[fontId]) {
                var newVerts = {};
                for (var command in fromVerts[fontId][glyph]) {
                    var fromCom = fonts[fontId].font.glyphs.glyphs[glyph].path.commands[fromVerts[fontId][glyph][command].command - flipOffset];
                    var toCom = fonts[fontId].font.glyphs.glyphs[glyph].path.commands[toVerts[fontId][glyph][command].command];
                    if (fromCom.type == "Z" || toCom.type == "Z" || fromCom.x == toCom.x && fromCom.y == toCom.y) continue;
                    switch (toCom.type) {
                    case "M": case "L":
                        newVerts[command] = {type: "L", x: (fromCom.x + toCom.x) / 2, y: (fromCom.y + toCom.y) / 2};
                        break;
                    case "Q":
                        var fx1 = (fromCom.x + toCom.x1) / 2;
                        var fy1 = (fromCom.y + toCom.y1) / 2;
                        var tx1 = (toCom.x1 + toCom.x) / 2;
                        var ty1 = (toCom.y1 + toCom.y) / 2;
                        var midX = (fx1 + tx1) / 2;
                        var midY = (fy1 + ty1) / 2;
                        toCom.x1 = tx1;
                        toCom.y1 = ty1;
                        newVerts[command] = {type: "Q", x1: fx1, y1: fy1, x: midX, y: midY};
                        break;
                    case "C":
                        var fx1 = (fromCom.x + toCom.x1) / 2;
                        var fy1 = (fromCom.y + toCom.y1) / 2;
                        var ftx = (toCom.x1 + toCom.x2) / 2;
                        var fty = (toCom.y1 + toCom.y2) / 2;
                        var tx2 = (toCom.x2 + toCom.x) / 2;
                        var ty2 = (toCom.y2 + toCom.y) / 2;
                        var fx2 = (fx1 + ftx) / 2;
                        var fy2 = (fy1 + fty) / 2;
                        var tx1 = (ftx + tx2) / 2;
                        var ty1 = (fty + ty2) / 2;
                        var midX = (fx2 + tx1) / 2;
                        var midY = (fy2 + ty1) / 2;
                        toCom.x1 = tx1;
                        toCom.y1 = ty1;
                        toCom.x2 = tx2;
                        toCom.y2 = ty2;
                        newVerts[command] = {type: "C", x1: fx1, y1: fy1, x2: fx2, y2: fy2, x: midX, y: midY};
                        break;
                    }
                }
                var sorted = [];
                for (var command in newVerts) {
                    sorted.push(command);
                }
                sorted.sort(comparer);
                for (var key = sorted.length; key --> 0; ) {
                    fonts[fontId].font.glyphs.glyphs[glyph].path.commands.splice(+sorted[key] + 1, 0, newVerts[sorted[key]]);
                }
                var offset = 1;
                for (var key = 0; key < sorted.length; key++, offset++) {
                    newSelection.push(makePos(fontId, glyph, +sorted[key] + offset, ""));
                }
            }
        }
        return newSelection;
    }
    rotateRightTransform = rotate(-Math.PI / 2);
    /**
        Calculate the normal vector at vertex v given
            A vector from the previous to current node p_to_v
            A vector from the current to next node v_to_n
    */
    calcNormal = function(p_to_v, v_to_n) {
        p_to_v = normalize(p_to_v);
        p_to_v = rotateRightTransform(p_to_v);

        v_to_n = normalize(v_to_n);
        v_to_n = rotateRightTransform(v_to_n);
        
        var norm = {
            x: (p_to_v.x + v_to_n.x) / 2,
            y: (p_to_v.y + v_to_n.y) / 2,
        };
        return normalize(norm);
    }
    duplicateTo = function(glyph) {
        if (!(fontSelected in fontsList)) return;
        var newVerts = [];
        for (var key in vertSelection) {
            var vert = vertSelection[key];
            if (vert.index != "") continue;
            var newCommand = {};
            $.extend(newCommand, fontsList[vert.font].font.glyphs.glyphs[vert.id].path.commands[vert.command])
            newVerts.push(newCommand);
        }
        if (!newVerts.length) return;
        if (fontsList[fontSelected].font.glyphs.glyphs[glyph].path.commands.length) {
            var end = fontsList[fontSelected].font.glyphs.glyphs[glyph].path.commands.splice(-1, 1);
            fontsList[fontSelected].font.glyphs.glyphs[glyph].path.commands.push.apply(fontsList[fontSelected].font.glyphs.glyphs[glyph].path.commands, newVerts);
            fontsList[fontSelected].font.glyphs.glyphs[glyph].path.commands.push(end);
        } else {
            if (newVerts[0].type != "M") {
                fontsList[fontSelected].font.glyphs.glyphs[glyph].path.commands.push({type: "M", x: Math.random(), y: Math.random()});
            }
            fontsList[fontSelected].font.glyphs.glyphs[glyph].path.commands.push.apply(fontsList[fontSelected].font.glyphs.glyphs[glyph].path.commands, newVerts);
            if (fontsList[fontSelected].font.glyphs.glyphs[glyph].path.commands.slice(-1)[0].type == "M") {
                fontsList[fontSelected].font.glyphs.glyphs[glyph].path.commands.splice(-1, 0, {type: "Z"});
            }
            if (fontsList[fontSelected].font.glyphs.glyphs[glyph].path.commands.slice(-1)[0].type != "Z") {
                fontsList[fontSelected].font.glyphs.glyphs[glyph].path.commands.push({type: "Z"});
            }
        }
    }
}

setupSelects = function() {
    setHandlePosition = function() {
        if (!vertSelection.length) return;
        var vert = vertSelection[0];
        var other = vert.index == "1" ? "2" : "1";
        var bad = true;
        if (vert.id in handles) {
            if (vert.command in handles[vert.id] && vert.index == "2") {
                bad = false;
                var handle = handles[vert.id][vert.command];
                if ("sibling" in handle) {
                    vertSelection.splice(0, 0, makePos(fontSelected, vert.id, handle.sibling, other));
                } else {
                    vertSelection.splice(0, 0, makePos(fontSelected, vert.id, handle.c1, other));
                }
                var pos = makePos(fontSelected, vert.id, handle.c2, "");
                vertSelection.splice(0, 0, pos);
                pos = getPos(pos);
                posX = pos.x;
                posY = pos.y;
            } else if (+vert.command - 1 in handles[vert.id] && vert.index == "1") {
                bad = false;
                var handle = handles[vert.id][+vert.command - 1];
                var pos;
                if ("parent" in handle) {
                    vertSelection.splice(0, 0, makePos(fontSelected, vert.id, handle.parent, other));
                    pos = makePos(fontSelected, vert.id, handle.parent, "");
                } else {
                    vertSelection.splice(0, 0, makePos(fontSelected, vert.id, handle.c2, other));
                    pos = makePos(fontSelected, vert.id, handle.c2, "");
                }
                vertSelection.splice(0, 0, pos);
                pos = getPos(pos);
                posX = pos.x;
                posY = pos.y;
            }
        }
        if (bad) {
            vertSelection = [];
        }
    }
    circleSelect = function() {
        var intersections = fontsList[fontSelected].isect(mouseX, mouseY, selectRadius * fontScale, ISECT_CIRCLE);
        if (hState) {
            vertSelection = [];
            if (intersections.length) vertSelection.push(intersections[0]);
            setHandlePosition();
            updatePosition();
        } else if (ctrl) {
            for (var key in intersections) {
                var selected = -1;
                var pos = getPos(intersections[key]);
                for (var sel in vertSelection) {
                    var vert = getPos(vertSelection[sel]);
                    if (pos.x === vert.x && pos.y === vert.y) {
                        selected = sel;
                        break;
                    }
                }
                if (selected == -1) continue;
                verts--;
                selectionOrigin.x -= pos.x / vertSelection.length;
                selectionOrigin.y -= pos.y / vertSelection.length;
                selectionOrigin.x *= vertSelection.length / (vertSelection.length - 1);
                selectionOrigin.y *= vertSelection.length / (vertSelection.length - 1);
                vertSelection.splice(selected, 1);
            }
            posX = selectionOrigin.x;
            posY = selectionOrigin.y;
            updatePosition();
        } else {
            for (var key in intersections) {
                var selected = false;
                var pos = getPos(intersections[key]);
                for (var sel in vertSelection) {
                    var vert = getPos(vertSelection[sel]);
                    if (pos.x === vert.x && pos.y === vert.y) {
                        selected = true;
                        break;
                    }
                }
                if (selected) continue;
                verts++;
                vertSelection.push(intersections[key]);
                selectionOrigin.x *= (vertSelection.length - 1) / vertSelection.length;
                selectionOrigin.y *= (vertSelection.length - 1) / vertSelection.length;
                selectionOrigin.x += pos.x / vertSelection.length;
                selectionOrigin.y += pos.y / vertSelection.length;
            }
            posX = selectionOrigin.x;
            posY = selectionOrigin.y;
            updatePosition();
        }
        vertCount.text(verts + "/" + vertsTotal);    
    }
    
    greedySelect = function() {
        var delta = DELTA * fontScale;
        var intersections = fontsList[fontSelected].isect(mouseX, mouseY, delta, ISECT_CIRCLE, alt);
        if (shift) {
            var selected = -1;
            for (var key in intersections) {
                var exists = false;;
                var pos = getPos(intersections[key]);
                for (var sel in vertSelection) {
                    var vert = getPos(vertSelection[sel]);
                    if (pos.x === vert.x && pos.y === vert.y) {
                        selected = sel;
                        exists = true;
                        break;
                    }
                }
                if (exists) continue;
                verts++;
                vertSelection.push(intersections[key]);
                selectionOrigin.x *= (vertSelection.length - 1) / vertSelection.length;
                selectionOrigin.y *= (vertSelection.length - 1) / vertSelection.length;
                selectionOrigin.x += pos.x / vertSelection.length;
                selectionOrigin.y += pos.y / vertSelection.length;
            }
            if (selected != -1) {
                vertSelection.push(vertSelection[selected]);
                vertSelection.splice(selected, 1);
            }
            posX = selectionOrigin.x;
            posY = selectionOrigin.y;
            updatePosition();
        } else if (ctrl) {
            for (var key in intersections) {
                var selected = -1;
                var pos = getPos(intersections[key]);
                for (var sel in vertSelection) {
                    var vert = getPos(vertSelection[sel]);
                    if (pos.x === vert.x && pos.y === vert.y) {
                        selected = sel;
                        break;
                    }
                }
                if (selected == -1) continue;
                verts--;
                selectionOrigin.x -= pos.x / vertSelection.length;
                selectionOrigin.y -= pos.y / vertSelection.length;
                selectionOrigin.x *= vertSelection.length / (vertSelection.length - 1);
                selectionOrigin.y *= vertSelection.length / (vertSelection.length - 1);
                vertSelection.splice(selected, 1);
            }
            posX = selectionOrigin.x;
            posY = selectionOrigin.y;
            updatePosition();
        } else {
            verts = 0, posX = 0, posY = 0;
            vertSelection = [];
            for (var key in intersections) {
                var pos = getPos(intersections[key]);
                vertSelection.push(intersections[key]);
                verts++;
                posX += pos.x;
                posY += pos.y;
            }
            posX /= vertSelection.length;
            posY /= vertSelection.length;
            updatePosition();
        }
    }
    
    defaultSelect = function() {
        var delta = DELTA * fontScale;
        var intersections = fontsList[fontSelected].isect(mouseX, mouseY, delta, ISECT_CIRCLE, alt);
        if (hState) {
            var selected = false;
            if (intersections.length && vertSelection.length) {
                var iPos = getPos(intersections[0]);
                var pos = getPos(vertSelection[vertSelection.length - 1]);
                if (iPos.x == pos.x && iPos.y == pos.y) selected = true;
            }
            vertSelection = [];
            if (intersections.length) vertSelection.push(intersections[0]);
            setHandlePosition();
            updatePosition();
            if (selected) vertSelection.splice(1, 1);
        } else if (shift) {
            var selected = -1;
            for (var key in intersections) {
                selected = -1;
                var pos = getPos(intersections[key]);
                for (var sel in vertSelection) {
                    var vert = getPos(vertSelection[sel]);
                    if (pos.x === vert.x && pos.y === vert.y) {
                        selected = sel;
                        break;
                    }
                }
                if (selected != -1) continue;
                verts++;
                vertSelection.push(intersections[key]);
                selectionOrigin.x *= (vertSelection.length - 1) / vertSelection.length;
                selectionOrigin.y *= (vertSelection.length - 1) / vertSelection.length;
                selectionOrigin.x += pos.x / vertSelection.length;
                selectionOrigin.y += pos.y / vertSelection.length;
                posX = selectionOrigin.x;
                posY = selectionOrigin.y;
                updatePosition();
                break;
            }
            if (selected != -1) {
                vertSelection.push(vertSelection[selected]);
                vertSelection.splice(selected, 1);
            }
        } else if (ctrl) {
            for (var key in intersections) {
                var selected = -1;
                var pos = getPos(intersections[key]);
                for (var sel in vertSelection) {
                    var vert = getPos(vertSelection[sel]);
                    if (pos.x === vert.x && pos.y === vert.y) {
                        selected = sel;
                        break;
                    }
                }
                if (selected == -1) continue;
                verts--;
                selectionOrigin.x -= pos.x / vertSelection.length;
                selectionOrigin.y -= pos.y / vertSelection.length;
                selectionOrigin.x *= vertSelection.length / (vertSelection.length - 1);
                selectionOrigin.y *= vertSelection.length / (vertSelection.length - 1);
                posX = selectionOrigin.x;
                posY = selectionOrigin.y;
                updatePosition();
                vertSelection.splice(selected, 1);
                break;
            }
        } else {
            verts = 0;
            vertSelection = [];
            if (intersections.length) {
                if (mouseButton === 1) gState = TOOL_EDIT;
                verts = 1;
                vertSelection.push(intersections[0]);
                var pos = getPos(intersections[0]);
                posX = pos.x;
                posY = pos.y;
                updatePosition();
            }
        }
    }
    
    boxSelect = function() {
        var dx = Math.abs(mouseX - beginBoxX);
        var dy = Math.abs(mouseY - beginBoxY);
        var x = Math.min(mouseX, beginBoxX);
        var y = Math.min(mouseY, beginBoxY);
        var delta = Math.min(dx, dy) / 2;
        var end = Math.max(dx, dy) - delta;
        var intersections = [];
        if (dx < dy) {
            for (var ddy = delta; ddy < end; ddy += 2 * delta) {
                intersections.push.apply(intersections, fontsList[fontSelected].isect(x + delta, y + ddy, delta * fontScale, ISECT_SQUARE));
            }
            intersections.push.apply(intersections, fontsList[fontSelected].isect(x + delta, y + end, delta * fontScale, ISECT_SQUARE));
        } else {
            for (var ddx = delta; ddx < end; ddx += 2 * delta) {
                intersections.push.apply(intersections, fontsList[fontSelected].isect(x + ddx, y + delta, delta * fontScale, ISECT_SQUARE));
            }
            intersections.push.apply(intersections, fontsList[fontSelected].isect(x + end, y + delta, delta * fontScale, ISECT_SQUARE));
        }
        if (hState) {
            vertSelection = [];
            if (intersections.length) vertSelection.push(intersections[0]);
            setHandlePosition();
            updatePosition();
        } else if (ctrl) {
            for (var key in intersections) {
                var selected = -1;
                var pos = getPos(intersections[key]);
                for (var sel in vertSelection) {
                    var vert = getPos(vertSelection[sel]);
                    if (pos.x === vert.x && pos.y === vert.y) {
                        selected = sel;
                        break;
                    }
                }
                if (selected == -1) continue;
                verts--;
                selectionOrigin.x -= pos.x / vertSelection.length;
                selectionOrigin.y -= pos.y / vertSelection.length;
                selectionOrigin.x *= vertSelection.length / (vertSelection.length - 1);
                selectionOrigin.y *= vertSelection.length / (vertSelection.length - 1);
                vertSelection.splice(selected, 1);
            }
            posX = selectionOrigin.x;
            posY = selectionOrigin.y;
            updatePosition();
        } else {
            for (var key in intersections) {
                var selected = false;
                var pos = getPos(intersections[key]);
                for (var sel in vertSelection) {
                    var vert = getPos(vertSelection[sel]);
                    if (pos.x === vert.x && pos.y === vert.y) {
                        selected = true;
                        break;
                    }
                }
                if (selected) continue;
                verts++;
                vertSelection.push(intersections[key]);
                selectionOrigin.x *= (vertSelection.length - 1) / vertSelection.length;
                selectionOrigin.y *= (vertSelection.length - 1) / vertSelection.length;
                selectionOrigin.x += pos.x / vertSelection.length;
                selectionOrigin.y += pos.y / vertSelection.length;
            }
            posX = selectionOrigin.x;
            posY = selectionOrigin.y;
            updatePosition();
        }
        if (!vertSelection.length) {
            posX = posY = 0;
            updatePosition();
        }
        vertCount.text(verts + "/" + vertsTotal);
        selectMode = SELECT_NONE;
    }
}

addWindowListeners = function() {  
    $(window).resize(function() {
        change = true;
        viewChanged = true;
    });
    
    $(window).on("mousemove", function(event) {
        if (!(fontSelected in fontsList)) return;
        var rect = canvas.getBoundingClientRect();
        mouseX = event.clientX - rect.left;
        mouseY = event.clientY - rect.top;
        dMouseX = mouseX - beginMouseX;
        dMouseY = mouseY - beginMouseY;
        if (mouseButton > 1) {
            if (clickElement === overlayCanvas) {
                originX += mouseX - lastMouseX;
                originY += mouseY - lastMouseY;
                change = true;
            } else if (clickElement === viewCanvas) {
                viewOriginX += mouseX - lastMouseX;
                viewOriginY += mouseY - lastMouseY;
                viewChanged = true;
            }
        } else if (mouseButton == 1 && selectMode === SELECT_CIRCLE) {
            circleSelect();
        }
        if (!number) move(constrain, lastMouseX, lastMouseY, mouseX, mouseY);
        lastMouseX = mouseX;
        lastMouseY = mouseY;
        guiChanged = true;
    });
    
    $(window).on("mousedown", function(event) {
        if (!(fontSelected in fontsList)) return;
        mouseButton = event.which;
        clickElement = event.target;
        if (!dialog && mouseX > 0 && mouseX <= canvas.width && mouseY > 0 && mouseY <= canvas.height) {
            if (window.getSelection) {
                if (window.getSelection().empty) {  // Chrome
                    window.getSelection().empty();
                } else if (window.getSelection().removeAllRanges) {  // Firefox
                    window.getSelection().removeAllRanges();
                }
            } else if (document.selection) {  // IE?
                document.selection.empty();
            }
            event.preventDefault();
        }
        if (mouseButton !== 1) {
            if (constrain === CONSTRAIN_X) constrain = CONSTRAIN_Y;
            else if (constrain === CONSTRAIN_Y) constrain = CONSTRAIN_X;
            undo(constrain);
            gState = rState = sState = TOOL_NONE;
            if (selectMode !== SELECT_NONE) selectMode = SELECT_NONE;
        }
        if (gState || rState || sState) return;
        if (mouseButton === 1) {
            if (selectMode === SELECT_READY) {
                beginBoxX = mouseX;
                beginBoxY = mouseY;
                selectMode = SELECT_BOX;
            } else if (selectMode === SELECT_CIRCLE) {
                circleSelect();
            }
            beginMouseX = mouseX;
            beginMouseY = mouseY;
            dMouseX = 0;
            dMouseY = 0;
            if (mode === MODE_EDIT && selectMode === SELECT_NONE && clickElement === overlayCanvas && fontSelected in fontsList) {
                if (alt) {
                    if (hState) defaultSelect();
                    else greedySelect();
                } else {
                    defaultSelect();
                }
                if (!vertSelection.length) {
                    posX = posY = 0;
                    updatePosition();
                }
                vertCount.text(verts + "/" + vertsTotal);
            }
        }
        guiChanged = true;
    });
    
    $(window).on("mouseup", function(event) {
        if (!(fontSelected in fontsList)) return;
        if (mouseButton === 1) {
            if (gState) gState = TOOL_DONE;
            if (rState) rState = TOOL_DONE;
            if (sState) sState = TOOL_DONE;
            if (gState || rState || sState) recalculate = true;
        }
        mouseButton = 0;
        guiChanged = true;
        if (selectMode === SELECT_BOX) {
            boxSelect();
        }
    });
}

addDialogHooks = function() {
    enterTriggerButton = function(which) {
        return function (event) {
            if (event.keyCode == 13) {
                $(this).parent().find("button:eq(" + which + ")").trigger("click");
                return false;
            }
        }
    };
    
    autoClose = function(which) {
        return function() {
            $(".ui-widget-overlay").bind("click", function(){
                $(which).dialog("close");
            });
        };
    };
    
    buttonColor = function(f) {
        return function() {
            $(this).parent().find(".ui-button").addClass("dialog-button-bg");
            if (f) f();
        }
    }
    
    $("#dialog-new").keydown(enterTriggerButton(1));
    $("#dialog-close").keydown(enterTriggerButton(1));
    $("#dialog-rename").keydown(enterTriggerButton(1));
    $("#dialog-confirm").keydown(enterTriggerButton(1));
    $("#dialog-input").keydown(enterTriggerButton(1));
    $("#dialog-error").keydown(enterTriggerButton(0));
    $("#dialog-dupe-to").keydown(enterTriggerButton(1));
    
    $("#dialog-new").on("dialogopen", buttonColor());
    $("#dialog-close").on("dialogopen", buttonColor());
    $("#dialog-rename").on("dialogopen", buttonColor(function() {$("#dialog-rename-in").select();}));
    $("#dialog-confirm").on("dialogopen", buttonColor(autoClose("#dialog-confirm")));
    $("#dialog-input").on("dialogopen", buttonColor(function() {$("#dialog-in").select();}));
    $("#dialog-error").on("dialogopen", buttonColor(autoClose("#dialog-error")));
    $("#dialog-saving").on("dialogopen", buttonColor());
    $("#dialog-help").on("dialogopen", buttonColor(autoClose("#dialog-help")));
    $("#dialog-load-font").on("dialogopen", buttonColor(autoClose("#dialog-load-font")));
    $("#dialog-import-quality").on("dialogopen", buttonColor());
    $("#dialog-json-editor").on("dialogopen", buttonColor());
    $("#dialog-json-editor2").on("dialogopen", buttonColor());
    $("#dialog-dupe-to").on("dialogopen", buttonColor(function() {$("#dialog-dupe-in").focus();}));
}

setupResizeables = function() {
    $("#menu").resizable({
        handles: 's',
        minHeight: 25,
        resize: function(event, ui){
            var current = ui.size.height;
            $(this).height(current);
            $("#applet").css("top", current);
        }
    });
    $("#main-area").resizable({
        handles: 'e',
        resize: function(event, ui){
            var current = ui.size.width;
            var width = $(window).width();
            var str = "calc(100% - " + (width - current) + "px)";
            $(this).css("width", str);
            $("#overview").css("margin-left", str);
        }
    });
    $("#fonts").resizable({
        handles: 's',
        resize: function(event, ui){
            var current = ui.size.height;
            $(this).height(current);
            $("#properties").css("top", current);
        }
    });
    $("#edit-area").resizable({
        handles: 's',
        resize: function(event, ui){
            var current = ui.size.height;
            var menu = $("#menu").height();
            var height = $(window).height();
            var str = "calc(100% - " + (height - menu - current) + "px)";
            $(this).css("height", str);
            $("#viewer").css("top", str);
        }
    });
    $("#toolbox").resizable({
        handles: 'e',
        minWidth: 200,
        resize: function(event, ui){
            var current = ui.size.width;
            $(this).width(current);
            $("#editor").css("margin-left", current);
        }
    });
    $("#position").resizable({
        handles: 's',
        resize: function(event, ui){
            var current = ui.size.height;
            $(this).height(current);
            $("#tools").css("top", current);
        }
    });
}

setupMenu = function() {
    vertCount = $("#vert-count");
    lineCount = $("#line-count");
    curveCount = $("#curve-count");
    bezierCount = $("#bezier-count");
    fpsDisplay = $("#fps-display");
    setInterval( function() { fpsDisplay.text(fps); }, 50);
    newFont = function() {
        var val = "Untitled" + untitledNum;
        $("#dialog-new-in").val(val);
        dialog = true;
        $( "#dialog-new" ).dialog({
            modal: true,
            closeOnEscape: true,
            buttons: {
                Create: function() {
                    var name = $("#dialog-new-in").val();
                    
                    if (name == val) untitledNum++;
                    
                    BVHSEGMENTS = 8;
                    makeDefaultFont(name);
        
                    $( this ).dialog( "close" );
                    dialog = false;
                },
                Cancel: function() {
                    $( this ).dialog( "close" );
                    dialog = false;
                }
            }
        });
    }
    $("#open-file").on('change', function(event) {
        var file = event.target.files[0];
        if (!file) return;
        var reader = new FileReader();

        reader.onload = function(event) {
            try {
                font = opentype.parse(event.target.result);
                dialog = true;
                $("#import-quality").on("input", function(event) {
                    BVHSEGMENTS = $(this).val();
                    var complexity = 0;
                    for (var key in font.glyphs.glyphs) {
                        complexity += font.glyphs.glyphs[key].path.commands.length;
                    }
                    complexity *= BVHSEGMENTS * (BVHSEGMENTS + 2) / 1000;
                    $("#import-complexity").text(complexity.toFixed(3) + " [" + BVHSEGMENTS + "]");
                });
                $("#import-quality").trigger("input");
                $( "#dialog-import-quality" ).dialog({
                    modal: true,
                    closeOnEscape: true,
                    buttons: {
                        Confirm: function() {
                            loadFont(file.name, font);
                            $( this ).dialog( "close" );
                            dialog = false;
                        },
                        Cancel: function() {
                            $( this ).dialog( "close" );
                            dialog = false;
                        }
                    }
                });
            } catch (error) {
                showError(error);
                if (error.stack) console.log(error.stack);
                throw(error);
            }
            document.getElementById("open-file").value = "";
        }
        reader.onerror = function(error) {
            showError(error.toString());
        }

        reader.readAsArrayBuffer(file);
    });
    importFont = function() {
        $("#open-file").click();
    }
    exportFont = function() {
        if (fontSelected in fontsList) exportFonts([fontSelected]);
    }
    exportAllFonts = function() {
        var files = []
        for (var key in fontsList) {
            files.push(key);
        }
        exportFonts(files);
    }
    exportFonts = function(files) {
        saving = true;
        if (!files || !files.length) {
            saving = false;
            return;
        }
        var key = files.pop()
        try {
            fontsList[key].font.download();
            fontsList[key].save = true;
        } catch (error) {
            showError(error);
        }
        $( "#dialog-saving" ).dialog({
            modal: true,
            closeOnEscape: false,
            buttons: {
                Ok: function() {
                    setTimeout(exportFonts, 0, files);
                    $( this ).dialog( "close" );
                }
            }
        });
        $( "#dialog-saving" ).dialog("option", "title", "Exporting " + fontsList[key].name);
    }
    showHelp = function() {
        $( "#dialog-help" ).dialog({
            modal: false,
            closeOnEscape: false,
            buttons: {
                Ok: function() {
                    $( this ).dialog( "close" );
                }
            }
        });
    }
    closeFont = function() {
        if (!$("#" + fontSelected).length) return;
        if (!fontsList[fontSelected].save) {
            dialog = true;
            $( "#dialog-close" ).dialog({
                resizable: true,
                height: 192,
                modal: true,
                closeOnEscape: true,
                buttons: {
                    Yes: function() {
                        exportFont();
                        removeFont();
                        $( this ).dialog( "close" );
                        dialog = false;
                        recalculate = true;
                    },
                    No: function() {
                        removeFont();
                        $( this ).dialog( "close" );
                        dialog = false;
                        recalculate = true;
                    },
                    Cancel: function() {
                        $( this ).dialog( "close" );
                        dialog = false;
                    }
                }
            });
            $( "#dialog-close" ).dialog("option", "title", "Save " + fontsList[fontSelected].name + " before closing?");
        } else {
            removeFont();
        }
    }
    closeAllFonts = function() {
        if (!$("#" + fontSelected).length) return;
        if (!fontsList[fontSelected].save) {
            dialog = true;
            $( "#dialog-close" ).dialog({
                resizable: true,
                height: 192,
                modal: true,
                closeOnEscape: true,
                buttons: {
                    Yes: function() {
                        exportFont();
                        removeFont();
                        $( this ).dialog( "close" );
                        dialog = false;
                        recalculate = true;
                        setTimeout(closeAllFonts, 0);
                    },
                    No: function() {
                        removeFont();
                        $( this ).dialog( "close" );
                        dialog = false;
                        recalculate = true;
                        setTimeout(closeAllFonts, 0);
                    },
                    Cancel: function() {
                        $( this ).dialog( "close" );
                        dialog = false;
                    }
                }
            });
            $( "#dialog-close" ).dialog("option", "title", "Close " + fontsList[fontSelected].name + " without saving?");
        } else {
            removeFont();
            setTimeout(closeAllFonts, 0);
        }
    }
    removeFont = function() {
        $("#" + fontSelected).remove();
        delete fontsList[fontSelected];
        for (key in fontsList) {
            fontSelected = key;
            selectFont(fontSelected);
            break;
        }
        if (!fontsList.length) {
            var glyphs = document.getElementsByClassName("item glyph-selected");
            for (var key in glyphs) {
                glyphs[key].className = "item";
            }
        }
        if (mode === MODE_EDIT) onMode();
        glyphSelection = {};
        vertsSelection = [];
        vertCount.text("0/0");
        lineCount.text("0");
        curveCount.text("0");
        bezierCount.text("0");
        viewChanged = true;
        recalculate = true;
    }
    resetLayout = function() {
        $("#menu").css("height", "");
        $("#applet").css("top", "");
        $("#main-area").css("width", "");
        $("#overview").css("margin-left", "");
        $("#fonts").css("height", "");
        $("#properties").css("top", "");
        $("#edit-area").css("height", "");
        $("#viewer").css("top", "");
        $("#toolbox").css("width", "");
        $("#editor").css("margin-left", "");
        $("#position").css("height", "");
        $("#tools").css("top", "");
        change = true;
        viewChanged = true;
        reset = true;
    }
}

addPositionEventHandlers = function() {
    var invert = false;
    updatePosition = function() {
        if (!$.isNumeric(posX)) posX = 0;
        if (!$.isNumeric(posY)) posY = 0;
        selectionOrigin.x = lastX = posX;
        selectionOrigin.y = lastY = posY;
        $("#pos-x").val(lastX.toFixed(3));
        $("#pos-y").val(lastY.toFixed(3));
    }
    applyPosition = function() {
        if (vertSelection.length && (posX != lastX || posY != lastY)) {
            updatePosition();
            gState = TOOL_DONE;
            recalculate = true;
        }
    }
    moveVerts = function(dx, dy) {
        if (vertSelection.length) {
            for (var key in vertSelection) {
                var pos = getPos(vertSelection[key]);
                setPos(vertSelection[key], pos.x + dx, pos.y + dy);
            }
            change = true;
        }
    }
    $("#pos-x").on("input", function() {
        var val = $(this).val();
        if (val == "") val = 0;
        if (!$.isNumeric(val)) {
            if (invert) {
                posX *= -1;
                invert = false;
            }
            $(this).val(posX);
            return;
        }
        posX = +val;
        $(this).val(val);
    });
    $("#pos-y").on("input", function() {
        var val = $(this).val();
        if (val == "") val = 0;
        if (!$.isNumeric(val)) {
            if (invert) {
                posY *= -1;
                invert = false;
            }
            $(this).val(posY);
            return;
        }
        posY = +val;
        $(this).val(val);
    });
    $("#pos-x").on("mousewheel", function(event) {
        posX += event.deltaY;
        moveVerts(event.deltaY, 0);
        $(this).val(posX.toFixed(3));
    });
    $("#pos-y").on("mousewheel", function(event) {
        posY += event.deltaY;
        moveVerts(0, event.deltaY);
        $(this).val(posY.toFixed(3));
    });
    $("#toolbox input").on("blur", applyPosition);
    $("#toolbox input").on("focus", applyPosition);
    $("#toolbox input").on("mouseleave", applyPosition);
    $("#toolbox input").keydown(function(event) {
        var keyCode = event.keyCode || event.which; 
        if (keyCode == 13) {
            moveVerts(posX - lastX, posY - lastY);
            $(this).blur();
            event.preventDefault();
        } else if (keyCode == 109 || keyCode == 189) {
            invert = true;
        }
    });
}

setupButtons = function() {
    comparer = function(a, b) {
        return a - b;
    }
    onTranslate = function() {
        if (hState) {
            window.alert("You are currently in handle-edit mode. Please use rotate and scale. We've already centered the cursor for you :D");
        } else if (selectMode === SELECT_NONE && !gState && !rState && !sState) {
            gState = TOOL_EDIT;
            beginMouseX = mouseX;
            beginMouseY = mouseY;
            dMouseX = 0;
            dMouseY = 0;
            number = null, decimal = false, negative = false;
            constrain = CONSTRAIN_NONE;
        }
    }
    onRotate = function() {
        if (selectMode === SELECT_NONE && !gState && !rState && !sState) {
            rState = TOOL_EDIT;
            beginMouseX = mouseX;
            beginMouseY = mouseY;
            dMouseX = 0;
            dMouseY = 0;
            var x = originX + posX / fontScale;
            var y = originY - posY / fontScale;
            angle = Math.atan2(y - mouseY, mouseX - x); 
            number = null, decimal = false, negative = false;
            constrain = CONSTRAIN_NONE;
        }
    }
    onScale = function() {
        if (selectMode === SELECT_NONE && !gState && !rState && !sState) {
            sState = TOOL_EDIT;
            beginMouseX = mouseX;
            beginMouseY = mouseY;
            dMouseX = 0;
            dMouseY = 0;
            var x = originX + posX / fontScale;
            var y = originY - posY / fontScale;
            distance = Math.sqrt( (x -= mouseX) * x + (y -= mouseY) * y );
            number = null, decimal = false, negative = false;
            constrain = CONSTRAIN_NONE;
        }
    }
    selectAll = function(bbox) {
        var intersections = [];
        if (bbox.nodes.length) {
            return bbox.nodes;
        }
        for (var key in bbox.quads) {
            if (bbox.quads[key]) intersections.push.apply(intersections, selectAll(bbox.quads[key]));
        }
        return intersections;
    }
    onAll = function() {
        if (hState) {
            vertSelection = [];
            posX = posY = 0;
            updatePosition();
            vertCount.text(verts + "/" + vertsTotal);
            guiChanged = true;
        } else if (!gState && !rState && !sState) {
            if (vertSelection.length) {
                vertSelection = [];
            } else {
                vertSelection = [];
                for (var key in glyphSelection) {
                    if (!glyphSelection[key]) continue;
                    var coords = selectAll(bvh[fontSelected][key]);
                    vertSelection.push.apply(vertSelection, coords);
                }
                posX = 0, posY = 0;
                for (var key in vertSelection) {
                    var pos = getPos(vertSelection[key]);
                    posX += pos.x;
                    posY += pos.y;
                }
                posX /= vertSelection.length;
                posY /= vertSelection.length;
                updatePosition();
            }
            verts = vertSelection.length;
            if (!vertSelection.length) {
                posX = posY = 0;
                updatePosition();
            }
            vertCount.text(verts + "/" + vertsTotal);
            guiChanged = true;
        }
    }
    onBox = function() {
        if (confirmModal || gState || rState || sState) return;
        if (selectMode === SELECT_READY || selectMode === SELECT_BOX) selectMode = SELECT_NONE;
        else selectMode = SELECT_READY;
        guiChanged = true;
    }
    onCircle = function() {
        if (confirmModal || gState || rState || sState) return;
        if (selectMode === SELECT_CIRCLE) selectMode = SELECT_NONE;
        else selectMode = SELECT_CIRCLE;
        guiChanged = true;
    }
    onMode = function() {
        if (mode === MODE_VIEW && fontSelected in fontsList) {
            mode = MODE_EDIT;
            overlayCanvas.style.cursor = "crosshair";
            $("#view-area").hide();
            $("#select-area").show();
        } else {
            mode = MODE_VIEW;
            overlayCanvas.style.cursor = "default";
            $("#view-area").show();
            $("#select-area").hide();
        }
        change = true;
        viewChanged = true;
    }
    onLinear = function() {
        if (selectMode !== SELECT_NONE || !(fontSelected in fontsList) || gState || rState || sState || hState) return;
        var loc = mouseX < 0 || mouseY < 0 || mouseX > canvas.width || mouseY > canvas.height;
        var x = loc ? Math.random() : (mouseX - originX) * fontScale;
        var y = loc ? Math.random() : (originY - mouseY) * fontScale;
        var source = null;
        if (vertSelection.length) source = vertSelection[vertSelection.length - 1];
        verts = 0;
        vertSelection = [];
        for (var key in glyphSelection) {
            if (!glyphSelection[key] || !glyphSelection[key].path.commands.length) continue;
            if (source && source.id == key) {
                var prev = fontsList[source.font].font.glyphs.glyphs[source.id].path.commands[source.command];
                var next = fontsList[source.font].font.glyphs.glyphs[source.id].path.commands[+source.command + 1];
                if (flipped && prev.type == "M") {
                    glyphSelection[key].path.commands[source.command].type = "L";
                    glyphSelection[key].path.commands.splice(source.command, 0, {type: "M", x: x, y: y});
                } else {
                    glyphSelection[key].path.commands.splice(+source.command + 1 - (flipped ? 1 : 0), 0, {type: "L", x: x, y: y});
                }
                vertSelection.push(makePos(source.font, source.id, +source.command + 1 - (flipped ? 1 : 0), ""));
            } else {
                glyphSelection[key].path.commands.splice(-1, 0, {type: "L", x: x, y: y});
            }
            updateBVH(fontsList[fontSelected].font, fontSelected, key);
            vertsTotal++;
            linesTotal++;
        }
        verts = vertSelection.length;
        vertCount.text(verts + "/" + vertsTotal);
        lineCount.text(linesTotal);
        recalculate = true;
    }
    onQuadratic = function() {
        if (selectMode !== SELECT_NONE || !(fontSelected in fontsList) || gState || rState || sState || hState) return;
        var loc = mouseX < 0 || mouseY < 0 || mouseX > canvas.width || mouseY > canvas.height;
        var x = loc ? Math.random() : (mouseX - originX) * fontScale;
        var y = loc ? Math.random() : (originY - mouseY) * fontScale;
        var source = null;
        if (vertSelection.length) source = vertSelection[vertSelection.length - 1];
        verts = 0;
        vertSelection = [];
        for (var key in glyphSelection) {
            if (!glyphSelection[key]) continue;
            var length = glyphSelection[key].path.commands.length;
            if (source && source.id == key) {
                if (flipped) {
                    var prev = glyphSelection[key].path.commands[source.command];
                    var next = glyphSelection[key].path.commands[+source.command + 1];
                    if (next.type == "Z") {
                        var index = +vert.command + 1;
                        while (index > 0) {
                            index--;
                            if (glyphSelection[key].path.commands[index].type == "M") {
                                break;
                            }
                        }
                        next = glyphSelection[key].path.commands[index];
                    }
                    var distance = Math.sqrt((x - prev.x) * (x - prev.x) + (y - prev.y) * (y - prev.y));
                    var x1, x2;
                    switch (next.type) {
                    case "M":
                        if (prev.x == next.x && prev.y == next.y) {
                            x1 = x - prev.x;
                            y1 = y - prev.y;
                        } else {
                            x1 = prev.x - next.x;
                            y1 = prev.y - next.y;
                        }
                        break;
                    case "L":
                        x1 = prev.x - next.x;
                        y1 = prev.y - next.y;
                        break;
                    case "Q": case "C":
                        x1 = prev.x - next.x1;
                        y1 = prev.y - next.y1;
                        break;
                    default:
                        x1 = x - prev.x;
                        y1 = y - prev.y;
                    }
                    var ratio = distance / Math.sqrt(x1 * x1 + y1 * y1) / 2;
                    x1 = prev.x + ratio * x1;
                    y1 = prev.y + ratio * y1;
                    var insert = {type: "Q", x: prev.x, y: prev.y, x1: x1, y1: y1};
                    prev.x = x;
                    prev.y = y;
                    glyphSelection[key].path.commands.splice(+source.command + 1, 0, insert);
                } else {
                    var prev = glyphSelection[key].path.commands[source.command];
                    var start = glyphSelection[key].path.commands[+source.command - 1];
                    if (!start) {
                        var index = +source.command;
                        while (index < length - 1) {
                            index++;
                            if (glyphSelection[key].path.commands[index].type == "Z") {
                                break;
                            }
                        }
                        start = glyphSelection[key].path.commands[index - 1];
                    }
                    var distance = Math.sqrt((x - prev.x) * (x - prev.x) + (y - prev.y) * (y - prev.y));
                    var x1, x2;
                    switch (prev.type) {
                    case "M":
                        x1 = x - prev.x;
                        y1 = y - prev.y;
                        break;
                    case "L":
                        if (length < 3) {
                            x1 = x - prev.x;
                            y1 = y - prev.y;
                        } else {
                            x1 = prev.x - start.x;
                            y1 = prev.y - start.y;
                        }
                        break;
                    case "Q":
                        x1 = prev.x - prev.x1;
                        y1 = prev.y - prev.y1;
                        break;
                    case "C":
                        x1 = prev.x - prev.x2;
                        y1 = prev.y - prev.y2;
                        break;
                    default:
                        x1 = x - prev.x;
                        y1 = y - prev.y;
                    }
                    var ratio = distance / Math.sqrt(x1 * x1 + y1 * y1) / 2;
                    x1 = prev.x + ratio * x1;
                    y1 = prev.y + ratio * y1;
                    glyphSelection[key].path.commands.splice(+source.command + 1, 0, {type: "Q", x: x, y: y, x1: x1, y1: y1});
                }
                vertSelection.push(makePos(source.font, source.id, +source.command + 1 - (flipped ? 1 : 0), ""));
            } else {
                if (length < 2) continue;
                var prev = glyphSelection[key].path.commands[length - 2];
                var distance = Math.sqrt((x - prev.x) * (x - prev.x) + (y - prev.y) * (y - prev.y));
                var x1, x2;
                switch (prev.type) {
                case "M":
                    x1 = x - prev.x;
                    y1 = y - prev.y;
                    break;
                case "L":
                    if (length < 3) {
                        x1 = x - prev.x;
                        y1 = y - prev.y;
                    } else {
                        var start = glyphSelection[key].path.commands[length - 3];
                        x1 = prev.x - start.x;
                        y1 = prev.y - start.y;
                    }
                    break;
                case "Q":
                    x1 = prev.x - prev.x1;
                    y1 = prev.y - prev.y1;
                    break;
                case "C":
                    x1 = prev.x - prev.x2;
                    y1 = prev.y - prev.y2;
                    break;
                default:
                    x1 = x - prev.x;
                    y1 = y - prev.y;
                }
                var ratio = distance / Math.sqrt(x1 * x1 + y1 * y1) / 2;
                x1 = prev.x + ratio * x1;
                y1 = prev.y + ratio * y1;
                glyphSelection[key].path.commands.splice(-1, 0, {type: "Q", x: x, y: y, x1: x1, y1: y1});
            }
            updateBVH(fontsList[fontSelected].font, fontSelected, key);
            vertsTotal += 2;
            curvesTotal++;
        }
        vertCount.text(verts + "/" + vertsTotal);
        curveCount.text(curvesTotal);
        recalculate = true;
    }
    onCubic = function() {
        if (selectMode !== SELECT_NONE || !(fontSelected in fontsList) || gState || rState || sState || hState) return;
        var loc = mouseX < 0 || mouseY < 0 || mouseX > canvas.width || mouseY > canvas.height;
        var x = loc ? Math.random() : (mouseX - originX) * fontScale;
        var y = loc ? Math.random() : (originY - mouseY) * fontScale;
        var source = null;
        if (vertSelection.length) source = vertSelection[vertSelection.length - 1];
        verts = 0;
        vertSelection = [];
        var x1, x2;
        for (var key in glyphSelection) {
            if (!glyphSelection[key]) continue;
            var length = glyphSelection[key].path.commands.length;
            if (source && source.id == key) {
                if (flipped) {
                    var prev = glyphSelection[key].path.commands[source.command];
                    var next = glyphSelection[key].path.commands[+source.command + 1];
                    if (next.type == "Z") {
                        var index = +vert.command + 1;
                        while (index > 0) {
                            index--;
                            if (glyphSelection[key].path.commands[index].type == "M") {
                                break;
                            }
                        }
                        next = glyphSelection[key].path.commands[index];
                    }
                    var distance = Math.sqrt((x - prev.x) * (x - prev.x) + (y - prev.y) * (y - prev.y));
                    switch (next.type) {
                    case "M":
                        if (prev.x == next.x && prev.y == next.y) {
                            x1 = x - prev.x;
                            y1 = y - prev.y;
                        } else {
                            x1 = prev.x - next.x;
                            y1 = prev.y - next.y;
                        }
                        break;
                    case "L":
                        x1 = prev.x - next.x;
                        y1 = prev.y - next.y;
                        break;
                    case "Q": case "C":
                        x1 = prev.x - next.x1;
                        y1 = prev.y - next.y1;
                        break;
                    default:
                        x1 = x - prev.x;
                        y1 = y - prev.y;
                    }
                    var ratio = distance / Math.sqrt(x1 * x1 + y1 * y1) / 3;
                    x1 = prev.x + ratio * x1;
                    y1 = prev.y + ratio * y1;
                    var insert = {type: "C", x: prev.x, y: prev.y, x1: x, y1: y, x2: x1, y2: y1};
                    prev.x = x;
                    prev.y = y;
                    glyphSelection[key].path.commands.splice(+source.command + 1, 0, insert);
                    vertSelection.push(makePos(source.font, source.id, +source.command + 1, "1"));
                } else {
                    var prev = glyphSelection[key].path.commands[source.command];
                    var start = glyphSelection[key].path.commands[+source.command - 1];
                    if (!start) {
                        var index = +source.command;
                        while (index < length - 1) {
                            index++;
                            if (glyphSelection[key].path.commands[index].type == "Z") {
                                break;
                            }
                        }
                        start = glyphSelection[key].path.commands[index - 1];
                    }
                    var distance = Math.sqrt((x - prev.x) * (x - prev.x) + (y - prev.y) * (y - prev.y));
                    switch (prev.type) {
                    case "M":
                        x1 = x - prev.x;
                        y1 = y - prev.y;
                        break;
                    case "L":
                        if (length < 3) {
                            x1 = x - prev.x;
                            y1 = y - prev.y;
                        } else {
                            x1 = prev.x - start.x;
                            y1 = prev.y - start.y;
                        }
                        break;
                    case "Q":
                        x1 = prev.x - prev.x1;
                        y1 = prev.y - prev.y1;
                        break;
                    case "C":
                        x1 = prev.x - prev.x2;
                        y1 = prev.y - prev.y2;
                        break;
                    default:
                        x1 = x - prev.x;
                        y1 = y - prev.y;
                    }
                    var ratio = distance / Math.sqrt(x1 * x1 + y1 * y1) / 3;
                    x1 = prev.x + ratio * x1;
                    y1 = prev.y + ratio * y1;
                    glyphSelection[key].path.commands.splice(+source.command + 1, 0, {type: "C", x: x, y: y, x1: x1, y1: y1, x2: x, y2: y});
                    vertSelection.push(makePos(source.font, source.id, +source.command + 1, "2"));
                }
            } else {
                if (length < 2) continue;
                if (!glyphSelection[key]) continue;
                var length = glyphSelection[key].path.commands.length;
                var prev = glyphSelection[key].path.commands[length - 2];
                var distance = Math.sqrt((x - prev.x) * (x - prev.x) + (y - prev.y) * (y - prev.y));
                switch (prev.type) {
                case "M":
                    x1 = x - prev.x;
                    y1 = y - prev.y;
                    break;
                case "L":
                    if (length < 3) {
                        x1 = x - prev.x;
                        y1 = y - prev.y;
                    } else {
                        var start = glyphSelection[key].path.commands[length - 3];
                        x1 = prev.x - start.x;
                        y1 = prev.y - start.y;
                    }
                    break;
                case "Q":
                    x1 = prev.x - prev.x1;
                    y1 = prev.y - prev.y1;
                    break;
                case "C":
                    x1 = prev.x - prev.x2;
                    y1 = prev.y - prev.y2;
                    break;
                default:
                    x1 = x - prev.x;
                    y1 = y - prev.y;
                }
                var ratio = distance / Math.sqrt(x1 * x1 + y1 * y1) / 3;
                x1 = prev.x + ratio * x1;
                y1 = prev.y + ratio * y1;
                glyphSelection[key].path.commands.splice(-1, 0, {type: "C", x: x, y: y, x1: x1, y1: y1, x2: x, y2: y});
                vertSelection.push(makePos(fontSelected, key, length - 1, "2"));
                updateBVH(fontsList[fontSelected].font, fontSelected, key);
            }
        }
        if (x1 && y1) {
            posX = x;
            posY = y;
            updatePosition();
            onTranslate();
            beginMouseX = originX + (x + x1) / 2 / fontScale;
            beginMouseY = originY - (y + y1) / 2 / fontScale;
            dMouseX = mouseX - beginMouseX;
            dMouseY = mouseY - beginMouseY;
        }
        recalculate = true;
    }
    onDupe = function() {
        if (selectMode !== SELECT_NONE || !(fontSelected in fontsList) || gState || rState || sState || hState) return;
        $("#dialog-dupe-in").val("");
        dialog = true;
        $( "#dialog-dupe-to" ).dialog({
            resizable: true,
            modal: true,
            closeOnEscape: true,
            buttons: {
                Duplicate: function() {
                    var val = $("#dialog-dupe-in").val();
                    try {
                        val = eval(val);
                    } catch (error) {}
                    if ($.isNumeric(val)) {
                        val = Math.floor(val);
                        if (val >= 0 && val < fontsList[fontSelected].font.numGlyphs) {
                            duplicateTo(val);
                            updateBVH(fontsList[fontSelected].font, fontSelected, val);
                        }
                    }
                    $( this ).dialog( "close" );
                    dialog = false;
                },
                Cancel: function() {
                    $( this ).dialog( "close" );
                    dialog = false;
                }
            }
        });
    }
    onToggle = function() {
        if (fontSelected in fontsList && selectMode === SELECT_NONE && !gState && !rState && !sState && !hState) {
            var flipOffset = flipped ? 1 : 0;
            var delPos = [], addPos = [], edited = {};
            for (var key in vertSelection) {
                var vert = vertSelection[key];
                if (vert.command - flipOffset < 0 || +vert.command + 1 - flipOffset >= fontsList[vert.font].font.glyphs.glyphs[vert.id].path.commands.length) continue;
                var command = fontsList[vert.font].font.glyphs.glyphs[vert.id].path.commands[+vert.command + 1 - flipOffset];
                if (vert.index != "") continue;
                switch (command.type) {
                case "L":
                    command.type = "Q";
                    var start = fontsList[vert.font].font.glyphs.glyphs[vert.id].path.commands[+vert.command - flipOffset];
                    if (!("x1" in command)) command.x1 = (command.x + start.x) / 2;
                    if (!("y1" in command)) command.y1 = (command.y + start.y) / 2;
                    addPos.push(makePos(vert.font, vert.id, +vert.command + 1 - flipOffset, "1"));
                    break;
                case "Q":
                    command.type = "C";
                    if (!("x2" in command)) command.x2 = (command.x + command.x1) / 2;
                    if (!("y2" in command)) command.y2 = (command.y + command.y1) / 2;
                    addPos.push(makePos(vert.font, vert.id, +vert.command + 1 - flipOffset, "2"));
                    break;
                case "C":
                    fontsList[vert.font].font.glyphs.glyphs[vert.id].path.commands[+vert.command + 1 - flipOffset] = {type: "L", x: command.x, y: command.y};
                    delPos.push(vert);
                    break;
                }
                if (!(vert.font in edited)) {
                    edited[vert.font] = {};
                }
                edited[vert.font][vert.id] = true;
            }
            for (var key = vertSelection.length - 1; key >= 0; key--) {
                var vert = vertSelection[key];
                var command = fontsList[vert.font].font.glyphs.glyphs[vert.id].path.commands[+vert.command - flipOffset];
                if (vert.index == "" || command.type != "L") continue;
                for (var dkey in delPos) {
                    var delVert = delPos[dkey];
                    if (vert.font == delVert.font && vert.id == delVert.id && vert.command == delVert.command + 1) {
                        vertSelection.splice(key, 1);
                        break;
                    }
                }
            }
            vertSelection.push.apply(vertSelection, addPos);
            for (var fontId in edited) {
                if (fontId in fontsList) {
                    for (var glyph in edited[fontId]) {
                        updateBVH(fontsList[fontId].font, fontId, glyph);
                    }
                }
            }
            change = true;
        }
    }
    onOrigin = function() {
        if (gState || rState || sState || hState) return;
        var loc = mouseX < 0 || mouseY < 0 || mouseX > canvas.width || mouseY > canvas.height;
        if (loc) {
            posX = 0, posY = 0;
            for (var key in vertSelection) {
                var pos = getPos(vertSelection[key]);
                posX += pos.x;
                posY += pos.y;
            }
            posX /= vertSelection.length;
            posY /= vertSelection.length;
        } else {
            posX = (mouseX - originX) * fontScale;
            posY = (originY - mouseY) * fontScale;
        }
        updatePosition();
        guiChanged = true;
    }
    onFlip = function() {
        flipped = !flipped;
        guiChanged = true;
    }
    onSubdivide = function() {
        if (fontSelected in fontsList && selectMode === SELECT_NONE && !gState && !rState && !sState && !hState) {
            vertSelection = subdivide(fontsList, vertSelection);
            gState = TOOL_DONE;
            recalculate = true;
        }        
    }
    onExtrude = function() {
        if (selectMode !== SELECT_NONE || !(fontSelected in fontsList) || gState || rState || sState || hState) return;
        var loc = mouseX < 0 || mouseY < 0 || mouseX > canvas.width || mouseY > canvas.height;
        var x = loc ? Math.random() : (mouseX - originX) * fontScale;
        var y = loc ? Math.random() : (originY - mouseY) * fontScale;
        verts = 0;
        vertSelection = [];
        for (var key in glyphSelection) {
            if (!glyphSelection[key]) continue;
            glyphSelection[key].path.commands.push({type: "M", x: x, y: y})
            glyphSelection[key].path.commands.push({type: "Z"});
            updateBVH(fontsList[fontSelected].font, fontSelected, key);
            vertsTotal++;
        }
        vertCount.text(verts + "/" + vertsTotal);
        change = true;
    }
    onDelete = function() {
        if (fontSelected in fontsList && selectMode === SELECT_NONE && !gState && !rState && !sState) {
            var newSelection = [], fromVerts = {}, toVerts = {}, controls = {}, edited = {};
            for (var key in vertSelection) {
                var vert = vertSelection[key];
                if (+vert.command + 1 >= fontsList[vert.font].font.glyphs.glyphs[vert.id].path.commands.length) continue;
                var command = fontsList[vert.font].font.glyphs.glyphs[vert.id].path.commands[+vert.command + 1];
                if (!(vert.font in edited)) {
                    edited[vert.font] = {};
                }
                edited[vert.font][vert.id] = true;
                if (vert.index != "") {
                    if (!(vert.font in controls)) {
                        controls[vert.font] = {};
                    }
                    if (!(vert.id in controls[vert.font])) {
                        controls[vert.font][vert.id] = {};
                    }
                    if (!(vert.command in controls[vert.font][vert.id])) {
                        controls[vert.font][vert.id][vert.command] = {};
                    }
                    controls[vert.font][vert.id][vert.command][vert.index] = true;
                    continue;
                }
                if (!(vert.font in fromVerts)) {
                    fromVerts[vert.font] = {};
                    toVerts[vert.font] = {};
                }
                if (!(vert.id in fromVerts[vert.font])) {
                    fromVerts[vert.font][vert.id] = {};
                    toVerts[vert.font][vert.id] = {};
                }
                fromVerts[vert.font][vert.id][vert.command] = vert;
                if (command.type == "Z") {
                    var index = +vert.command + 1;
                    while (index > 0) {
                        index--;
                        if (fontsList[vert.font].font.glyphs.glyphs[vert.id].path.commands[index].type == "M") {
                            break;
                        }
                    }
                    toVerts[vert.font][vert.id][vert.command] = makePos(vert.font, vert.id, index, "");
                } else {
                    toVerts[vert.font][vert.id][vert.command] = makePos(vert.font, vert.id, +vert.command + 1, "");
                }
            }
            for (var fontId in controls) {
                for (var glyph in controls[fontId]) {
                    for (var command in controls[fontId][glyph]) {
                        var vert = controls[fontId][glyph][command];
                        if (!("2" in vert)) continue;
                        var com = fontsList[fontId].font.glyphs.glyphs[glyph].path.commands[command];
                        fontsList[fontId].font.glyphs.glyphs[glyph].path.commands[command] = {type: "Q", x1: com.x1, y1: com.y1, x: com.x, y: com.y};
                        vertsTotal--;
                    }
                    for (var command in controls[fontId][glyph]) {
                        var vert = controls[fontId][glyph][command];
                        if (!("1" in vert)) continue;
                        var com = fontsList[fontId].font.glyphs.glyphs[glyph].path.commands[command];
                        switch (com.type) {
                        case "Q":
                            fontsList[fontId].font.glyphs.glyphs[glyph].path.commands[command] = {type: "L", x: com.x, y: com.y};
                            break;
                        case "C":
                            fontsList[fontId].font.glyphs.glyphs[glyph].path.commands[command] = {type: "Q", x1: com.x2, y1: com.y2, x: com.x, y: com.y};
                            break;
                        }
                        vertsTotal--;
                    }
                }
            }
            for (var fontId in fromVerts) {
                for (var glyph in fromVerts[fontId]) {
                    var delVerts = [];
                    for (var command in fromVerts[fontId][glyph]) {
                        var fromCom = fontsList[fontId].font.glyphs.glyphs[glyph].path.commands[fromVerts[fontId][glyph][command].command];
                        var toCom = fontsList[fontId].font.glyphs.glyphs[glyph].path.commands[toVerts[fontId][glyph][command].command];
                        if (flipped && fromCom.type == "M" || !flipped && toCom.type == "M") {
                            if (fromVerts[fontId][glyph][command].command == toVerts[fontId][glyph][command].command) {
                                delVerts.push(fromVerts[fontId][glyph][command].command);
                                delVerts.push(toVerts[fontId][glyph][command].command);
                                vertsTotal++; // offset deleting Z
                            }
                            continue;
                        }
                        if (!flipped) {
                            switch (fromCom.type) {
                            case "M":
                                fontsList[fontId].font.glyphs.glyphs[glyph].path.commands[toVerts[fontId][glyph][command].command] = {type: "M", x: toCom.x, y: toCom.y};
                                break;
                            case "L":
                                fontsList[fontId].font.glyphs.glyphs[glyph].path.commands[toVerts[fontId][glyph][command].command] = {type: "L", x: toCom.x, y: toCom.y};
                                break;
                            case "Q":
                                fontsList[fontId].font.glyphs.glyphs[glyph].path.commands[toVerts[fontId][glyph][command].command] = {type: "Q", x1: fromCom.x1, y1: fromCom.y1, x: toCom.x, y: toCom.y};
                                break;
                            case "C":
                                fontsList[fontId].font.glyphs.glyphs[glyph].path.commands[toVerts[fontId][glyph][command].command] = {type: "C", x1: fromCom.x1, y1: fromCom.y1, x2: fromCom.x2, y2: fromCom.y2, x: toCom.x, y: toCom.y};
                                break;
                            }
                        }
                        delVerts.push(command);
                    }
                    delVerts.sort(comparer);
                    for (var key = delVerts.length; key --> 0; ) {
                        switch (fontsList[fontId].font.glyphs.glyphs[glyph].path.commands[delVerts[key]]) {
                        case "L":
                            linesTotal--;
                            break;
                        case "Q":
                            curvesTotal--;
                            break;
                        case "C":
                            beziersTotal--;
                            break;
                        }
                        fontsList[fontId].font.glyphs.glyphs[glyph].path.commands.splice(delVerts[key], 1);
                    }
                    vertsTotal -= delVerts.length;
                }
            }
            for (var fontId in edited) {
                if (fontId in fontsList) {
                    for (var glyph in edited[fontId]) {
                        updateBVH(fontsList[fontId].font, fontId, glyph);
                    }
                }
            }
            verts = 0;
            vertSelection = [];
            vertCount.text(verts + "/" + vertsTotal);
            lineCount.text(linesTotal);
            curveCount.text(curvesTotal);
            bezierCount.text(beziersTotal);
            recalculate = true;
        }
    }
    onHandle = function() {
        hState = !hState;
        vertSelection = [];
        recalculate = true;
    }
    onMass = function() {
        mState = 1 - mState;
        if (mState !== MASS_NONE) {
            window.alert("Warning! This feature did not load correctly, and thus is not supported for use.");
        }
    }
}

setupEditor = function() {
    canvas = document.getElementById("canvas-editor");
    overlayCanvas = document.getElementById("canvas-editor-overlay");
    editor = $("#editor");
    canvas.width = editor.innerWidth();
    canvas.height = editor.innerHeight();
    overlayCanvas.width = canvas.width;
    overlayCanvas.height = canvas.height;
    ctx = canvas.getContext("2d");
    overlayCtx = overlayCanvas.getContext("2d");
    
    editor.on("mousewheel", function(event) {
        if (!(fontSelected in fontsList) || gState || rState || sState) return true;
        var diff = event.deltaY * (fontSize / 48 + 1);
        if (fontSize + diff > MAX_FONT_SIZE) return false;
        if (selectMode === SELECT_CIRCLE || mState !== MASS_NONE) {
            selectRadius += event.deltaY;
            if (selectRadius < 0) selectRadius = 0;
        } else {
            if (fontSize >= 16) {
                var pow = (fontSize - 16) / 16;
                if (pow > 1) pow = 1;
                originX -= (mouseX - originX) / fontSize * diff * pow;
                originY -= (mouseY - originY) / fontSize * diff * pow;
            }
            fontSize += diff;
            if (fontSize < 1) fontSize = 1;
            fontScale = fontsList[fontSelected].font.unitsPerEm / fontSize;
        }
        change = true;
        return false;
    });

    overlayCanvas.oncontextmenu = function(event) {
        event.preventDefault();
    };
}

drawGrid = function() {
    ctx.fillStyle = "#ffffff";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    var step = fontSize;
    while (step > 128) {
        step /= 2;
    }
    
    if (step > 64) {
        ctx.setLineDash([1]);
        ctx.beginPath();
        for (var x = originX % step; x < canvas.width; x += step) {
            if (x < 0 || x == originX) continue;
            ctx.moveTo(x, 0);
            ctx.lineTo(x, canvas.height);
        }
        for (var y = originY % step; y < canvas.height; y += step) {
            if (y < 0 || y == originY) continue;
            ctx.moveTo(0, y);
            ctx.lineTo(canvas.width, y);
        }
        ctx.strokeStyle = colorStroke;
        ctx.lineWidth = widthStroke / 2;
        ctx.stroke();
    
        step /= 2;
        ctx.beginPath();
        for (var x = originX % step; x < canvas.width; x += step) {
            if (x < 0 || x == originX) continue;
            ctx.moveTo(x, 0);
            ctx.lineTo(x, canvas.height);
        }
        for (var y = originY % step; y < canvas.height; y += step) {
            if (y < 0 || y == originY) continue;
            ctx.moveTo(0, y);
            ctx.lineTo(canvas.width, y);
        }
        ctx.strokeStyle = colorStroke;
        ctx.lineWidth = widthStroke / 2 * (step / 32 - 1);
        ctx.stroke();
    }
    
    step = fontSize;
    if (step < 1) step = 1;
    while (step < 16) {
        step *= 2;
    }
    ctx.setLineDash([]);
    ctx.beginPath();
    for (var x = originX % step; x < canvas.width; x += step) {
        if (x < 0 || x == originX) continue;
        ctx.moveTo(x, 0);
        ctx.lineTo(x, canvas.height);
    }
    for (var y = originY % step; y < canvas.height; y += step) {
        if (y < 0 || y == originY) continue;
        ctx.moveTo(0, y);
        ctx.lineTo(canvas.width, y);
    }
    ctx.strokeStyle = "#000000";
    ctx.lineWidth = widthStroke;
    ctx.stroke();
}

drawOrigin = function(context, posX, posY) {
    context.beginPath();
    if (posX >= 0 && posX < context.canvas.width) {
        context.moveTo(posX, 0);
        context.lineTo(posX, context.canvas.height);
    }
    if (posY >= 0 && posY < context.canvas.height) {
        context.moveTo(0, posY);
        context.lineTo(context.canvas.width, posY);
    }
    context.strokeStyle = INVERT; // blender selection color
    context.lineWidth = 2 * widthStroke;
    context.stroke();
}

// Recalculate all pieces of the font
recalculateFont = function() {
    verts = vertSelection.length;
    vertsTotal = linesTotal = curvesTotal = beziersTotal = 0;
    for (var key in glyphSelection) {
        if (!glyphSelection[key]) continue;
        vertsTotal += bvh[fontSelected][key].length;
        for (var ckey in glyphSelection[key].path.commands) {
            var type = glyphSelection[key].path.commands[ckey].type;
            switch (type) {
                case "L":
                    linesTotal++;
                    break;
                case "Q":
                    curvesTotal++;
                    break;
                case "C":
                    beziersTotal++;
            }
        }
    }
    vertCount.text(verts + "/" + vertsTotal);
    lineCount.text(linesTotal);
    curveCount.text(curvesTotal);
    bezierCount.text(beziersTotal);

    if (gState === TOOL_DONE || rState === TOOL_DONE || sState === TOOL_DONE) {
        var recalcItems = {};
        for (var key in vertSelection) {
            var vert = vertSelection[key];
            if (!(vert.font in recalcItems)) {
                recalcItems[vert.font] = {};
            }
            recalcItems[vert.font][vert.id] = true;
        }
        for (var fontId in recalcItems) {
            if (fontId in fontsList) {
                for (var glyph in recalcItems[fontId]) {
                    updateBVH(fontsList[fontId].font, fontId, glyph);
                }
            }
        }
        gState = TOOL_NONE;
        rState = TOOL_NONE;
        sState = TOOL_NONE;
        fontsList[fontSelected].save = false;
        modified = true;
    }
    
    if (hState) {
        handles = {};
        for (var key in glyphSelection) {
            if (!glyphSelection[key]) continue;
            var len = glyphSelection[key].path.commands.length;
            var last;
            for (var ckey = 0; ckey < len - 1; ckey++) {
                var prev = glyphSelection[key].path.commands[ckey];
                var next = glyphSelection[key].path.commands[ckey + 1];
                if (prev.type == "M") last = ckey;
                if (prev.type == "C" && next.type == "C") {
                    if (!(key in handles)) {
                        handles[key] = {};
                    }
                    handles[key][ckey] = {c1: ckey + 1, c2: ckey};
                } else if (prev.type == "C" && next.type == "Z") {
                    var move = glyphSelection[key].path.commands[last];
                    var first = glyphSelection[key].path.commands[last + 1];
                    if (first.type == "C") {
                        if (!(key in handles)) {
                            handles[key] = {};
                        }
                        if (Math.abs(move.x - prev.x) < EPS_D && Math.abs(move.y - prev.y) < EPS_D) {
                            handles[key][ckey] = {c1: null, c2: ckey, sibling: last + 1};
                            handles[key][last] = {c1: last + 1, c2: null, parent: ckey};
                        }
                    }
                }
            }
        }
    }

    if (modified) {
        modifiedGlyphs = {};
        var modifiersOrder = [];
        for (var id in modifiersList) {
            modifiersOrder.push(id);
        }
        if (modifiersOrder.length) {
            modifiersOrder.sort(comparer);
            for (var key in glyphSelection) {
                if (!glyphSelection[key]) continue;
                modifiedGlyphs[key] = [];
                var commands = fontsList[fontSelected].font.glyphs.glyphs[key].path.commands;
                for (var ckey = 0; ckey < commands.length; ckey++) {
                    modifiedGlyphs[key].push($.extend({}, commands[ckey]));
                }
                for (var id = 0; id < modifiersOrder.length; id++) {
                    var modifier = modifiersList[modifiersOrder[id]];
                    switch (modifier.type) {
                    case MODIFIER_SUBDIV:
                        applySubdivide(key, modifier.recurse, modifier.subdiv);
                        break;
                    case MODIFIER_DECIMATE:
                        applyDecimate(key, modifier.percent);
                        break;
                    case MODIFIER_DISPLACE:
                        applyDisplace(key, modifier.texture, modifier.seed, modifier.strength, modifier.midpoint);
                        break;
                    case MODIFIER_ALIGN:
                        applyAlign(key, modifier.scale, modifier.align);
                        break;
                    }
                }
            }
        }
        onFontLoaded();
    }
    modified = false;
    change = true;
    guiChanged = true;
    recalculating = false;
}

redraw = function() {
    requestAnimationFrame(redraw);
    if (viewChanged || change && mode === MODE_VIEW) {
        viewChanged = false;
        if (reset) {
            viewFontSize = 72;
            viewOriginX = 8;
            viewOriginY = 4 * viewCanvas.height / 5;
        }
        if (mode === MODE_VIEW) {
            viewCanvas.width = viewControl.innerWidth();
            viewCanvas.height = Math.max(0, viewer.innerHeight() - viewLabel.innerHeight() - viewControl.innerHeight() - 17);
            if (fontSelected in fontsList) {
                viewCtx.clearRect(0, 0, viewCanvas.width, viewCanvas.height);
                drawOrigin(viewCtx, viewOriginX, viewOriginY);
                var path = fontsList[fontSelected].font.getPath(currentPangram, viewOriginX, viewOriginY, viewFontSize, {kerning: true});
                path.fill = BEIGE;
                path.draw(viewCtx);
            }
        } else {
            selectCanvas.css("height", Math.max(0, viewer.innerHeight() - selectControl.innerHeight() - viewLabel.innerHeight() - 6) + "px");
        }
    }
    if (change || recalculate) {
        change = false;
        canvas.width = editor.innerWidth();
        canvas.height = editor.innerHeight();
        overlayCanvas.width = canvas.width;
        overlayCanvas.height = canvas.height;
        if (reset) {
            fontSize = 256;
            if (fontSelected in fontsList) fontScale = fontsList[fontSelected].font.unitsPerEm / fontSize;
            originX = canvas.width / 2 - fontSize;
            originY = canvas.height / 2 + fontSize / 2;
        }
        if (fontSelected in fontsList) {
            drawGrid();
            if (mode === MODE_EDIT) {
                for (var key in glyphSelection) {
                    if (!glyphSelection[key]) continue;
                    glyphSelection[key].drawMetrics(ctx, originX, originY, fontSize);
                }
            }
            drawOrigin(ctx, originX, originY);
            for (var key in glyphSelection) {
                if (!glyphSelection[key]) continue;
                var path = glyphSelection[key].getPath(originX, originY, fontSize);
                if ("fill" in glyphSelection[key].path) path.fill = glyphSelection[key].path.fill;
                if ("stroke" in glyphSelection[key].path) path.stroke = glyphSelection[key].path.stroke;
                if ("strokeWidth" in glyphSelection[key].path) path.strokeWidth = glyphSelection[key].path.strokeWidth;
                path.draw(ctx);
            }
            if (mode === MODE_EDIT) {
                ctx.beginPath();
                if (hState) {
                    ctx.setLineDash([]);
                    for (var key in handles) {
                        for (var ckey in handles[key]) {
                            if (!glyphSelection[key]) continue;
                            var prev = glyphSelection[key].path.commands[handles[key][ckey].c2];
                            var next = glyphSelection[key].path.commands[handles[key][ckey].c1];
                            if ("sibling" in handles[key][ckey]) {
                                next = glyphSelection[key].path.commands[handles[key][ckey].sibling];
                            }
                            if (prev && next) {
                                ctx.moveTo(originX + prev.x2 / fontScale, originY - prev.y2 / fontScale);
                                ctx.lineTo(originX + prev.x / fontScale, originY - prev.y / fontScale);
                                ctx.lineTo(originX + next.x1 / fontScale, originY - next.y1 / fontScale);
                            }
                        }
                    }
                } else {
                    ctx.setLineDash([3, 2]);
                    for (var key in glyphSelection) {
                        if (!glyphSelection[key]) continue;
                        var len = glyphSelection[key].path.commands.length;
                        for (var ckey = 0; ckey < len - 1; ckey++) {
                            var prev = glyphSelection[key].path.commands[ckey];
                            var next = glyphSelection[key].path.commands[ckey + 1];
                            if (next.type == "Q") {
                                ctx.moveTo(originX + prev.x / fontScale, originY - prev.y / fontScale);
                                ctx.lineTo(originX + next.x1 / fontScale, originY - next.y1 / fontScale);
                                ctx.lineTo(originX + next.x / fontScale, originY - next.y / fontScale);
                            } else if (next.type == "C") {
                                ctx.moveTo(originX + prev.x / fontScale, originY - prev.y / fontScale);
                                ctx.lineTo(originX + next.x1 / fontScale, originY - next.y1 / fontScale);
                                ctx.moveTo(originX + next.x2 / fontScale, originY - next.y2 / fontScale);
                                ctx.lineTo(originX + next.x / fontScale, originY - next.y / fontScale);
                            }
                        }
                    }
                }
                ctx.lineWidth = 2 * widthStroke;
                ctx.strokeStyle = "#00ff00";
                ctx.stroke();
                ctx.setLineDash([]);

                for (var key in glyphSelection) {
                    if (!glyphSelection[key]) continue;
                    glyphSelection[key].drawPoints(ctx, originX, originY, fontSize);
                }
            }
            for (var key in modifiedGlyphs) {
                if (!modifiedGlyphs[key]) continue;
                var glyph = new opentype.Glyph({path: {unitsPerEm: fontsList[fontSelected].font.unitsPerEm, commands: modifiedGlyphs[key]}});
                var path = glyph.getPath(originX, originY, fontSize);
                path.fill = "rgba(127, 127, 127, 0.5)";
                path.stroke = "#7f7f7f";
                path.draw(ctx);
            }
            if (debug) {
                ctx.strokeStyle = "#ff00ff";
                ctx.beginPath();
                var scale = fontSize / fontsList[fontSelected].font.unitsPerEm;
                for (var key in glyphSelection) {
                    if (!glyphSelection[key]) continue;
                    addBBoxPath(bvh[fontSelected][key], scale);
                }
                ctx.stroke();
            }
        } else {
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            overlayCtx.clearRect(0, 0, overlayCanvas.width, overlayCanvas.height);
            viewCtx.clearRect(0, 0, viewCanvas.width, viewCanvas.height);
        }
        if (recalculate && !recalculating) {
            recalculating = true;
            setTimeout(recalculateFont, 0);
        }
        recalculate = false;
        guiChanged = true;
    }
    if (guiChanged) {
        guiChanged = false;
        overlayCtx.clearRect(0, 0, overlayCanvas.width, overlayCanvas.height);
        if (mode === MODE_EDIT && fontSelected in fontsList) {
            var delta = selectMode === SELECT_CIRCLE ? selectRadius : 10 * DELTA;
            var intersections = fontsList[fontSelected].isect(mouseX, mouseY, delta * fontScale, ISECT_CIRCLE);
            var hoverSelection = [];
            
            var positions = []
            
            for (var key in intersections) {
                var pos = getPos(intersections[key]);
                pos.x = originX + pos.x / fontScale;
                pos.y = originY - pos.y / fontScale;
                if (mouseX + DELTA < pos.x || mouseX - DELTA > pos.x || mouseY + DELTA < pos.y || mouseY - DELTA > pos.y) continue;
                hoverSelection.push(pos);
            }
            
            if (intersections.length) {
                overlayCtx.beginPath();
                for (var key in intersections) {
                    var pos = getPos(intersections[key]);
                    overlayCtx.rect(originX + pos.x / fontScale - DELTA - 0.5, originY - pos.y / fontScale - DELTA - 0.5, 2 * DELTA, 2 * DELTA);
                }
                overlayCtx.lineWidth = widthStroke;
                overlayCtx.strokeStyle = "#ffffff";
                overlayCtx.stroke();
                
                overlayCtx.beginPath();
                for (var key in intersections) {
                    var pos = getPos(intersections[key]);
                    overlayCtx.rect(originX + pos.x / fontScale - DELTA + 0.5, originY - pos.y / fontScale - DELTA + 0.5, 2 * DELTA, 2 * DELTA);
                }
                overlayCtx.lineWidth = widthStroke;
                overlayCtx.strokeStyle = "#000000";
                overlayCtx.stroke();            
            }
            
            if (vertSelection.length) {
                if (!hState) {
                    var flipOffset = flipped ? 1 : 0;
                    overlayCtx.setLineDash([5, 3]);
                    overlayCtx.beginPath();
                    for (var key in vertSelection) {
                        var vert = vertSelection[key];
                        if (vert.index != "") continue;
                        var pos = getPos(vert);
                        var posTo = null;
                        var index = +vert.command + 1 - flipOffset;
                        var type = fontsList[vert.font].font.glyphs.glyphs[vert.id].path.commands[index].type;
                        if (flipped && type == "M") {
                            while (index < fontsList[vert.font].font.glyphs.glyphs[vert.id].path.commands.length) {
                                index++;
                                if (fontsList[vert.font].font.glyphs.glyphs[vert.id].path.commands[index].type == "Z") {
                                    break;
                                }
                            }
                            var posTo = fontsList[vert.font].font.glyphs.glyphs[vert.id].path.commands[index - 1];
                        } else if (!flipped && type == "Z") {
                            while (index > 0) {
                                index--;
                                if (fontsList[vert.font].font.glyphs.glyphs[vert.id].path.commands[index].type == "M") {
                                    break;
                                }
                            }
                            var posTo = fontsList[vert.font].font.glyphs.glyphs[vert.id].path.commands[index];
                        }
                        if (posTo) {
                            overlayCtx.moveTo(originX + pos.x / fontScale + 1, originY - pos.y / fontScale + 1);
                            overlayCtx.lineTo(originX + posTo.x / fontScale + 1, originY - posTo.y / fontScale + 1);
                        }
                    }
                    overlayCtx.lineWidth = 2 * widthStroke;
                    overlayCtx.strokeStyle = "#ff0000";
                    overlayCtx.stroke();

                    overlayCtx.setLineDash([]);
                    overlayCtx.beginPath();
                    for (var key in vertSelection) {
                        var vert = vertSelection[key];
                        if (+vert.command - flipOffset < 0 || vert.index != "") continue;
                        var pos = getPos(makePos(vert.font, vert.id, +vert.command - flipOffset, vert.index));
                        var command = fontsList[vert.font].font.glyphs.glyphs[vert.id].path.commands[+vert.command + 1 - flipOffset];
                        switch (command.type) {
                        case "L":
                            overlayCtx.moveTo(originX + pos.x / fontScale + 1, originY - pos.y / fontScale + 1);
                            overlayCtx.lineTo(originX + command.x / fontScale + 1, originY - command.y / fontScale + 1);
                            break;
                        case "Q":
                            overlayCtx.moveTo(originX + pos.x / fontScale + 1, originY - pos.y / fontScale + 1);
                            overlayCtx.quadraticCurveTo(originX + command.x1 / fontScale + 1, originY - command.y1 / fontScale + 1, originX + command.x / fontScale + 1, originY - command.y / fontScale + 1);
                            break;
                        case "C":
                            overlayCtx.moveTo(originX + pos.x / fontScale + 1, originY - pos.y / fontScale + 1);
                            overlayCtx.bezierCurveTo(originX + command.x1 / fontScale + 1, originY - command.y1 / fontScale + 1, originX + command.x2 / fontScale + 1, originY - command.y2 / fontScale + 1, originX + command.x / fontScale + 1, originY - command.y / fontScale + 1);
                            break;
                        }
                    }
                    overlayCtx.lineWidth = 2 * widthStroke;
                    overlayCtx.strokeStyle = "#ff0000";
                    overlayCtx.stroke();
                }

                overlayCtx.beginPath();
                for (var key in vertSelection) {
                    var pos = getPos(vertSelection[key]);
                    overlayCtx.rect(originX + pos.x / fontScale - DELTA, originY - pos.y / fontScale - DELTA, 2 * DELTA, 2 * DELTA);
                }
                var lastPos = getPos(vertSelection[vertSelection.length - 1])
                lastPos.x = originX + lastPos.x / fontScale;
                lastPos.y = originY - lastPos.y / fontScale;
                overlayCtx.moveTo(lastPos.x + SQRT2 * DELTA, lastPos.y);
                overlayCtx.lineTo(lastPos.x, lastPos.y + SQRT2 * DELTA);
                overlayCtx.lineTo(lastPos.x - SQRT2 * DELTA, lastPos.y);
                overlayCtx.lineTo(lastPos.x, lastPos.y - SQRT2 * DELTA);
                overlayCtx.lineTo(lastPos.x + SQRT2 * DELTA, lastPos.y);
                overlayCtx.lineWidth = 2 * widthStroke;
                overlayCtx.strokeStyle = SELECT;
                overlayCtx.stroke();
                
                var x = originX + posX / fontScale;
                var y = originY - posY / fontScale;
                overlayCtx.beginPath();
                overlayCtx.moveTo(x - 3 * DELTA, y);
                overlayCtx.lineTo(x + 3 * DELTA, y);
                overlayCtx.moveTo(x, y - 3 * DELTA);
                overlayCtx.lineTo(x, y + 3 * DELTA);
                overlayCtx.moveTo(x + 2 * DELTA, y);
                overlayCtx.arc(x, y, 2 * DELTA, 0, 2 * Math.PI);
                overlayCtx.setLineDash([2]);
                if (gState === TOOL_EDIT) {
                    overlayCtx.moveTo(x, y);
                    if (number) {
                        overlayCtx.lineTo(x - (constrain === CONSTRAIN_Y ? 0 : Number(number)), y - (constrain === CONSTRAIN_X ? 0 : Number(number)));
                    } else {
                        overlayCtx.lineTo(x - (constrain === CONSTRAIN_Y ? 0 : dMouseX), y - (constrain === CONSTRAIN_X ? 0 : dMouseY));
                    }
                } else if (rState === TOOL_EDIT || sState === TOOL_EDIT) {
                    overlayCtx.moveTo(x, y);
                    overlayCtx.lineTo(mouseX, mouseY);
                }
                overlayCtx.setLineDash([]);
                overlayCtx.lineWidth = widthStroke;
                overlayCtx.strokeStyle = INVERT;
                overlayCtx.stroke();
                
                if ((gState || rState || sState)) {
                    var disp = gState ? "Translate: " : rState ? "Rotate: " : "Scale: ";
                    if (number) {
                        disp += Number(number);
                    } else {
                        var dx = constrain === CONSTRAIN_Y ? 0 : dMouseX * fontScale;
                        var dy = constrain === CONSTRAIN_X ? 0 : -dMouseY * fontScale;
                        var a = (Math.atan2(y - mouseY, mouseX - x) - angle) * 180 / Math.PI;
                        if (a < -180) a += 360;
                        if (a > 180) a -= 360;
                        var d = Math.sqrt((x - mouseX) * (x - mouseX) + (y - mouseY) * (y - mouseY)) / distance * 100;
                        if (negative) d *= -1;
                        if (gState) disp += dx.toFixed(3) + ", " + dy.toFixed(3);
                        if (rState) disp += a.toFixed(3) + " deg";
                        if (sState) disp += d.toFixed(3) + "%";
                    }
                    if (constrain === CONSTRAIN_X) {
                        disp += " (x)";
                    } else if (constrain === CONSTRAIN_Y) {
                        disp += " (y)";
                    }
                    overlayCtx.font = '16px Lato';
                    overlayCtx.fillStyle = SELECT;
                    overlayCtx.fillText(disp, 5, canvas.height - 5);
                }
                
                if (mState !== MASS_NONE && selectMode !== SELECT_CIRCLE) {
                    overlayCtx.setLineDash([2]);
                    overlayCtx.beginPath();
                    overlayCtx.moveTo(x + selectRadius, y);
                    overlayCtx.arc(x, y, selectRadius, 0, 2 * Math.PI);
                    overlayCtx.fillStyle = "rgba(0, 255, 255, 0.5)";
                    overlayCtx.fill();
                    overlayCtx.lineWidth = widthStroke;
                    overlayCtx.strokeStyle = FG;
                    overlayCtx.stroke();
                    overlayCtx.setLineDash([]);
                }
            }
            
            if (hoverSelection.length) {
                overlayCtx.beginPath();
                for (var key in hoverSelection) {
                    var pos = hoverSelection[key];
                    overlayCtx.rect(pos.x - DELTA, pos.y - DELTA, 2 * DELTA, 2 * DELTA);
                }
                overlayCtx.lineWidth = 2 * widthStroke;
                overlayCtx.strokeStyle = INVERT;
                overlayCtx.stroke();
            }
            
            if (selectMode === SELECT_CIRCLE) {
                overlayCtx.setLineDash([2]);
                overlayCtx.beginPath();
                overlayCtx.moveTo(mouseX + selectRadius, mouseY);
                overlayCtx.arc(mouseX, mouseY, selectRadius, 0, 2 * Math.PI);
                overlayCtx.fillStyle = "rgba(255, 255, 0, 0.5)";
                overlayCtx.fill();
                overlayCtx.lineWidth = widthStroke;
                overlayCtx.strokeStyle = FG;
                overlayCtx.stroke();
                
                overlayCtx.setLineDash([]);
                overlayCtx.beginPath();
                overlayCtx.moveTo(mouseX - (3 - SQRT2) * DELTA, mouseY - 3 * DELTA)
                overlayCtx.arc(mouseX - 3 * DELTA, mouseY - 3 * DELTA, SQRT2 * DELTA, 0, 2 * Math.PI);
                overlayCtx.lineWidth = 2 * widthStroke;
                overlayCtx.strokeStyle = "#ff00ff";
                overlayCtx.stroke();
            } else if (selectMode === SELECT_BOX || selectMode === SELECT_READY) {
                if (selectMode === SELECT_BOX) {
                    overlayCtx.setLineDash([2]);
                    overlayCtx.beginPath();
                    overlayCtx.moveTo(beginBoxX, beginBoxY);
                    overlayCtx.rect(beginBoxX, beginBoxY, mouseX - beginBoxX, mouseY - beginBoxY);
                    overlayCtx.fillStyle = "rgba(255, 255, 0, 0.5)";
                    overlayCtx.fill();
                    overlayCtx.lineWidth = widthStroke;
                    overlayCtx.strokeStyle = FG;
                    overlayCtx.stroke();
                }

                overlayCtx.setLineDash([]);
                overlayCtx.beginPath();
                overlayCtx.rect(mouseX - 4 * DELTA, mouseY - 4 * DELTA, 2 * DELTA, 2 * DELTA);
                overlayCtx.lineWidth = 2 * widthStroke;
                overlayCtx.strokeStyle = "#ff00ff";
                overlayCtx.stroke();
            }
            
            if (shift || ctrl) {
                overlayCtx.beginPath();
                overlayCtx.moveTo(mouseX + 2 * DELTA, mouseY - 3 * DELTA);
                overlayCtx.lineTo(mouseX + 4 * DELTA, mouseY - 3 * DELTA);
                if (shift) {
                    overlayCtx.moveTo(mouseX + 3 * DELTA, mouseY - 4 * DELTA);
                    overlayCtx.lineTo(mouseX + 3 * DELTA, mouseY - 2 * DELTA);
                }
                overlayCtx.lineWidth = 2 * widthStroke;
                overlayCtx.strokeStyle = "#ff00ff";
                overlayCtx.stroke();
            }
            
            if (alt) {
                overlayCtx.beginPath();
                overlayCtx.moveTo(mouseX - 3 * DELTA, mouseY + 2 * DELTA);
                overlayCtx.lineTo(mouseX - 3 * DELTA, mouseY + 4 * DELTA);
                overlayCtx.moveTo(mouseX - (3 - SQRT3 / 2) * DELTA, mouseY + 2.5 * DELTA);
                overlayCtx.lineTo(mouseX - (3 + SQRT3 / 2) * DELTA, mouseY + 3.5 * DELTA);
                overlayCtx.moveTo(mouseX - (3 - SQRT3 / 2) * DELTA, mouseY + 3.5 * DELTA);
                overlayCtx.lineTo(mouseX - (3 + SQRT3 / 2) * DELTA, mouseY + 2.5 * DELTA);
                overlayCtx.lineWidth = 2 * widthStroke;
                overlayCtx.strokeStyle = "#ff00ff";
                overlayCtx.stroke();
            }
        }
    }
    var time = new Date().getTime();
    frames[frameIndex] = time;
    if (fps < 60) fps++;
    frameIndex = (frameIndex + 1) % 60;
    for (var index = frameIndex; index < frameIndex + 60 && time - frames[index % 60] > 1000 && fps > 0; index++) {
        frames[index % 60] = time;
        fps--;
    }
    reset = false;
}

setupKeybindings = function() {
    $(window).on("keydown", function(event) {
        var keyCode = event.keyCode || event.which; 
        var controllerKey = keyCode >= 16 && keyCode <= 18;
        
        if ((dialog || mouseX < 0 || mouseY < 0 || mouseX > canvas.width || mouseY > canvas.height) && !controllerKey) return;
        switch(keyCode) {
        case 71: // g
            onTranslate();
            break;
        case 82: // r
            onRotate();
            break;
        case 83: // s
            onScale();
            break;
        case 65: // a
            onAll();
            break;
        case 66: // b
            onBox();
            break;
        case 67: // c
            onCircle();
            break;
        case 9: // tab
            onMode();
            event.preventDefault();
            break;
        case 49: case 97: // 1
            onLinear();
            break;
        case 50: case 98: // 2
            onQuadratic();
            break;
        case 51: case 99: // 3
            onCubic();
            break;
        case 68: // d
            onDupe();
            break;
        case 79: // o
            onOrigin();
            break;
        case 70: // f
            onFlip();
            break;
        case 81: // q
            onToggle();
            break;
        case 87: // w
            onSubdivide();
            break;
        case 69: // e
            onExtrude();
            break;
        case 88: // x
            if (gState || sState) {
                if (number !== null) {
                    var num = Number(number);
                    if (constrain !== CONSTRAIN_X) {
                        redo(constrain, num);
                        constrain = CONSTRAIN_X;
                        undo(constrain, num);
                    }
                } else {
                    if (constrain === CONSTRAIN_X) {
                        redo(constrain);
                        constrain = CONSTRAIN_NONE;
                    } else {
                        if (constrain === CONSTRAIN_Y) redo(constrain);
                        constrain = CONSTRAIN_X;
                        undo(constrain);
                    }
                }
            } else if (!rState && !hState) {
                onDelete();
            }
            break;
        case 89: // y
            if (gState || sState) {
                if (number !== null) {
                    var num = Number(number);
                    if (constrain !== CONSTRAIN_Y) {
                        redo(constrain, num);
                        constrain = CONSTRAIN_Y;
                        undo(constrain, num);
                    }
                } else {
                    if (constrain === CONSTRAIN_Y) {
                        redo(constrain);
                        constrain = CONSTRAIN_NONE;
                    } else {
                        if (constrain === CONSTRAIN_X) redo(constrain);
                        constrain = CONSTRAIN_Y;
                        undo(constrain);
                    }
                }
            }
            break;
        case 72: // h
            onHandle();
            change = true;
            break;
        case 77: // m
            onMass();
            guiChanged = true;
            break;
        case 16: // shift
            shift = true;
            guiChanged = true;
            event.preventDefault();
            break;
        case 17: // ctrl
            ctrl = true;
            guiChanged = true;
            event.preventDefault();
            break;
        case 18: // alt
            alt = true;
            guiChanged = true;
            event.preventDefault();
            break;
        case 114: // F3
            debug = !debug;
            change = true;
            event.preventDefault();
            break;
        case 109: case 189: // negative (overwrites number input)
            constrain ^= CONSTRAIN_BOTH;
            redo(constrain);
            negative = !negative;
            undo(constrain);
            constrain ^= CONSTRAIN_BOTH;
            break;
/* number input
        default:
            if (keyCode >= 48 && keyCode <= 57 || keyCode >= 96 && keyCode <= 105) { // numbers || numpad
                var num = keyCode - (keyCode > 57 ? 96 : 48);
                if (number === null) {
                    number = ""
                    constrain = CONSTRAIN_X;
                    undo(constrain);
                    number = number.concat(num);
                    redo(constrain, num);
                } else {
                    var prev = Number(number);
                    number = number.concat(num);
                    var next = Number(number);
                    undo(constrain, prev);
                    redo(constrain, next);
                }
            } else if (keyCode == 110 || keyCode == 190) { // period
                if (number === null) number = "0";
                if (!decimal) number = number.concat(".");
                decimal = true;
            } else if (keyCode == 8) { // backspace
                if (number && number.length > 0) {
                    if (number.slice(-1) == ".") decimal = false;
                    var prev = Number(number);
                    number = number.slice(0, -1);
                    var next = Number(number);
                    undo(constrain, prev);
                    redo(constrain, next);
                }
                event.preventDefault();
            } else if (keyCode == 109 || keyCode == 189) { // negative
                negative = !negative;
            }
*/
        }
        if (!controllerKey && "activeElement" in document) document.activeElement.blur();
    });
    $(window).on("keyup", function(event) {
        var keyCode = event.keyCode || event.which; 

        switch(keyCode) {
            case 13: // enter
                if (gState) gState = TOOL_DONE;
                if (rState) rState = TOOL_DONE;
                if (sState) sState = TOOL_DONE;
                recalculate = gState + rState + sState > 0;
                break;
            case 16: // shift
                shift = false;
                guiChanged = true;
                break;
            case 17: // ctrl
                ctrl = false;
                guiChanged = true;
                break;
            case 18: // alt
                alt = false;
                guiChanged = true;
                break;
        }
    });
}

setupViewer = function() {
    newPangram = function() {
        previousPangram = currentPangram;
        for (var i = 0; i < 10 && previousPangram == currentPangram; i++) {
            currentPangram = pangrams[Math.floor(Math.random() * pangrams.length)];
        }
        $("#view-area-text").val(currentPangram);
        viewChanged = true;
    }
    newPangram();
    $("#view-area-text").on("input", function(event) {
        currentPangram = $(this).val();
        viewChanged = true;
    });
    $("#view-area-canvas").on("mousewheel", function(event) {
        if (!(fontSelected in fontsList)) return true;
        var diff = event.deltaY * (viewFontSize / 32 + 1);
        if (fontSize + diff > MAX_FONT_SIZE) return false;
        if (viewFontSize >= 16) {
            var pow = (viewFontSize - 16) / 16;
            if (pow > 1) pow = 1;
            viewOriginX -= (mouseX - viewOriginX) / viewFontSize * diff * pow;
        }
        viewFontSize += diff;
        if (viewFontSize < 1) viewFontSize = 1;
        viewChanged = true;
        return false;
    });
    newGlyph = function() {
        if (gState || rState || sState || dialog) return;
        var glyphs = document.getElementsByClassName("item glyph-selected");
        for (var key in glyphs) {
            glyphs[key].className = "item";
        }
        var font = fontsList[fontSelected].font;
        var glyphIndex = font.glyphs.length;
        font.glyphs.glyphs[glyphIndex] = new opentype.Glyph({index: glyphIndex})
        font.glyphs.glyphs[glyphIndex].advanceWidth = 0;
        font.glyphs.glyphs[glyphIndex].leftSideBearing = 0;
        font.glyphs.glyphs[glyphIndex].unicode = 0;
        font.glyphs.glyphs[glyphIndex].unicodes.push(0);
        font.glyphs.glyphs[glyphIndex].path.fill = "black";
        font.glyphs.glyphs[glyphIndex].path.stroke = null;
        font.glyphs.glyphs[glyphIndex].path.strokeWidth = 1;
        font.glyphs.glyphs[glyphIndex].path.unitsPerEm = font.unitsPerEm;
        updateBVH(font, fontSelected, glyphIndex);
        openPropertyEditor2(glyphIndex);
        font.numGlyphs++;
        font.glyphs.length++;
        selectFont(fontSelected);
    }
    viewCanvas = document.getElementById("view-area-canvas");
    viewCtx = viewCanvas.getContext("2d");
    viewControl = $("#view-control");
    viewLabel = $("#view-label");
    viewer = $("#viewer");
    
    viewCanvas.oncontextmenu = function(event) {
        event.preventDefault();
    };

    viewCanvas.width = viewControl.innerWidth();
    viewCanvas.height = Math.max(0, viewer.innerHeight() - viewLabel.innerHeight() - viewControl.innerHeight() - 17);
    
    selectCanvas = $("#select-area-canvas");
    selectControl = $("#select-control");
    selectCanvas.css("height", Math.max(0, viewer.innerHeight() - selectControl.innerHeight() - viewLabel.innerHeight() - 6) + "px");
    
    setupGlyphInspector();
};

setupGlyphInspector = function() {
    var cellCount = 100,
        cellWidth = 64,
        cellHeight = 60,
        cellMarginTop = 1,
        cellMarginBottom = 8,
        cellMarginLeftRight = 1;

    var pageSelected, itemFontScale, itemFontSize, itemFontBaseline;

    function renderGlyphItem(canvas, glyphIndex) {
        var font = fontsList[fontSelected].font;
        var cellMarkSize = 4;
        var miniCtx = canvas.getContext('2d');
        miniCtx.clearRect(0, 0, cellWidth, cellHeight);
        if (glyphIndex >= font.numGlyphs) return;

        miniCtx.fillStyle = FG;
        miniCtx.font = '10px Lato';
        miniCtx.fillText(glyphIndex, 1, cellHeight-1);
        var glyph = font.glyphs.get(glyphIndex),
            glyphWidth = glyph.advanceWidth * itemFontScale,
            xmin = (cellWidth - glyphWidth)/2,
            xmax = (cellWidth + glyphWidth)/2,
            x0 = xmin;

        miniCtx.fillStyle = FG;
        miniCtx.fillRect(xmin-cellMarkSize+1, itemFontBaseline, cellMarkSize, 1);
        miniCtx.fillRect(xmin, itemFontBaseline, 1, cellMarkSize);
        miniCtx.fillRect(xmax, itemFontBaseline, cellMarkSize, 1);
        miniCtx.fillRect(xmax, itemFontBaseline, 1, cellMarkSize);

        var path = glyph.getPath(x0, itemFontBaseline, itemFontSize);
        path.fill = BEIGE;
        path.draw(miniCtx);
    }

    function displayGlyphPage(pageNum) {
        pageSelected = pageNum;
        document.getElementById('p'+pageNum).className = 'button-selected';
        var firstGlyph = pageNum * cellCount;
        for (var i = 0; i < cellCount; i++) {
            renderGlyphItem(document.getElementById('g'+i), firstGlyph+i);
        }
        for (var id in glyphSelection) {
            if (!glyphSelection[id]) continue;
            if (id >= firstGlyph && id < firstGlyph + cellCount) {
                document.getElementById('g'+(id - firstGlyph)).className = "item glyph-selected";
            }
        }
    }

    function pageSelect(event) {
        var glyphs = document.getElementsByClassName("item glyph-selected");
        for (var key in glyphs) {
            glyphs[key].className = "item";
        }
        if (document.getElementsByClassName('button-selected').length) {
            document.getElementsByClassName('button-selected')[0].className = '';
        }
        var pageNum = +event.target.id.substr(1);
        displayGlyphPage(pageNum);
    }

    onFontLoaded = function() {
        var font = fontsList[fontSelected].font;
        var w = cellWidth - cellMarginLeftRight * 2,
            h = cellHeight - cellMarginTop - cellMarginBottom,
            head = font.tables.head,
            maxHeight = head.yMax - head.yMin;
        itemFontScale = Math.min(w/(head.xMax - head.xMin), h/maxHeight);
        itemFontSize = itemFontScale * font.unitsPerEm;
        itemFontBaseline = cellMarginTop + h * head.yMax / maxHeight;

        var pagination = document.getElementById("select-buttons");
        pagination.innerHTML = '';
        var fragment = document.createDocumentFragment();
        var numPages = Math.ceil(font.numGlyphs / cellCount);
        for(var i = 0; i < numPages; i++) {
            var link = document.createElement('button');
            var lastIndex = Math.min(font.numGlyphs-1, (i+1)*cellCount-1);
            link.style.margin = "0px 0px 3px 0px";
            link.textContent = i*cellCount + '-' + lastIndex;
            link.id = 'p' + i;
            link.addEventListener('click', pageSelect, false);
            fragment.appendChild(link);
            // A white space allows to break very long lines into multiple lines.
            // This is needed for fonts with thousands of glyphs.
            fragment.appendChild(document.createTextNode(' '));
        }
        pagination.appendChild(fragment);

        displayGlyphPage(0);
    }

    function cellSelect(event) {
        var font = fontsList[fontSelected].font;
        if (!font) return;
        var firstGlyph = pageSelected*cellCount,
            cellIndex = +event.target.id.substr(1),
            glyphIndex = firstGlyph + cellIndex;
        if (glyphIndex < font.numGlyphs) {
            if (!shift && !ctrl) {
                for (var id in glyphSelection) {
                    if (!glyphSelection[id] || +id == glyphIndex) continue;
                    glyphSelection[id] = null;
                    if (id >= firstGlyph && id < firstGlyph + cellCount) {
                        document.getElementById('g'+(id - firstGlyph)).className = "item";
                    }
                }
            }
            if (glyphIndex in glyphSelection && glyphSelection[glyphIndex]) {
                event.target.className = "item";
                glyphSelection[glyphIndex] = null;
            } else {
                event.target.className = "item glyph-selected";
                glyphSelection[glyphIndex] = fontsList[fontSelected].font.glyphs.glyphs[glyphIndex];
            }
            recalculate = true;
        }
    }

    function prepareGlyphList() {
        var marker = document.getElementById('select-glyphs'),
            parent = marker.parentElement;
        for(var i = 0; i < cellCount; i++) {
            var canvas = document.createElement('canvas');
            canvas.width = cellWidth;
            canvas.height = cellHeight;
            canvas.className = 'item';
            canvas.id = 'g'+i;
            canvas.onclick = cellSelect;
            canvas.onmousedown = function(event) {
                var font = fontsList[fontSelected].font;
                if (!font) return;
                var firstGlyph = pageSelected*cellCount,
                    cellIndex = +event.target.id.substr(1),
                    glyphIndex = firstGlyph + cellIndex;
                if (glyphIndex < font.numGlyphs) {
                    if (event.which > 1) {
                        openPropertyEditor2(glyphIndex);
                        event.preventDefault();
                        return;
                    }
                }
            };
            canvas.oncontextmenu = function(event) {
                event.preventDefault();
            };
            parent.insertBefore(canvas, marker);
        }
    }

    prepareGlyphList();
}

setupFontsList = function() {
    progressbar = $( "#progressbar" );
    progressLabel = $( ".progress-label" );
    selectFont = function(font) {
        if ($("#" + fontSelected).length) $("#" + fontSelected).css("backgroundColor", "");
        $("#" + font).css("backgroundColor", FG);
        fontSelected = font;
        
        fontScale = fontsList[fontSelected].font.unitsPerEm / fontSize;
        
        // render on mini canvas
        var miniCtx = document.getElementById("c" + fontSelected).getContext("2d");
        miniCtx.clearRect(0, 0, cellWidth, cellHeight);
        
        font = fontsList[fontSelected].font;
        var w = cellWidth - cellMargin * 2,
            h = cellHeight - cellMargin * 2,
            head = font.tables.head,
            maxHeight = head.yMax - head.yMin,
            miniFontScale = Math.min(w/(head.xMax - head.xMin), h/maxHeight),
            miniFontSize = miniFontScale * font.unitsPerEm,
            miniFontBaseline = cellMargin + h * head.yMax / maxHeight;

        var path = font.getPath("abc", 2, miniFontBaseline + 2, miniFontSize - 2, {kerning: true});
        path.fill = "#151718";
        path.draw(miniCtx);
        path = font.getPath("abc", 0, miniFontBaseline, miniFontSize - 2, {kerning: true});
        path.fill = BEIGE;
        path.draw(miniCtx);
        
        for (var key in glyphSelection) {
            if (glyphSelection[key]) glyphSelection[key] = font.glyphs.glyphs[key];
            if (glyphSelection[key] === null || glyphSelection[key] === undefined) {
                delete glyphSelection[key];
            }
        }
        
        onFontLoaded();
                            
        viewChanged = true;
        recalculate = true;
    }
    rename = function(event, font) {
        $("#rename-font").text(fontsList[font].name);
        $("#dialog-rename-in").val(fontsList[font].name);
        dialog = true;
        $( "#dialog-rename" ).dialog({
            resizable: true,
            modal: true,
            closeOnEscape: true,
            buttons: {
                Rename: function() {
                    var val = $("#dialog-rename-in").val();
                    fontsList[font].name = val;
                    fontsList[font].font.names.fontFamily = {en: val};
                    $("#n" + font).text(fontsList[font].name);
                    $( this ).dialog( "close" );
                    dialog = false;
                },
                Cancel: function() {
                    $( this ).dialog( "close" );
                    dialog = false;
                }
            }
        });

        event = event || window.event;
        event.preventDefault();
    }
}

// Add "Apply Modifiers" Button
setupModifiers = function() {
    applySubdivide = function(key, recursion, interpolation) {
        var fonts = {};
        fonts[fontSelected] = {font: {glyphs: {glyphs: {}}}};
        fonts[fontSelected].font.glyphs.glyphs[key] = {path: {commands: []}};
        fonts[fontSelected].font.glyphs.glyphs[key].path.commands = modifiedGlyphs[key];
        if (interpolation === SUBDIV_EXACT) {
            for (var i = +recursion; i --> 0;) {
                var coords = [];
                var len = modifiedGlyphs[key].length;
                for (var j = 0; j < len; j++) {
                    coords.push(makePos(fontSelected, key, j, ""));
                }
                subdivide(fonts, coords);
            }
        } else if (interpolation === SUBDIV_SMOOTH) {
            // derive smoothing function
            // idea: move all control point angles towards 180 degrees, turn lines into quadratics for control point manipulation
        }
    }
    applyDecimate = function(key, percent) {
        var len = modifiedGlyphs[key].length;
        var coords = [], norms = [], edgenorms = [];
        var subpath = 0, total = 0;
        for (var j = 0; j < len; j++) {
            if (!(subpath in coords)) {
                coords[subpath] = [];
                norms[subpath] = [];
                edgenorms[subpath] = [];
            }
            var command = modifiedGlyphs[key][j];
            switch (command.type) {
            case "M": case "L":
                coords[subpath].push(makePos(fontSelected, key, j, ""));
                break;
            case "Q":
                coords[subpath].push(makePos(fontSelected, key, j, "1"));
                coords[subpath].push(makePos(fontSelected, key, j, ""));
                break;
            case "C":
                coords[subpath].push(makePos(fontSelected, key, j, "1"));
                coords[subpath].push(makePos(fontSelected, key, j, "2"));
                coords[subpath].push(makePos(fontSelected, key, j, ""));
                break;
            case "Z":
                subpath++;
                break;
            }
        }
        for (var subpath = 0; subpath < coords.length; subpath++) {
            var len = coords[subpath].length;
            total += len;
            for (var i = 0; i < len; i++) {
                var prev = coords[subpath].slice((i - 1) % len)[0];
                var vert = coords[subpath][i];
                var next = coords[subpath][(i + 1) % len];
                
                var p_to_v = {
                    x: modifiedGlyphs[key][vert.command]["x" + vert.index] - modifiedGlyphs[key][prev.command]["x" + prev.index],
                    y: modifiedGlyphs[key][vert.command]["y" + vert.index] - modifiedGlyphs[key][prev.command]["y" + prev.index]
                };                
                var v_to_n = {
                    x: modifiedGlyphs[key][next.command]["x" + next.index] - modifiedGlyphs[key][vert.command]["x" + vert.index],
                    y: modifiedGlyphs[key][next.command]["y" + next.index] - modifiedGlyphs[key][vert.command]["y" + vert.index]
                };
                edgenorms[subpath].push(normalize(rotateRightTransform(v_to_n)));
                norms[subpath].push(calcNormal(p_to_v, v_to_n));
            }
        }
        var repeat = (1 - percent / 100) * total;
        while (0 <=-- repeat) {
            var bestSubpath = 0, bestId = 0, bestScore = Number.POSITIVE_INFINITY;
            for (var subpath = 0; subpath < coords.length; subpath++) {
                var len = coords[subpath].length;
                for (var i = 0; i < coords[subpath].length; i++) {
                    var prev = edgenorms[subpath].slice((i - 1) % len)[0];
                    var vert = norms[subpath][i];
                    var next = edgenorms[subpath][i];
                    
                    var valid = true;
                    if (coords[subpath][i].index == "") {
                        valid = false;
                        var before = modifiedGlyphs[key][coords[subpath][i].command];
                        var after = modifiedGlyphs[key][coords[subpath][(i + 1) % len].command];
                        if (before && after && (before.type == "M" || before.type == "L") && (after.type == "M" || after.type == "L")) {
                            valid = true;
                        }
                    }
                    
                    if (valid) {
                        var p_to_vLen = Math.sqrt((vert.x - prev.x) * (vert.x - prev.x) + (vert.y - prev.y) * (vert.y - prev.y));
                        var v_to_nLen = Math.sqrt((next.x - vert.x) * (next.x - vert.x) + (next.y - vert.y) * (next.y - vert.y));
                        
                        if (p_to_vLen + v_to_nLen < bestScore) {
                            bestSubpath = subpath;
                            bestId = i;
                            bestScore = p_to_vLen + v_to_nLen;
                        }
                    }
                }
            }
            var command = modifiedGlyphs[key][coords[bestSubpath][bestId].command];
            if (!command) return;
            switch (command.type) {
            case "M": case "L":
                modifiedGlyphs[key].splice(coords[bestSubpath][bestId].command, 1);
                break;
            case "Q":
                modifiedGlyphs[key][coords[bestSubpath][bestId].command] = {type: "L", x: command.x, y: command.y};
                break;
            case "C":
                modifiedGlyphs[key][coords[bestSubpath][bestId].command] = {type: "Q", x: command.x, y: command.y, x1: command.x1, y1: command.y1};
                break;
            }
            coords[bestSubpath].splice(bestId, 1);
            norms[bestSubpath].splice(bestId, 1);
            edgenorms[bestSubpath].splice(bestId, 1);
        }
    }
    applyDisplace = function(key, texture, seed, strength, midpoint) {
        var random = new Math.seedrandom(seed);
        var len = modifiedGlyphs[key].length;
        var offset = midpoint / 100;
        var coords = [];
        var subpath = 0;
        for (var j = 0; j < len; j++) {
            if (!(subpath in coords)) {
                coords[subpath] = [];
            }
            var command = modifiedGlyphs[key][j];
            switch (command.type) {
            case "M": case "L":
                coords[subpath].push(makePos(fontSelected, key, j, ""));
                break;
            case "Q":
                coords[subpath].push(makePos(fontSelected, key, j, "1"));
                coords[subpath].push(makePos(fontSelected, key, j, ""));
                break;
            case "C":
                coords[subpath].push(makePos(fontSelected, key, j, "1"));
                coords[subpath].push(makePos(fontSelected, key, j, "2"));
                coords[subpath].push(makePos(fontSelected, key, j, ""));
                break;
            case "Z":
                subpath++;
                break;
            }
        }
        if (texture === DISPLACE_RANDOM_N) {
            for (var subpath = 0; subpath < coords.length; subpath++) {
                var len = coords[subpath].length;
                var norms = [];
                for (var i = 0; i < len; i++) {
                    var prev = coords[subpath].slice((i - 1) % len)[0];
                    var vert = coords[subpath][i];
                    var next = coords[subpath][(i + 1) % len];
                    
                    var p_to_v = {
                        x: modifiedGlyphs[key][vert.command]["x" + vert.index] - modifiedGlyphs[key][prev.command]["x" + prev.index],
                        y: modifiedGlyphs[key][vert.command]["y" + vert.index] - modifiedGlyphs[key][prev.command]["y" + prev.index]
                    };
                    var v_to_n = {
                        x: modifiedGlyphs[key][next.command]["x" + next.index] - modifiedGlyphs[key][vert.command]["x" + vert.index],
                        y: modifiedGlyphs[key][next.command]["y" + next.index] - modifiedGlyphs[key][vert.command]["y" + vert.index]
                    };
                    norms.push(calcNormal(p_to_v, v_to_n));
                }
                for (var i = 0; i < coords[subpath].length; i++) {
                    var vert = coords[subpath][i];
                    var scalar = (random() - offset) * strength;
                    modifiedGlyphs[key][vert.command]["x" + vert.index] += scalar * norms[i].x;
                    modifiedGlyphs[key][vert.command]["y" + vert.index] += scalar * norms[i].y;
                }
            }
        } else {
            for (var subpath = 0; subpath < coords.length; subpath++) {
                for (var i = 0; i < coords[subpath].length; i++) {
                    var vert = coords[subpath][i];
                    if (texture === DISPLACE_RANDOM_X || texture === DISPLACE_RANDOM_XY) {
                        modifiedGlyphs[key][vert.command]["x" + vert.index] += (random() - offset) * strength;
                    }
                    if (texture === DISPLACE_RANDOM_Y || texture === DISPLACE_RANDOM_XY) {
                        modifiedGlyphs[key][vert.command]["y" + vert.index] += (random() - offset) * strength;
                    }
                }
            }
        }
    }
    applyAlign = function(key, scale, align) {
        var len = modifiedGlyphs[key].length;
        var size = fontsList[fontSelected].font.unitsPerEm / scale;
        var total = Math.abs(align) + 1;
        var coords = [];
        for (var j = 0; j < len; j++) {
            var command = modifiedGlyphs[key][j];
            switch (command.type) {
            case "C":
                coords.push(makePos(fontSelected, key, j, "2"));
            case "Q":
                coords.push(makePos(fontSelected, key, j, "1"));
            case "M": case "L":
                coords.push(makePos(fontSelected, key, j, ""));
            }
        }
        for (var i = 0; i < coords.length; i++) {
            var vert = coords[i];
            modifiedGlyphs[key][vert.command]["x" + vert.index] += align * size * Math.round(modifiedGlyphs[key][vert.command]["x" + vert.index] / size);
            modifiedGlyphs[key][vert.command]["x" + vert.index] /= total;
            modifiedGlyphs[key][vert.command]["y" + vert.index] += align * size * Math.round(modifiedGlyphs[key][vert.command]["y" + vert.index] / size);
            modifiedGlyphs[key][vert.command]["y" + vert.index] /= total;
        }
    }
    addModifier = function(modifier) {
        modifier = makeModifier(modifier);
        modifiersList[modifier["id"]] = modifier["data"];
        $("#modifier-stack").append(modifier["html"]);
        modified = true;
        recalculate = true;
    }
    confirmDeleteModifier = function(id) {
        dialog = true;
        $( "#dialog-confirm" ).dialog({
            resizable: true,
            height: 192,
            modal: true,
            closeOnEscape: true,
            buttons: {
                "Delete modifier": function() {
                    deleteModifier(id);
                    $( this ).dialog( "close" );
                    dialog = false;
                },
                Cancel: function() {
                    $( this ).dialog( "close" );
                    dialog = false;
                }
            }
        });
    }
    deleteModifier = function(id) {
        $("#modifier-" + id).remove();
        delete modifiersList[id];
        modified = true;
        recalculate = true;
    }
    applyModifiers = function() {
        if (gState || rState || sState) return;
        vertSelection = [];
        for (var key in modifiedGlyphs) {
            if (!modifiedGlyphs[key] || !glyphSelection[key]) continue;
            glyphSelection[key].path.commands = [];
            glyphSelection[key].path.commands.push.apply(glyphSelection[key].path.commands, modifiedGlyphs[key]);
            updateBVH(fontsList[fontSelected].font, fontSelected, key);
        }
        for (var key in modifiersList) {
            deleteModifier(key);
        }
        modifiersList = {};
        fontsList[fontSelected].save = false;
        modified = true;
        recalculate = true;
    }
    setModifier = function(id, param) {
        var modifier = modifiersList[id];
        var element = $("#modifier-" + id + " #" + param);
        
        if (param == "subdiv") {
            var val = modifier[param];
            // val = val == SUBDIV_EXACT ? SUBDIV_SMOOTH : SUBDIV_EXACT;
            val = SUBDIV_EXACT;
            modifier[param] = val;
            element.text(val + " interpolation");
            modified = true;
            recalculate = true;
        } else if (param == "recurse") {
            $("#dialog-in").val(modifier[param]);
            dialog = true;
            $( "#dialog-input" ).dialog({
                resizable: true,
                modal: true,
                closeOnEscape: true,
                buttons: {
                    "Set recursion depth": function() {
                        var val = $("#dialog-in").val();
                        try {
                            val = eval(val);
                        } catch (error) {}
                        if ($.isNumeric(val)) {
                            if (val < 0) {
                                val = 0;
                            }
                            if (val > 5) {
                                val = 5;
                            }
                            val = Math.floor(val);
                            modifier[param] = val;
                            element.text("Recurse x" + val);
                            modified = true;
                            recalculate = true;
                        }
                        $( this ).dialog( "close" );
                        dialog = false;
                    },
                    Cancel: function() {
                        $( this ).dialog( "close" );
                        dialog = false;
                    }
                }
            });
        } else if (param == "percent") {
            $("#dialog-in").val(modifier[param]);
            dialog = true;
            $( "#dialog-input" ).dialog({
                resizable: true,
                modal: true,
                closeOnEscape: true,
                buttons: {
                    "Set decimate percentage": function() {
                        var val = $("#dialog-in").val();
                        try {
                            val = eval(val);
                        } catch (error) {}
                        if ($.isNumeric(val)) {
                            if (val < 0) {
                                val = 0;
                            }
                            if (val > 100) {
                                val = 100;
                            }
                            val = Number(val);
                            modifier[param] = val;
                            element.text(val.toFixed(3)+ "%");
                            modified = true;
                            recalculate = true;
                        }
                        $( this ).dialog( "close" );
                        dialog = false;
                    },
                    Cancel: function() {
                        $( this ).dialog( "close" );
                        dialog = false;
                    }
                }
            });
        } else if (param == "texture") {
            var val = modifier[param];
            val = val == DISPLACE_RANDOM_X ? DISPLACE_RANDOM_Y :
                  val == DISPLACE_RANDOM_Y ? DISPLACE_RANDOM_XY :
                  val == DISPLACE_RANDOM_XY? DISPLACE_RANDOM_N :
                  DISPLACE_RANDOM_X;
            modifier[param] = val;
            element.text(val + " texture");
            modified = true;
            recalculate = true;
        } else if (param == "seed") {
            $("#dialog-in").val(modifier[param]);
            dialog = true;
            $( "#dialog-input" ).dialog({
                resizable: true,
                modal: true,
                closeOnEscape: true,
                buttons: {
                    "Set seed": function() {
                        var val = $("#dialog-in").val();
                        if (val) {
                            modifier[param] = val;
                            element.text("Seed: " + val);
                            modified = true;
                            recalculate = true;
                        }
                        $( this ).dialog( "close" );
                        dialog = false;
                    },
                    Cancel: function() {
                        $( this ).dialog( "close" );
                        dialog = false;
                    }
                }
            });
        } else if (param == "strength") {
            $("#dialog-in").val(modifier[param]);
            dialog = true;
            $( "#dialog-input" ).dialog({
                resizable: true,
                modal: true,
                closeOnEscape: true,
                buttons: {
                    "Set displacement strength": function() {
                        var val = $("#dialog-in").val();
                        try {
                            val = eval(val);
                        } catch (error) {}
                        if ($.isNumeric(val)) {
                            val = Number(val);
                            modifier[param] = val;
                            element.text("Strength: " + val.toFixed(3));
                            modified = true;
                            recalculate = true;
                        }
                        $( this ).dialog( "close" );
                        dialog = false;
                    },
                    Cancel: function() {
                        $( this ).dialog( "close" );
                        dialog = false;
                    }
                }
            });
        } else if (param == "midpoint") {
            $("#dialog-in").val(modifier[param]);
            dialog = true;
            $( "#dialog-input" ).dialog({
                resizable: true,
                modal: true,
                closeOnEscape: true,
                buttons: {
                    "Set displacement midpoint": function() {
                        var val = $("#dialog-in").val();
                        try {
                            val = eval(val);
                        } catch (error) {}
                        if ($.isNumeric(val)) {
                            val = Number(val);
                            modifier[param] = val;
                            element.text("Midpoint: " + val.toFixed(3) + "%");
                            modified = true;
                            recalculate = true;
                        }
                        $( this ).dialog( "close" );
                        dialog = false;
                    },
                    Cancel: function() {
                        $( this ).dialog( "close" );
                        dialog = false;
                    }
                }
            });
        } else if (param == "scale") {
            $("#dialog-in").val(modifier[param]);
            dialog = true;
            $( "#dialog-input" ).dialog({
                resizable: true,
                modal: true,
                closeOnEscape: true,
                buttons: {
                    "Set grid scale": function() {
                        var val = $("#dialog-in").val();
                        try {
                            val = eval(val);
                        } catch (error) {}
                        if ($.isNumeric(val)) {
                            if (val < EPS_D) {
                                val = EPS_D;
                            }
                            val = Number(val);
                            modifier[param] = val;
                            element.text("Grid scale: " + val.toFixed(3));
                            modified = true;
                            recalculate = true;
                        }
                        $( this ).dialog( "close" );
                        dialog = false;
                    },
                    Cancel: function() {
                        $( this ).dialog( "close" );
                        dialog = false;
                    }
                }
            });
        } else if (param == "align") {
            $("#dialog-in").val(modifier[param]);
            dialog = true;
            $( "#dialog-input" ).dialog({
                resizable: true,
                modal: true,
                closeOnEscape: true,
                buttons: {
                    "Set alignment strength": function() {
                        var val = $("#dialog-in").val();
                        try {
                            val = eval(val);
                        } catch (error) {}
                        if ($.isNumeric(val)) {
                            val = Number(val);
                            modifier[param] = val;
                            element.text("Strength: " + val.toFixed(3));
                            modified = true;
                            recalculate = true;
                        }
                        $( this ).dialog( "close" );
                        dialog = false;
                    },
                    Cancel: function() {
                        $( this ).dialog( "close" );
                        dialog = false;
                    }
                }
            });
        }
    }
}

setupPropertyEditor = function() {
    container = document.getElementById("jsoneditor");
    container2 = document.getElementById("jsoneditor2");
    jsonEditor = new JSONEditor(container, {
        onEditable: function(node) {
            switch (node.field) {
            case "os2": case "cmap": case "head": case "hhea": case "maxp": case "post": case "name": case "cff":
                return {field: false, value: true};
            default:
                return true;
            }
        },
        onChange: function() {
            jsonEditor.setName(jsonEditor.getName().replace("(saved)", "(unsaved)"));
        }
    });
    jsonEditor2 = new JSONEditor(container2, {
        onEditable: function(node) {
            if (node.field == "index") return false;
            return {field: false, value: true};
        },
        onChange: function() {
            jsonEditor2.setName(jsonEditor2.getName().replace("(saved)", "(unsaved)"));
        }
    });
    $("#dialog-json-editor").on("keydown", function(event) {
        var keyCode = event.keyCode || event.which; 
        if (keyCode == 13) {
            if ("activeElement" in document)
                document.activeElement.blur();
            event.preventDefault();
        }
    });
    openPropertyEditor = function() {
        if (!(fontSelected in fontsList) || dialog) return;
        jsonEditor.setName(fontsList[fontSelected].name + " (saved)");
        jsonEditor.set(fontsList[fontSelected].font.tables);
        jsonEditor.focus();
        dialog = true;
        $( "#dialog-json-editor" ).dialog({
            autoResize: false,
            modal: false,
            closeOnEscape: false,
            buttons: {
                Ok: function() {
                    try {
                        $.extend(fontsList[fontSelected].font.tables, jsonEditor.get());
                        recalculate = true;
                    } catch (error) {
                        showError(error, "Your edits could not be parsed! Please fix them and then reapply");
                    }
                    $(this).dialog( "close" );
                },
                Cancel: function() {
                    $(this).dialog( "close" );
                },
                Apply: function() {
                    try {
                        $.extend(fontsList[fontSelected].font.tables, jsonEditor.get());
                        jsonEditor.setName(jsonEditor.getName().replace("(unsaved)", "(saved)"));
                        recalculate = true;
                    } catch (error) {
                        showError(error, "Your edits could not be parsed! Please fix them and then reapply");
                    }
                }
            },
            close: function() {
                dialog = false;
            }
        });
    }
    openPropertyEditor2 = function(glyph) {
        if (!(fontSelected in fontsList) || dialog) return;
        jsonEditor2.setName("Glyph id #" + glyph + " (saved)");
        var path = fontsList[fontSelected].font.glyphs.glyphs[glyph].path;
        var object = {fill: path.fill, stroke: path.stroke, strokeWidth: path.strokeWidth};
        $.extend(object, fontsList[fontSelected].font.glyphs.glyphs[glyph]);
        jsonEditor2.set(object);
        jsonEditor2.focus();
        dialog = true;
        $( "#dialog-json-editor2" ).dialog({
            autoResize: false,
            modal: false,
            closeOnEscape: false,
            buttons: {
                Ok: function() {
                    try {
                        var object = jsonEditor2.get();
                        for (var key in object) {
                            if (key == "fill" || key == "stroke" || key == "strokeWidth") {
                                fontsList[fontSelected].font.glyphs.glyphs[glyph].path[key] = object[key];
                            } else {
                                var insert = {};
                                insert[key] = object[key];
                                $.extend(fontsList[fontSelected].font.glyphs.glyphs[glyph], insert);
                            }
                        }
                        recalculate = true;
                    } catch (error) {
                        showError(error, "Your edits could not be parsed! Please fix them and then reapply");
                    }
                    $(this).dialog( "close" );
                },
                Cancel: function() {
                    $(this).dialog( "close" );
                },
                Apply: function() {
                    try {
                        var object = jsonEditor2.get();
                        for (var key in object) {
                            if (key == "fill" || key == "stroke" || key == "strokeWidth") {
                                fontsList[fontSelected].font.glyphs.glyphs[glyph].path[key] = object[key];
                            } else {
                                var insert = {};
                                insert[key] = object[key];
                                $.extend(fontsList[fontSelected].font.glyphs.glyphs[glyph], insert);
                            }
                        }
                        jsonEditor2.setName(jsonEditor2.getName().replace("(unsaved)", "(saved)"));
                        recalculate = true;
                    } catch (error) {
                        showError(error, "Your edits could not be parsed! Please fix them and then reapply");
                    }
                }
            },
            close: function() {
                dialog = false;
            }
        });
    }
}

initBVH = function(font, fontId, quiet) {
    makePos = function(font, id, command, index) {
        return {font: font, id: id, command: command, index: index};
    }
    getPos = function(pos) {
        return {x: fontsList[pos.font].font.glyphs.glyphs[pos.id].path.commands[pos.command]["x" + pos.index],
                y: fontsList[pos.font].font.glyphs.glyphs[pos.id].path.commands[pos.command]["y" + pos.index]};
    }
    setPos = function(pos, x, y) {
        fontsList[pos.font].font.glyphs.glyphs[pos.id].path.commands[pos.command]["x" + pos.index] = x;
        fontsList[pos.font].font.glyphs.glyphs[pos.id].path.commands[pos.command]["y" + pos.index] = y;
    }
    getBBox = function(positions) {
        var bbox = {minX : Number.POSITIVE_INFINITY,
                    minY : Number.POSITIVE_INFINITY,
                    maxX : Number.NEGATIVE_INFINITY,
                    maxY : Number.NEGATIVE_INFINITY};
        
        for (var key in positions) {
            var pos = getPos(positions[key]);
            bbox.minX = Math.min(bbox.minX, pos.x);
            bbox.minY = Math.min(bbox.minY, pos.y);
            bbox.maxX = Math.max(bbox.maxX, pos.x);
            bbox.maxY = Math.max(bbox.maxY, pos.y);
        }

        return bbox;
    }
    EMPTY_BBOX = {minX : Number.POSITIVE_INFINITY,
                  minY : Number.POSITIVE_INFINITY,
                  maxX : Number.NEGATIVE_INFINITY,
                  maxY : Number.NEGATIVE_INFINITY,
                  quads : [null, null, null, null],
                  nodes : [],
                  length : 0};
    makeBBox = function(positions, depth) {
        if (depth == 0) {
            throw "BVH build unsuccessful!";
            return null;
        }
        var count = positions.length;
        if (!count) return null;
        var bbox = {minX : Number.POSITIVE_INFINITY,
                    minY : Number.POSITIVE_INFINITY,
                    maxX : Number.NEGATIVE_INFINITY,
                    maxY : Number.NEGATIVE_INFINITY,
                    quads : [null, null, null, null],
                    nodes : [],
                    length : count};
        
        var box = getBBox(positions);
        bbox.minX = box.minX;
        bbox.minY = box.minY;
        bbox.maxX = box.maxX;
        bbox.maxY = box.maxY;
        
        if (count > 4) {
            var extX = bbox.maxX - bbox.minX;
            var extY = bbox.maxY - bbox.minY;
            var divX, divY, bestArea = Number.POSITIVE_INFINITY;
            for (var segX = 1; segX < BVHSEGMENTS; segX++) {
                for (var segY = 1; segY < BVHSEGMENTS; segY++) {
                    var quadPos = [[], [], [], []];
                    for (var key in positions) {
                        var pos = getPos(positions[key]);
                        if (pos.x < bbox.minX + segX / BVHSEGMENTS * extX) {
                            if (pos.y < bbox.minY + segY / BVHSEGMENTS * extY) {
                                quadPos[0].push(positions[key]);
                            } else {
                                quadPos[2].push(positions[key]);
                            }
                        } else {
                            if (pos.y < bbox.minY + segY / BVHSEGMENTS * extY) {
                                quadPos[1].push(positions[key]);
                            } else {
                                quadPos[3].push(positions[key]);
                            }
                        }
                    }
                    
                    var area = 0;
                    for (var quad in quadPos) {
                        if (!quadPos[quad].length) continue;
                        box = getBBox(quadPos[quad]);
                        var a = (box.maxX - box.minX) * (box.maxY - box.minY);
                        area += quadPos[quad].length * a * a;
                    }

                    if (area < bestArea) {
                        bestArea = area;
                        divX = bbox.minX + segX / BVHSEGMENTS * extX;
                        divY = bbox.minY + segY / BVHSEGMENTS * extY;
                    }
                }
            }
            
            var quadPos = [[], [], [], []];
            for (var key in positions) {
                var pos = getPos(positions[key]);
                if (pos.x < divX) {
                    if (pos.y < divY) {
                        quadPos[0].push(positions[key]);
                    } else {
                        quadPos[2].push(positions[key]);
                    }
                } else {
                    if (pos.y < divY) {
                        quadPos[1].push(positions[key]);
                    } else {
                        quadPos[3].push(positions[key]);
                    }
                }
            }
            
            var cluster = 0;
            for (var quad in quadPos) {
                if (quadPos[quad].length == 0) cluster++;
            }
            
            if (cluster == 3) { // degenerate
                bbox.nodes.push.apply(bbox.nodes, positions);
            } else {
                for (var quad in quadPos) {
                    bbox.quads[quad] = makeBBox(quadPos[quad], depth - 1);
                }
            }
        } else {
            bbox.nodes.push.apply(bbox.nodes, positions);
        }
        
        return bbox;
    }
    intersect_square = function(bbox, x, y, delta) {
        var intersections = [];
        if (bbox == null || x + delta < bbox.minX || x - delta > bbox.maxX || y + delta < bbox.minY || y - delta > bbox.maxY) return intersections;
        if (bbox.nodes.length) {
            if (x - delta < bbox.minX && x + delta > bbox.maxX && y - delta < bbox.minY && y + delta > bbox.maxY) { // degenerate
                intersections.push.apply(intersections, bbox.nodes);
            } else {
                for (var key in bbox.nodes) {
                    var pos = getPos(bbox.nodes[key]);
                    if (Math.abs(pos.x - x) < delta && Math.abs(pos.y - y) < delta) {
                        intersections.push(bbox.nodes[key]);
                    }
                }
            }
            return intersections;
        }
        for (var key in bbox.quads) {
            intersections.push.apply(intersections, intersect_square(bbox.quads[key], x, y, delta));
        }
        return intersections;
    }
    intersect_circle = function(bbox, x, y, delta) {
        var intersections = [];
        if (bbox == null || x + delta < bbox.minX || x - delta > bbox.maxX || y + delta < bbox.minY || y - delta > bbox.maxY) return intersections;
        if (bbox.nodes.length) {
            if (x - delta / SQRT2 < bbox.minX && x + delta / SQRT2 > bbox.maxX && y - delta / SQRT2 < bbox.minY && y + delta / SQRT2 > bbox.maxY) { // degenerate
                intersections.push.apply(intersections, bbox.nodes);
            } else {
                for (var key in bbox.nodes) {
                    var pos = getPos(bbox.nodes[key]);
                    if ((pos.x - x) * (pos.x - x) + (pos.y - y) * (pos.y - y) < (delta * delta)) {
                        intersections.push(bbox.nodes[key]);
                    }
                }
            }
            return intersections;
        }
        for (var key in bbox.quads) {
            intersections.push.apply(intersections, intersect_circle(bbox.quads[key], x, y, delta));
        }
        return intersections;
    }
    intersect_greedy = function(intersections) {
        var starts = [];
        var coords = [];
        for (var key in intersections) {
            var intersection = intersections[key];
            var commands = fontsList[fontSelected].font.glyphs.glyphs[intersection.id].path.commands;
            var command = intersection.command;
            var beginning = true;
            while (command > 0) {
                if (commands[command].type == "Z") {
                    var exists = false;
                    for (var ckey in starts) {
                        var start = starts[ckey];
                        if (start.glyph == intersection.id && start.command == command) {
                            exists = true;
                            break;
                        }
                    }
                    if (!exists) {
                        starts.push({glyph: intersection.id, command: command});
                    }
                    beginning = false;
                    break;
                }
                command--;
            }
            if (beginning) {
                var exists = false;
                for (var ckey in starts) {
                    var start = starts[ckey];
                    if (start.glyph == intersection.id && start.command == -1) {
                        exists = true;
                        break;
                    }
                }
                if (!exists) {
                    starts.push({glyph: intersection.id, command: -1});
                }
            }
        }
        for (var key in starts) {
            var glyph = starts[key].glyph;
            var ckey = starts[key].command;
            var length = fontsList[fontSelected].font.glyphs.glyphs[glyph].path.commands.length;
            while (++ckey < length) {
                var command = fontsList[fontSelected].font.glyphs.glyphs[glyph].path.commands[ckey];
                if (command.type == "Z") break;
                if (command.x != null) {
                    coords.push(makePos(fontSelected, glyph, ckey, ""));
                }
                if (command.x1 != null) {
                    coords.push(makePos(fontSelected, glyph, ckey, "1"));
                }
                if (command.x2 != null) {
                    coords.push(makePos(fontSelected, glyph, ckey, "2"));
                }
            }
        }
        return coords;
    }
    addBBoxPath = function(bbox, scale) {
        ctx.lineWidth = 0.5;
        ctx.rect(originX + scale * bbox.minX, originY - scale * bbox.minY,
                 scale * (bbox.maxX - bbox.minX), scale * (bbox.minY - bbox.maxY));
        for (var key in bbox.quads) {
            if (bbox.quads[key]) addBBoxPath(bbox.quads[key], scale);
        }
    }
    updateBVH = function(font, fontId, key) {
        var commands = font.glyphs.glyphs[key].path.commands;
        var positions = [];
        for (var ckey in commands) {
            var command = commands[ckey];
            if (command.x != null) {
                positions.push(makePos(fontId, key, ckey, ""));
            }
            if (command.x1 != null) {
                positions.push(makePos(fontId, key, ckey, "1"));
            }
            if (command.x2 != null) {
                positions.push(makePos(fontId, key, ckey, "2"));
            }
        }
        bvh[fontId][key] = makeBBox(positions, 1000);
        if (!bvh[fontId][key]) bvh[fontId][key] = EMPTY_BBOX;
    }
    bvh[fontId] = {}
    var key = 0;
    setTimeout( function buildGlyphBVH() {
        var success = true;
        try {
            updateBVH(font, fontId, key);
            glyphId = key;
            if (!quiet) progressbar.progressbar( "value", 100 * (glyphId + 1) / glyphCount );
        } catch (error) {
            showError(error);
            success = false;
        }
        if (success && ++key < glyphCount) {
            setTimeout(buildGlyphBVH, 0);
        } else {
            selectFont(fontId);
            glyphId = -1;
        }
    }, 0);
    return function(x, y, delta, type, greedy) {
        var intersect = type === ISECT_SQUARE ? intersect_square : intersect_circle;
        var intersections = [];
        x = (x - originX) * fontScale;
        y = (originY - y) * fontScale;
        for (var key in glyphSelection) {
            if (!glyphSelection[key]) continue;
            var coords = intersect(bvh[fontSelected][key], x, y, delta);
            if (!coords) continue;
            intersections.push.apply(intersections, coords);
        }
        if (hState) {
            for (var key = intersections.length; key --> 0;) {
                var vert = intersections[key];
                var remove = true;
                if (vert.index != "" && vert.font == fontSelected && vert.id in handles) {
                    if (vert.command in handles[vert.id] && handles[vert.id][vert.command].c2 == vert.command && vert.index == "2" || +vert.command - 1 in handles[vert.id] && handles[vert.id][+vert.command - 1].c1 == vert.command && vert.index == "1") {
                        remove = false;
                    }
                }
                if (remove) intersections.splice(key, 1);
            }      
        } else if (greedy) intersections = intersect_greedy(intersections);
        return intersections;
    }
}

loadFont = function(name, font, generated, quiet) {
    var id = new Date().getTime();
    var html = "<li id=\"" + id + "\"><a href='javascript:void(0)' onclick='selectFont(\"" + id + "\")' oncontextmenu='rename(event, \"" + id + "\")'><table><tr><td style='top: 4px; vertical-align:top; width: " + cellWidth + "px;'><canvas id='c" + id + "' width=" + cellWidth + " height=" + cellHeight + " style='padding-top: 4px'></canvas></td><td><span id='n" + id + "' style='padding-left: 2px;'>" + name + "</span></td></tr></table></a></li>"
    
    $("#fonts ul").append(html);
    
    glyphCount = font.glyphs.length;
    glyphId = 0;

    if (!quiet) {
        dialog = true;
        $( "#dialog-load-font" ).dialog({
            modal: true,
            closeOnEscape: true,
            beforeClose: function() {
                dialog = glyphId > -1;
                return !dialog;
            }
        }),
        $( "#progressbar" ).progressbar({
            value: false,
            change: function() {
                progressLabel.text( "Processing glyphs [" + glyphId + "/" + glyphCount +"]" );
            },
            complete: function() {
                progressLabel.text( "Complete!" );
            }
        });
        if (generated) {
            $( "#dialog-load-font" ).dialog("option", "title", "Generating new font");
        } else {
            $( "#dialog-load-font" ).dialog("option", "title", "Loading " + name);
        }
    }
    
    fontsList[id] = {"font"  : font,
                     "name"  : name,
                     "save"  : true,
                     "isect" : initBVH(font, id, quiet)};
};

makeDefaultFont = function(fontName, quiet) {
    opentype.load("fonts/FiraSansOTMedium-Regular-Abridged.otf", function(error, font) {
        if (error) {
            var notdefGlyph = new opentype.Glyph({
                name: '.notdef',
                unicode: 0,
                advanceWidth: 650,
                path: new opentype.Path()
            });

            var font = new opentype.Font({familyName: fontName, styleName: 'Regular', unitsPerEm: 1000, ascender: 800, descender: -200, glyphs: [notdefGlyph]});
            loadFont(fontName, font, true, quiet);
        } else {
            font.names.fontFamily = {en: fontName};
            font.names.fontSubfamily = {en: "Regular"};
            loadFont(fontName, font, true, quiet);
        }
    });
}

// Detect unsaved fonts
window.onbeforeunload = function() {
    var unsaved = false;
    for (var key in fontsList) {
        if (!fontsList[key].save) unsaved = true;
    }
    if (!saving && unsaved) {
        return "You still have unsaved fonts!";
    }
}

showError = function(error, message) {
    if (error) {
        console.log(error.stack);
        $("#error-message").text(error);
    } else {
        $("#error-message").text("There's an issue with this font file that caused an error we didn't know how to fix :(");
    }
    if (message) {
        $("#error-message").text(message);
    }
    $( "#dialog-error" ).dialog({
        modal: true,
        closeOnEscape: true,
        buttons: {
            Ok: function() {
                $( this ).dialog( "close" );
                dialog = false;
            }
        }
    });
}
