<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Joseph Chen  |  CS 184</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/style.css" media="screen" />
</head>
<body id="zoomwrapper">
<br />
<h1 align="middle">Assignment 4: Lens Simulator</h1>
    <h2 align="middle">Joseph Chen</h2>
        <p>In this project, we simulate the effect of physics-based lens on rendering ray-traced images. When ray-tracing, we need to generate a large amount of rays which we shoot into the world, collecting light data and returning a color. This allows us to render reflections and shadows effectively by simply following the path of a ray until it hits a light source. However, we can further simulate a real camera by tracing those rays through a special lens model that will refract light in interesting and useful ways.</p>

        <p><i>Note: Hover over any image for a zoomed in version. Up-scaling is done using nearest-pixel to preserve pixel definition. This can be used to see very small details like edge lines.</i></p>

    <h2 align="middle">Part 1: Tracing Rays through Lenses</h2>
        <p>So why do we want to use lenses in the first place?</p>
        
        <p>Without lenses, the only effective way to capture an image is to use a pinhole camera. This involves passing light from the world through a small hole, and projecting onto a screen. However, there are a few issues with this. One of the most prominent is the fact that light is continuous (for all intents and purposes), and when we pass light through a hole, it in fact integrates the light passing through that area. This means that if we have a larger pinhole, our image becomes more and more blurred, as the incoming light is being integrated over a larger area.</p>
        
        <p>This seems like an easy issue to fix. Just make the pinhole very very small. Smaller than the image resolution. Small enough that integrating the light just fills in a patch of a pixel. But of course there is still a problem. Since we are integrating light over the area, we now have much less area, and thus much less light per unit time. Our only hope is to increase exposure time, which really isn't a problem for virtual cameras, but when dealing with the real world, items moving around the scene will leave blurry trails behind them.</p>
        
        <p>Another issue that pinhole cameras bring up is scaling. Say we want to take a picture of some animal in the jungle, preferably from far far away. Using a pinhole camera, in order to magnify such an image, yet maintain a reasonable resolution, we would need to move our detector device far far from the pinhole. This is because in this situation, we care about the angular resolution. Since our objective is very far away, it takes up only a small portion of the world (in terms of angular view). In order to enlarge it, we can't change the apparent size, and thus we need to increase the distance proportionally to scale the image and achieve high resolution.</p>
        
        <p>Enough reasons why pinhole cameras just won't do. So what happens when we use a real lens? One of the most apparent benefits is the ability to scale a scene. We can zoom into a particular portion, and enlarge it's angular diameter without needing to scale the camera up proportionally. We can also focus an image, improving clarity in specific depths. Here, we present a series of rays refracting through various sets of lenses. Note that the image detector is always on the right side, and the world is on the left side.</p>
        
        <p>We present various lens setups for different effects. See if you can imagine how an image will look using each set of lens.</p>
        
        <img class="zoomable" src="images/lens1.1.png"/>
        <figcaption align="middle">Fig. 1.1: 50mm lens, forward ray-tracing.</figcaption>

        <img class="zoomable" src="images/lens1.2.png"/>
        <figcaption align="middle">Fig. 1.2: 50mm lens, reverse ray-tracing.</figcaption>
        
        <img class="zoomable" src="images/lens2.1.png"/>
        <figcaption align="middle">Fig. 1.3: 22mm lens, forward ray-tracing.</figcaption>
        
        <img class="zoomable" src="images/lens2.2.png"/>
        <figcaption align="middle">Fig. 1.4: 22mm lens, reverse ray-tracing.</figcaption>
        
        <img class="zoomable" src="images/lens3.1.png"/>
        <figcaption align="middle">Fig. 1.5: 250mm lens, forward ray-tracing.</figcaption>
        
        <img class="zoomable" src="images/lens3.2.png"/>
        <figcaption align="middle">Fig. 1.6: 250mm lens, reverse ray-tracing.</figcaption>
        
        <img class="zoomable" src="images/lens4.1.png"/>
        <figcaption align="middle">Fig. 1.7: 10mm lens, forward ray-tracing.</figcaption>
        
        <img class="zoomable" src="images/lens4.2.png"/>
        <figcaption align="middle">Fig. 1.8: 10mm lens, reverse ray-tracing.</figcaption>
        
        <p>Let's compare the two extremes: the 250mm telephoto lens, and the 10mm fisheye lens. When we shoot rays through the telephoto lens, notice that all rays are nearly parallel. In fact, they only converge after a very large distance, and when capturing the simulation, we needed to zoom out a large amount to actually see the convergence point. Also note that in the reverse ray tracing, very few rays actually enter the lens. In other words, the spatial resolution, or angular size of the objective was very small and narrow. As you can probably guess, that means the telephoto can be used for some very zoomed in photos.</p>
        
        <p>The fisheye, on the other hand, converged very close to the lens. In fact, when we reverse ray-trace, we notice that it is able to capture very wide angles. This creates another effect that real lenses tend to have - warping of the image. As the lenses artificially change the angular diameter of the scene, some parts will seem warped from the distortion that the lense cause.</p>
        
        <p>Now let's go back to the telephoto lens. We realize that in order to take an image using this camera, we need a much longer camera. This is simply because the rays don't converge onto the detector side as quickly, and thus increases the distance from the lens that we must place our sensor in order to get a focused image. Here, we have a table of the various attributes of the four lens sets.</p>
        
        <img class="zoomable" src="images/table.png"/>
        <figcaption align="middle">Fig. 1.9: Calculated attributes of each lens.</figcaption>
        
        <p>We notice immediately that the infinity focus depth for the telephoto is very large. The infinity focus is simply the sensor location at which an infinitely far object comes into focus. The close focus, on the other hand, is the closest an object can be in the world that our lens is able to focus onto. In the case of the telephoto, this is 1.6 meters, while for the fisheye lens, it is only 6 cm! Furthermore, the various focal lengths show how long the camera barrel must be to accommodate the lens, and accurately place the sensor.</p>
        
        <p>We can also note the relationship between the distance to our objective compared with the location of our sensor. Placing our sensor in various depths changes the focus region. Here we graph this relationship.</p>
        
        <img class="zoomable" src="images/graph.png"/>
        <figcaption align="middle">Fig. 1.10: Sensor depth relationship.</figcaption>

        <p>We can see that there is an inverse relationship between the two. We plot 0% as the infinity_focus, and 100% as the near_focus (from Fig. 1.9). As we move our sensor plane towards the infinity_focus, our focusable region tends towards infinite. This relationship forms an inverse curve between the two.</p>
        
        <p>Now that we have our lens ready to go, we can try rendering a ray-traced image! Here, we render a dragon using the telephoto lens. We can manually adjust the sensor plane until we are focused on the head. This is what we get:</p>
        
        <img class="zoomable" src="images/dragon_manual.png"/>
        <figcaption align="middle">Fig. 1.11: Manually focused dragon.</figcaption>
        
        <p>Notice the narrow focus range. Specifically, the back of the dragon is blurred due to its depth being farther, and thus not focused. Same for the floor in the bottom, as it is much closer to the camera. These blur affects are completely generated using the real lens, and simply ray-tracing through them instead.</p>
        
        <p>So how do we generate the correct rays for the image? Instead, we generate rays across the lens of the camera. We select points on the camera which we project our rays towards, each ray emitted from a sensor plane. This acts as the "integration" process to gather all light samples. This ray is then traced through the lens, and eventually makes its way into the world where it can gather light samples.</p>
        
        <p>However, we have a slight issue. Since we have a fixed barrel, rays that do not pass through all lenses simply terminate somewhere in the barrel, which we say is just "black" or no color. Note that this method results in a very noisy image. Similar to why low light samples result in lots of noise around shadows, if we cast a ray that doesn't make it through all lenses, we just end up with a black pixel.</p>
        
        <p>An easy way to fix this is to simply resample rays that don't make it through all lenses. This works quite well for removing noise. But we meet another problem. Possibly the most important question is how to maintain an unbiased and uniform sample. Since we oversample in areas we need to correctly divide by the number of samples we've tried total to remove the bias.</p>
        
        <p>Finally, we can also include an additional factor to make our lens more realistic. We realize that the center of the sensor is closer to the lens than the edges. This means when we integrate over light, the center area should be brighter, while the edges should be dimmer, as less light hits those areas in comparison. It turns out that if we simply calculate the angle between our rays and the normal direction of our sensor plane, we can use this angle to estimate a factor to properly scale our colors. Specifically, we want to take the fourth power of the cosine of this angle in order to maintain a uniform and unbiased distribution. Interestingly, this also adds a subtle vignette around the subject, as can be seen along the edges of Fig 1.11!</p>
        
        <p>In any case, our dragon image was manually adjusted and focused. Now, we continue to see if we can automatically focus an image.</p>

    <h2 align="middle">Part 2: Contrast-Based Autofocus</h2>
        <p>What defines something in focus? In order for our program to automatically adjust the lens parameters and focus the image, we need a good definition of focus.</p>

        <p>We make a heuristic that rates an image on how "sharp" it is. How do we test this? We can calculate "sharpness" quite easily. The main idea is to filter out points which stand out amongst the points surrounding it. This "stand out"-ness can be easily calculated by measuring the variance across pixels in our image patch.</p>

        <p>First, we find the mean color. Then, we calculate the difference between this color against all colors, and square each value. By summing across these values, we basically calculate variance, which is a good measure of how different an image is.</p>

        <p>Of course, a very noisy image also has very high variance. So to solve this, we simply calculate variance of patches across the image. Each patch is a small sample of pixels which we can get a variance on. This ends up telling us if each individual patch is self-similar (and thus blurry).</p>

        <p>Now that we can get a numeric value on how in-focus an image is, it's easy to write an auto-focus function. First, we calculate the size of the circle of confusion. This is basically the amount of blurring that occurs due to varying sensor depths. We need this value in order to determine the granularity of our autofocus - namely, what change in sensor depth will make a noticiable difference. By calculating this value, we can determine the maximum amount we can change our sensor depth without affecting the quality of the image, because our circle of confusion is smaller than a pixel, and thus it's effects are unnoticable.</p>

        <p>Finally, we simply iterate from the near_focus all the way to the infinity_focus, using our granularity calculated previously to determine how much we should step by. We evaluate our heuristic to determine the "quality" of an image rendered at this sensor depth, and continue. The output sensor depth is simply the one that resulted in the best quality image.</p>

    <h3>A Slow Showing Problem</h3>
        <p>This all sounds like it would work. So what's the issue when we apply it? Well what happens when we try using this technique on the telephoto lens? Let's do a quick calculation. The difference between the telephoto near_focus and infinity_focus (as taken from Fig. 1.9) is about 48mm. The minimum step size required is about 0.3mm. This totals 160 calculations. Raytracing a small patch of only around 20 pixels takes about a second. Factoring the overhead time, the entire process for autofocusing the telephoto lens takes 5 minutes!</p>

        <p>However, since we want high quality auto-focusing, we usually use a patch size of about 50 pixels, and ramp up samples to 16 for rays and lights. The total time it takes to autofocus this is pretty long. So where was the problem?</p>

        <p>Let's start from the root of it. We really don't want to test all 160 sensor_depths. The issue we need to is because our heuristic is <b>not consistent</b>. What this means is that if we move our sensor_depth towards the optimal sensor depth, our heuristic does not always increase. Due to the very finicky nature of variance across an entire image, our heuristic ends up not being monotonic (at all). So, in order to solve our autofocus scheme, we need to first fix our heuristic.</p>

    <h3>A Better Heuristic</h3>
        <p>We restart our heuristic and instead try approaching it a differnet way. Variance across an image can tell us if there is lots of sharp edges. But it also picks up noise quite easily. Instead, we need a way of weighing dynamically different areas across our image patch. We turn to a Laplacian kernel.</p>

        <p>The Laplacian kernel is simply a matrix containing weight values such that the sum of all values sum to 0. Here is the 3x3 Laplacian operator, one without diagonals, and one with diagonals:</p>

        <pre>
                        0 -1  0             -1 -1 -1
                       -1  4 -1             -1  8 -1
                        0 -1  0             -1 -1 -1
        </pre>

        <p>As we can see, when we convolve this matrix against our image, we get a better high-pass filter. The weighted sum helps differentiate far away points that aren't likely to be blur, but just random noise.</p>

        <p>This one is slightly better. In particular, this matrix ends up picking out areas of high frequencies like edges. But also noise. Yes, we still end up tracking noise. Why is this? Well our Laplacian filter is actually approximating the second derivative of the image. In other words, we are looking at the <i>change in color</i> across a spatial dimension. Areas with a large derivative simply mean the color is changing fast (but still smoothly). However, areas where that first derivative fluctuates a lot mean that the second derivative is doing something (the image is changing variably). As a result, any noise that is already in the image is accented strongly due to the definition of it being a region of high change.</p>

        <p>To fix this, we introduce a second idea. What if we could somehow remove or smooth out all that noise so our filter is less likely to care as much about it? Turns out we can. We just apply a gaussian blur to the image, and maintain strong edges, but noise is significantly reduced (as they are usually point-noise).</p>

        <p>That's a lot of work. Taking a gaussian blur. Luckily, it turns out that convolution is associative. So instead of taking a gaussian blur, we instead determine the matrix that when convolved produces such a blur. And then we just combine it with the Laplacian operator. What we end up with is a Laplacian of Gaussian kernel.</p>

        <p>We can hard-code this matrix into our edge-detection function, since this matrix does not change. In this way, we can now detect edges efficiently, and with a much more consistent heuristic. Here is the 9x9 Laplcian of Gaussian kernel that we used:</p>

        <pre>
                      {0, 1, 1,   2,   2,   2, 1, 1, 0,
                       1, 2, 4,   5,   5,   5, 4, 2, 1,
                       1, 4, 5,   3,   0,   3, 5, 4, 1,
                       2, 5, 3, -12, -24, -12, 3, 5, 2,
                       2, 5, 0, -24, -40, -24, 0, 5, 2,
                       2, 5, 3, -12, -24, -12, 3, 5, 2,
                       1, 4, 5,   3,   0,   3, 5, 4, 1,
                       1, 2, 4,   5,   5,   5, 4, 2, 1,
                       0, 1, 1,   2,   2,   2, 1, 1, 0};
        </pre>

        <p>Why do we want a consistent heuristic? Ah yes. We don't want to do 160 ray traces. Let's see how we can apply a consistent heuristic now!</p>

    <h3>Focus Quickly!</h3>
        <p>We notice that a monotonic heuristic allows for two things - if we can get the derivative, then we can follow it to a local maxima, and we can occasionally interpolate values. This means that we no longer need to test the entire range between near_focus and infinity_focus, but instead we can just pick points and interpolate.</p>

        <p>Here's the basic idea: we execute a binary search upon a range of sensor depths, picking and choosing points we've interpolated and guessed as the best focus. When we find one such point, we simply narrow our range to around that point. Finally, we just recurse, since we've started back at the original problem!</p>

        <p>Applying this idea, we now run auto-focus on our telephoto lens. Here are the measurements we get:</p>

        <table border="1" style="width:100%">
            <tr>
                <td>Iteration</td>
                <td>Sensor Depth</td>
                <td>Focus Heuristic</td>
            </tr>
            <tr>
                <td>0</td>
                <td>236.857 (near_focus)</td>
                <td>83019</td>
            </tr>
            <tr>
                <td>0</td>
                <td>188.758 (infinit_focus)</td>
                <td>133138</td>
            </tr>
            <tr>
                <td>0</td>
                <td>212.807 (1/2)</td>
                <td>118639</td>
            </tr>
            <tr>
                <td>1</td>
                <td>200.783 (1/4)</td>
                <td>136331</td>
            </tr>
            <tr>
                <td>2</td>
                <td>206.795 (3/8)</td>
                <td>122200</td>
            </tr>
            <tr>
                <td>2</td>
                <td>194.77 (1/8)</td>
                <td>149994</td>
            </tr>
            <tr>
                <td>3</td>
                <td>197.776 (3/16)</td>
                <td>178724</td>
            </tr>
            <tr>
                <td>4</td>
                <td>199.28 (7/32)</td>
                <td>170005</td>
            </tr>
            <tr>
                <td>4</td>
                <td>196.273 (5/32)</td>
                <td>170700</td>
            </tr>
            <tr>
                <td>5</td>
                <td>198.528 (13/64)</td>
                <td>187866</td>
            </tr>
            <tr>
                <td>5</td>
                <td>197.025 (11/64)</td>
                <td>178931</td>
            </tr>
            <tr>
                <td>6</td>
                <td>198.152 (25/128)</td>
                <td>188458</td>
            </tr>
            <tr>
                <td>7</td>
                <td>198.34 (51/256)</td>
                <td>180677</td>
            </tr>
            <tr>
                <td>7</td>
                <td>197.776 (49/256)</td>
                <td>178724</td>
            </tr>
            <tr>
                <td>8</td>
                <td>198.34 (101/512)</td>
                <td>180677</td>
            </tr>
            <tr>
                <td>8</td>
                <td>197.964 (99/512)</td>
                <td>180167</td>
            </tr>
            <tr>
                <td>9</td>
                <td>198.246 (201/1024)</td>
                <td>178450</td>
            </tr>
            <tr>
                <td>9</td>
                <td>198.058 (199/1024)</td>
                <td>185009</td>
            </tr>
            <tr>
                <td>10</td>
                <td>198.152 (BEST)</td>
                <td>188458</td>
            </tr>
        </table>

        <p>The table shows the process our heuristic function takes to reach the its conclusions on best focus. At every iteration, we move closer and narrow down the range of possible values. Although our heuristic isn't purely consistent (there are irregularities and possibly random local maxima), the process our autofocus takes to reach its conclusions nearly always result in the best focus (as long as a high-contrast region is selected).</p>

        <p>In each of the sensor depths attempted, we've also included a fractional component. This signifies if we attempted to naively iterate through all possible sensor depths the corresponding iteration we would presumable test our heuristic along. As you can tell, the total number of raytraces is only 18, and after 9 iterations, our algorithm was able to identify a sensor depth which had "good enough" focus.</p>

        <p>Here is a graph of what the values look like. Notice that the maxima is where we set our focus to.</p>

        <img class="zoomable" src="images/heuristic.png"/>
        <figcaption align="middle">Fig. 2.1: Graph of heuristics against sensor depths.</figcaption>

        <p>Now that we have an optimized autofocus, we can start rendering images with nice focus (no need to manually fiddle with sensor depth and wait for renders :D.</p>

    <h3>Autofocus Renderings</h3>
        <p>We will use a scene from last time for testing our autofocus. This is the dragon scene, but with a mirror beneath it, and some colored glass orbs floating above.</p>

        <p>We start with the D-Gauss rendering.</p>

        <img class="zoomable" src="images/dragonlens1.png"/>
        <figcaption align="middle">Fig. 2.2: D-Gauss lens rendering of Dragon.</figcaption>

        <p>We've autofocused this image at the paw of the dragon (on the left). Notice that even the reflection and claws are very sharp. On the opposite side, in the corner of the image, however, we have a very slight amount of blur occuring, almost negligible. Since this lens is neither on the extremes of lenses, it does not exhibit as much depth of field, as we've selected a focus distance that is on the flatter side of this lens' focus curve (Fig. 1.10).</p>

        <p>Next, we use a wider angle lens. This time, we move our camera to the top corner of the room. Wider angle lenses are able to capture a larger area of the scene, and thus one might possibly see similarities between this rendering and that of security cameras.</p>

        <img class="zoomable" src="images/dragonlens2.png"/>
        <figcaption align="middle">Fig. 2.3: Wide-angle lens rendering of Dragon.</figcaption>

        <p>Notice that the green orb (which is now directly beneath the camera) is closer to our camera than the near_focus conjugate. No matter how hard we try to focus our camera, we aren't able to bring the green orb in focus. In the image, we've instead auto-focused on the dragon's head.</p>

        <p>There are many issues on this rendering. One of the most significant issues is that the vignette seems very strong, and there is a large amount of noise. The reason for this is because the wide-angle lens is very difficult to predict whether rays will exit successfully. As a result, many rays we try end up not exiting the camera barrel, and we get very dark and noisy renderings. Of course, if we simulated exposure time, we could scale the radiance values and get a brighter (yet still just as noisy) image.</p>

        <p>Now, we render with the telephoto lens!</p>

        <img class="zoomable" src="images/dragonlens3.png"/>
        <figcaption align="middle">Fig. 2.4: Telephoto lens rendering of Dragon.</figcaption>

        <p>For this rendering, we've moved our camera behind the wall to amplify the depth of field effect, and setup a more realistic scene. We focus on the head of the dragon (now super fast autofocus)! Notice that the telephoto lens is much closer to an orthographic projection. This is because of the very zoomed-in lens setup (and truly, we get a feeling of a hyper-zoomed image).</p>

        <p>Finally, we render with the fisheye lens. This one has a very large amount of distortion.</p>

        <img class="zoomable" src="images/dragonlens4.png"/>
        <figcaption align="middle">Fig. 2.5: Fisheye lens rendering of Dragon.</figcaption>

        <p>We can tell that many rays we test never even leave the camera. This is why there is a large black area around the rendering. Furthermore, it is very obvious that the image is "squashed". This is because our film sensor is 36x24, but our render does not reflect the same aspect ratio (oops). Finally, notice the high amount of distortion. The fisheye lens results in a very wide angle, but at the same time, objects in the center seem much closer than they really are in the scene.</p>

        <p>And those are the four lens that we test on! Of course, we could make additional lenses and render more images and the effects of each set of lens could vary the image in different ways! From nice rendering (Fig 2.2) to phone quality (Fig 2.3) to hyper-zoom (Fig 2.4) to fisheye (Fig 2.5), this project overall was very fun, and I hope you enjoyed looking through all of the renderings!</p>

<div id="zoomed" class="zoomed"></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="js/index.js"></script>

</body>
</html>




